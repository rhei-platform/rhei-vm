import platform

# build bpscript VM
# scons --vm
AddOption('--vm',
          dest='build_vm',
          action='store_true',
          default=False,
          help='Build BPScript bytecode VM.')

# enable TRACE macro
# scons --vm-trace
AddOption('--vm-trace',
          dest='vm_trace',
          action='store_true',
          default=False,
          help='Enable TRACE macro for VM.')

# build VM tests
# scons --tests
AddOption('--tests',
          dest='build_vm_tests',
          action='store_true',
          default=False,
          help='Builds VM tests.')

# build and run selected tests
# scons --run
AddOption('--run',
          dest='run_tests',
          action='store_true',
          default=False,
          help='Builds and then runs selected tests.')


opt_build_vm = GetOption('build_vm')
opt_vm_trace = GetOption('vm_trace')
opt_build_vm_tests = GetOption('build_vm_tests')

opt_run_tests = GetOption('run_tests')


######################################################
#
#
#
######################################################

def detect_msvc(env):
    if 'msvc_version' in env:
        env['MSVC_VERSION'] = env['msvc_version']

    msvc_version = env.get('MSVC_VERSION', '')
    if msvc_version:
        env.msvc = True

    # FIXME: using mingw instead of msvc 12.0
    # if msvc_version == '12.0':
    #     env.msvc = False
    #     env.PrependENVPath('PATH', 'C:\\Applications\\mingw32\\bin')
    #     env.Tool('mingw', toolpath='C:\\Applications\\mingw32\\bin')
    #     env.Tool('gnulink', toolpath='C:\\Applications\\mingw32\\bin')
    #     env.Replace(CC = 'C:\\Applications\\mingw32\\bin\\gcc.exe')
    #     env.Replace(CCFLAGS = '') # to remove /nologo flag meant for msvc


######################################################
#
#
#
######################################################

env = Environment()

# i.e. cc_cxx_values is ['gcc', 'g++', '7.3.0', '7.3.0']
cc_cxx_values = env.Dictionary('CC', 'CXX', 'CCVERSION', 'CXXVERSION')
cc_name = cc_cxx_values[0] # 'CC'
cxx_name = cc_cxx_values[1] # 'CXX'
cc_version = cc_cxx_values[2] # 'CCVERSION'
cxx_version = cc_cxx_values[3] # 'CXXVERSION'

env.msvc = False
detect_msvc(env)


## DEBUG build
if env.msvc:
    # Compiler Options: https://msdn.microsoft.com/en-us/library/fwkeyyhe.aspx
    env.Append(CXXFLAGS = ['/EHsc', '/Z7', '/Od'])
    env.Append(LINKFLAGS=['/DEBUG'])
else:
    env.Append(CXXFLAGS = ['-std=c++11', '-g', '-O0', '-Wall', '-Wextra'])
    env.Append(CFLAGS = ['-std=c99', '-g', '-O0', '-Wall', '-Wextra'])
    if platform.system() == 'Linux':
        # they say this is needed by cpp-peglib on Ubuntu
        # https://github.com/yhirose/cpp-peglib/issues/23#issuecomment-261126127
        env.Append(LINKFLAGS=['-pthread'])

if opt_vm_trace:
    env.Append(CFLAGS = ['-DVM_TRACE'])

## RELEASE build
    # -Wall -Wextra -O3 -march=native ...
    # -Wall -Wextra -O3 -march=armv7 ...
    # -Wall -Wextra -Os -march=armv7 ...

if cxx_name == 'g++':
    # turn on 
    env.Append(CXXFLAGS = ['-Wmissing-field-initializers', '-g'])


######################################################
#
# Bytecode interpreter VM
#
######################################################

vm_project_root = '#'
vm_include_root = vm_project_root + '/src'
vm_source_root = vm_project_root + '/src'

vm_include_paths = [
    Dir(vm_include_root+''),
    # Dir(vm_include_root+'/contrib'),
    # Dir(vm_include_root+'/extras'),
    # Dir(vm_include_root+'/vm'),
]

# TODO: move contrib out of 'src' folder
vm_sources = \
    Glob(vm_source_root+'/vm/core/*.c') + \
    Glob(vm_source_root+'/vm/debug/*.c') + \
    Glob(vm_source_root+'/vm/interpreter/*.c') + \
    Glob(vm_source_root+'/vm/lib/*.c') + \
    Glob(vm_source_root+'/vm/util/*.c') + \
    Glob(vm_source_root+'/vm/*.c') + \
    Glob(vm_source_root+'/contrib/*.c') + \
    Glob(vm_source_root+'/extras/*.c')

vm_static_library_name = 'bpsvm'
vm_build_output_location = vm_project_root + '/build'

######################################################
#
# Build options
#
######################################################

if opt_build_vm:
    # env['CC'] = 'clang-7'
    # env['CXX'] = 'clang++-7'

    env.Append(CPPPATH = vm_include_paths)

    env.StaticLibrary(vm_build_output_location+'/bpsvm', vm_sources)
    env.Program(
        vm_build_output_location+'/vm_runner',
        Glob(vm_source_root+'/extras/binaries/vm.c'),
        LIBS=[ vm_static_library_name ],
        LIBPATH=vm_build_output_location+''
    )

if opt_build_vm_tests:
    # env['CC'] = 'clang-7'
    # env['CXX'] = 'clang++-7'

    vm_test_sources = \
        Glob('tests/test_*.cpp') + \
        Glob('tests/test_*.c')

    env.Append(CPPPATH = vm_include_paths)
    env.Append(CPPPATH = Dir('#common/src'))
    env.Append(CPPPATH = Dir('tests'))

    env.Program(
        vm_build_output_location+'/vm_tests',
        Glob('tests/vm_test_main.cpp') + vm_test_sources,
        LIBS=[ vm_static_library_name ],
        LIBPATH=vm_build_output_location+''
    )


######################################################
#
# Build helpers
#
######################################################

def build_vm_tests(target, source, env):
    from subprocess import call
    # call([source])
    call(['./build/vm_tests'])

if opt_run_tests and opt_build_vm_tests:
    vm_tests_command = \
        Command( target = 'vm_tests',
                 source = vm_build_output_location+'/vm_tests',
                 action = build_vm_tests )
    AlwaysBuild( vm_tests_command )
