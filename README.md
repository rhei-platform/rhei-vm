## BPScript virtual machine

The source code currently builds a library that is linked with your app code.

A developer has to either:

- write code to build a network from scratch (by using `NetworkBuilder`) and then call VM (from the library) to process I/Os
- use `bpnet` to generate C-code from net/component specifications and then compile & link the code with the application and just call VM to process I/Os


## Build VM library and tests

From the root folder (where the `SConstruct` file is):

- to build VM library:

~~~
    scons --vm
~~~

- to build VM tests binary:

~~~
    scons --vm-tests
~~~

- to run VM tests:

~~~
    scons --run --vm-tests
~~~
