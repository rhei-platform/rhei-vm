#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes requests") {

    SECTION("request read imm") {

        byte_t code[] = {
            OP_BREQUEST_READ,
            4,
            5,
            OP_BPUSH,
            22,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_READ );
        REQUIRE( 4 == context.contextRequestData.informationPacketIndex );
        REQUIRE( 5 == context.contextRequestData.portIndex );
        REQUIRE( IS_STACK_EMPTY( &context ) );

        context.state = CONTEXT_RUNNING;
        run_status = fbContext_run( &context );
        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 22 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("request read") {

        byte_t code[] = {
            OP_REQUEST_READ,
            OP_BPUSH,
            22,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 5 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_READ );
        REQUIRE( 4 == context.contextRequestData.informationPacketIndex );
        REQUIRE( 5 == context.contextRequestData.portIndex );
        REQUIRE( IS_STACK_EMPTY( &context ) );

        context.state = CONTEXT_RUNNING;
        run_status = fbContext_run( &context );
        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 22 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("request write imm") {

        byte_t code[] = {
            OP_BREQUEST_WRITE,
            4,
            5,
            OP_BPUSH,
            22,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_WRITE );
        REQUIRE( 4 == context.contextRequestData.informationPacketIndex );
        REQUIRE( 5 == context.contextRequestData.portIndex );
        REQUIRE( IS_STACK_EMPTY( &context ) );

        context.state = CONTEXT_RUNNING;
        run_status = fbContext_run( &context );
        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 22 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("request write") {

        byte_t code[] = {
            OP_REQUEST_WRITE,
            OP_BPUSH,
            22,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 5 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_WRITE );
        REQUIRE( 4 == context.contextRequestData.informationPacketIndex );
        REQUIRE( 5 == context.contextRequestData.portIndex );
        REQUIRE( IS_STACK_EMPTY( &context ) );

        context.state = CONTEXT_RUNNING;
        run_status = fbContext_run( &context );
        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 22 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
