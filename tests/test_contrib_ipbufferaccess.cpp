#include "catch.hpp"
#include "contrib/ipbufferaccess.h"

TEST_CASE("ipbufferaccess") {

    SECTION("basics") {

        FBIPBufferAcces ipb_b;
        FBIPBufferAcces *ipb = &ipb_b;
        byte_t data[10];


        fbIPBufferAccess_init( ipb, data, 10 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer == &data[0] );
        REQUIRE( ipb->end == &data[10] );
        REQUIRE( ipb->position == ipb->buffer );
        REQUIRE( ipb->buffer == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( 10 == fbIPBufferAccess_length( ipb ) );
        REQUIRE( ! fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_position( ipb, 3 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 3 == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( ! fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_position( ipb, 11 );

        REQUIRE( ! fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 3 == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( ! fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_resetSatus( ipb );
        fbIPBufferAccess_position( ipb, 10 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 10 == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_position( ipb, 0 );
        fbIPBufferAccess_advance( ipb, 5 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 5 == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( ! fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_advance( ipb, 5 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 10 == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_advance( ipb, 5 );

        REQUIRE( ! fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 10 == fbIPBufferAccess_dataAtPosition( ipb ) );
        REQUIRE( fbIPBufferAccess_atEndOfBuffer( ipb ) );


        fbIPBufferAccess_resetSatus( ipb );
        fbIPBufferAccess_position( ipb, 10 );
        fbIPBufferAccess_retreat( ipb, 5 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer + 5 == fbIPBufferAccess_dataAtPosition( ipb ) );


        fbIPBufferAccess_retreat( ipb, 5 );

        REQUIRE( fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer == fbIPBufferAccess_dataAtPosition( ipb ) );


        fbIPBufferAccess_retreat( ipb, 5 );

        REQUIRE( ! fbIPBufferAccess_satusOK( ipb ) );
        REQUIRE( ipb->buffer == fbIPBufferAccess_dataAtPosition( ipb ) );
    }

    SECTION("access") {

        #define fbIPBufferAccess_currentIndex fbIPBufferAccess_headCount

        FBIPBufferAcces ipb_b;
        FBIPBufferAcces *ipb = &ipb_b;
        byte_t data[] = {1,2,3,4,5,6,7,8,9,10};
        int8_t value;

        fbIPBufferAccess_init( ipb, data, sizeof(data) );


        REQUIRE( 0 == fbIPBufferAccess_currentIndex( ipb ) );
        value = fbIPBufferAccess_get_int8( ipb );
        REQUIRE( 0 == fbIPBufferAccess_currentIndex( ipb ) );
        REQUIRE( 1 == value );


        REQUIRE( 0 == fbIPBufferAccess_currentIndex( ipb ) );
        value = fbIPBufferAccess_read_int8( ipb );
        REQUIRE( 1 == fbIPBufferAccess_currentIndex( ipb ) );
        REQUIRE( 1 == value );


        REQUIRE( 1 == fbIPBufferAccess_currentIndex( ipb ) );
        value = fbIPBufferAccess_read_int8( ipb );
        REQUIRE( 2 == fbIPBufferAccess_currentIndex( ipb ) );
        REQUIRE( 2 == value );


        REQUIRE( 2 == fbIPBufferAccess_currentIndex( ipb ) );
        value = fbIPBufferAccess_get_int8( ipb );
        REQUIRE( 2 == fbIPBufferAccess_currentIndex( ipb ) );
        REQUIRE( 3 == value );


        REQUIRE( 2 == fbIPBufferAccess_currentIndex( ipb ) );
        fbIPBufferAccess_set_int8( ipb, 40 );
        REQUIRE( 2 == fbIPBufferAccess_currentIndex( ipb ) );
        value = fbIPBufferAccess_get_int8( ipb );
        REQUIRE( 40 == value );


        REQUIRE( 2 == fbIPBufferAccess_currentIndex( ipb ) );
        fbIPBufferAccess_write_int8( ipb, 41 );
        REQUIRE( 3 == fbIPBufferAccess_currentIndex( ipb ) );
        fbIPBufferAccess_retreat( ipb, 1 );
        value = fbIPBufferAccess_get_int8( ipb );
        fbIPBufferAccess_advance( ipb, 1 );
        REQUIRE( 41 == value );
    }
}
