#include "catch.hpp"
#include "vm/interpreter/types.h"
#include "vm/core/scheduler.h"
#include "vm/core/scheduler_internal.h"

#include "vm/core/connection.h"
#include "vm/interpreter/context.h"

#include "vm/interpreter/opcode.h"


TEST_CASE("scheduler") {

    SECTION("terminate context") {

        FBContext context;
        fbContext_init_allocate_iii( &context, 0, 2, 2 );

        // force open ports
        fbContext_getInputPort( &context, 0 )->closed = false;
        fbContext_getInputPort( &context, 1 )->closed = false;
        fbContext_getOutputPort( &context, 0 )->closed = false;
        fbContext_getOutputPort( &context, 1 )->closed = false;


        fbContext_reset( &context );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbInputOutput_isOpen( fbContext_getInputPort( &context, 0 ) ) );
        REQUIRE( fbInputOutput_isOpen( fbContext_getInputPort( &context, 1 ) ) );
        REQUIRE( fbInputOutput_isOpen( fbContext_getOutputPort( &context, 0 ) ) );
        REQUIRE( fbInputOutput_isOpen( fbContext_getOutputPort( &context, 1 ) ) );


        fbScheduler_terminateContext( &context );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbInputOutput_isClosed( fbContext_getInputPort( &context, 0 ) ) );
        REQUIRE( fbInputOutput_isClosed( fbContext_getInputPort( &context, 1 ) ) );
        REQUIRE( fbInputOutput_isClosed( fbContext_getOutputPort( &context, 0 ) ) );
        REQUIRE( fbInputOutput_isClosed( fbContext_getOutputPort( &context, 1 ) ) );


        fbContext_free( &context );
    }
}
