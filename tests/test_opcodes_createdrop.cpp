#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/information_packet.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes create/drop ip") {

    SECTION("create/drop") {

        byte_t code[] = {
            OP_CREATE_IP,
            1,
            0,
            16,
            OP_DROP_IP,
            1,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBContextStatus run_status;
        
        
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_CREATE );
        REQUIRE( context.contextRequestData.informationPacketIndex == 1 );
        REQUIRE( context.contextRequestData.packetSize == 16 );

        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_DROP );
        REQUIRE( context.contextRequestData.informationPacketIndex == 1 );
        REQUIRE( context.contextRequestData.packetSize == 0 );

        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );

    }

    SECTION("screate/drop") {

        byte_t code[] = {
            OP_BPUSH,
            16,
            OP_SCREATE_IP,
            1,
            OP_DROP_IP,
            1,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBContextStatus run_status;
        
        
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_CREATE );
        REQUIRE( context.contextRequestData.informationPacketIndex == 1 );
        REQUIRE( context.contextRequestData.packetSize == 16 );

        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_DROP );
        REQUIRE( context.contextRequestData.informationPacketIndex == 1 );
        REQUIRE( context.contextRequestData.packetSize == 0 );

        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );

    }
}