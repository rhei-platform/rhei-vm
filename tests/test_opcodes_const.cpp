#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes const") {

    SECTION("op bconst/sconst i8") {

        byte_t code[] = {
            OP_BCONST_I8,
            2,
            OP_SCONST_I8,
            0,
            3,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            3,
            4,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 4 == fbContext_pop( &context ) );
        REQUIRE( 3 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op bconst/sconst i16") {

        byte_t code[] = {
            OP_BCONST_I16,
            2,
            OP_SCONST_I16,
            0,
            4,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            5,
            0,
            10,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 10 == fbContext_pop( &context ) );
        REQUIRE( 5 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op bconst/sconst i32") {

        byte_t code[] = {
            OP_BCONST_I32,
            2,
            OP_SCONST_I32,
            0,
            6,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            22,
            0,
            0,
            0,
            55,
            0,
            0,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 55 == fbContext_pop( &context ) );
        REQUIRE( 22 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op bconst/sconst i64") {

        byte_t code[] = {
            OP_BCONST_I64,
            2,
            OP_SCONST_I64,
            0,
            10,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            42,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            24,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 24 == fbContext_pop64( &context ) );
        REQUIRE( 42 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op pconst i8") {

        byte_t code[] = {
            OP_PCONST_I8,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            // 5
            5,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );

        fbContext_push( &context, 2 );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 5 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op pconst i16") {

        byte_t code[] = {
            OP_PCONST_I16,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            // 258
            2,
            1,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );

        fbContext_push( &context, 2 );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 258 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op pconst i32") {

        byte_t code[] = {
            OP_PCONST_I32,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            // 3456789
            0x15,
            0xBF,
            0x34,
            0x00,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );

        fbContext_push( &context, 2 );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 3456789 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op pconst i64") {

        byte_t code[] = {
            OP_PCONST_I64,
            OP_END,
        };

        byte_t constant_pool[] = {
            1,
            2,
            // 345678912345
            0x59,
            0x9B,
            0x11,
            0x7C,
            0x50,
            0x00,
            0x00,
            0x00,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );

        fbContext_push( &context, 2 );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 345678912345 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("op const 0, 1, -1") {

        byte_t code[] = {
            OP_CONST_0,
            OP_CONST_1,
            OP_CONST_M1,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
