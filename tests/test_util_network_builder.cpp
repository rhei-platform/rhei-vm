#include "catch.hpp"
#include "vm/util/network_builder.h"

#include "samples/summator.h"
#include "samples/negator.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"


static void dummyFn( FBNativeContextHandle handle ) {
    (void)handle;
}


TEST_CASE("network builder") {

    SECTION("add/set") {

        FBComponent* sumamtor = fbSamples_getSummator();
        FBComponent* negator = fbSamples_getNegator();
/*
    OUT = - ( A + B + C )

       +sum1+
    A--|0   |   +sum2+
    B--|1  0|---|0   |   +neg+
       +----+   |   0|---|0 0|---OUT
    C-----------|1   |   +---+
                +----+

*/

        FBNetworkBuilderData nbdata;

        fbNetworkBuilder_init( &nbdata );

        index_t ctxSum1idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        REQUIRE( ctxSum1idx != SIZE_MAX );
        index_t ctxSum2idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        REQUIRE( ctxSum2idx != SIZE_MAX );
        index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );
        REQUIRE( ctxNegidx != SIZE_MAX );

        index_t connSum1Sum2idx = fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum1idx, 0, ctxSum2idx, 0, 1 );
        REQUIRE( connSum1Sum2idx != SIZE_MAX );
        index_t connSum2Negidx = fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum2idx, 0, ctxNegidx, 0, 1 );
        REQUIRE( connSum2Negidx != SIZE_MAX );

        index_t portAidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 0 );
        REQUIRE( portAidx != SIZE_MAX );
        index_t portBidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 1 );
        REQUIRE( portBidx != SIZE_MAX );
        index_t portCidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum2idx, 1 );
        REQUIRE( portCidx != SIZE_MAX );
        index_t portOUTidx = fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );
        REQUIRE( portOUTidx != SIZE_MAX );

        FBPacketHandlerFn nativeFn = dummyFn;

        index_t nativeInIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInIdx, ctxSum1idx, 0, 1 );
        REQUIRE( nativeInIdx != SIZE_MAX );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxNegidx, 0, nativeOutIdx, 1 );
        REQUIRE( nativeOutIdx != SIZE_MAX );



        FBComponent *ctx;
        ctx = fbNetworkBuilder_getComponent( &nbdata, ctxSum1idx );
        REQUIRE( sumamtor == ctx );
        ctx = fbNetworkBuilder_getComponent( &nbdata, ctxSum2idx );
        REQUIRE( sumamtor == ctx );
        ctx = fbNetworkBuilder_getComponent( &nbdata, ctxNegidx );
        REQUIRE( negator == ctx );


        FBNetworkBuilderDataConnection *conn;
        conn = fbNetworkBuilder_getConnectionData( &nbdata, connSum1Sum2idx );
        REQUIRE( conn->sourceContextIndex == ctxSum1idx );
        REQUIRE( conn->sourceOutputPortIndex == 0 );
        REQUIRE( conn->sinkContextIndex == ctxSum2idx );
        REQUIRE( conn->sinkInputPortIndex == 0 );
        REQUIRE( conn->capacity == 1 );

        conn = fbNetworkBuilder_getConnectionData( &nbdata, connSum2Negidx );
        REQUIRE( conn->sourceContextIndex == ctxSum2idx );
        REQUIRE( conn->sourceOutputPortIndex == 0 );
        REQUIRE( conn->sinkContextIndex == ctxNegidx );
        REQUIRE( conn->sinkInputPortIndex == 0 );
        REQUIRE( conn->capacity == 1 );

        FBNetworkBuilderDataIO *np;
        np = fbNetworkBuilder_getNetworkInputData( &nbdata, portAidx );
        REQUIRE( np->contextIndex == ctxSum1idx );
        REQUIRE( np->portIndex == 0 );
        np = fbNetworkBuilder_getNetworkInputData( &nbdata, portBidx );
        REQUIRE( np->contextIndex == ctxSum1idx );
        REQUIRE( np->portIndex == 1 );
        np = fbNetworkBuilder_getNetworkInputData( &nbdata, portCidx );
        REQUIRE( np->contextIndex == ctxSum2idx );
        REQUIRE( np->portIndex == 1 );
        np = fbNetworkBuilder_getNetworkOutputData( &nbdata, portOUTidx );
        REQUIRE( np->contextIndex == ctxNegidx );
        REQUIRE( np->portIndex == 0 );

        FBNetworkBuilderDataNativeIO *nip;
        nip = fbNetworkBuilder_getNativeInputData( &nbdata, nativeInIdx );
        REQUIRE( nip->fn == nativeFn );

        nip = fbNetworkBuilder_getNativeOutputData( &nbdata, nativeOutIdx );
        REQUIRE( nip->fn == nativeFn );



        fbNetworkBuilder_free( &nbdata );
    }

    SECTION("build") {

        FBComponent* sumamtor = fbSamples_getSummator();
        FBComponent* negator = fbSamples_getNegator();
/*
    OUT = - ( A + B + C )

       +sum1+
    A--|0   |   +sum2+
    B--|1  0|---|0   |   +neg+
       +----+   |   0|---|0 0|---OUT
    C-----------|1   |   +---+
                +----+

*/

        FBNetworkBuilderData nbdata;

        fbNetworkBuilder_init( &nbdata );

        index_t ctxSum1idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        index_t ctxSum2idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );

        index_t connSum1Sum2idx = fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum1idx, 0, ctxSum2idx, 0, 1 );
        index_t connSum2Negidx = fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum2idx, 0, ctxNegidx, 0, 1 );

        index_t portAidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 0 );
        index_t portBidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 1 );
        index_t portCidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum2idx, 1 );
        index_t portOUTidx = fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );

        FBPacketHandlerFn nativeFn = dummyFn;

        index_t nativeInIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInIdx, ctxSum1idx, 0, 1 );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxNegidx, 0, nativeOutIdx, 1 );


        FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );

        fbNetworkBuilder_free( &nbdata );


        FBInputOutput *portA;
        FBInputOutput *portB;
        FBInputOutput *portC;
        FBInputOutput *portOUT;
        FBContext *ctxSum1;
        FBContext *ctxSum2;
        FBContext *ctxNeg;
        FBConnection *connSum1Sum2;
        FBConnection *connSum2Neg;

        ctxSum1 = fbNetwork_getContext( networkContext, ctxSum1idx );
        REQUIRE( ctxSum1 != NULL );
        ctxSum2 = fbNetwork_getContext( networkContext, ctxSum2idx );
        REQUIRE( ctxSum2 != NULL );
        ctxNeg = fbNetwork_getContext( networkContext, ctxNegidx );
        REQUIRE( ctxNeg != NULL );


        portA = fbContext_getInputPort( ctxSum1, 0 );
        portB = fbContext_getInputPort( ctxSum1, 1 );
        portC = fbContext_getInputPort( ctxSum2, 1 );
        portOUT = fbContext_getOutputPort( ctxNeg, 0 );

        REQUIRE( portA == fbNetwork_getInput( networkContext, portAidx ) );
        REQUIRE( portB == fbNetwork_getInput( networkContext, portBidx ) );
        REQUIRE( portC == fbNetwork_getInput( networkContext, portCidx ) );
        REQUIRE( portOUT == fbNetwork_getOutput( networkContext, portOUTidx ) );


        connSum1Sum2 = fbNetwork_getConnection( networkContext, connSum1Sum2idx );
        connSum2Neg = fbNetwork_getConnection( networkContext, connSum2Negidx );

        REQUIRE( fbContext_getOutputPort( ctxSum1, 0 ) == connSum1Sum2->source );
        REQUIRE( fbContext_getOutputPort( ctxSum1, 0 )->connection == connSum1Sum2 );
        REQUIRE( fbContext_getInputPort( ctxSum2, 0 ) == connSum1Sum2->sink );
        REQUIRE( fbContext_getInputPort( ctxSum2, 0 )->connection == connSum1Sum2 );

        REQUIRE( fbContext_getOutputPort( ctxSum2, 0 ) == connSum2Neg->source );
        REQUIRE( fbContext_getOutputPort( ctxSum2, 0 )->connection == connSum2Neg );
        REQUIRE( fbContext_getInputPort( ctxNeg, 0 ) == connSum2Neg->sink );
        REQUIRE( fbContext_getInputPort( ctxNeg, 0 )->connection == connSum2Neg );


        FBNetworkNativeIO *nativeIOInput = fbNetwork_getNativeInput( networkContext, nativeInIdx );
        // this crashes catch2
        // REQUIRE( nativeIOInput->fn == nativeFn );
        FBNetworkNativeIO *nativeIOOutput = fbNetwork_getNativeOutput( networkContext, nativeOutIdx );
        // this crashes catch2
        // REQUIRE( nativeIOOutput->fn == nativeFn );

        REQUIRE( nativeIOInput->port->connection == portA->connection );
        REQUIRE( nativeIOInput->port->connection->source == nativeIOInput->port );
        REQUIRE( nativeIOInput->port->connection->sink == portA );
        REQUIRE( nativeIOOutput->port->connection == portOUT->connection );
        REQUIRE( nativeIOOutput->port->connection->source == portOUT );
        REQUIRE( nativeIOOutput->port->connection->sink == nativeIOOutput->port );

        fbNetwork_destroy( networkContext );
    }
}
