#include "catch.hpp"
#include "vm/interpreter/types.h"
#include "vm/lib/memory.h"

#include "vm/core/information_packet.h"


TEST_CASE("information packet") {

    SECTION("copy range") {

        FBInformationPacketTuple dst = fbInformationPacket_create_tuple( 42 );
        FBInformationPacketTuple src = fbInformationPacket_create_tuple( 42 );


        index_t copiedCount;


        copiedCount = fbInformationPacket_copyRange( dst.handle, 42, src.handle, 0, 42 );
        REQUIRE( 0 == copiedCount );

        copiedCount = fbInformationPacket_copyRange( dst.handle, 0, src.handle, 42, 42 );
        REQUIRE( 0 == copiedCount );

        copiedCount = fbInformationPacket_copyRange( dst.handle, 20, src.handle, 0, 42 );
        REQUIRE( 22 == copiedCount );

        copiedCount = fbInformationPacket_copyRange( dst.handle, 0, src.handle, 20, 42 );
        REQUIRE( 22 == copiedCount );

        copiedCount = fbInformationPacket_copyRange( dst.handle, 10, src.handle, 20, 0 );
        REQUIRE( 0 == copiedCount );


        FB_SET( dst.buffer, 0, 42 );
        FB_SET( src.buffer, 1, 42 );
        copiedCount = fbInformationPacket_copyRange( dst.handle, 0, src.handle, 0, 42 );

        REQUIRE( 42 == copiedCount );

        byte_t sum = 0;
        while(copiedCount) {
            copiedCount--;
            sum += dst.buffer[copiedCount];
        }
        REQUIRE( 42 == sum );

        fbInformationPacket_drop( dst.handle );
        fbInformationPacket_drop( src.handle );
    }
}
