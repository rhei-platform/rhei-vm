#include "catch.hpp"
#include "vm/interpreter/types.h"

#include "vm/core/information_packet_internal.h"


extern index_t _internal_IPRegistry_entryCount;
extern registry_entry_t _internal_IPRegistry_entryArray[ MAX_HANDLES ];

extern FBInformationPacketHandle _internal_IPRegistry_nextHandle;



static void print_handle( FBInformationPacketHandle handle ) {
    printf("@%d\n", handle);
}

void print_registry() {
    printf("REG -----\n");
    for(index_t idx = 0; idx < MAX_HANDLES; idx++) {
        print_handle(_internal_IPRegistry_entryArray[ idx ].handle);
    }
    printf("\n");
}

TEST_CASE("information packet: internal algorithms") {

    SECTION("is_valid/get_next handle") {

        _internal_fbInformationPacket_resetRegistry();

        FBInformationPacketHandle initial_handle = _internal_IPRegistry_nextHandle;
        REQUIRE( ! _internal_fbInformationPacket_isValidHandle( initial_handle ) );

        FBInformationPacketHandle next_handle = _internal_fbInformationPacket_getNextHandle();
        REQUIRE( ! _internal_fbInformationPacket_isValidHandle( next_handle ) );
        REQUIRE( initial_handle == next_handle );

        next_handle = _internal_fbInformationPacket_createEntry(0);
        REQUIRE( _internal_fbInformationPacket_isValidHandle( next_handle ) );
        REQUIRE( initial_handle == next_handle );

        next_handle = _internal_fbInformationPacket_getNextHandle();
        REQUIRE( ! _internal_fbInformationPacket_isValidHandle( next_handle ) );
        REQUIRE( initial_handle != next_handle );
    }

    SECTION("create/drop entry, max out registry") {

        _internal_fbInformationPacket_resetRegistry();

        FBInformationPacketHandle ipA = _internal_fbInformationPacket_createEntry(0);
        REQUIRE( ipA != FB_INVALID_PACKET_HANDLE );

        FBInformationPacketHandle ipB = _internal_fbInformationPacket_createEntry(0);
        REQUIRE( ipB != FB_INVALID_PACKET_HANDLE );
        REQUIRE( ipB != ipA );

        FBInformationPacketHandle ipC = _internal_fbInformationPacket_createEntry(0);
        REQUIRE( ipC != FB_INVALID_PACKET_HANDLE );
        REQUIRE( ipC != ipA );
        REQUIRE( ipC != ipB );
        // print_registry();

        REQUIRE( _internal_fbInformationPacket_isValidHandle( ipB ) );
        _internal_fbInformationPacket_dropEntry( ipB );
        REQUIRE( ! _internal_fbInformationPacket_isValidHandle( ipB ) );
        // print_registry();
        for( index_t idx = 0; idx < _internal_IPRegistry_entryCount; idx++ ) {
            REQUIRE( _internal_IPRegistry_entryArray[ idx ].handle != FB_INVALID_PACKET_HANDLE );
        }

        _internal_fbInformationPacket_resetRegistry();

        for( int i = 0; i < MAX_HANDLES; i++ )
            _internal_fbInformationPacket_createEntry(0);
        // no space left in registry
        REQUIRE( FB_INVALID_PACKET_HANDLE == _internal_fbInformationPacket_createEntry(0) );
    }

    SECTION("get entry") {

        _internal_fbInformationPacket_resetRegistry();

        FBInformationPacketHandle ipA = _internal_fbInformationPacket_createEntry(0);
        REQUIRE( NULL != _internal_fbInformationPacket_getEntry( ipA ) );

        _internal_fbInformationPacket_dropEntry( ipA );
        REQUIRE( NULL == _internal_fbInformationPacket_getEntry( ipA ) );
    }

    // for other tests to be able to create IPs
    _internal_fbInformationPacket_resetRegistry();
}
