#include "catch.hpp"
#include "vm/lib/ipringbuffer.h"

TEST_CASE("ipringbuffer") {

    SECTION("init/destroy") {

        FBIPRingBuffer rb;

        fbIPRingBuffer_init( &rb, 10 );

        REQUIRE( rb.buffer != NULL );
        REQUIRE( rb.capacity == 10 );
        REQUIRE( rb.count == 0 );
        REQUIRE( fbIPRingBuffer_isEmpty( &rb ) );
        REQUIRE( 0 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 10 == fbIPRingBuffer_availableSpace( &rb ) );


        fbIPRingBuffer_destroy( &rb );

        REQUIRE( rb.buffer == NULL );
        REQUIRE( rb.capacity == 0 );
        REQUIRE( rb.count == 0 );
    }

    SECTION("data/space, empty/full") {

        FBIPRingBuffer rb;

        FBInformationPacketHandle value = (FBInformationPacketHandle) 1;

        fbIPRingBuffer_init( &rb, 3 );
        REQUIRE( 0 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 3 == fbIPRingBuffer_availableSpace( &rb ) );
        REQUIRE( fbIPRingBuffer_isEmpty( &rb ) );
        REQUIRE( ! fbIPRingBuffer_isFull( &rb ) );

        REQUIRE( fbIPRingBuffer_push( &rb, value ) );
        REQUIRE( 1 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 2 == fbIPRingBuffer_availableSpace( &rb ) );
        REQUIRE( ! fbIPRingBuffer_isEmpty( &rb ) );
        REQUIRE( ! fbIPRingBuffer_isFull( &rb ) );

        REQUIRE( fbIPRingBuffer_push( &rb, value ) );
        REQUIRE( 2 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 1 == fbIPRingBuffer_availableSpace( &rb ) );
        REQUIRE( ! fbIPRingBuffer_isEmpty( &rb ) );
        REQUIRE( ! fbIPRingBuffer_isFull( &rb ) );

        REQUIRE( fbIPRingBuffer_push( &rb, value ) );
        REQUIRE( 3 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 0 == fbIPRingBuffer_availableSpace( &rb ) );
        REQUIRE( ! fbIPRingBuffer_isEmpty( &rb ) );
        REQUIRE( fbIPRingBuffer_isFull( &rb ) );

        REQUIRE( ! fbIPRingBuffer_push( &rb, value ) );
        REQUIRE( 3 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 0 == fbIPRingBuffer_availableSpace( &rb ) );

        REQUIRE( fbIPRingBuffer_pop( &rb, &value ) );
        REQUIRE( 2 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 1 == fbIPRingBuffer_availableSpace( &rb ) );

        REQUIRE( fbIPRingBuffer_pop( &rb, &value ) );
        REQUIRE( 1 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 2 == fbIPRingBuffer_availableSpace( &rb ) );

        REQUIRE( fbIPRingBuffer_pop( &rb, &value ) );
        REQUIRE( 0 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 3 == fbIPRingBuffer_availableSpace( &rb ) );

        REQUIRE( ! fbIPRingBuffer_pop( &rb, &value ) );
        REQUIRE( 0 == fbIPRingBuffer_availableData( &rb ) );
        REQUIRE( 3 == fbIPRingBuffer_availableSpace( &rb ) );

        fbIPRingBuffer_destroy( &rb );
    }

    SECTION("data/space, empty/full") {

        FBIPRingBuffer rb;

        FBInformationPacketHandle value_1 = (FBInformationPacketHandle) 1;
        FBInformationPacketHandle value_2 = (FBInformationPacketHandle) 2;
        FBInformationPacketHandle value_3 = (FBInformationPacketHandle) 3;
        FBInformationPacketHandle value_4 = (FBInformationPacketHandle) 4;
        FBInformationPacketHandle value;

        fbIPRingBuffer_init( &rb, 3 );

        fbIPRingBuffer_push( &rb, value_1 );
        fbIPRingBuffer_push( &rb, value_2 );
        fbIPRingBuffer_push( &rb, value_3 );

        fbIPRingBuffer_pop( &rb, &value );
        REQUIRE( value == value_1 );

        fbIPRingBuffer_push( &rb, value_4 );

        fbIPRingBuffer_peek( &rb, &value );
        REQUIRE( value == value_2 );

        fbIPRingBuffer_pop( &rb, &value );
        REQUIRE( value == value_2 );

        fbIPRingBuffer_pop( &rb, &value );
        REQUIRE( value == value_3 );

        fbIPRingBuffer_peek( &rb, &value );
        REQUIRE( value == value_4 );

        fbIPRingBuffer_pop( &rb, &value );
        REQUIRE( value == value_4 );

        fbIPRingBuffer_destroy( &rb );
    }
}
