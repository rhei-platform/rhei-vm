#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes cload") {

    SECTION("cload i8") {

        byte_t code[] = {
            OP_CLOAD_I8,
            OP_END,
        };

        byte_t component_data[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 2 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("cload i16") {

        byte_t code[] = {
            OP_CLOAD_I16,
            OP_END,
        };

        byte_t component_data[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 3 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("cload i32") {

        byte_t code[] = {
            OP_CLOAD_I32,
            OP_END,
        };

        byte_t component_data[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 5 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 55 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("cload i64") {

        byte_t code[] = {
            OP_CLOAD_I64,
            OP_END,
        };

        byte_t component_data[] = {
            255,
            255,
            42,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 2 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes load") {

    SECTION("load i8") {

        byte_t code[] = {
            OP_LOAD_I8,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 2 );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("load i16") {

        byte_t code[] = {
            OP_LOAD_I16,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 3 );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("load i32") {

        byte_t code[] = {
            OP_LOAD_I32,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 5 );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 55 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("load i64") {

        byte_t code[] = {
            OP_LOAD_I64,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 2 );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes bload") {

    SECTION("bload i8") {

        byte_t code[] = {
            OP_BLOAD_I8,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 2 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("bload i16") {

        byte_t code[] = {
            OP_BLOAD_I16,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 3 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("bload i32") {

        byte_t code[] = {
            OP_BLOAD_I32,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            24,
            0,
            55,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 5 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 55 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("bload i64") {

        byte_t code[] = {
            OP_BLOAD_I64,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            255,
            255,
            42,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // offset
        fbContext_push( &context, 2 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
