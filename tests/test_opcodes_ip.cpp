#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/information_packet.h"
#include "vm/debug/vm_debug.h"
#include "vm/core/scheduler_internal.h"

TEST_CASE("opcodes ip related") {

    SECTION("length") {

        byte_t code[] = {
            OP_CREATE_IP,
            1,
            0,
            42,
            OP_IP_LENGTH,
            1,
            OP_DROP_IP,
            1,
            OP_END,
        };

        FBContext context;

        fbContext_init_allocate_i( &context, 1 );
        fbContext_setCode( &context, code, sizeof(code) );

        fbContext_reset( &context );

        FBContextStatus run_status;
        

        // produces STATUS_REQUEST_CREATE
        run_status = fbContext_run( &context );
        // service STATUS_REQUEST_CREATE
        _internal_fbScheduler_serviceRequests( &context, run_status );
        // produces STATUS_REQUEST_DROP
        run_status = fbContext_run( &context );
        // service STATUS_REQUEST_DROP
        _internal_fbScheduler_serviceRequests( &context, run_status );
        // end run
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop( &context ) );

    }

    SECTION("copy") {

        byte_t code[] = {
            OP_CREATE_IP,
            0,
            0,
            42,
            OP_CREATE_IP,
            1,
            0,
            42,
            OP_BPUSH,
            0, // dst offset
            OP_BPUSH,
            10, // src offset
            OP_BPUSH,
            42, // length
            OP_IP_COPY,
            1,
            0,
            OP_DROP_IP,
            0,
            OP_DROP_IP,
            1,
            OP_END,
        };

        FBContext context;

        fbContext_init_allocate_i( &context, 2 );
        fbContext_setCode( &context, code, sizeof(code) );

        fbContext_reset( &context );

        FBContextStatus run_status;
        

        // produces STATUS_REQUEST_CREATE
        run_status = fbContext_run( &context );
        // service STATUS_REQUEST_CREATE
        _internal_fbScheduler_serviceRequests( &context, run_status );
        // produces STATUS_REQUEST_CREATE
        run_status = fbContext_run( &context );
        // service STATUS_REQUEST_CREATE
        _internal_fbScheduler_serviceRequests( &context, run_status );

        // produces STATUS_REQUEST_DROP
        run_status = fbContext_run( &context );
        // service STATUS_REQUEST_DROP
        _internal_fbScheduler_serviceRequests( &context, run_status );
        // produces STATUS_REQUEST_DROP
        run_status = fbContext_run( &context );
        // service STATUS_REQUEST_DROP
        _internal_fbScheduler_serviceRequests( &context, run_status );

        // end run
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 32 == fbContext_pop( &context ) );

    }
}