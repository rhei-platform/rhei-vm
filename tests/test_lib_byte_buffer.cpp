#include "catch.hpp"

#include "vm/lib/databuffer.h"

TEST_CASE("byte buffer") {

    SECTION("init") {

        FBByteBuffer dataBuffer;

        fbByteBuffer_init( &dataBuffer );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 0 );
        REQUIRE( dataBuffer.buffer == NULL );

        fbByteBuffer_init_i( &dataBuffer, 64 );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 64 );
        REQUIRE( dataBuffer.buffer != NULL );
    }

    SECTION("free") {

        FBByteBuffer dataBuffer;

        fbByteBuffer_init( &dataBuffer );

        fbByteBuffer_free( &dataBuffer );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 0 );
        REQUIRE( dataBuffer.buffer == NULL );


        fbByteBuffer_init_i( &dataBuffer, 64 );

        fbByteBuffer_free( &dataBuffer );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 0 );
        REQUIRE( dataBuffer.buffer == NULL );
    }

    SECTION("add value") {

        FBByteBuffer dataBuffer;

        fbByteBuffer_init( &dataBuffer );

        fbByteBuffer_addValue( &dataBuffer, 100 );

        REQUIRE( dataBuffer.count == 1 );
        REQUIRE( dataBuffer.count <= dataBuffer.capacity );

        for( index_t i = dataBuffer.count; i < dataBuffer.capacity; i++ ) {
            fbByteBuffer_addValue( &dataBuffer, 55 );
        }
        REQUIRE( dataBuffer.count == dataBuffer.capacity );
        fbByteBuffer_addValue( &dataBuffer, 55 );
        REQUIRE( dataBuffer.count <= dataBuffer.capacity );

        fbByteBuffer_free( &dataBuffer );
    }

    SECTION("add array of values") {

        FBByteBuffer dataBuffer;
        byte_t bunchOfData[4] = { 8, 7, 6, 5, };
        size_t dataCount = sizeof(bunchOfData);

        fbByteBuffer_init( &dataBuffer );

        fbByteBuffer_addArray( &dataBuffer, bunchOfData, dataCount );

        REQUIRE( dataBuffer.count == dataCount );
        REQUIRE (dataBuffer.buffer != &bunchOfData[0] );

        for( index_t idx = 0; idx < dataBuffer.count; idx++) {
            REQUIRE( dataBuffer.buffer[ idx ] == bunchOfData[ idx ] );
        }

        fbByteBuffer_free( &dataBuffer );
    }
}