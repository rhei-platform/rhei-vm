#include <limits.h>
#include <string.h>
#include <math.h>

#include "catch.hpp"

#include "contrib/ipbufferaccess.h"
#include "contrib/buffer_printf.h"

extern "C" {

typedef struct _CtbPrintfOpResult {
    CtbPrintfStatus status;
    union {
        char c;
        int64_t i64;
        uint64_t ui64;
        // float f;
        double d;
        unsigned int n;
    } value;
} CtbPrintfOpResult;

    CtbPrintfOpResult _ctb_parse_number( FBIPBufferAcces *fmt );
    CtbPrintfOpResult _ctb_parse_length( FBIPBufferAcces *fmt );

    // CtbPrintfFormatType _ctb_parse_type( const char* *pfmt );
    unsigned int _ctb_parse_type( FBIPBufferAcces *fmt );

    unsigned int _ctb_dtoa( FBByteBuffer *s, double value );
    unsigned int _ctb_ltoa( FBByteBuffer *output, int64_t value );
    unsigned int _ctb_ultoa( FBByteBuffer *output, uint64_t value );
    unsigned int _ctb_ultoh( FBByteBuffer *output, uint64_t value, bool use_caps );

}

TEST_CASE("fbContrib_printf") {

    SECTION("_ctb_parse_number") {

        CtbPrintfOpResult result;
        FBIPBufferAcces fmt_b;
        FBIPBufferAcces *fmt = &fmt_b;
        const char *format_string;


        format_string = "";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string );


        format_string = "ab 5";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string );


        format_string = "5";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 5 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );


        format_string = "42";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 42 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 2 );


        format_string = "472rw ";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 472 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 3 );


        format_string = "1234567890";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 1234567890 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 10 );


        format_string = "1234567890.";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_number( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 1234567890 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 10 );
    }

    SECTION("_ctb_parse_length") {

        CtbPrintfOpResult result;
        FBIPBufferAcces fmt_b;
        FBIPBufferAcces *fmt = &fmt_b;
        const char *format_string;


        format_string = "";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string );


        format_string = "ab B";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string );


        format_string = "Brw ";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 1 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );


        format_string = "B";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 1 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );


        format_string = "S";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 2 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );


        format_string = "I";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 4 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );


        format_string = "L";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_length( fmt );

        REQUIRE( result.status == 0 );
        REQUIRE( result.value.n == 8 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );
    }

    SECTION("_ctb_parse_type") {

        unsigned int result;
        FBIPBufferAcces fmt_b;
        FBIPBufferAcces *fmt = &fmt_b;
        const char *format_string;


        format_string = "";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );
        result = _ctb_parse_type( fmt );

        REQUIRE( result == 0 ); // FORMAT_TYPE_NONE
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string );


        format_string = "diucsxXef%";
        fbIPBufferAccess_init( fmt, (byte_t*)format_string, strlen(format_string) );

        result = _ctb_parse_type( fmt ); // d
        REQUIRE( result != 0 ); // FORMAT_TYPE_NONE
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 1 );

        result = _ctb_parse_type( fmt ); // i
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 2 );

        result = _ctb_parse_type( fmt ); // u
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 3 );

        result = _ctb_parse_type( fmt ); // c
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 4 );

        result = _ctb_parse_type( fmt ); // s
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 5 );

        result = _ctb_parse_type( fmt ); // x
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 6 );

        result = _ctb_parse_type( fmt ); // X
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 7 );

        result = _ctb_parse_type( fmt ); // e
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 8 );

        result = _ctb_parse_type( fmt ); // f
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 9 );

        result = _ctb_parse_type( fmt ); // %
        REQUIRE( result != 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 10 );

        result = _ctb_parse_type( fmt ); // EOS
        REQUIRE( result == 0 );
        REQUIRE( fbIPBufferAccess_dataAtPosition(fmt) == (byte_t*)format_string + 10 );
    }

    SECTION("_ctb_dtoa") {

        double value;
        FBByteBuffer output_b;
        FBByteBuffer *output = &output_b;
        fbByteBuffer_init( output );
        unsigned int num_chars;


        value = 0.0;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 1 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0", num_chars ) );


        value = NAN;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 3 );
        REQUIRE( ! strncmp( (char *)output->buffer, "nan", num_chars ) );


        value = INFINITY;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 3 );
        REQUIRE( ! strncmp( (char *)output->buffer, "inf", num_chars ) );


        value = 0.3;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 16 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.29999999999999", num_chars ) );


        value = -1.5;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 4 );
        REQUIRE( ! strncmp( (char *)output->buffer, "-1.5", num_chars ) );


        value = 1.0e13;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 14 );
        REQUIRE( ! strncmp( (char *)output->buffer, "10000000000000", num_chars ) );


        value = 10.0e12;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 14 );
        REQUIRE( ! strncmp( (char *)output->buffer, "10000000000000", num_chars ) );


        value = 1.6e13;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 14 );
        REQUIRE( ! strncmp( (char *)output->buffer, "16000000000000", num_chars ) );


        value = 16.0e12;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 14 );
        REQUIRE( ! strncmp( (char *)output->buffer, "16000000000000", num_chars ) );


        value = 1.0e14;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 5 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1e+14", num_chars ) );


        value = 1.6e14;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 7 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1.6e+14", num_chars ) );


        value = 10.0e13;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 5 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1e+14", num_chars ) );


        value = 16.0e13;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 7 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1.6e+14", num_chars ) );


        value = -1.0e8;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 10 );
        REQUIRE( ! strncmp( (char *)output->buffer, "-100000000", num_chars ) );


        value = -1.0e9;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 5 );
        REQUIRE( ! strncmp( (char *)output->buffer, "-1e+9", num_chars ) );


        value = 1.0e-8;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 10 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.00000001", num_chars ) );


        value = 100.0e-10;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 10 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.00000001", num_chars ) );


        value = 1.0e-9;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 4 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1e-9", num_chars ) );


        value = 100.0e-11;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 4 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1e-9", num_chars ) );


        value = 1.6e-8;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 11 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.000000016", num_chars ) );


        value = 160.0e-10;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 11 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.000000016", num_chars ) );


        value = 1.6e-9;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 12 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.0000000016", num_chars ) );


        value = 160.0e-11;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 12 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.0000000016", num_chars ) );


        value = 1.5e-9;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 16 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.00000000149999", num_chars ) );


        value = 15.0e-10;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        REQUIRE( num_chars == 16 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0.00000000149999", num_chars ) );


        value = 1.5e-10;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 7 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1.5e-10", num_chars ) );


        value = 15.0e-11;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 7 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1.5e-10", num_chars ) );


        value = 0.15e-10;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 7 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1.5e-11", num_chars ) );


        value = 123.15e-14;
        fbByteBuffer_reset( output );
        num_chars = _ctb_dtoa( output, value );

        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( num_chars == 10 );
        REQUIRE( ! strncmp( (char *)output->buffer, "1.2315e-12", num_chars ) );


        fbByteBuffer_free( output );
    }

    SECTION("_ctb_ltoa") {

        int64_t value;
        FBByteBuffer output_b;
        FBByteBuffer *output = &output_b;
        fbByteBuffer_init( output );
        unsigned int num_digits;


        value = 0;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ltoa( output, value );

        REQUIRE( num_digits == 1 );
        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( ! strncmp( (char *)output->buffer, "0", num_digits ) );


        value = 42;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ltoa( output, value );

        REQUIRE( num_digits == 2 );
        REQUIRE( ! strncmp( (char *)output->buffer, "42", num_digits ) );


        value = -24;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ltoa( output, value );

        REQUIRE( num_digits == 3 );
        REQUIRE( ! strncmp( (char *)output->buffer, "-24", num_digits ) );


        value = 9223372036854775807; // 18446744073709551615 >> 1
        fbByteBuffer_reset( output );
        num_digits = _ctb_ltoa( output, value );

        REQUIRE( num_digits == 19 );
        REQUIRE( ! strncmp( (char *)output->buffer, "9223372036854775807", num_digits ) );


        value = -9223372036854775807;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ltoa( output, value );

        REQUIRE( num_digits == 20 );
        REQUIRE( ! strncmp( (char *)output->buffer, "-9223372036854775807", num_digits ) );


        fbByteBuffer_free( output );
    }

    SECTION("_ctb_ultoa") {

        uint64_t value;
        FBByteBuffer output_b;
        FBByteBuffer *output = &output_b;
        fbByteBuffer_init( output );
        unsigned int num_digits;


        value = 0;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoa( output, value );

        REQUIRE( num_digits == 1 );
        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( ! strncmp( (char *)output->buffer, "0", num_digits ) );


        value = 42;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoa( output, value );

        REQUIRE( num_digits == 2 );
        REQUIRE( ! strncmp( (char *)output->buffer, "42", num_digits ) );


        value = 18446744073709551615U;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoa( output, value );

        REQUIRE( num_digits == 20 );
        REQUIRE( ! strncmp( (char *)output->buffer, "18446744073709551615", num_digits ) );


        fbByteBuffer_free( output );
    }

    SECTION("_ctb_ultoh") {

        uint64_t value;
        FBByteBuffer output_b;
        FBByteBuffer *output = &output_b;
        fbByteBuffer_init( output );
        unsigned int num_digits;


        value = 0;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoh( output, value, false );

        REQUIRE( num_digits == 1 );
        // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
        REQUIRE( ! strncmp( (char *)output->buffer, "0", num_digits ) );


        value = 0;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoh( output, value, true );

        REQUIRE( num_digits == 1 );
        REQUIRE( ! strncmp( (char *)output->buffer, "0", num_digits ) );


        value = 47806;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoh( output, value, false );

        REQUIRE( num_digits == 4 );
        REQUIRE( ! strncmp( (char *)output->buffer, "babe", num_digits ) );


        value = 47806;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoh( output, value, true );

        REQUIRE( num_digits == 4 );
        REQUIRE( ! strncmp( (char *)output->buffer, "BABE", num_digits ) );


        value = 18446744073709551615U;
        fbByteBuffer_reset( output );
        num_digits = _ctb_ultoh( output, value, false );

        REQUIRE( num_digits == 16 );
        REQUIRE( ! strncmp( (char *)output->buffer, "ffffffffffffffff", num_digits ) );


        fbByteBuffer_free( output );
    }

    SECTION("fbContrib_printf") {

        SECTION("limits") {

            CtbPrintfResult result;
            FBByteBuffer output_b;
            FBByteBuffer *output = &output_b;
            fbByteBuffer_init( output );


            byte_t data_buf[] = {'a', 'b', 'c', 'd', 'e', 'f', 0};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%s", data_buf, 4 );

            REQUIRE( result.status == 0 );
            REQUIRE( result.chars_printed == 4 );
            // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
            REQUIRE( ! strncmp( (char *)output->buffer, "abcd", result.chars_printed ) );


            byte_t data_buf2[] = {'a', 'b', 'c'};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%s", data_buf2, sizeof(data_buf2) );

            REQUIRE( result.status == 0 );
            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "abc", result.chars_printed ) );


            byte_t data_buf3[] = {};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Bi", data_buf3, sizeof(data_buf3) );

            REQUIRE( result.status != 0 );
            REQUIRE( result.chars_printed == 0 );
            REQUIRE( output->count == 0 );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Bu", data_buf3, sizeof(data_buf3) );

            REQUIRE( result.status != 0 );
            REQUIRE( result.chars_printed == 0 );
            REQUIRE( output->count == 0 );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Bx", data_buf3, sizeof(data_buf3) );

            REQUIRE( result.status != 0 );
            REQUIRE( result.chars_printed == 0 );
            REQUIRE( output->count == 0 );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%BX", data_buf3, sizeof(data_buf3) );

            REQUIRE( result.status != 0 );
            REQUIRE( result.chars_printed == 0 );
            REQUIRE( output->count == 0 );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%s", data_buf3, sizeof(data_buf3) );

            // TODO make this error instead returning empty string?
            REQUIRE( result.status == 0 );
            REQUIRE( result.chars_printed == 0 );
            REQUIRE( output->count == 0 );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%c", data_buf3, sizeof(data_buf3) );

            REQUIRE( result.status != 0 );
            REQUIRE( result.chars_printed == 0 );
            REQUIRE( output->count == 0 );


            fbByteBuffer_free( output );
        }

        SECTION("chars/strings") {

            CtbPrintfResult result;
            FBByteBuffer output_b;
            FBByteBuffer *output = &output_b;
            fbByteBuffer_init( output );


            byte_t data_buf[] = {'a', 'b', 'c', 0};

            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%s", data_buf, sizeof(data_buf) );

            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "abc", result.chars_printed ) );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%c%c%c", data_buf, sizeof(data_buf) );

            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "abc", result.chars_printed ) );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%c%s", data_buf, sizeof(data_buf) );

            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "abc", result.chars_printed ) );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, ">%s<", data_buf, sizeof(data_buf) );

            REQUIRE( result.chars_printed == 5 );
            REQUIRE( ! strncmp( (char *)output->buffer, ">abc<", result.chars_printed ) );


            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, ">%c<%s", data_buf, sizeof(data_buf) );

            REQUIRE( result.chars_printed == 5 );
            REQUIRE( ! strncmp( (char *)output->buffer, ">a<bc", result.chars_printed ) );


            // byte_t data_buf_2[] = "Hello";
            byte_t data_buf_2[] = {
                // cs <> [Hello]
                72, 101, 108, 108, 111, 0
            };

            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%s world", data_buf_2, sizeof(data_buf_2) );

            // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
            REQUIRE( result.chars_printed == 11 );
            REQUIRE( ! strncmp( (char *)output->buffer, "Hello world", result.chars_printed ) );


            fbByteBuffer_free( output );
        }

        SECTION("integers") {

            CtbPrintfResult result;
            FBByteBuffer output_b;
            FBByteBuffer *output = &output_b;
            fbByteBuffer_init( output );


            byte_t data_buf_4[] = {0x10, 0, 0x10, 0};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Si%Si", data_buf_4, sizeof(data_buf_4) );

            REQUIRE( result.chars_printed == 4 );
            REQUIRE( ! strncmp( (char *)output->buffer, "1616", result.chars_printed ) );


            byte_t data_buf_5[] = {0x10, 0, 0x10, 0};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Ii", data_buf_5, sizeof(data_buf_5) );

            REQUIRE( result.chars_printed == 7 );
            // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
            REQUIRE( ! strncmp( (char *)output->buffer, "1048592", result.chars_printed ) );


            byte_t data_buf_6[] = {0x10, 0, 0x10, 0};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Sx%Sx", data_buf_6, sizeof(data_buf_6) );

            REQUIRE( result.chars_printed == 4 );
            REQUIRE( ! strncmp( (char *)output->buffer, "1010", result.chars_printed ) );


            byte_t data_buf_7[] = {0x10, 0, 0x10, 0};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%x", data_buf_7, sizeof(data_buf_7) );

            REQUIRE( result.chars_printed == 6 );
            REQUIRE( ! strncmp( (char *)output->buffer, "100010", result.chars_printed ) );


            fbByteBuffer_free( output );
        }

        SECTION("floats") {

            CtbPrintfResult result;
            FBByteBuffer output_b;
            FBByteBuffer *output = &output_b;
            fbByteBuffer_init( output );



            // 1.5 = 0x3fc00000
            byte_t data_buf_1[] = {0x00, 0x00, 0xc0, 0x3f};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%f", data_buf_1, sizeof(data_buf_1) );

            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "1.5", result.chars_printed ) );


            // 0.1 = 0x3dcccccd
            byte_t data_buf_2[] = {0xcd, 0xcc, 0xcc, 0x3d};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%f", data_buf_2, sizeof(data_buf_2) );

            REQUIRE( result.chars_printed == 16 );
            REQUIRE( ! strncmp( (char *)output->buffer, "0.10000000149011", result.chars_printed ) );


            // 0.1 = 0x3dcccccd
            byte_t data_buf_3[] = {0xcd, 0xcc, 0xcc, 0x3d};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%If", data_buf_3, sizeof(data_buf_3) );

            REQUIRE( result.chars_printed == 16 );
            REQUIRE( ! strncmp( (char *)output->buffer, "0.10000000149011", result.chars_printed ) );


            // 0.1 = 0x3dcccccd
            byte_t data_buf_4[] = {0xcd, 0xcc, 0xcc, 0x3d};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%e", data_buf_4, sizeof(data_buf_4) );

            REQUIRE( result.chars_printed == 16 );
            REQUIRE( ! strncmp( (char *)output->buffer, "0.10000000149011", result.chars_printed ) );


            // 0.1 = 0x3FB999999999999A
            byte_t data_buf_5[] = {0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0xb9, 0x3f};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Lf", data_buf_5, sizeof(data_buf_5) );

            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "0.1", result.chars_printed ) );


            // 0.1 = 0x3FB999999999999A
            byte_t data_buf_6[] = {0x9a, 0x99, 0x99, 0x99, 0x99, 0x99, 0xb9, 0x3f};
            fbByteBuffer_reset( output );
            result = fbContrib_printf( output, "%Le", data_buf_6, sizeof(data_buf_6) );

            // fbByteBuffer_addValue( output, 0 ); printf( "output->buffer = '%s'\n", (char*)output->buffer );
            REQUIRE( result.chars_printed == 3 );
            REQUIRE( ! strncmp( (char *)output->buffer, "0.1", result.chars_printed ) );


            fbByteBuffer_free( output );
        }
    }
}
