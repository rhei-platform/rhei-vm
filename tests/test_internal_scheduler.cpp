#include "catch.hpp"
#include "vm/interpreter/types.h"
#include "vm/core/scheduler.h"
#include "vm/core/scheduler_internal.h"

// #include "vm/interpreter/data_access.h"
#include "vm/core/information_packet.h"
#include "vm/core/information_packet_internal.h"
#include "vm/interpreter/opcode.h"


extern index_t _internal_IPRegistry_entryCount;

TEST_CASE("scheduler: internal algorithms") {

    SECTION("run slice") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context;
        fbContext_init_allocate( &context );
        fbContext_setCode( &context, code, 1 );

        FBContextStatus runStatus;



        fbContext_reset( &context );
        // SUSPENDED context with any status
        fbContext_setState( &context, CONTEXT_SUSPENDED );
        fbError_setStatus( &context, STATUS_NONE );

        runStatus = _internal_fbScheduler_runSlice( &context );

        REQUIRE( runStatus == STATUS_NONE );
        REQUIRE( fbContext_getState( &context ) == CONTEXT_SUSPENDED );


        fbContext_reset( &context );
        // RUNNING context with any status
        fbContext_setState( &context, CONTEXT_RUNNING );
        fbError_setStatus( &context, STATUS_NONE );

        runStatus = _internal_fbScheduler_runSlice( &context );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( runStatus == STATUS_CODE_END );


        fbContext_reset( &context );
        // TERMINATED context with any status
        fbContext_setState( &context, CONTEXT_TERMINATED );
        fbError_setStatus( &context, STATUS_NONE );

        runStatus = _internal_fbScheduler_runSlice( &context );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( runStatus == STATUS_NONE );

        fbContext_free( &context );
    }

    SECTION("simple status handlers") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context;
        fbContext_init_allocate( &context );
        fbContext_setCode( &context, code, 1 );



        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_TERMINATE );
        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_NONE );


        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );
        _internal_fbScheduler_serviceRequests( &context, STATUS_ERROR_OVERRUN );
        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_NONE );


        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );
        _internal_fbScheduler_serviceRequests( &context, STATUS_ERROR_UNKNOWN_OP );
        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_NONE );


        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );
        _internal_fbScheduler_serviceRequests( &context, STATUS_YIELD );
        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING);
        REQUIRE( fbError_getStatus( &context ) == STATUS_OK );


        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );
        _internal_fbScheduler_serviceRequests( &context, STATUS_CODE_END );
        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbError_getStatus( &context ) == STATUS_CODE_END );
        REQUIRE( context.ip == context.code );


        fbContext_free( &context );
    }

    SECTION("handle packet read request") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context;
        // allocate one INPUT
        fbContext_init_allocate_iii( &context, 4, 1, 1 );
        fbContext_setCode( &context, code, 1 );
        // connection and packet we use to set up read request pre-conditions
        FBConnection conn;
        fbConnection_init( &conn, fbContext_getOutputPort( &context, 0 ) , fbContext_getInputPort( &context, 0 ) );
        FBInformationPacketHandle ip;
        ip = fbInformationPacket_create( 4 );


        //
        // EMPTY connction
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        REQUIRE( fbConnection_isEmpty( &conn ) );

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_READ );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_SUSPENDED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_REQUEST_READ );


        //
        // DATA available in connction
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        REQUIRE( fbConnection_write( &conn, ip ) );
        REQUIRE( ! fbConnection_isEmpty( &conn ) );

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_READ );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbError_getStatus( &context ) == STATUS_OK );


        //
        // CLOSED port
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        fbInputOutput_close( fbContext_getInputPort( &context, 0 ) );

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_READ );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_ERROR_PORT_CLOSED );


        //
        // TERMINATED connction
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        // force connection terminated state with port being open
        conn.terminated = true;
        fbContext_getInputPort( &context, 0 )->closed = false;

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_READ );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_ERROR_CONNECTION_TERMINATED );


        fbConnection_terminate( &conn );
        fbContext_free( &context );
    }

    SECTION("handle packet write request") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context;
        // allocate one OUTPUT
        fbContext_init_allocate_iii( &context, 4, 1, 1 );
        fbContext_setCode( &context, code, 1 );
        // connection and packet we use to set up read request pre-conditions
        FBConnection conn;
        fbConnection_init( &conn, fbContext_getOutputPort( &context, 0 ), fbContext_getInputPort( &context, 0 ) );
        FBInformationPacketHandle ip;
        ip = fbInformationPacket_create( 4 );


        //
        // FULL connction
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        REQUIRE( fbConnection_write( &conn, ip ) );
        REQUIRE( fbConnection_isFull( &conn ) );

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_WRITE );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_SUSPENDED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_REQUEST_WRITE );


        //
        // SPACE available in connction
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        // make some space
        REQUIRE( fbConnection_read( &conn ) != FB_INVALID_PACKET_HANDLE );
        REQUIRE( ! fbConnection_isFull( &conn ) );
        // set up IP to be written
        context.packetHandles[ 0 ] = ip;


        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_WRITE );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbError_getStatus( &context ) == STATUS_OK );


        //
        // CLOSED port
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        fbInputOutput_close( fbContext_getOutputPort( &context, 0 ) );

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_WRITE );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_ERROR_PORT_CLOSED );


        //
        // TERMINATED connction
        //
        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        // force connection terminated state with port being open
        conn.terminated = true;
        fbContext_getOutputPort( &context, 0 )->closed = false;

        context.contextRequestData.informationPacketIndex = 3;
        context.contextRequestData.portIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_WRITE );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_TERMINATED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_ERROR_CONNECTION_TERMINATED );


        fbConnection_terminate( &conn );
        fbContext_free( &context );
    }

    SECTION("handle packet create request") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context;
        // allocate one IP location
        fbContext_init_allocate_iii( &context, 1, 0, 0 );
        fbContext_setCode( &context, code, 1 );



        _internal_fbInformationPacket_resetRegistry();

        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        // simulate full registry
        _internal_IPRegistry_entryCount = MAX_HANDLES;
        context.contextRequestData.informationPacketIndex = 0;
        context.contextRequestData.packetSize = 4;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_CREATE );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_SUSPENDED );
        REQUIRE( fbError_getStatus( &context ) == STATUS_REQUEST_CREATE );

        REQUIRE( context.packetHandles[ 0 ] == FB_INVALID_PACKET_HANDLE );
        REQUIRE( context.packets[ 0 ] == NULL );
        _internal_IPRegistry_entryCount = 0;



        _internal_fbInformationPacket_resetRegistry();

        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        context.contextRequestData.informationPacketIndex = 0;
        context.contextRequestData.packetSize = 4;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_CREATE );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbError_getStatus( &context ) == STATUS_OK );

        REQUIRE( context.packetHandles[ 0 ] != FB_INVALID_PACKET_HANDLE );
        REQUIRE( context.packets[ 0 ] != NULL );


        fbContext_free( &context );
    }

    SECTION("handle packet drop request") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context;
        // allocate one IP location
        fbContext_init_allocate_iii( &context, 1, 0, 0 );
        fbContext_setCode( &context, code, 1 );



        _internal_fbInformationPacket_resetRegistry();

        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        REQUIRE( context.packetHandles[ 0 ] == FB_INVALID_PACKET_HANDLE );
        REQUIRE( context.packets[ 0 ] == NULL );

        REQUIRE( _internal_IPRegistry_entryCount == 0 );

        context.contextRequestData.informationPacketIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_DROP );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbError_getStatus( &context ) == STATUS_OK );

        REQUIRE( context.packetHandles[ 0 ] == FB_INVALID_PACKET_HANDLE );
        REQUIRE( context.packets[ 0 ] == NULL );

        REQUIRE( _internal_IPRegistry_entryCount == 0 );



        _internal_fbInformationPacket_resetRegistry();

        fbContext_reset( &context );
        fbError_setStatus( &context, STATUS_NONE );

        FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( 4 );
        context.packetHandles[ 0 ] = tuple.handle;
        context.packets[ 0 ] = tuple.buffer;

        REQUIRE( _internal_IPRegistry_entryCount == 1 );

        context.contextRequestData.informationPacketIndex = 0;
        _internal_fbScheduler_serviceRequests( &context, STATUS_REQUEST_DROP );

        REQUIRE( fbContext_getState( &context ) == CONTEXT_RUNNING );
        REQUIRE( fbError_getStatus( &context ) == STATUS_OK );

        REQUIRE( context.packetHandles[ 0 ] == FB_INVALID_PACKET_HANDLE );
        REQUIRE( context.packets[ 0 ] == NULL );

        REQUIRE( _internal_IPRegistry_entryCount == 0 );


        fbContext_free( &context );
    }

    // so other tests don't get confused
    _internal_fbInformationPacket_resetRegistry();
}
