#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes arithmetic bitwise") {

    SECTION("opcode << i") {

        byte_t code[] = {
            OP_SHIFT_LEFT_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 22 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 22 << 4 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode << l") {

        byte_t code[] = {
            OP_SHIFT_LEFT_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 22 );
        fbContext_push( &context, 40 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 22ULL << 40 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode >> i") {

        byte_t code[] = {
            OP_SHIFT_RIGHT_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 10022 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 10022 >> 4 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode >> l") {

        byte_t code[] = {
            OP_SHIFT_RIGHT_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 10022 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 10022L >> 4 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode & i") {

        byte_t code[] = {
            OP_BIT_AND_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 1024 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( (1024 & 4) == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode & l") {

        byte_t code[] = {
            OP_BIT_AND_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 1024 );
        fbContext_push64( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( (1024 & 4) == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode ^ i") {

        byte_t code[] = {
            OP_BIT_OR_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 1024 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( (1024 ^ 4) == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode ^ l") {

        byte_t code[] = {
            OP_BIT_XOR_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 1024 );
        fbContext_push64( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( (1024 ^ 4) == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode | i") {

        byte_t code[] = {
            OP_BIT_OR_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 1024 );
        fbContext_push( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( (1024 | 4) == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode | l") {

        byte_t code[] = {
            OP_BIT_OR_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 1024 );
        fbContext_push64( &context, 4 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( (1024 | 4) == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
