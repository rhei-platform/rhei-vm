#include "catch.hpp"

#include "vm/lib/databuffer.h"

TEST_CASE("element buffer") {

    SECTION("init") {

        FBElementBuffer dataBuffer;

        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 0 );
        REQUIRE( dataBuffer.buffer == NULL );

        fbElementBuffer_init_i( &dataBuffer, sizeof(int32_t), 64 );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 64 );
        REQUIRE( dataBuffer.buffer != NULL );
    }

    SECTION("free") {

        FBElementBuffer dataBuffer;

        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );

        fbElementBuffer_free( &dataBuffer );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 0 );
        REQUIRE( dataBuffer.buffer == NULL );


        fbElementBuffer_init_i( &dataBuffer, sizeof(int32_t), 64 );

        fbElementBuffer_free( &dataBuffer );

        REQUIRE( dataBuffer.count == 0 );
        REQUIRE( dataBuffer.capacity == 0 );
        REQUIRE( dataBuffer.buffer == NULL );
    }

    SECTION("add value") {

        FBElementBuffer dataBuffer;
        int32_t aValue;

        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );

        aValue = 100;
        fbElementBuffer_addValue( &dataBuffer, &aValue );

        REQUIRE( dataBuffer.count == 1 );
        REQUIRE( dataBuffer.count <= dataBuffer.capacity );

        aValue = 55;
        for( index_t i = dataBuffer.count; i < dataBuffer.capacity; i++ ) {
            fbElementBuffer_addValue( &dataBuffer, &aValue );
        }
        REQUIRE( dataBuffer.count == dataBuffer.capacity );
        fbElementBuffer_addValue( &dataBuffer, &aValue );
        REQUIRE( dataBuffer.count <= dataBuffer.capacity );

        fbElementBuffer_free( &dataBuffer );
    }

    SECTION("add array of values") {

        FBElementBuffer dataBuffer;
        int32_t bunchOfData[] = { 8, 7, 6, 5, };
        size_t dataCount = sizeof(bunchOfData)/sizeof(int32_t);

        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );

        fbElementBuffer_addArray( &dataBuffer, bunchOfData, dataCount );

        REQUIRE( dataBuffer.count == dataCount );
        REQUIRE( dataBuffer.buffer != &bunchOfData[0] );

        for( index_t idx = 0; idx < dataBuffer.count; idx++) {
            REQUIRE( fbElementBuffer_get( &dataBuffer, idx, int32_t ) == bunchOfData[ idx ] );
        }

        fbElementBuffer_free( &dataBuffer );
    }

    SECTION("remove element") {

        FBElementBuffer dataBuffer;
        int32_t bunchOfData[] = { 8, 7, 6, 5, 4, 3, 2, 1 };
        size_t dataCount = sizeof(bunchOfData)/sizeof(int32_t);



        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );
        fbElementBuffer_addArray( &dataBuffer, bunchOfData, dataCount );

        // remove at start
        fbElementBuffer_removeElement( &dataBuffer, 0 );

        REQUIRE( dataBuffer.count == dataCount - 1 );
        REQUIRE( fbElementBuffer_get( &dataBuffer, 0, int32_t ) == bunchOfData[1] );

        for( index_t idx = 0; idx < dataBuffer.count; idx++) {
            REQUIRE( fbElementBuffer_get( &dataBuffer, idx, int32_t ) == bunchOfData[ idx + 1 ] );
        }


        fbElementBuffer_free( &dataBuffer );
        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );
        fbElementBuffer_addArray( &dataBuffer, bunchOfData, dataCount );

        // remove at end
        fbElementBuffer_removeElement( &dataBuffer, dataBuffer.count - 1 );

        REQUIRE( dataBuffer.count == dataCount - 1 );
        REQUIRE( fbElementBuffer_get( &dataBuffer, dataBuffer.count - 1, int32_t ) == bunchOfData[ dataCount - 2 ] );

        for( index_t idx = 0; idx < dataBuffer.count; idx++) {
            REQUIRE( ((int32_t*)dataBuffer.buffer)[ idx ] == bunchOfData[ idx ] );
        }


        fbElementBuffer_free( &dataBuffer );
        fbElementBuffer_init( &dataBuffer, sizeof(int32_t) );
        fbElementBuffer_addArray( &dataBuffer, bunchOfData, dataCount );

        // remove in the middle
        index_t elIndex = dataBuffer.count / 2;
        fbElementBuffer_removeElement( &dataBuffer, elIndex );

        REQUIRE( dataBuffer.count == dataCount - 1 );
        REQUIRE( fbElementBuffer_get( &dataBuffer, elIndex, int32_t ) == bunchOfData[ elIndex + 1 ] );

        for( index_t idx = 0; idx < elIndex; idx++) {
            REQUIRE( ((int32_t*)dataBuffer.buffer)[ idx ] == bunchOfData[ idx ] );
        }

        for( index_t idx = elIndex; idx < dataBuffer.count; idx++) {
            REQUIRE( ((int32_t*)dataBuffer.buffer)[ idx ] == bunchOfData[ idx + 1 ] );
        }



        fbElementBuffer_free( &dataBuffer );
    }
}