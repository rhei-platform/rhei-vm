#include "catch.hpp"
#include "vm/core/input_output.h"
#include "vm/interpreter/context.h"
#include "vm/core/information_packet.h"

TEST_CASE("core connection") {

    SECTION("all") {

        // SETUP
        FBContext context;
        fbContext_init_allocate( &context );
        fbContext_reset( &context );

        FBInputOutput source;
        FBInputOutput sink;
        fbInputOutput_init( &source, &context );
        fbInputOutput_init( &sink, &context );

        FBInformationPacketHandle ipA = fbInformationPacket_create( 4 );
        FBInformationPacketHandle ipB = fbInformationPacket_create( 4 );
        FBInformationPacketHandle ipC = fbInformationPacket_create( 4 );


        // TESTS
        FBConnection connection;

        fbConnection_init_i( &connection, &source, &sink, 2 );

        REQUIRE( source.connection == &connection );
        REQUIRE( sink.connection == &connection );
        REQUIRE( connection.source == &source );
        REQUIRE( connection.sink == &sink );

        REQUIRE( fbInputOutput_isOpen( &source ) );
        REQUIRE( fbInputOutput_isOpen( &sink ) );

        REQUIRE( fbConnection_isEmpty( &connection ) );
        REQUIRE( ! fbConnection_isFull( &connection ) );
        REQUIRE( ! fbConnection_isTerminated( &connection ) );


        REQUIRE( fbConnection_write( &connection, ipA ) );
        REQUIRE( ! fbConnection_isEmpty( &connection ) );
        REQUIRE( ! fbConnection_isFull( &connection ) );
        REQUIRE( STATUS_OK == fbError_getStatus( &context ) );


        REQUIRE( fbConnection_write( &connection, ipB ) );
        REQUIRE( ! fbConnection_isEmpty( &connection ) );
        REQUIRE( fbConnection_isFull( &connection ) );
        REQUIRE( STATUS_OK == fbError_getStatus( &context ) );


        REQUIRE( ! fbConnection_write( &connection, ipC ) );
        REQUIRE( STATUS_ERROR_CONNECTION_FULL == fbError_getStatus( &context ) );
        fbError_setStatus( &context, STATUS_OK );


        ipC = fbConnection_read( &connection );
        REQUIRE( ipC != FB_INVALID_PACKET_HANDLE );
        REQUIRE( ipC == ipA );
        REQUIRE( STATUS_OK == fbError_getStatus( &context ) );


        ipC = fbConnection_read( &connection );
        REQUIRE( ipC != FB_INVALID_PACKET_HANDLE );
        REQUIRE( ipC == ipB );
        REQUIRE( STATUS_OK == fbError_getStatus( &context ) );


        ipC = fbConnection_read( &connection );
        REQUIRE( ipC == FB_INVALID_PACKET_HANDLE );
        REQUIRE( STATUS_ERROR_CONNECTION_EMPTY == fbError_getStatus( &context ) );
        fbError_setStatus( &context, STATUS_OK );


        fbConnection_write( &connection, ipA );
        REQUIRE( ! fbConnection_isEmpty( &connection ) );
        // ipA packet is dropped by connection here
        fbConnection_terminate( &connection );
        REQUIRE( fbConnection_isTerminated( &connection ) );
        REQUIRE( fbConnection_isEmpty( &connection ) );
        REQUIRE( fbInputOutput_isClosed( &source ) );
        REQUIRE( fbInputOutput_isClosed( &sink ) );


        ipC = fbConnection_read( &connection );
        REQUIRE( ipC == FB_INVALID_PACKET_HANDLE );
        REQUIRE( STATUS_ERROR_CONNECTION_TERMINATED == fbError_getStatus( &context ) );
        fbError_setStatus( &context, STATUS_OK );


        fbConnection_write( &connection, ipB );
        REQUIRE( ipC == FB_INVALID_PACKET_HANDLE );
        REQUIRE( STATUS_ERROR_CONNECTION_TERMINATED == fbError_getStatus( &context ) );


        // CLEANUP
        fbInformationPacket_drop( ipA );
        fbInformationPacket_drop( ipB );
        fbInformationPacket_drop( ipC );
    }
}