#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes cstore") {

    SECTION("cstore i8") {

        byte_t code[] = {
            OP_CSTORE_I8,
            OP_END,
        };

        byte_t component_data[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int8( component_data + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("cstore i16") {

        byte_t code[] = {
            OP_CSTORE_I16,
            OP_END,
        };

        byte_t component_data[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int16( component_data + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("cstore i32") {

        byte_t code[] = {
            OP_CSTORE_I32,
            OP_END,
        };

        byte_t component_data[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int32( component_data + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("cstore i64") {

        byte_t code[] = {
            OP_CSTORE_I64,
            OP_END,
        };

        byte_t component_data[] = {
            0,
            42,
            0,
            42,
            0,
            42,
            0,
            42,
            0,
            42,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push64( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int64( component_data + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes store") {

    SECTION("store i8") {

        byte_t code[] = {
            OP_STORE_I8,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int8( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("store i16") {

        byte_t code[] = {
            OP_STORE_I16,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int16( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("store i32") {

        byte_t code[] = {
            OP_STORE_I32,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int32( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("store i64") {

        byte_t code[] = {
            OP_STORE_I64,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push64( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        // index
        fbContext_push( &context, 1 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int64( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes bstore") {

    SECTION("bstore i8") {

        byte_t code[] = {
            OP_BSTORE_I8,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int8( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("bstore i16") {

        byte_t code[] = {
            OP_BSTORE_I16,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int16( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("bstore i32") {

        byte_t code[] = {
            OP_BSTORE_I32,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int32( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("bstore i64") {

        byte_t code[] = {
            OP_BSTORE_I64,
            1,
            OP_END,
        };

        byte_t information_packet[] = {
            0,
            42,
            0,
            42,
            0,
            42,
            0,
            42,
            0,
            42,
        };

        byte_t* ips[2] = { NULL, &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        // value
        fbContext_push64( &context, 75 );
        index_t offset = 1;
        fbContext_push( &context, offset );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 75 == fbData_get_int64( information_packet + offset ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
