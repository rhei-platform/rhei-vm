#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"

TEST_CASE("opcodes misc") {

    SECTION("asd") {

        byte_t code[] = {
            OP_BPUSH,
            10,
            OP_BPUSH,
            2,
            OP_ADD_I,
            OP_END,
        };

        byte_t component_data[] = {
            0,
            42,
        };

        byte_t constant_pool[] = {
            0,
            42,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
    }

    SECTION("op invalid") {

        byte_t code[] = {
            OP_INVALID,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_ERROR_INVALID_OP );
    }

    SECTION("op end") {

        byte_t code[] = {
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
    }

    SECTION("op terminate") {

        byte_t code[] = {
            OP_TERMINATE,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_REQUEST_TERMINATE );
    }

    SECTION("op unknown") {

        byte_t code[] = {
            255,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_ERROR_UNKNOWN_OP );
    }

    SECTION("op yield") {

        byte_t code[] = {
            OP_BPUSH,
            22,
            OP_YIELD,
            OP_BPUSH,
            44,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBContextStatus run_status;
        
        REQUIRE( context.ip == context.code );

        run_status = fbContext_run( &context );
        REQUIRE( run_status == STATUS_YIELD );
        REQUIRE( context.ip == context.code + 3 );
        
        run_status = fbContext_run( &context );
        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( context.ip == context.codeEnd + 1 );
    }
}