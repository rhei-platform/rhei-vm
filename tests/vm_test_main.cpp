#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
// #define CATCH_CONFIG_RUNNER
#include "catch.hpp"


#include "vm/interpreter/context.h"

static std::string convertRunStatusToString( FBContextStatus const& value ) {

    switch (value)
    {
        case STATUS_ERROR_INVALID_OP:
            return "STATUS_ERROR_INVALID_OP";
        case STATUS_ERROR_UNKNOWN_OP:
            return "STATUS_ERROR_UNKNOWN_OP";
        case STATUS_ERROR_OVERRUN:
            return "STATUS_ERROR_OVERRUN";

        case STATUS_REQUEST_WRITE:
            return "STATUS_REQUEST_WRITE";
        case STATUS_REQUEST_READ:
            return "STATUS_REQUEST_READ";

        case STATUS_INVALID_STATE:
            return "STATUS_INVALID_STATE";
        case STATUS_CODE_END:
            return "STATUS_CODE_END";
        case STATUS_REQUEST_TERMINATE:
            return "STATUS_REQUEST_TERMINATE";
    
        default:
            break;
    }

    return std::to_string( value );
}
// namespace Catch {
//     template<>
//     struct StringMaker<FBContextStatus> {
//         static std::string convert( FBContextStatus const& value ) {
//             return convertRunStatusToString( value );
//         }
//     };
// }
std::ostream& operator << ( std::ostream& os, FBContextStatus const& value ) {
    os << convertRunStatusToString( value );
    return os;
}
