#include "catch.hpp"
#include "vm/util/network_builder.h"

#include "samples/negator.h"
#include "samples/passthrough.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/debug/vm_debug.h"

#include "vm/native.h"

#include <iostream>


static uint32_t fnInputValue;
static uint32_t fnOutputValue;

static void inputFn( FBNativeContextHandle handle ) {

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) return;
    // or
    // if( fbNativeConnection_isTerminated( handle ) ) return;

    static FBInformationPacketHandle packet = FB_INVALID_PACKET_HANDLE;

    FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    packet = tuple.handle;
    *((int32_t *)tuple.buffer) = fnInputValue;

    fbNativeConnection_write( handle, packet );

    fbNativeConnection_close( handle );
}

static void outputFn( FBNativeContextHandle handle ) {

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) return;
    // or
    // if( fbNativeConnection_isTerminated( handle ) ) return;

    FBInformationPacketHandle ip = fbNativeConnection_read( handle );

    if( ip != FB_INVALID_PACKET_HANDLE ) {

        byte_t* buffer = fbInformationPacket_getDataBuffer( ip );

        fnOutputValue = *((int32_t*)buffer);
        // std::cout << fnOutputValue << std::endl;

        fbInformationPacket_drop( ip );
    }
}


int nativeInputCallCount;
int nativeOutputCallCount;
bool nativeInputTerminatedCall;
bool nativeOutputTerminatedCall;

static void inputFn_2( FBNativeContextHandle handle ) {

    nativeInputCallCount++;

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) {
        nativeInputTerminatedCall = true;
        return;
    }
    FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( sizeof( int32_t ) );
    fbData_set_int32( tuple.buffer, 42 );
    fbNativeConnection_write( handle, tuple.handle );
    fbNativeConnection_close( handle );
}

static void outputFn_2( FBNativeContextHandle handle ) {

    nativeOutputCallCount++;

    if( FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) ) {
        nativeOutputTerminatedCall = true;
        return;
    }
    FBInformationPacketHandle ip = fbNativeConnection_read( handle );
    if( ip != FB_INVALID_PACKET_HANDLE ) {
        fbInformationPacket_drop( ip );
    }
}

TEST_CASE("native i/o handler") {

    SECTION("smoke") {

        FBComponent* negator = fbSamples_getNegator();

        FBNetworkBuilderData nbdata;

        fbNetworkBuilder_init( &nbdata );

        index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );

        index_t portAidx = fbNetworkBuilder_setNetworkInput( &nbdata, ctxNegidx, 0 );
        index_t portOUTidx = fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );

        index_t nativeInIdx = fbNetworkBuilder_setNativeInput( &nbdata, inputFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInIdx, ctxNegidx, 0, 1 );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, outputFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxNegidx, 0, nativeOutIdx, 1 );


        FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );

        fbNetworkBuilder_free( &nbdata );


        FBInputOutput *portA;
        FBInputOutput *portOUT;
        FBContext *ctxNeg;

        ctxNeg = fbNetwork_getContext( networkContext, ctxNegidx );
        REQUIRE( ctxNeg != NULL );


        portA = fbContext_getInputPort( ctxNeg, 0 );
        portOUT = fbContext_getOutputPort( ctxNeg, 0 );

        REQUIRE( portA == fbNetwork_getInput( networkContext, portAidx ) );
        REQUIRE( portOUT == fbNetwork_getOutput( networkContext, portOUTidx ) );



        FBNetworkNativeIO *nativeIOInput = fbNetwork_getNativeInput( networkContext, nativeInIdx );
        // this crashes catch2
        // REQUIRE( nativeIOInput->fn == nativeFn );
        FBNetworkNativeIO *nativeIOOutput = fbNetwork_getNativeOutput( networkContext, nativeOutIdx );
        // this crashes catch2
        // REQUIRE( nativeIOOutput->fn == nativeFn );

        REQUIRE( nativeIOInput->port->connection == portA->connection );
        REQUIRE( nativeIOInput->port->connection->source == nativeIOInput->port );
        REQUIRE( nativeIOInput->port->connection->sink == portA );
        REQUIRE( nativeIOOutput->port->connection == portOUT->connection );
        REQUIRE( nativeIOOutput->port->connection->source == portOUT );
        REQUIRE( nativeIOOutput->port->connection->sink == nativeIOOutput->port );


        fnInputValue = 42;
        //---------------------------------------------------------------
        // Run the network
        //---------------------------------------------------------------
        while( fbScheduler_mainLoop( networkContext ) ) {
            // the network will run until component terminates
            // by reaching OP_TERMINATE opcode at which point
            // scheduler will terminate both input connections
            // and close all ports on them and then close
            // component's output port.
            // output connection with result in it will still
            // be alive and our portResult will still be open

            // DEBUG INFO
            // fbDebug_printNetworkContextState( networkContext );
        }

        REQUIRE( fnOutputValue == -42 );

        fbNetwork_destroy( networkContext );
    }

    SECTION("terminated") {

        FBComponent* passthrough = fbSamples_getPassthrough();

        FBNetworkBuilderData nbdata;

        fbNetworkBuilder_init( &nbdata );

        index_t ctxPassIdx = fbNetworkBuilder_addComponentInstance( &nbdata, passthrough );

        index_t nativeInIdx = fbNetworkBuilder_setNativeInput( &nbdata, inputFn_2, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInIdx, ctxPassIdx, 0, 1 );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, outputFn_2, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxPassIdx, 0, nativeOutIdx, 1 );


        FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );

        fbNetworkBuilder_free( &nbdata );


        FBNetworkNativeIO *nativeIOInput = fbNetwork_getNativeInput( networkContext, nativeInIdx );
        FBNetworkNativeIO *nativeIOOutput = fbNetwork_getNativeOutput( networkContext, nativeOutIdx );
        FBContext *ctxPass = fbNetwork_getContext( networkContext, ctxPassIdx );


        REQUIRE( nativeIOInput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( nativeIOOutput->state == STATUS_NATIVE_FN_LIVE );

        nativeInputCallCount = 0;
        nativeOutputCallCount = 0;
        nativeInputTerminatedCall = false;
        nativeOutputTerminatedCall = false;

        bool hasServicedAtLeastOneContext;

        // fbDebug_printNetworkContextState( networkContext );
        // runs nativeIn, which closes port,
        // and runs component SETUP code
        hasServicedAtLeastOneContext = fbScheduler_mainLoop( networkContext );
        REQUIRE( true == hasServicedAtLeastOneContext );
        REQUIRE( 1 == nativeInputCallCount );
        REQUIRE( 0 == nativeOutputCallCount );
        REQUIRE( false == nativeInputTerminatedCall );
        REQUIRE( false == nativeOutputTerminatedCall );
        REQUIRE( nativeIOInput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( nativeIOOutput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( ctxPass->state == CONTEXT_RUNNING );

        // fbDebug_printNetworkContextState( networkContext );
        // runs component LOOP code, blocks on read IN
        hasServicedAtLeastOneContext = fbScheduler_mainLoop( networkContext );
        REQUIRE( true == hasServicedAtLeastOneContext );
        REQUIRE( 1 == nativeInputCallCount );
        REQUIRE( 0 == nativeOutputCallCount );
        REQUIRE( false == nativeInputTerminatedCall );
        REQUIRE( false == nativeOutputTerminatedCall );
        REQUIRE( nativeIOInput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( nativeIOOutput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( ctxPass->state == CONTEXT_RUNNING );

        // fbDebug_printNetworkContextState( networkContext );
        // runs nativeIn in termination mode,
        // writes to component OUT,
        // and runs nativeOut
        hasServicedAtLeastOneContext = fbScheduler_mainLoop( networkContext );
        REQUIRE( true == hasServicedAtLeastOneContext );
        REQUIRE( 2 == nativeInputCallCount );
        REQUIRE( 1 == nativeOutputCallCount );
        REQUIRE( true == nativeInputTerminatedCall );
        REQUIRE( false == nativeOutputTerminatedCall );
        REQUIRE( nativeIOInput->state == STATUS_NATIVE_FN_TERMINATED );
        REQUIRE( nativeIOOutput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( ctxPass->state == CONTEXT_RUNNING );

        // fbDebug_printNetworkContextState( networkContext );
        // after component write to OUT, runs LOOP code to the end
        hasServicedAtLeastOneContext = fbScheduler_mainLoop( networkContext );
        REQUIRE( true == hasServicedAtLeastOneContext );
        REQUIRE( 2 == nativeInputCallCount );
        REQUIRE( 1 == nativeOutputCallCount );
        REQUIRE( true == nativeInputTerminatedCall );
        REQUIRE( false == nativeOutputTerminatedCall );
        REQUIRE( nativeIOInput->state == STATUS_NATIVE_FN_TERMINATED );
        REQUIRE( nativeIOOutput->state == STATUS_NATIVE_FN_LIVE );
        REQUIRE( ctxPass->state == CONTEXT_RUNNING );

        // fbDebug_printNetworkContextState( networkContext );
        // component requests read from IN which is closed so component is terminated,
        // OUT connection is terminated so nativeOut is ran in termination mode
        hasServicedAtLeastOneContext = fbScheduler_mainLoop( networkContext );
        REQUIRE( true == hasServicedAtLeastOneContext );
        REQUIRE( 2 == nativeInputCallCount );
        REQUIRE( 2 == nativeOutputCallCount );
        REQUIRE( true == nativeInputTerminatedCall );
        REQUIRE( true == nativeOutputTerminatedCall );
        REQUIRE( nativeIOInput->state == STATUS_NATIVE_FN_TERMINATED );
        REQUIRE( nativeIOOutput->state == STATUS_NATIVE_FN_TERMINATED );
        REQUIRE( ctxPass->state == CONTEXT_TERMINATED );

        // the only component in network has been terminated so nothing to do anymore
        hasServicedAtLeastOneContext = fbScheduler_mainLoop( networkContext );
        REQUIRE( false == hasServicedAtLeastOneContext );

        // fbDebug_printNetworkContextState( networkContext );
        fbNetwork_destroy( networkContext );
    }
}
