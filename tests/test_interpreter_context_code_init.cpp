#include "catch.hpp"

// #include "metadata.h"
#include "vm/debug/vm_debug.h"
#include "vm/interpreter/data_access.h"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"

TEST_CASE("dynamic code init") {

    FBContext context;
    FBByteBuffer code;
    FBByteBuffer constantPool;

    fbByteBuffer_init_i( &constantPool, 64 );
    fbByteBuffer_init( &code );

    fbByteBuffer_addValue( &code, OP_BPUSH );
    fbByteBuffer_addValue( &code, 10 );
    fbByteBuffer_addValue( &code, OP_BPUSH );
    fbByteBuffer_addValue( &code, 2 );
    fbByteBuffer_addValue( &code, OP_ADD_I );
    fbByteBuffer_addValue( &code, OP_END );

    // fbDebug_decodeCode( &code );

    fbContext_init_allocate( &context );
    fbContext_setCode( &context, code.buffer, code.count );
    fbContext_createData( &context, 64 );
    fbContext_setConstantPool( &context, constantPool.buffer );
    fbContext_reset( &context );
    FBContextStatus run_status = fbContext_run( &context );
    // printf("\n\n");

    fbContext_free( &context );
    fbByteBuffer_free( &code );
    fbByteBuffer_free( &constantPool );

    REQUIRE( run_status == STATUS_CODE_END );
}

TEST_CASE("static code init") {

    byte_t code[] = {
        OP_BPUSH,
        10,
        OP_BPUSH,
        2,
        OP_ADD_I,
        OP_END,
    };

    byte_t component_data[] = {
        0,
        42,
    };

    byte_t constant_pool[] = {
        0,
        42,
    };

    FBContext context = {
        .ip = NULL,
        .state = CONTEXT_INITIALIZED,
        .status = STATUS_OK,

        .code = &code[0],
        .codeEnd = &code[0] + sizeof(code) - 1,

        .componentData = &component_data[0],
        .constantPool = &constant_pool[0],
        .packets = NULL,
        .packetHandles = NULL,

        .stack = {0},
        .stackTop = NULL,

        .contextRequestData = {0,{0}},

        .inputs = NULL,
        .outputs = NULL,

        .inputsCount = 0,
        .outputsCount = 0,

        .component = 0,
    };

    // printf("\n\n");
    // fbDebug_decodeCode_bv( code, sizeof(code) );

    fbContext_reset( &context );
    FBContextStatus run_status = fbContext_run( &context );
    // printf("\n\n");

    REQUIRE( run_status == STATUS_CODE_END );
}