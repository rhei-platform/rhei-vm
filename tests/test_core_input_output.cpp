#include "catch.hpp"
#include "vm/core/input_output.h"
#include "vm/interpreter/context.h"
#include "vm/core/information_packet.h"

TEST_CASE("core input/output") {

    SECTION("all") {

        FBContext context;
        fbContext_init_allocate( &context );
        fbContext_reset( &context );

        FBInputOutput other_side;
        fbInputOutput_init( &other_side, &context );


        FBInputOutput port;

        fbInputOutput_init( &port, &context );

        REQUIRE( port.closed );
        REQUIRE( port.connection == NULL );
        REQUIRE( fbInputOutput_isClosed( &port ) );
        REQUIRE( ! fbInputOutput_isOpen( &port ) );


        FBConnection connection;
        fbConnection_init_i( &connection, &port, &other_side, 2 );

        REQUIRE( ! fbInputOutput_isClosed( &port ) );
        REQUIRE( fbInputOutput_isOpen( &port ) );
        REQUIRE( ! fbInputOutput_isClosed( &other_side ) );
        REQUIRE( fbInputOutput_isOpen( &other_side ) );
        // empty connection FIFO
        REQUIRE( fbInput_cannotRead( &port ) );
        REQUIRE( fbOutput_canWrite( &port ) );
        REQUIRE( ! fbInput_canRead( &port ) );


        FBInformationPacketHandle ipA = fbInformationPacket_create( 4 );
        FBInformationPacketHandle ipB = fbInformationPacket_create( 4 );

        // write 2 packets to fill up connection FIFO
        // partially full connection FIFO
        fbOutput_write( &port, ipA );
        REQUIRE( fbInput_canRead( &port ) );
        REQUIRE( fbOutput_canWrite( &port ) );

        // connection FIFO full
        fbOutput_write( &port, ipB );
        REQUIRE( fbOutput_cannotWrite( &port ) );
        REQUIRE( fbInput_canRead( &port ) );
        REQUIRE( ! fbOutput_canWrite( &port ) );

        REQUIRE( ipA == fbInput_read( &port ) );
        REQUIRE( ipB == fbInput_read( &port ) );

        fbInformationPacket_drop( ipA );
        fbInformationPacket_drop( ipB );


        fbInputOutput_close( &port );
        REQUIRE( fbInputOutput_isClosed( &port ) );
        REQUIRE( ! fbInputOutput_isOpen( &port ) );
        REQUIRE( ! fbConnection_isTerminated( &connection ) );
        REQUIRE( ! fbInputOutput_isClosed( &other_side ) );
        REQUIRE( fbInputOutput_isOpen( &other_side ) );
        // port closed
        REQUIRE( fbOutput_cannotWrite( &port ) );
        REQUIRE( ! fbInput_canRead( &port ) );
        REQUIRE( ! fbOutput_canWrite( &port ) );


        fbInputOutput_closeConnection( &port );
        REQUIRE( fbInputOutput_isClosed( &port ) );
        REQUIRE( ! fbInputOutput_isOpen( &port ) );
        REQUIRE( fbConnection_isTerminated( &connection ) );
        REQUIRE( fbInputOutput_isClosed( &other_side ) );
        REQUIRE( ! fbInputOutput_isOpen( &other_side ) );

        // just for clarity sake, fbInputOutput_closeConnection has already terminated it
        fbConnection_terminate( &connection );
    }
}