#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes arithmetic binary mul") {

    SECTION("opcode mul i") {

        byte_t code[] = {
            OP_MUL_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 42 );
        fbContext_push( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 * 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode mul l") {

        byte_t code[] = {
            OP_MUL_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 42 );
        fbContext_push64( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 * 24 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode mul f") {

        byte_t code[] = {
            OP_MUL_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = 42.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = 24.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt32 = fbContext_pop( &context );
        REQUIRE( 42.0f * 24.0f == value.asFloat );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode mul d") {

        byte_t code[] = {
            OP_MUL_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = 42.0;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = 24.0;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt64 = fbContext_pop64( &context );
        REQUIRE( 42.0 * 24.0 == value.asDouble );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes arithmetic binary div") {

    SECTION("opcode div i") {

        byte_t code[] = {
            OP_DIV_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 48 );
        fbContext_push( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 48 / 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode div l") {

        byte_t code[] = {
            OP_DIV_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 48 );
        fbContext_push64( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 48 / 24 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode div f") {

        byte_t code[] = {
            OP_DIV_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = 48.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = 24.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt32 = fbContext_pop( &context );
        REQUIRE( 48.0f / 24.0f == value.asFloat );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode div d") {

        byte_t code[] = {
            OP_DIV_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = 48.0;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = 24.0;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt64 = fbContext_pop64( &context );
        REQUIRE( 48.0 / 24.0 == value.asDouble );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes arithmetic binary mod") {

    SECTION("opcode mod i") {

        byte_t code[] = {
            OP_MOD_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 48 );
        fbContext_push( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 48 % 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode mod l") {

        byte_t code[] = {
            OP_MOD_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 48 );
        fbContext_push64( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 48 % 24 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes arithmetic binary add") {

    SECTION("opcode add i") {

        byte_t code[] = {
            OP_ADD_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 42 );
        fbContext_push( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 + 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode add l") {

        byte_t code[] = {
            OP_ADD_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 42 );
        fbContext_push64( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 + 24 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode add f") {

        byte_t code[] = {
            OP_ADD_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = 42.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = 24.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt32 = fbContext_pop( &context );
        REQUIRE( 42.0f + 24.0f == value.asFloat );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode add d") {

        byte_t code[] = {
            OP_ADD_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = 42.0;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = 24.0;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt64 = fbContext_pop64( &context );
        REQUIRE( 42.0 + 24.0 == value.asDouble );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes arithmetic binary sub") {

    SECTION("opcode sub i") {

        byte_t code[] = {
            OP_SUB_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 42 );
        fbContext_push( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 - 24 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode sub l") {

        byte_t code[] = {
            OP_SUB_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 42 );
        fbContext_push64( &context, 24 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 - 24 == fbContext_pop64( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode sub f") {

        byte_t code[] = {
            OP_SUB_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = 42.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = 24.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt32 = fbContext_pop( &context );
        REQUIRE( 42.0f - 24.0f == value.asFloat );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode sub d") {

        byte_t code[] = {
            OP_SUB_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = 42.0;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = 24.0;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        value.asInt64 = fbContext_pop64( &context );
        REQUIRE( 42.0 - 24.0 == value.asDouble );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
