#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes logic cmp i") {

    SECTION("case >") {

        byte_t code[] = {
            OP_CMP_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 44 );
        fbContext_push( &context, -22 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case ==") {

        byte_t code[] = {
            OP_CMP_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 44 );
        fbContext_push( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case <") {

        byte_t code[] = {
            OP_CMP_I,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, -55 );
        fbContext_push( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes logic cmp ui") {

    SECTION("case >") {

        byte_t code[] = {
            OP_CMP_UI,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 44 );
        fbContext_push( &context, 22 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case ==") {

        byte_t code[] = {
            OP_CMP_UI,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 44 );
        fbContext_push( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case <") {

        byte_t code[] = {
            OP_CMP_UI,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push( &context, 22 );
        fbContext_push( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes logic cmp l") {

    SECTION("case >") {

        byte_t code[] = {
            OP_CMP_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 44 );
        fbContext_push64( &context, -22 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case ==") {

        byte_t code[] = {
            OP_CMP_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 44 );
        fbContext_push64( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case <") {

        byte_t code[] = {
            OP_CMP_L,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, -55 );
        fbContext_push64( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes logic cmp ul") {

    SECTION("case >") {

        byte_t code[] = {
            OP_CMP_UL,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 44 );
        fbContext_push64( &context, 22 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case ==") {

        byte_t code[] = {
            OP_CMP_UL,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 44 );
        fbContext_push64( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case <") {

        byte_t code[] = {
            OP_CMP_UL,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        fbContext_push64( &context, 22 );
        fbContext_push64( &context, 44 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes logic cmp f") {

    SECTION("case >") {

        byte_t code[] = {
            OP_CMP_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = 44.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = -22.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case ==") {

        byte_t code[] = {
            OP_CMP_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = 44.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = 44.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case <") {

        byte_t code[] = {
            OP_CMP_F,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBFloatValue value;
        value.asFloat = -55.0f;
        fbContext_push( &context, value.asInt32 );
        value.asFloat = 44.0f;
        fbContext_push( &context, value.asInt32 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes logic cmp d") {

    SECTION("case >") {

        byte_t code[] = {
            OP_CMP_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = 44.0f;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = -22.0f;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case ==") {

        byte_t code[] = {
            OP_CMP_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = 44.0f;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = 44.0f;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("case <") {

        byte_t code[] = {
            OP_CMP_D,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBDoubleValue value;
        value.asDouble = -55.0f;
        fbContext_push64( &context, value.asInt64 );
        value.asDouble = 44.0f;
        fbContext_push64( &context, value.asInt64 );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( -1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}

TEST_CASE("opcodes logic and/or") {

    SECTION("opcode and") {

        byte_t code[] = {
            OP_AND,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };


        FBContextStatus run_status;

        fbContext_reset( &context );
        fbContext_push( &context, 11 );
        fbContext_push( &context, -11 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );
        fbContext_push( &context, 11 );
        fbContext_push( &context, 0 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );
        fbContext_push( &context, 0 );
        fbContext_push( &context, -11 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );
        fbContext_push( &context, 0 );
        fbContext_push( &context, 0 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("opcode or") {

        byte_t code[] = {
            OP_OR,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };


        FBContextStatus run_status;

        fbContext_reset( &context );
        fbContext_push( &context, 11 );
        fbContext_push( &context, -11 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );
        fbContext_push( &context, 11 );
        fbContext_push( &context, 0 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );
        fbContext_push( &context, 0 );
        fbContext_push( &context, -11 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 1 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );
        fbContext_push( &context, 0 );
        fbContext_push( &context, 0 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 0 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}
