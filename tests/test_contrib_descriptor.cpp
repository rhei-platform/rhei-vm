// #include <limits.h>
// #include <string.h>
// #include <math.h>

#include "catch.hpp"

#include "contrib/descriptor.h"

#include "vm/lib/databuffer.h"
#include "vm/interpreter/data_access.h"
#include "vm/lib/memory.h"


extern "C" {
}

TEST_CASE("contrib descriptor") {

    SECTION("smoke") {


        FBDescriptorElement dsca[] = {
            { T_RECORD_HDR, 5, 0 }, // descriptor record header
            { T_CSTRING, 1, 0 },   // cs   name
            { T_UINT8, 1, 0 },     // u8   age
            { T_CSTRING, 1, 0 },   // cs   address
            { T_UINT32, 1, 0 },    // u32  zip_code
            { T_FLOAT, 2, 0 },     // f[2]  coords
        };

        byte_t data[] = {
            'R','h','e','i',0,
            16,
            'A','d','d','r','e','s','s',' ','1','2','3',0,
            0x8c, 0x63, 0x01, 0x00, // 91020
            // https://gregstoll.com/~gregstoll/floattohex/
            0x21, 0x65, 0x38, 0x42, // 46.09876
            0x38, 0x18, 0xb5, 0xc2, // -90.54732
        };


        FBElementBuffer* table = fbElementBuffer_create( sizeof(FBDescriptorTableElement) );
        FBDescriptorTableElement *element;
        index_t total_size = fbContrib_descriptor( dsca, data, sizeof(data), table );
        REQUIRE( sizeof(data) == total_size );
        // printf("--------------------------\n");
        // for( index_t idx = 0; idx < 5; idx++ ) {
        //     element = (FBDescriptorTableElement *)fbElementBuffer_getElementPointer( table, idx );
        //     printf("ofs %zu\tlen %zu\tel %zu\n", element->offset, element->element_size * element->count, element->count);
        // }

        element = (FBDescriptorTableElement *) table->buffer;
        // printf( "name %s\n", data + element->offset );
        REQUIRE( ! IS_TABLE_INDEX( element->element_size ) );
        REQUIRE( ! strcmp( "Rhei", (const char *)(data + element->offset) ) );

        element++;
        // printf( "age %i\n", fbData_get_int8( data + element->offset ) );
        REQUIRE( 16 == fbData_get_int8( data + element->offset ) );

        element++;
        // printf( "address %s\n", data + element->offset );
        REQUIRE( ! IS_TABLE_INDEX( element->element_size ) );
        REQUIRE( ! strcmp( "Address 123", (const char *)(data + element->offset) ) );

        element++;
        // printf( "zip %i\n", fbData_get_int32( data + element->offset ) );
        REQUIRE( 91020 == fbData_get_int32( data + element->offset ) );

        element++;
        REQUIRE( ! IS_TABLE_INDEX( element->element_size ) );
        REQUIRE( 2 == element->count );
        // printf( "lat %f\n", fbData_get_float( data + element->offset + 0 * element->element_size ) );
        REQUIRE( Approx( 46.09876 ) == fbData_get_float( data + element->offset + 0 * element->element_size ) );
        // printf( "lon %f\n", fbData_get_float( data + element->offset + 1 * element->element_size ) );
        REQUIRE( Approx( -90.54732 ) == fbData_get_float( data + element->offset + 1 * element->element_size ) );

        fbElementBuffer_destroy( table );

        REQUIRE( 1 == 1 );
    }

    SECTION("string arrays") {


        FBDescriptorElement dsca[] = {
            { T_RECORD_HDR, 5, 0 }, // descriptor record header
            { T_CSTRING, 1, 0 },   // cs   name
            { T_UINT8, 1, 0 },     // u8   age
            { T_CSTRING, 2, 0 },   // cs   address
            { T_UINT32, 1, 0 },    // u32  zip_code
            { T_FLOAT, 2, 0 },     // f[2]  coords
        };

        byte_t data[] = {
            'R','h','e','i',0,
            16,
            'A','d','d','r','e','s','s',' ','1','2','3',0,
            'A','d','d','r','e','s','s','2',' ','4',0,
            0x8c, 0x63, 0x01, 0x00, // 91020
            // https://gregstoll.com/~gregstoll/floattohex/
            0x21, 0x65, 0x38, 0x42, // 46.09876
            0x38, 0x18, 0xb5, 0xc2, // -90.54732
        };


        FBElementBuffer* table = fbElementBuffer_create( sizeof(FBDescriptorTableElement) );
        FBDescriptorTableElement *element;
        index_t total_size = fbContrib_descriptor( dsca, data, sizeof(data), table );
        REQUIRE( sizeof(data) == total_size );
        // printf("--------------------------\n");
        // for( index_t idx = 0; idx < table->count; idx++ ) {
        //     element = (FBDescriptorTableElement *)fbElementBuffer_getElementPointer( table, idx );
        //     index_t element_size = element->element_size;
        //     index_t length = element->count;
        //     char isidx = ' ';
        //     if( IS_TABLE_INDEX( element_size ) ) {
        //         length = 1;
        //         element_size = GET_TABLE_INDEX( element_size );
        //         isidx = '>';
        //     }
        //     printf("ofs %zu\tel %zu\tlen %zu %c\n", element->offset, element->count, element_size * length, isidx);
        // }

        element = (FBDescriptorTableElement *) table->buffer;
        // printf( "name %s\n", data + element->offset );
        REQUIRE( ! IS_TABLE_INDEX( element->element_size ) );
        REQUIRE( ! strcmp( "Rhei", (const char *)(data + element->offset) ) );

        element++;
        // printf( "age %i\n", fbData_get_int8( data + element->offset ) );
        REQUIRE( 16 == fbData_get_int8( data + element->offset ) );

        element++;
        REQUIRE( IS_TABLE_INDEX( element->element_size ) );
        REQUIRE( 2 == element->count );
        index_t element_index = GET_TABLE_INDEX( element->element_size );

        FBDescriptorTableElement *array_element = (FBDescriptorTableElement *)fbElementBuffer_getElementPointer( table, element_index );
        // printf( "address[0] %s\n", data + element->offset + array_element->offset );
        REQUIRE( ! strcmp( "Address 123", (const char *)(data + array_element->offset) ) );

        array_element++;
        // printf( "address[1] %s\n", data + element->offset + array_element->offset );
        REQUIRE( ! strcmp( "Address2 4", (const char *)(data + array_element->offset) ) );

        element++;
        // printf( "zip %i\n", fbData_get_int32( data + element->offset ) );
        REQUIRE( 91020 == fbData_get_int32( data + element->offset ) );

        element++;
        REQUIRE( ! IS_TABLE_INDEX( element->element_size ) );
        // printf( "lat %f\n", fbData_get_float( data + element->offset + 0 * element->element_size ) );
        REQUIRE( Approx( 46.09876 ) == fbData_get_float( data + element->offset + 0 * element->element_size ) );
        // printf( "lon %f\n", fbData_get_float( data + element->offset + 1 * element->element_size ) );
        REQUIRE( Approx( -90.54732 ) == fbData_get_float( data + element->offset + 1 * element->element_size ) );

        fbElementBuffer_destroy( table );

        REQUIRE( 1 == 1 );
    }

    SECTION("record ref") {


        FBDescriptorElement dsca[] = {
            { T_RECORD_HDR, 3, 0 }, // descriptor record header, 3 fields
            { T_RECORD_REF, 1, 4 },   // RecUser   user
            { T_CSTRING, 1, 0 },   // cs   nickname
            { T_UINT8, 1, 0 },     // u8   count
            { T_RECORD_HDR, 5, 0 }, // descriptor record header (RecUser), 5 fields
            { T_CSTRING, 1, 0 },   // cs   name
            { T_UINT8, 1, 0 },     // u8   age
            { T_CSTRING, 2, 0 },   // cs   address
            { T_UINT32, 1, 0 },    // u32  zip_code
            { T_FLOAT, 2, 0 },     // f[2]  coords
        };

        byte_t data[] = {
            'R','h','e','i',0,
            16,
            'A','d','d','r','e','s','s',' ','1','2','3',0,
            'A','d','d','r','e','s','s','2',' ','4',0,
            0x8c, 0x63, 0x01, 0x00, // 91020
            // https://gregstoll.com/~gregstoll/floattohex/
            0x21, 0x65, 0x38, 0x42, // 46.09876
            0x38, 0x18, 0xb5, 0xc2, // -90.54732
            'K','i','k','i',0,
            42,
        };


        FBElementBuffer* table = fbElementBuffer_create( sizeof(FBDescriptorTableElement) );
        FBDescriptorTableElement *element;
        index_t total_size = fbContrib_descriptor( dsca, data, sizeof(data), table );
        REQUIRE( sizeof(data) == total_size );
        // printf("--------------------------\n");
        // for( index_t idx = 0; idx < table->count; idx++ ) {
        //     element = (FBDescriptorTableElement *)fbElementBuffer_getElementPointer( table, idx );
        //     index_t element_size = element->element_size;
        //     index_t length = element->count;
        //     char isidx = ' ';
        //     if( IS_TABLE_INDEX( element_size ) ) {
        //         length = 1;
        //         element_size = GET_TABLE_INDEX( element_size );
        //         isidx = '>';
        //     }
        //     printf("ofs %zu\tel %zu\tlen %zu %c\n", element->offset, element->count, element_size * length, isidx);
        // }

        element = (FBDescriptorTableElement *) table->buffer;
        REQUIRE( IS_TABLE_INDEX( element->element_size ) );
        REQUIRE( 1 == element->count );
        index_t element_index = GET_TABLE_INDEX( element->element_size );

        FBDescriptorTableElement *record_element = (FBDescriptorTableElement *)fbElementBuffer_getElementPointer( table, element_index );
        // printf( "name %s\n", data + record_element->offset );
        REQUIRE( ! strcmp( "Rhei", (const char *)(data + record_element->offset) ) );

        record_element++;
        // printf( "age %i\n", fbData_get_int8( data + record_element->offset ) );
        REQUIRE( 16 == fbData_get_int8( data + record_element->offset ) );

        record_element++;
        REQUIRE( IS_TABLE_INDEX( record_element->element_size ) );
        REQUIRE( 2 == record_element->count );
        element_index = GET_TABLE_INDEX( record_element->element_size );

        FBDescriptorTableElement *array_element = (FBDescriptorTableElement *)fbElementBuffer_getElementPointer( table, element_index );
        // printf( "address[0] %s\n", data + record_element->offset + array_element->offset );
        REQUIRE( ! strcmp( "Address 123", (const char *)(data + array_element->offset) ) );

        array_element++;
        // printf( "address[1] %s\n", data + record_element->offset + array_element->offset );
        REQUIRE( ! strcmp( "Address2 4", (const char *)(data + array_element->offset) ) );

        record_element++;
        // printf( "zip %i\n", fbData_get_int32( data + record_element->offset ) );
        REQUIRE( 91020 == fbData_get_int32( data + record_element->offset ) );

        record_element++;
        REQUIRE( ! IS_TABLE_INDEX( record_element->element_size ) );
        // printf( "lat %f\n", fbData_get_float( data + record_element->offset + 0 * record_element->element_size ) );
        REQUIRE( Approx( 46.09876 ) == fbData_get_float( data + record_element->offset + 0 * record_element->element_size ) );
        // printf( "lon %f\n", fbData_get_float( data + record_element->offset + 1 * record_element->element_size ) );
        REQUIRE( Approx( -90.54732 ) == fbData_get_float( data + record_element->offset + 1 * record_element->element_size ) );


        fbElementBuffer_destroy( table );

        REQUIRE( 1 == 1 );
    }
}
