#include "catch.hpp"
#include "vm/util/network_builder.h"

#include "samples/summator.h"
#include "samples/negator.h"


#include "vm/core/information_packet.h"
#include "vm/core/scheduler.h"
#include "vm/interpreter/data_access.h"
#include "vm/core/connection.h"

#include "vm/native.h"

#include "vm/debug/vm_debug.h"


static void dummyFn( FBNativeContextHandle handle ) {
    (void)handle;
}

static uint32_t fnInputValue;
static uint32_t fnOutputValue;

static void inputFn( FBNativeContextHandle handle ) {

    static FBInformationPacketHandle packet = FB_INVALID_PACKET_HANDLE;

        FBInformationPacketTuple tuple = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        packet = tuple.handle;
        *((int32_t *)tuple.buffer) = fnInputValue;

        fbNativeConnection_write( handle, packet );

        fbNativeConnection_close( handle );
}

static void outputFn( FBNativeContextHandle handle ) {

    FBInformationPacketHandle ip = fbNativeConnection_read( handle );

    if( ip != FB_INVALID_PACKET_HANDLE ) {

        byte_t* buffer = fbInformationPacket_getDataBuffer( ip );

        fnOutputValue = *((int32_t*)buffer);
        // std::cout << fnOutputValue << std::endl;

        fbInformationPacket_drop( ip );
    }
}


TEST_CASE("network builder samples") {

/*
    OUT = - ( A + B + C )

       +sum1+
    A--|0   |   +sum2+
    B--|1  0|---|0   |   +neg+
       +----+   |   0|---|0 0|---OUT
    C-----------|1   |   +---+
                +----+

*/

    SECTION("sample 1") {

        FBComponent* sumamtor = fbSamples_getSummator();
        FBComponent* negator = fbSamples_getNegator();

        FBNetworkBuilderData nbdata;

        fbNetworkBuilder_init( &nbdata );

        index_t ctxSum1idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        index_t ctxSum2idx = fbNetworkBuilder_addComponentInstance( &nbdata, sumamtor );
        index_t ctxNegidx = fbNetworkBuilder_addComponentInstance( &nbdata, negator );

        fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum1idx, 0, ctxSum2idx, 0, 1 );
        fbNetworkBuilder_addConnection_Ctx2Ctx( &nbdata, ctxSum2idx, 0, ctxNegidx, 0, 1 );

        fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 0 );
        fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum1idx, 1 );
        fbNetworkBuilder_setNetworkInput( &nbdata, ctxSum2idx, 1 );
        fbNetworkBuilder_setNetworkOutput( &nbdata, ctxNegidx, 0 );

        FBPacketHandlerFn nativeFn = dummyFn;

        index_t nativeInAIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInAIdx, ctxSum1idx, 0, 1 );
        index_t nativeInBIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInBIdx, ctxSum1idx, 1, 1 );
        index_t nativeInCIdx = fbNetworkBuilder_setNativeInput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ctx( &nbdata, nativeInCIdx, ctxSum2idx, 1, 1 );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, nativeFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ctx2Ntv( &nbdata, ctxNegidx, 0, nativeOutIdx, 1 );



        FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );

        fbNetworkBuilder_free( &nbdata );

        FBNetworkNativeIO *nativeIOInputA = fbNetwork_getNativeInput( networkContext, nativeInAIdx );
        FBNetworkNativeIO *nativeIOInputB = fbNetwork_getNativeInput( networkContext, nativeInBIdx );
        FBNetworkNativeIO *nativeIOInputC = fbNetwork_getNativeInput( networkContext, nativeInCIdx );
        FBNetworkNativeIO *nativeIOOutput = fbNetwork_getNativeOutput( networkContext, nativeOutIdx );

        // DEBUG INFO
        // fbDebug_printNetworkContext( networkContext );


        FBInformationPacketTuple packetA = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        FBInformationPacketTuple packetB = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        FBInformationPacketTuple packetC = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        FBInformationPacketHandle outputPacket;

        int32_t valueA = 55;
        int32_t valueB = 44;
        int32_t valueC = 33;

        // set data
        fbData_set_int32( packetA.buffer, valueA );
        fbData_set_int32( packetB.buffer, valueB );
        fbData_set_int32( packetC.buffer, valueC );


        //---------------------------------------------------------------
        // Send information packets into the sample network
        //---------------------------------------------------------------
        fbConnection_write( nativeIOInputA->port->connection, packetA.handle );
        fbConnection_write( nativeIOInputB->port->connection, packetB.handle );
        fbConnection_write( nativeIOInputC->port->connection, packetC.handle );


        //---------------------------------------------------------------
        // Run the network
        //---------------------------------------------------------------
        while( fbScheduler_mainLoop( networkContext ) ) {
            // the network will run until component terminates
            // by reaching OP_TERMINATE opcode at which point
            // scheduler will terminate both input connections
            // and close all ports on them and then close
            // component's output port.
            // output connection with result in it will still
            // be alive and our portResult will still be open

            // DEBUG INFO
            // fbDebug_printNetworkContextState( networkContext );
        }

        outputPacket = fbConnection_read( nativeIOOutput->port->connection );
        REQUIRE( outputPacket != FB_INVALID_PACKET_HANDLE );

        byte_t *outputData = fbInformationPacket_getDataBuffer( outputPacket );
        int32_t result = fbData_get_int32( outputData );

        REQUIRE( result == - ( valueA + valueB + valueC ) );


        fbInformationPacket_drop( outputPacket );
        fbNetwork_destroy( networkContext );
    }

/*
    OUT = IN

    inputFn -> outputFn

*/

    SECTION("native -> native") {

        FBNetworkBuilderData nbdata;

        fbNetworkBuilder_init( &nbdata );


        index_t nativeInAIdx = fbNetworkBuilder_setNativeInput( &nbdata, inputFn, (byte_t*)NULL, 0 );
        index_t nativeOutIdx = fbNetworkBuilder_setNativeOutput( &nbdata, outputFn, (byte_t*)NULL, 0 );
        fbNetworkBuilder_addConnection_Ntv2Ntv( &nbdata, nativeInAIdx, nativeOutIdx, 1 );


        FBNetworkContext *networkContext = fbNetworkBuilder_build( &nbdata );

        fbNetworkBuilder_free( &nbdata );

        // DEBUG INFO
        // fbDebug_printNetworkContext( networkContext );


        //---------------------------------------------------------------
        // Set input data value for the sample network
        //---------------------------------------------------------------
        fnInputValue = -155;
        fnOutputValue = 0;


        //---------------------------------------------------------------
        // Run the network
        //---------------------------------------------------------------
        while( fbScheduler_mainLoop( networkContext ) ) {
            // the network will run until component terminates
            // by reaching OP_TERMINATE opcode at which point
            // scheduler will terminate both input connections
            // and close all ports on them and then close
            // component's output port.
            // output connection with result in it will still
            // be alive and our portResult will still be open

            // DEBUG INFO
            // fbDebug_printNetworkContextState( networkContext );
        }

        REQUIRE( fnOutputValue == fnInputValue );

        fbNetwork_destroy( networkContext );
    }
}
