
/************************************************
 *
 * BPScript assembler
 *
 * !!! AUTO-GENERATED FILE !!!
 *
 ************************************************/
    
/*
 * Component DOC:
 *
 *    Simply passes IP from input to output.
 *    input 0:    'IN'
 *    output 0:   'OUT'
 *
 */
// '$DATA' [0] :

// '$CONST' [1] :
// u8[1] <> @0 [1]

// NAME = passthrough;
// .inputCount = 1;
// .outputCount = 1;
// .stackSize = 16;
// .informationPacketCount = 1;
// .dataSize = 0;

#include "vm/interpreter/types.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/context.h"
#include "vm/core/component.h"

    
static FBComponent* fbSamples_getPassthrough() {

    static byte_t constant_pool [] = {
        // u8 <> [ 1 ] = 0
        0,
    };

    static byte_t setup_code [] = {
        // end
        OP_END,
    };

    static byte_t loop_code [] = {
        // read 0, 0
        OP_BREQUEST_READ, 0, 0,
        // write 0, 0
        OP_BREQUEST_WRITE, 0, 0,
        // end
        OP_END,
    };


    static FBComponent component = {
        // byte_t *setupCode;
        // index_t setupCodeSize;
        &setup_code[0],
        sizeof( setup_code ),
        // byte_t *loopCode;
        // index_t loopCodeSize;
        &loop_code[0],
        sizeof( loop_code ),

        // byte_t *constantPool;
        // index_t constantPoolSize;
        &constant_pool[0],
        sizeof( constant_pool ),
    
        // index_t dataSize;
        0,
        // index_t stackSize;
        16,
        // index_t informationPacketCount;
        1,
        // index_t inputCount;
        1,
        // index_t outputCount;
        1,
    };

    return &component;
}
    