#include "vm/interpreter/types.h"
#include "vm/interpreter/opcode.h"
#include "vm/core/component.h"

static FBComponent* fbSamples_getNegator() {

    static byte_t setup_code[] = {
        OP_END,
    };

    /*
        receive ipA from input_port_0
        ipA.value = - ipA.value
        send ipA to output_port_0
    */

    static byte_t loop_code[] = {
        OP_BREQUEST_READ,
        0, // packet
        0, // port
        OP_BPUSH,
        0, // packet field offset
        OP_BLOAD_I32,
        0, // packet index
        OP_NEG_I,
        OP_BPUSH,
        0, // packet field offset
        OP_BSTORE_I32,
        0, // packet index
        OP_BREQUEST_WRITE,
        0, // packet
        0, // port
        OP_TERMINATE,
        OP_END,
    };

    static FBComponent component = {
        // byte_t *setupCode;
        // index_t setupCodeSize;
        &setup_code[0],
        sizeof( setup_code ),
        // byte_t *loopCode;
        // index_t loopCodeSize;
        &loop_code[0],
        sizeof( loop_code ),

        // byte_t *constantPool;
        // index_t constantPoolSize;
        NULL,
        0,

        // index_t dataSize;
        0,
        // index_t stackSize;
        16,
        // index_t informationPacketCount;
        1,
        // index_t inputCount;
        1,
        // index_t outputCount;
        1,
    };

    return &component;
}