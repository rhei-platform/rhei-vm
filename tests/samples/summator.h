#include "vm/interpreter/types.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/context.h"
#include "vm/core/component.h"

static FBComponent* fbSamples_getSummator() {

    static byte_t constant_pool[] = {
        0,
    };

    static byte_t setup_code[] = {
        OP_END,
    };

    /*
        receive ipA from input_port_0
        receive ipB from input_port_1
        ipA.value = ipA.value + ipB.value
        send ipA to output_port_0
        drop ipB
    */

    static byte_t loop_code[] = {
        OP_BREQUEST_READ,
        0, // packet
        0, // port
        OP_BREQUEST_READ,
        1, // packet
        1, // port
        OP_BPUSH,
        0, // packet field offset
        OP_BLOAD_I32,
        0, // packet index
        OP_BPUSH,
        0, // packet field offset
        OP_BLOAD_I32,
        1, // packet index
        OP_ADD_I,
        OP_BPUSH,
        0, // packet field offset
        OP_BSTORE_I32,
        0, // packet index
        OP_BREQUEST_WRITE,
        0, // packet
        0, // port
        // OP_DROP_IP,
        // 0, // packet index
        OP_DROP_IP,
        1, // packet index
        OP_TERMINATE,
        OP_END,
    };

    static FBComponent component = {
        // byte_t *setupCode;
        // index_t setupCodeSize;
        &setup_code[0],
        sizeof( setup_code ),
        // byte_t *loopCode;
        // index_t loopCodeSize;
        &loop_code[0],
        sizeof( loop_code ),

        // byte_t *constantPool;
        // index_t constantPoolSize;
        &constant_pool[0],
        sizeof( constant_pool ),

        // index_t dataSize;
        0,
        // index_t stackSize;
        16,
        // index_t informationPacketCount;
        2,
        // index_t inputCount;
        2,
        // index_t outputCount;
        1,
    };

    return &component;
}
