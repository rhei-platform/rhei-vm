#include "catch.hpp"
#include "vm/interpreter/types.h"
#include "vm/core/scheduler.h"

#include "vm/core/connection.h"
#include "vm/interpreter/context.h"

#include "vm/interpreter/data_access.h"
#include "vm/core/information_packet.h"
#include "vm/interpreter/opcode.h"


TEST_CASE("scheduler run sample") {

    SECTION("sample") {

        /*
            receive ipA from input_port_0
            receive ipB from input_port_1
            ipA.value = ipA.value + ipB.value
            send ipA to output_port_0
            drop ipB
        */

        byte_t code[] = {
            OP_BREQUEST_READ,
            0, // packet
            0, // port
            OP_BREQUEST_READ,
            1, // packet
            1, // port
            OP_BPUSH,
            0, // packet field offset
            OP_BLOAD_I32,
            0, // packet index
            OP_BPUSH,
            0, // packet field offset
            OP_BLOAD_I32,
            1, // packet index
            OP_ADD_I,
            OP_BPUSH,
            0, // packet field offset
            OP_BSTORE_I32,
            0, // packet index
            OP_BREQUEST_WRITE,
            0, // packet
            0, // port
            // OP_DROP_IP,
            // 0, // packet index
            OP_DROP_IP,
            1, // packet index
            // we have to request terminate,
            // otherwise network execution will never stop
            OP_TERMINATE,
            OP_END,
        };



        FBConnection allConnections[4];
        // const index_t allConnectionsCount = sizeof( allConnections ) / sizeof( allConnections[0] );
        const index_t allConnectionsCount = 3;

        FBContext allContexts[4];
        // const index_t allContextsCount = sizeof( allContexts ) / sizeof( allContexts[0] );
        const index_t allContextsCount = 1;

        FBNetworkContext networkContext = {
            &allContexts[0],
            allContextsCount,
            &allConnections[0],
            allConnectionsCount,
            NULL,
            0,
            NULL,
            0,
            NULL,
            0,
            NULL,
            0,
        };



        //---------------------------------------------------------------
        // Context for the only component in the network
        //---------------------------------------------------------------
        FBContext *context = &allContexts[0];

        fbContext_init_allocate_iii( context, 3, 2, 1 );
        fbContext_setCode( context, code, sizeof(code) );
        fbContext_reset( context );

        //---------------------------------------------------------------
        // Ports on our side
        //---------------------------------------------------------------
        FBInputOutput portA;
        FBInputOutput portB;
        FBInputOutput portResult;
        fbInputOutput_init( &portA, NULL );
        fbInputOutput_init( &portB, NULL );
        fbInputOutput_init( &portResult, NULL );

        //---------------------------------------------------------------
        // Connect our ports to the component's ports
        //---------------------------------------------------------------
        fbConnection_init( &allConnections[0],    &portA,                                 fbContext_getInputPort( context, 0 ) );
        fbConnection_init( &allConnections[1],    &portB,                                 fbContext_getInputPort( context, 1 ) );
        fbConnection_init( &allConnections[2],    fbContext_getOutputPort( context, 0 ),  &portResult                          );


        //---------------------------------------------------------------
        // Create our information packets and set data values
        //---------------------------------------------------------------
        FBInformationPacketTuple packetA = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        FBInformationPacketTuple packetB = fbInformationPacket_create_tuple( sizeof( int32_t ) );
        FBInformationPacketHandle outputPacket;

        int32_t valueA = 55;
        int32_t valueB = 44;

        // set data
        fbData_set_int32( packetA.buffer, valueA );
        fbData_set_int32( packetB.buffer, valueB );


        //---------------------------------------------------------------
        // Send information packets into the one-component network
        //---------------------------------------------------------------
        fbOutput_write( &portA, packetA.handle );
        fbOutput_write( &portB, packetB.handle );


        //---------------------------------------------------------------
        // Run the network
        //---------------------------------------------------------------
        while( fbScheduler_mainLoop( &networkContext ) ) {
            // the network will run until component terminates
            // by reaching OP_TERMINATE opcode at which point
            // scheduler will terminate both input connections
            // and close all ports on them and then close
            // component's output port.
            // output connection with result in it will still
            // be alive and our portResult will still be open
        }

        REQUIRE( ! fbConnection_isTerminated( &allConnections[2] ) );
        REQUIRE( fbInputOutput_isOpen( &portResult ) );

        REQUIRE( fbConnection_isTerminated( &allConnections[0] ) );
        REQUIRE( fbInputOutput_isClosed( &portA ) );
        REQUIRE( fbConnection_isTerminated( &allConnections[1] ) );
        REQUIRE( fbInputOutput_isClosed( &portB ) );


        //---------------------------------------------------------------
        // Get the result
        //---------------------------------------------------------------
        outputPacket = fbInput_read( &portResult );
        // terminate connection which will also close portResult
        fbConnection_terminate( &allConnections[2] );
        REQUIRE( fbInputOutput_isClosed( &portResult ) );


        REQUIRE( outputPacket != FB_INVALID_PACKET_HANDLE );

        byte_t *outputData = fbInformationPacket_getDataBuffer( outputPacket );
        int32_t result = fbData_get_int32( outputData );

        REQUIRE( result == valueA + valueB );

        //---------------------------------------------------------------
        // Cleanup
        //---------------------------------------------------------------
        fbInformationPacket_drop( outputPacket );
        fbContext_free( context );

    }
}
