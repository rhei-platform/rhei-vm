#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes switch") {

    SECTION("switch") {

        byte_t code[] = {
            OP_SWITCH,
            0,
            3,
            // case #0
            OP_BPUSH,
            2,
            OP_JUMP,
            0,
            12,
            // case #1
            OP_BPUSH,
            3,
            OP_JUMP,
            0,
            7,
            // case #2
            OP_BPUSH,
            4,
            OP_JUMP,
            0,
            2,
            // default
            OP_BPUSH,
            42,
            OP_END,
        };

        byte_t constant_pool[] = {
            // filler, just 'cause
            13,
            31,
            42,
            // start of lookup table
            // default offset
            15,
            0,
            0,
            0,
            // # of pairs
            3,
            0,
            0,
            0,
            // pair #0
            10,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            // pair #1
            20,
            0,
            0,
            0,
            5,
            0,
            0,
            0,
            // pair #2
            30,
            0,
            0,
            0,
            10,
            0,
            0,
            0,
            // end of lookup table
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = &constant_pool[0],
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        FBContextStatus run_status;


        fbContext_reset( &context );

        // trigger default case
        fbContext_push( &context, 55 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );

        // trigger #0 case
        fbContext_push( &context, 10 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 2 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );

        // trigger #1 case
        fbContext_push( &context, 20 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 3 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );


        fbContext_reset( &context );

        // trigger #2 case
        fbContext_push( &context, 30 );
        run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 4 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }
}