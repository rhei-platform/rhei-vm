#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/lib/databuffer.h"

TEST_CASE("vm context") {

    SECTION("init") {

        FBContext context;

        fbContext_init_allocate( &context );

        REQUIRE( context.state == CONTEXT_INITIALIZED );
        REQUIRE( context.status == STATUS_OK );

        REQUIRE( context.code == NULL );
        REQUIRE( context.code == context.codeEnd );
        REQUIRE( context.ip == NULL );

        REQUIRE( context.componentData == NULL );
        REQUIRE( context.constantPool == NULL );
        REQUIRE( context.packets == NULL );
        REQUIRE( context.packetHandles == NULL );

        REQUIRE( context.stackTop == &context.stack[0] );

        REQUIRE( context.inputs == NULL );
        REQUIRE( context.inputsCount == 0 );
        REQUIRE( context.outputs == NULL );
        REQUIRE( context.outputsCount == 0 );


        const int numberOfPackets = 4;
        fbContext_init_allocate_i( &context, numberOfPackets );

        REQUIRE( context.state == CONTEXT_INITIALIZED );
        REQUIRE( context.status == STATUS_OK );

        REQUIRE( context.code == NULL );
        REQUIRE( context.code == context.codeEnd );
        REQUIRE( context.ip == NULL );

        REQUIRE( context.componentData == NULL );
        REQUIRE( context.constantPool == NULL );
        REQUIRE( context.packets != NULL );
        REQUIRE( context.packetHandles != NULL );
        for( int i = 0; i < numberOfPackets; i++ ) {
            REQUIRE( context.packetHandles[ i ] == FB_INVALID_PACKET_HANDLE );
            REQUIRE( context.packets[ i ] == NULL );
        }

        REQUIRE( context.stackTop == &context.stack[0] );

        REQUIRE( context.inputs == NULL );
        REQUIRE( context.inputsCount == 0 );
        REQUIRE( context.outputs == NULL );
        REQUIRE( context.outputsCount == 0 );


        const int numberOfInputs = 1;
        const int numberOfOutputs = 2;
        fbContext_init_allocate_iii( &context, numberOfPackets, numberOfInputs, numberOfOutputs );

        REQUIRE( context.state == CONTEXT_INITIALIZED );
        REQUIRE( context.status == STATUS_OK );

        REQUIRE( context.code == NULL );
        REQUIRE( context.code == context.codeEnd );
        REQUIRE( context.ip == NULL );

        REQUIRE( context.componentData == NULL );
        REQUIRE( context.constantPool == NULL );
        REQUIRE( context.packets != NULL );
        REQUIRE( context.packetHandles != NULL );
        for( int i = 0; i < numberOfPackets; i++ ) {
            REQUIRE( context.packetHandles[ i ] == FB_INVALID_PACKET_HANDLE );
            REQUIRE( context.packets[ i ] == NULL );
        }

        REQUIRE( context.stackTop == &context.stack[0] );

        REQUIRE( context.inputs != NULL );
        REQUIRE( context.inputsCount == numberOfInputs );
        for( int i = 0; i < numberOfInputs; i++ ) {
            REQUIRE( context.inputs[ i ].owner == &context );
            REQUIRE( fbInputOutput_isClosed( &context.inputs[ i ] ) );
        }

        REQUIRE( context.outputs != NULL );
        REQUIRE( context.outputsCount == numberOfOutputs );
        for( int i = 0; i < numberOfOutputs; i++ ) {
            REQUIRE( context.outputs[ i ].owner == &context );
            REQUIRE( fbInputOutput_isClosed( &context.outputs[ i ] ) );
        }


        fbContext_free( &context );

    }

    SECTION("init code") {

        FBByteBuffer code;
        fbByteBuffer_init( &code );
        const int codeSize = 3;
        for( int i = 0; i < codeSize; i++ ) fbByteBuffer_addValue( &code, 42 );

        FBContext context;

        fbContext_init_allocate( &context );
        fbContext_setCode( &context, code.buffer, code.count );

        REQUIRE( context.state == CONTEXT_INITIALIZED );

        REQUIRE( context.code == code.buffer );
        REQUIRE( context.codeEnd == (code.buffer + codeSize - 1) );
        REQUIRE( context.ip == context.code );


        fbContext_free( &context );

        fbByteBuffer_free( &code );
    }

    SECTION("init data") {

        FBContext context;

        fbContext_init_allocate( &context );
        fbContext_createData( &context, 8 );

        REQUIRE( context.state == CONTEXT_INITIALIZED );

        REQUIRE( context.componentData != NULL );

        fbContext_free( &context );
    }

    SECTION("init constant pool") {

        FBByteBuffer constantPool;
        fbByteBuffer_init_i( &constantPool, 8 );

        FBContext context;

        fbContext_init_allocate( &context );
        fbContext_setConstantPool( &context, constantPool.buffer );

        REQUIRE( context.state == CONTEXT_INITIALIZED );

        REQUIRE( context.constantPool == constantPool.buffer );


        fbContext_free( &context );

        fbByteBuffer_free( &constantPool );
    }

    SECTION("prepare") {

        FBByteBuffer code;
        fbByteBuffer_init( &code );
        const int codeSize = 3;
        for( int i = 0; i < codeSize; i++ ) fbByteBuffer_addValue( &code, 42 );

        FBContext context;

        fbContext_init_allocate( &context );
        fbContext_setCode( &context, code.buffer, code.count );

        REQUIRE( context.state == CONTEXT_INITIALIZED );


        fbContext_reset( &context );

        REQUIRE( context.code == code.buffer );
        REQUIRE( context.ip == context.code );
        REQUIRE( context.state == CONTEXT_RUNNING );
        REQUIRE( context.stackTop == &context.stack[0] );


        fbContext_free( &context );

        fbByteBuffer_free( &code );

    }

    SECTION("free") {

        FBByteBuffer buffer;
        fbByteBuffer_init_i( &buffer, 16 );
        fbByteBuffer_addValue( &buffer, 42 );
        fbByteBuffer_addValue( &buffer, 42 );

        FBContext context;

        const int packetCount = 8;
        fbContext_init_allocate_i( &context, packetCount );
        fbContext_setCode( &context, buffer.buffer, buffer.count );
        fbContext_createData( &context, 16 );
        fbContext_setConstantPool( &context, buffer.buffer );

        REQUIRE( context.state == CONTEXT_INITIALIZED );
        REQUIRE( context.status == STATUS_OK );

        REQUIRE( context.code != NULL );
        REQUIRE( context.code != context.codeEnd );
        REQUIRE( context.ip != NULL );

        REQUIRE( context.componentData != NULL );
        REQUIRE( context.constantPool != NULL );
        REQUIRE( context.packets != NULL );
        REQUIRE( context.packetHandles != NULL );

        REQUIRE( context.stackTop == &context.stack[0] );


        fbContext_free( &context );

        REQUIRE( context.state == CONTEXT_TERMINATED );
        REQUIRE( context.status == STATUS_NONE );

        REQUIRE( context.code == NULL );
        REQUIRE( context.code == context.codeEnd );
        REQUIRE( context.ip == NULL );

        REQUIRE( context.componentData == NULL );
        REQUIRE( context.constantPool == NULL );
        REQUIRE( context.packets == NULL );
        REQUIRE( context.packetHandles == NULL );

        REQUIRE( context.stackTop == NULL );

        fbByteBuffer_free( &buffer );

    }
}