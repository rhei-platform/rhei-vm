#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes jump") {

    SECTION("jump") {

        byte_t code[] = {
            OP_JUMP,
            0,
            5,
            OP_BPUSH,
            24,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP,
            255,
            250,
            OP_END,
        };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = NULL,
            .constantPool = NULL,
            .packets = NULL,
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };

        fbContext_reset( &context );
        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
        REQUIRE( 42 == fbContext_pop( &context ) );
        REQUIRE( IS_STACK_EMPTY( &context ) );
    }

    SECTION("jump if False") {

        byte_t code[] = {
            OP_JUMP_IF_FALSE,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_FALSE,
            255,
            250,
            OP_END,
        };

        SECTION("if True") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if False") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );
            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if <=") {

        byte_t code[] = {
            OP_JUMP_IF_LESS_EQUAL,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_LESS_EQUAL,
            255,
            250,
            OP_END,
        };

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );
            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );
            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, +1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if >=") {

        byte_t code[] = {
            OP_JUMP_IF_GREATER_EQUAL,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_GREATER_EQUAL,
            255,
            250,
            OP_END,
        };

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, +1 );
            fbContext_push( &context, +1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );
            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if <") {

        byte_t code[] = {
            OP_JUMP_IF_LESS,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_LESS,
            255,
            250,
            OP_END,
        };

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );
            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, +1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if >") {

        byte_t code[] = {
            OP_JUMP_IF_GREATER,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_GREATER,
            255,
            250,
            OP_END,
        };

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, +1 );
            fbContext_push( &context, +1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if ==") {

        byte_t code[] = {
            OP_JUMP_IF_EQUAL,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_EQUAL,
            255,
            250,
            OP_END,
        };

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, +1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );
            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if !=") {

        byte_t code[] = {
            OP_JUMP_IF_NOT_EQUAL,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_NOT_EQUAL,
            255,
            250,
            OP_END,
        };

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, +1 );
            fbContext_push( &context, +1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push( &context, -1 );
            fbContext_push( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }

    SECTION("jump if 0 long value") {

        byte_t code[] = {
            OP_JUMP_IF_ZERO_2,
            0,
            8,
            OP_BPUSH,
            24,
            OP_JUMP,
            0,
            2,
            OP_BPUSH,
            42,
            OP_END,
            OP_JUMP_IF_ZERO_2,
            255,
            250,
            OP_END,
        };

        SECTION("if >") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push64( &context, 1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if ==") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push64( &context, 0 );
            fbContext_push64( &context, 0 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 42 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }

        SECTION("if <") {

            FBContext context = {
                .ip = NULL,
                .state = CONTEXT_INITIALIZED,
                .status = STATUS_OK,

                .code = &code[0],
                .codeEnd = &code[0] + sizeof(code) - 1,

                .componentData = NULL,
                .constantPool = NULL,
                .packets = NULL,
                .packetHandles = NULL,

                .stack = {0},
                .stackTop = NULL,

                .contextRequestData = {0,{0}},

                .inputs = NULL,
                .outputs = NULL,

                .inputsCount = 0,
                .outputsCount = 0,

                .component = 0,
            };
            fbContext_reset( &context );

            fbContext_push64( &context, -1 );

            FBContextStatus run_status = fbContext_run( &context );

            REQUIRE( run_status == STATUS_CODE_END );
            REQUIRE( 24 == fbContext_pop( &context ) );
            REQUIRE( IS_STACK_EMPTY( &context ) );
        }
    }
}
