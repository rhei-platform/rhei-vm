
- [x] OP_NOP = 0,
- [x] OP_INVALID,

    // flow-control:
- [x] OP_END,
- [x] OP_TERMINATE,

- [x] OP_SWITCH, // find value from stack in lookup table whose offset is given as imm 16-bit value, and jump
- [x] OP_JUMP, // imm 16-bit jump offset from NEXT bytecode op

- [x] OP_JUMP_IF_FALSE, // boolean
- [x] OP_JUMP_IF_LESS_EQUAL, // <= 0
- [x] OP_JUMP_IF_GREATER_EQUAL, // >= 0
- [x] OP_JUMP_IF_LESS, // < 0
- [x] OP_JUMP_IF_GREATER, // > 0
- [x] OP_JUMP_IF_EQUAL, // == 0
- [x] OP_JUMP_IF_NOT_EQUAL, // != 0

    // version of OP_JUMP_IF_EQUAL for numbers
    // int, long, float & double all have 0 represented by 0x0
    // OP_JUMP_IF_ZERO is same as OP_JUMP_IF_EQUAL for 32-bit values
    // OP_JUMP_IF_ZERO, // int, float
    // 64-bit values
- [x] OP_JUMP_IF_ZERO_2, // long, double


    // constant values:
- [x] OP_BCONST_I8, // load const using imm byte offset
- [x] OP_BCONST_I16,
- [x] OP_BCONST_I32,
- [x] OP_BCONST_I64,
- [x] OP_SCONST_I8, // load const using imm short offset
- [x] OP_SCONST_I16,
- [x] OP_SCONST_I32,
- [x] OP_SCONST_I64,
- [x] OP_PCONST_I8, // load const using offset from stack
- [x] OP_PCONST_I16,
- [x] OP_PCONST_I32,
- [x] OP_PCONST_I64,

- [x] OP_CONST_0,
- [x] OP_CONST_1,
- [x] OP_CONST_M1,

- [x] OP_BPUSH, // push imm 8-bit signed value
- [x] OP_SPUSH, // push imm 16-bit signed value


    // stack manipulation:
- [x] OP_DUP,
- [x] OP_DUP_2,


    // information packet load/store:
- [x] OP_CLOAD_I8, // load value from component data buffer location using 32-bit offset on the stack
- [x] OP_CLOAD_I16,
- [x] OP_CLOAD_I32,
- [x] OP_CLOAD_I64,

- [x] OP_CLOAD_F,
- [x] OP_CLOAD_D,

- [x] OP_LOAD_I8, // load value from 8-bit data buffer location using 32-bit IP index and 32-bit offset on the stack
- [x] OP_LOAD_I16,
- [x] OP_LOAD_I32,
- [x] OP_LOAD_I64,

- [x] OP_LOAD_F,
- [x] OP_LOAD_D,

- [x] OP_BLOAD_I8, // load value from 8-bit data buffer location using 8-bit imm IP index and 32-bit offset on the stack
- [x] OP_BLOAD_I16,
- [x] OP_BLOAD_I32,
- [x] OP_BLOAD_I64,

- [x] OP_BLOAD_F,
- [x] OP_BLOAD_D,

- [x] OP_CSTORE_I8, // store value on the stack into component data buffer location using 32-bit offset on the stack
- [x] OP_CSTORE_I16,
- [x] OP_CSTORE_I32,
- [x] OP_CSTORE_I64,

- [x] OP_CSTORE_F,
- [x] OP_CSTORE_D,

- [x] OP_STORE_I8, // store value on the stack into 8-bit data buffer location using 32-bit IP index and 32-bit offset on the stack
- [x] OP_STORE_I16,
- [x] OP_STORE_I32,
- [x] OP_STORE_I64,

- [x] OP_STORE_F,
- [x] OP_STORE_D,

- [x] OP_BSTORE_I8, // store value on the stack into 8-bit data buffer location using 8-bit imm IP index and 32-bit offset on the stack
- [x] OP_BSTORE_I16,
- [x] OP_BSTORE_I32,
- [x] OP_BSTORE_I64,

- [x] OP_BSTORE_F,
- [x] OP_BSTORE_D,


    // arithmetic:
    // unary:
- [x] OP_NEG_I,
- [x] OP_NEG_L,
- [x] OP_NEG_F,
- [x] OP_NEG_D,

- [x] OP_NOT, // boolean

- [x] OP_BIT_INVERT_I,
- [x] OP_BIT_INVERT_L,

    // binary:
- [x] OP_MUL_I,
- [x] OP_MUL_L,
- [x] OP_MUL_F,
- [x] OP_MUL_D,

- [x] OP_DIV_I,
- [x] OP_DIV_L,
- [x] OP_DIV_F,
- [x] OP_DIV_D,

- [x] OP_MOD_I,
- [x] OP_MOD_L,

- [x] OP_ADD_I,
- [x] OP_ADD_L,
- [x] OP_ADD_F,
- [x] OP_ADD_D,

- [x] OP_SUB_I,
- [x] OP_SUB_L,
- [x] OP_SUB_F,
- [x] OP_SUB_D,

    // bitwise:
- [x] OP_SHIFT_LEFT_I,
- [x] OP_SHIFT_LEFT_L,
- [x] OP_SHIFT_RIGHT_I,
- [x] OP_SHIFT_RIGHT_L,
- [x] OP_BIT_AND_I,
- [x] OP_BIT_AND_L,
- [x] OP_BIT_XOR_I,
- [x] OP_BIT_XOR_L,
- [x] OP_BIT_OR_I,
- [x] OP_BIT_OR_L,

    // logic:
    // push 1 if A > B, 0 if A == B, -1 if A < B
- [x] OP_CMP_I,
- [x] OP_CMP_UI,
- [x] OP_CMP_L,
- [x] OP_CMP_UL,
- [x] OP_CMP_F,
- [x] OP_CMP_D,

- [x] OP_AND, // boolean
- [x] OP_OR, // boolean


    // system:
- [x] OP_BREQUEST_READ, // local IP index and INPUT port index
- [x] OP_REQUEST_READ, // local IP index and INPUT port index
- [x] OP_BREQUEST_WRITE, // local IP index and OUTPUT port index
- [x] OP_REQUEST_WRITE, // local IP index and OUTPUT port index
- [x] OP_CREATE_IP, // local IP index and packet size
- [x] OP_SCREATE_IP, // local IP index and packet size on stack
- [x] OP_DROP_IP, // local IP index

- [x] OP_YIELD,

- [x]  OP_IP_LENGTH, // local IP index
- [x]  OP_IP_COPY, // local IP index, local IP index
