#include "catch.hpp"
#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"
#include "vm/debug/vm_debug.h"

TEST_CASE("opcodes template") {

    SECTION("asd") {

        byte_t code[] = {
            OP_BPUSH,
            10,
            OP_BPUSH,
            2,
            OP_ADD_I,
            OP_END,
        };

        byte_t component_data[] = {
            0,
            42,
        };

        byte_t constant_pool[] = {
            0,
            42,
        };

        byte_t information_packet[] = {
            255,
            255,
        };

        byte_t* ips[1] = { &information_packet[0] };

        FBContext context = {
            .ip = NULL,
            .state = CONTEXT_INITIALIZED,
            .status = STATUS_OK,

            .code = &code[0],
            .codeEnd = &code[0] + sizeof(code) - 1,

            .componentData = &component_data[0],
            .constantPool = &constant_pool[0],
            .packets = &ips[0],
            .packetHandles = NULL,

            .stack = {0},
            .stackTop = NULL,

            .contextRequestData = {0,{0}},

            .inputs = NULL,
            .outputs = NULL,

            .inputsCount = 0,
            .outputsCount = 0,

            .component = 0,
        };
        fbContext_reset( &context );

        FBContextStatus run_status = fbContext_run( &context );

        REQUIRE( run_status == STATUS_CODE_END );
    }
}