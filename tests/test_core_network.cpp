#include "catch.hpp"
#include "vm/core/input_output.h"
#include "vm/interpreter/context.h"
#include "vm/core/information_packet.h"
#include "vm/core/network.h"

TEST_CASE("core network") {

    SECTION("create") {

        FBNetworkContext *networkContext;
        networkContext = fbNetwork_create( 1, 2, 3, 4, 5, 6 );

        REQUIRE( networkContext->allContexts != NULL );
        REQUIRE( networkContext->allContextsCount == 1 );

        REQUIRE( networkContext->allConnections != NULL );
        REQUIRE( networkContext->allConnectionsCount == 2 );

        REQUIRE( networkContext->networkInputs != NULL );
        REQUIRE( networkContext->networkInputsCount == 3 );

        REQUIRE( networkContext->networkOutputs != NULL );
        REQUIRE( networkContext->networkOutputsCount == 4 );

        REQUIRE( networkContext->nativeInputs != NULL );
        REQUIRE( networkContext->nativeInputsCount == 5 );

        REQUIRE( networkContext->nativeOutputs != NULL );
        REQUIRE( networkContext->nativeOutputsCount == 6 );

        fbNetwork_destroy( networkContext );
    }

    SECTION("clear") {

        FBNetworkContext networkContext;
        fbNetwork_clear( &networkContext );

        REQUIRE( networkContext.allContexts == NULL );
        REQUIRE( networkContext.allContextsCount == 0 );

        REQUIRE( networkContext.allConnections == NULL );
        REQUIRE( networkContext.allConnectionsCount == 0 );

        REQUIRE( networkContext.networkInputs == NULL );
        REQUIRE( networkContext.networkInputsCount == 0 );

        REQUIRE( networkContext.networkOutputs == NULL );
        REQUIRE( networkContext.networkOutputsCount == 0 );

        REQUIRE( networkContext.nativeInputs == NULL );
        REQUIRE( networkContext.nativeInputsCount == 0 );

        REQUIRE( networkContext.nativeOutputs == NULL );
        REQUIRE( networkContext.nativeOutputsCount == 0 );
    }

    SECTION("getters") {

        FBNetworkContext *networkContext;
        networkContext = fbNetwork_create( 2, 2, 2, 3, 2, 2 );

        FBInputOutput dummyIO;
        fbNetwork_setInput( networkContext, 0, &dummyIO );
        fbNetwork_setInput( networkContext, 1, &dummyIO );
        fbNetwork_setOutput( networkContext, 0, &dummyIO );
        fbNetwork_setOutput( networkContext, 1, &dummyIO );
        fbNetwork_setOutput( networkContext, 2, &dummyIO );

        REQUIRE( fbNetwork_getContext( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getContext( networkContext, 2 ) == NULL );

        REQUIRE( fbNetwork_getConnection( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getConnection( networkContext, 2 ) == NULL );

        REQUIRE( fbNetwork_getInputPtr( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getInputPtr( networkContext, 2 ) == NULL );

        REQUIRE( fbNetwork_getInput( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getInput( networkContext, 2 ) == NULL );

        REQUIRE( fbNetwork_getOutputPtr( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getOutputPtr( networkContext, 3 ) == NULL );

        REQUIRE( fbNetwork_getOutput( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getOutput( networkContext, 3 ) == NULL );

        REQUIRE( fbNetwork_getNativeInput( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getNativeInput( networkContext, 2 ) == NULL );

        REQUIRE( fbNetwork_getNativeOutput( networkContext, 1 ) != NULL );
        REQUIRE( fbNetwork_getNativeOutput( networkContext, 2 ) == NULL );

        fbNetwork_destroy( networkContext );
    }
}