# Assembler source files

Comments are lines starting with ';' or '#'

Source has **`sections`** each of which starts with a line containing:

- section name, followed by
- colon ':'

**Section names**:

- `name`
- `inputs`
- `outputs`
- `stack`
- `packets`
- `data`
- `constant_pool`
- `setup`
- `loop`

Each section has a **`section content`** that either:

- starts right after `:` on the same line and ends with line end, or
- starts on the next source line and is **indented** and ends either when a new section starts or at the end of file


**One-line sections**:

- `name`
- `inputs`
- `outputs`
- `stack`
- `packets`

**Multi-line sections**:

- `data`
- `constant_pool`
- `setup`
- `loop`

Example of a source file:

~~~
# sample component
; this is a comment too

name: a_sample_source
inputs: 2
outputs: 1
stack: 16
packets: 4
data:
    i32 a_value
constant_pool:
    i32 one_thing 55
    i32[2] an_array 101, 117
    f[2] 5.1, 3.14

    ps "a string"

setup:
    end

loop:
    end
~~~


# Component inputs and outputs

The `inputs` section defines number of input ports this component has.

The `outputs` section defines number of output ports this component has.

Instructions that work with ports reference them their index, starting with `0`.


# Component packets

The `packets` section defines number of packet handles this component will need for its work.

When a component is instantiated (a network node is created) VM will allocate an array of length equal to number specified in `packets` section. Each array element can contain one information packet handle. When VM instruction has information packet index as one of its operands, that index is index into this array: index `0` means `0`-th element of the array, index `1` means element `1` etc.


# Component structure

Once compiled, a component has three main parts:

- **component `local static storage`**
- **`setup`** code
- **`loop`** code

Component's *local static storage* is pre-allocated information packet (IP) that is assigned to a component instance when it is first created. Each component instance (a.k.a. "a network node") has its own local static storage IP. This IP has two parts:

- read-write part: described by **`data`** section
- read-only part: described by **`constant_pool`** section

You can think of *`data`* section as an equivalent of a part of memory that holds local variables for a *function* or a *method* in programming languages that have a *function* and/or a *method* constructs.

*`constant_pool`* section contains all literal values that appear in the component's source: numbers and strings.


# Instruction set

## NOP

**Operation**

Do nothing

**Format**

> `NOP`

**Operand stack**

No change

**Description**

Do nothing.


## INVALID

**Operation**

Invalid operation

**Format**

> `INVALID`

**Operand stack**

No change

**Description**

Stops VM execution and places it in error state.

## END

**Operation**

End of code

**Format**

> `END`

**Operand stack**

No change

**Description**

Marks the end of bytecode sequence in memory.

If VM fetches this opcode as a next operation to execute, there are two cases:

- `setup` is current execution code: VM should set `loop` as the current execution code and reset the instruction pointer to start of `loop` bytecode.
- `loop` is current execution code: VM should reset instruction pointer to start of `loop` bytecode.



## TERMINATE

**Operation**

Terminate execution

**Format**

> `TERMINATE`

**Operand stack**

No change

**Description**

Execution of this node's code stops and the node status is set to `terminated`. All ports are closed.

Depending on the network structure, this operation may cause a part of all of the network to terminate in cascade.


## SWITCH

**Operation**

Access jump table by key and jump

**Format**

> `SWITCH`
> `highbyte`
> `lowbyte`

**Operand stack**

..., *key*
...,

**Description**

A *switch* instruction works by fetching a *key* value from stack top and then finding a matching entry in a lookup table stored in *`constant_pool`* section. Constant pool section byte offset where the lookup table starts is given with unsigned 16-bit immediate operand which is constructed as *`(highyte << 8) | (lowbyte)`*.

Lookup table contains an array of key-value pairs each of which is a signed 32-bit value. The table starts with special **`default entry`** which has *key* set to a value of jump offset and *value* set to number of entries in the table that follow after the default entry.

After the *default entry*, table contains zero or more **`lookup entries`** each of which hase *key* set to a value that will be matched agains the stack operand, and *value* set to jump offset that will be used if the *key* matches.

If *key* is found which matches the value fetched from the stack, VM will treat *value* from the entry as a signed 32-bit integer jump offset and will add it to current value of the instruction pointer, effectively executing a **`jump`** instruction.

If there is no *key* in the table that matches the operand from stack, VM will use *key* from the *default entry* as signed 32-bit jump offset.


## JUMP

**Operation**

Continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP`
> `highbyte`
> `lowbyte`

**Operand stack**

No change

**Description**

A *jump* instruction adds signed 16-bit immediate operand to the current value of instruction pointer, effectively transferring exectuion to a new location in bytecode. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.


## JUMP_IF_FALSE

**Operation**

If value at stack top is **`0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_FALSE`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_false* is synonym for *jump_if_equal*.


## JUMP_IF_LESS_EQUAL

**Operation**

If value at stack top is **`less or equal to 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_LESS_EQUAL`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_less_equal* will fetch signed 32-bit value from top of the stack. If the stack operand is negative or equal to `0`, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.


## JUMP_IF_GREATER_EQUAL

**Operation**

If value at stack top is **`greater or equal to 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_GREATER_EQUAL`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_greater_equal* will fetch signed 32-bit value from top of the stack. If the stack operand is positive or equal to `0`, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.


## JUMP_IF_LESS

**Operation**

If value at stack top is **`less than 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_LESS`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_less* will fetch signed 32-bit value from top of the stack. If the stack operand is negative, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.


## JUMP_IF_GREATER

**Operation**

If value at stack top is **`greater than 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_GREATER`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_greater* will fetch signed 32-bit value from top of the stack. If the stack operand is positive, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.


## JUMP_IF_EQUAL

**Operation**

If 32-bit value at stack top is **`equal to 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_EQUAL`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_equal* will fetch signed 32-bit value from top of the stack. If the stack operand is equal to `0`, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.

This instruction works for `int` and `float` numeric types since both 32-bit integer and single precision floating point formats have a `0` value represented with `0x00000000`.



## JUMP_IF_NOT_EQUAL

**Operation**

If value at stack top is **`not equal to 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `JUMP_IF_NOT_EQUAL`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_not_equal* will fetch signed 32-bit value from top of the stack. If the stack operand is not equal to `0`, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.


## OP_JUMP_IF_ZERO_2

**Operation**

If 64-bit value at stack top is **`equal to 0`**, continue execution from the bytecode at a given offset from current position

**Format**

> `OP_JUMP_IF_ZERO_2`
> `highbyte`
> `lowbyte`

**Operand stack**

..., value
...,

**Description**

A *jump_if_not_zero_2* will fetch signed 64-bit value from top of the stack. If the stack operand is equal to `0`, a *jump* is executed using immediate operand as jump offset value. The signed 16-bit immediate operand is constructed as *`(highyte << 8) | (lowbyte)`*.

This instruction works for `long` and `double` numeric types since both 64-bit integer and double precision floating point formats have a `0` value represented with `0x0000000000000000`.


## CONST_I

**Operation**

Get a value from constant pool at given offset and push it onto stack

**Format**

> `BCONST_In`
> `lowbyte`

> `SCONST_In`
> `highbyte`
> `lowbyte`

**Operand stack**

...,
..., value

**Description**

*`const_in`* operation fetches a 8-, 16-, 32- or 64-bit value from constant pool at the given offset, and then pushes the value to the stack.

A *`const_in`* operation has 8 variants grouped into 2 main groups:

- *`bconst_in`* which takes unsigned 8-bit immediate operand
- *`sconst_in`* which takes unsigned 16-bit immediate operand

Each group contains 4 variants, one for 8-bit, 16-bit, 32-bit and 64-bit constant pool values:

- *`bconst_i8`*
- *`bconst_i16`*
- *`bconst_i32`*
- *`bconst_i64`*

and

- *`sconst_i8`*
- *`sconst_i16`*
- *`sconst_i32`*
- *`sconst_i64`*

*`bconst_in`* variant will construct signed 8-bit immediate operand as *`(lowbyte)`*, and *`sconst_in`* variant will construct signed 16-bit immediate operand as *`(highyte << 8) | (lowbyte)`*.


## PCONST_I

**Operation**

Get a value from constant pool at given offset and push it onto stack

**Format**

> `PCONST_In`

**Operand stack**

..., offset
..., value

**Description**

*`pconst_in`* operation fetches a 8-, 16-, 32- or 64-bit value from constant pool at the given offset, and then pushes the value to the stack.

A *`pconst_in`* operation has 4 variants:

- *`pconst_i8`*
- *`pconst_i16`*
- *`pconst_i32`*
- *`pconst_i64`*

VM will first fetch the offset into constant pool from top of the stack as unsigned 32-bit value. It will then load an 8-, 16-, 32- or 64-bit value, depending on the operation variant, from constant pool at the given offset, and push the loaded value onto stack.


## CONST_x

**Operation**

Push a pre-defined constant value to stack

**Format**

> `CONST_x`

**Operand stack**

...,
..., value

**Description**

*`const_x`* operation pushes pre-defined constant value onto stack.

There are 3 variants of this operation:

- *`const_0`*, push `0` onto stack
- *`const_1`*, push `1` onto stack
- *`const_m1`*, push `-1` onto stack


## PUSH

**Operation**

Push immediate operand value to stack

**Format**

> `BPUSH`
> `lowbyte`

> `SPUSH`
> `highbyte`
> `lowbyte`

**Operand stack**

...,
..., value

**Description**

*`push`* operation has two variants:

- *`bpush`*, push immediate 8-bit operand to stack
- *`spush`*, push immediate 16-bit operand to stack

*`bpush`* variant will construct signed 8-bit immediate operand as *`(lowbyte)`*, and *`spush`* variant will construct signed 16-bit immediate operand as *`(highyte << 8) | (lowbyte)`*.


## DUP

**Operation**

Push immediate operand value to stack

**Format**

> `DUP`

> `DUP_2`

**Operand stack**

..., value
..., value, value

**Description**

*`dup`* operation has two variants:

- *`dup`*, duplicate 32-bit value on stack top
- *`dup_2`*, duplicate 64-bit value on stack top

Both *`dup`* variants will pop a value off the stack and then push it back twice.


## CLOAD_I

**Operation**

Get a value from **component `local static storage`** **"`data`"** section and push it onto stack

**Format**

> `CLOAD_In`

**Operand stack**

..., offset
..., value

**Description**

A *`cload_in`* operation has 4 variants:

- *`cload_i8`*
- *`cload_i16`*
- *`cload_i32`*
- *`cload_i64`*

VM will first fetch the offset into component's **`data`** section from top of the stack as unsigned 32-bit value. It will then load an 8-, 16-, 32- or 64-bit value, depending on the operation variant, from the component's **`data`** section at the given offset, and push the loaded value onto stack.


## LOAD_I

**Operation**

Get a value from specified information packet and push it onto stack

**Format**

> `LOAD_In`

**Operand stack**

..., offset, index
..., value

**Description**

A *`load_in`* operation has 4 variants:

- *`load_i8`*
- *`load_i16`*
- *`load_i32`*
- *`load_i64`*

VM will first fetch the information packet index from top of the stack, and then it will fetch the offset into the informatioin packet from top of the stack, both as unsigned 32-bit values. It will then load an 8-, 16-, 32- or 64-bit value, depending on the operation variant, from the information packet's data buffer at the given offset, and push the loaded value onto stack.


## BLOAD_I

**Operation**

Get a value from specified information packet and push it onto stack

**Format**

> `BLOAD_In`
> `lowbyte`

**Operand stack**

..., offset
..., value

**Description**

A *`bload_in`* operation has 4 variants:

- *`bload_i8`*
- *`bload_i16`*
- *`bload_i32`*
- *`bload_i64`*

VM will first fetch the information packet index from unsigned 8-bit immediate operand. The operand is constructed as `(lowbyte)`. After that, it will fetch the offset into the informatioin packet from top of the stack as unsigned 32-bit value. It will then load an 8-, 16-, 32- or 64-bit value, depending on the operation variant, from the information packet's data buffer at the given offset, and push the loaded value onto stack.


## CSTORE_I

**Operation**

Pop a value from stack and store it in **component `local static storage`** **"`data`"** section at given offset

**Format**

> `CSTORE_In`

**Operand stack**

..., value, offset
..., 

**Description**

A *`cstore_in`* operation has 4 variants:

- *`cstore_i8`*
- *`cstore_i16`*
- *`cstore_i32`*
- *`cstore_i64`*

VM will first fetch the offset into component's **`data`** section from top of the stack as unsigned 32-bit value. Next, it will fetch a value to be stored from top of the stack. It will then store an 8-, 16-, 32- or 64-bit value loaded from the stack, depending on the operation variant, into the component's **`data`** section at the given offset.


## STORE_I

**Operation**

Store a value from stack into specified information packet at the given offset

**Format**

> `STORE_In`

**Operand stack**

..., value, offset, index
..., 

**Description**

A *`store_in`* operation has 4 variants:

- *`store_i8`*
- *`store_i16`*
- *`store_i32`*
- *`store_i64`*

VM will first fetch the information packet index from top of the stack, and then it will fetch the offset into the informatioin packet from top of the stack, both as unsigned 32-bit values. It will then fetch 32-bit value from top of the stack for the 8-, 16- and 32-bit variants, and 64-bit value for the 64-bit variant.

Once information packet index, offset and value are fetched, the 8-, 16-, 32- or 64-bit value will be stored in the data buffer of the information packet identified by the index, at the given offset.


## BSTORE_I

**Operation**

Store a value from stack into specified information packet at the given offset

**Format**

> `BSTORE_In`
> `lowbyte`

**Operand stack**

..., value, offset
..., 

**Description**

A *`bstore_in`* operation has 4 variants:

- *`bstore_i8`*
- *`bstore_i16`*
- *`bstore_i32`*
- *`bstore_i64`*

VM will first fetch the information packet index from unsigned 8-bit immediate operand. The operand is constructed as `(lowbyte)`. After that it will fetch the offset into the informatioin packet from top of the stack, as unsigned 32-bit value. It will then fetch 32-bit value from top of the stack for the 8-, 16- and 32-bit variants, and 64-bit value for the 64-bit variant.

Once information packet index, offset and value are fetched, the 8-, 16-, 32- or 64-bit value will be stored in the data buffer of the information packet identified by the index, at the given offset.


## NEG

**Operation**

Perform arithmetical negation on the top stack value

**Format**

> `NEG_x`

**Operand stack**

..., value
..., result

**Description**

A *`neg_x`* operation has 4 variants:

- *`neg_i`*, works with 32-bit integer value
- *`neg_l`*, works with 64-bit integer value
- *`neg_f`*, works with 32-bit precision floating value
- *`neg_d`*, works with 64-bit precision floating value

VM will fetch 32-bit value for `_I` and `_F` variants, and 64-bit value for `_L` and `_D` variants from top of the stack, multiply the value with `-1` and then push the result back onto stack.


## NOT

**Operation**

Perform logical negation on the top stack value

**Format**

> `NOT`

**Operand stack**

..., value
..., result

**Description**

VM will fetch 32-bit value from top of the stack, and then push `0` onto stack if the value is `not equal to 0`, or push `1` onto stack if the value is `equal to 0`.


## BIT_INVERT

**Operation**

Perform bit inversion on the top stack value

**Format**

> `BIT_INVERT_x`

**Operand stack**

..., value
..., result

**Description**

A *`bit_invert_x`* operation has 2 variants:

- *`bit_invert_i`*, works with 32-bit integer value
- *`bit_invert_l`*, works with 64-bit integer value

VM will fetch 32-bit value for `_I` variant, and 64-bit value for `_L` variant from top of the stack, perform binary negation on the value and then push the result back onto stack.


## MUL

**Operation**

Multiply two top stack values and push result back on stack

**Format**

> `MUL_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`mul_x`* operation has 4 variants:

- *`mul_i`*, works with 32-bit integer value
- *`mul_l`*, works with 64-bit integer value
- *`mul_f`*, works with 32-bit precision floating value
- *`mul_d`*, works with 64-bit precision floating value

VM will fetch 32-bit values for `_I` and `_F` variants, and 64-bit values for `_L` and `_D` variants from top of the stack, multiply left_op with right_op and then push the result back onto stack.


## DIV

**Operation**

Divide two top stack values and push result back on stack

**Format**

> `DIV_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`div_x`* operation has 4 variants:

- *`div_i`*, works with 32-bit integer value
- *`div_l`*, works with 64-bit integer value
- *`div_f`*, works with 32-bit precision floating value
- *`div_d`*, works with 64-bit precision floating value

VM will fetch 32-bit values for `_I` and `_F` variants, and 64-bit values for `_L` and `_D` variants from top of the stack, divide left_op with right_op and then push the result back onto stack.


## MOD

**Operation**

Calculate *division reminder* on operand from stack and place result back on stack

**Format**

> `MOD_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`mod_x`* operation has 2 variants:

- *`mod_i`*, works with 32-bit integer value
- *`mod_l`*, works with 64-bit integer value

VM will fetch 32-bit value for `_I` variant, and 64-bit value for `_L` variant from top of the stack, divide left_op with right_op and then push the division reminder back onto stack.


## ADD

**Operation**

Add two top stack values and push result back on stack

**Format**

> `ADD_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`add_x`* operation has 4 variants:

- *`add_i`*, works with 32-bit integer value
- *`add_l`*, works with 64-bit integer value
- *`add_f`*, works with 32-bit precision floating value
- *`add_d`*, works with 64-bit precision floating value

VM will fetch 32-bit values for `_I` and `_F` variants, and 64-bit values for `_L` and `_D` variants from top of the stack, adds right_op to left_op and then push the result back onto stack.


## SUB

**Operation**

Subtract two top stack values and push result back on stack

**Format**

> `SUB_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`sub_x`* operation has 4 variants:

- *`sub_i`*, works with 32-bit integer value
- *`sub_l`*, works with 64-bit integer value
- *`sub_f`*, works with 32-bit precision floating value
- *`sub_d`*, works with 64-bit precision floating value

VM will fetch 32-bit values for `_I` and `_F` variants, and 64-bit values for `_L` and `_D` variants from top of the stack, subtracts right_op from left_op and then push the result back onto stack.


## SHIFT_LEFT

**Operation**

Perform bitwise *shift left* on operand from stack and place result back on stack

**Format**

> `SHIFT_LEFT_x`

**Operand stack**

..., value, bits
..., result

**Description**

A *`shift_left_x`* operation has 2 variants:

- *`shift_left_i`*, works with 32-bit integer value
- *`shift_left_l`*, works with 64-bit integer value

VM will first fetch `bits` value from the stack top and then it will fetch 32-bit value for `_I` variant, and 64-bit value for `_L` variant from top of the stack. It will then shift `value` left for `bits` places and push the result onto the stack.


## SHIFT_RIGHT

**Operation**

Perform bitwise *shift right* on operand from stack and place result back on stack

**Format**

> `SHIFT_RIGHT_x`

**Operand stack**

..., value, bits
..., result

**Description**

A *`shift_right_x`* operation has 2 variants:

- *`shift_right_i`*, works with 32-bit integer value
- *`shift_right_l`*, works with 64-bit integer value

VM will first fetch `bits` value from the stack top and then it will fetch 32-bit value for `_I` variant, and 64-bit value for `_L` variant from top of the stack. It will then shift `value` right for `bits` places and push the result onto the stack.


## BIT_AND

**Operation**

Perform bitwise *and* on operands from stack and place result back on stack

**Format**

> `BIT_AND_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`bit_and_x`* operation has 2 variants:

- *`bit_and_i`*, works with 32-bit integer value
- *`bit_and_l`*, works with 64-bit integer value

VM will fetch 32-bit values for `_I` variant, and 64-bit values for `_L` variant from top of the stack, perform bitwise *and* operation between left_op and right_op and then push the result back onto stack.


## BIT_XOR

**Operation**

Perform bitwise *exclusive or* on operands from stack and place result back on stack

**Format**

> `BIT_XOR_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`bit_xor_x`* operation has 2 variants:

- *`bit_xor_i`*, works with 32-bit integer value
- *`bit_xor_l`*, works with 64-bit integer value

VM will fetch 32-bit values for `_I` variant, and 64-bit values for `_L` variant from top of the stack, perform bitwise *exclusive or* operation between left_op and right_op and then push the result back onto stack.


## BIT_OR

**Operation**

Perform bitwise *or* on operands from stack and place result back on stack

**Format**

> `BIT_OR_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`bit_or_x`* operation has 2 variants:

- *`bit_or_i`*, works with 32-bit integer value
- *`bit_or_l`*, works with 64-bit integer value

VM will fetch 32-bit values for `_I` variant, and 64-bit values for `_L` variant from top of the stack, perform bitwise *or* operation between left_op and right_op and then push the result back onto stack.


## CMP

**Operation**

Compare two values from stack and place result back on stack

**Format**

> `CMP_x`

**Operand stack**

..., left_op, right_op
..., result

**Description**

A *`cmp_x`* operation has 6 variants:

- *`cmp_i`*, works with 32-bit signed integer value
- *`cmp_ui`*, works with 32-bit unsigned integer value
- *`cmp_l`*, works with 64-bit signed integer value
- *`cmp_ul`*, works with 64-bit unsigned integer value
- *`cmp_f`*, works with 32-bit floating value
- *`cmp_d`*, works with 64-bit floating value

VM will fetch left_op and right_op from the stack, and compare two values. It will then:

- push `1` onto stack if `left_op > right_op`
- push `0` onto stack if `left_op == right_op`
- push `-1` onto stack if `left_op < right_op`


## AND

**Operation**

Perform logical *and* on operands from stack and place result back on stack

**Format**

> `AND`

**Operand stack**

..., left_op, right_op
..., result

**Description**

VM will fetch left_op and right_op from the stack, and perform logical *and* operation, placing the result back onto stack. For logical `true` it will place `1` onto stack and for `false` it will be `0`. Operands are considered to be `false` if their value is equal to `0` and `true` otherwise. This logic matches workings of `C` language `&&` operator.


## OR

**Operation**

Perform logical *or* on operands from stack and place result back on stack

**Format**

> `OR`

**Operand stack**

..., left_op, right_op
..., result

**Description**

VM will fetch left_op and right_op from the stack, and perform logical *or* operation, placing the result back onto stack. For logical `true` it will place `1` onto stack and for `false` it will be `0`. Operands are considered to be `false` if their value is equal to `0` and `true` otherwise. This logic matches workings of `C` language `||` operator.


## REQUEST_READ

**Operation**

Read information packet from input port

**Format**

> `REQUEST_READ`

**Operand stack**

..., port_index, ip_index
..., 

**Description**

TBD


## BREQUEST_READ

**Operation**

Read information packet from input port

**Format**

> `BREQUEST_READ`
> `ip_index`
> `port_index`

**Operand stack**

No changes

**Description**

TBD


## REQUEST_WRITE

**Operation**

Write information packet to output port

**Format**

> `REQUEST_WRITE`

**Operand stack**

..., port_index, ip_index
..., 

**Description**

TBD


## BREQUEST_WRITE

**Operation**

Write information packet to output port

**Format**

> `BREQUEST_WRITE`
> `ip_index`
> `port_index`

**Operand stack**

No changes

**Description**

TBD


## SCREATE_IP

**Operation**

Create information packet

**Format**

> `SCREATE_IP`
> `ip_index`

**Operand stack**

..., ip_size
..., 

**Description**

TBD


## CREATE_IP

**Operation**

Create information packet

**Format**

> `CREATE_IP`
> `ip_index`
> `ip_size`

**Operand stack**

No change

**Description**

TBD


## DROP_IP

**Operation**

Drop information packet

**Format**

> `DROP_IP`
> `ip_index`

**Operand stack**

No change

**Description**

TBD


## YIELD

**Operation**

Yield execution to some other network node

**Format**

> `YIELD`

**Operand stack**

No change

**Description**

TBD


## LENGTH

**Operation**

Fetch the length of specified IP and push it to stack

**Format**

> `IP_LENGTH`
> `ip_index`

**Operand stack**

..., 
..., ip_size

**Description**

VM will get data buffer size of the IP at index given by `ip_index` 8-bit value. The size will then be pushed onto stack.

If IP handle at given index is invalid, VM will stop execution of the context andcontext status to error.



## COPY

**Operation**

Copy a number of bytes from source IP's data buffer to destination IP's data buffer

**Format**

> `IP_COPY`
> `ip_dst_index`
> `ip_src_index`

**Operand stack**

..., dst_offset, src_offset, length
..., 

**Description**

This operation copies specified number of bytes from source IP data buffer starting at the given source offset, to destination IP buffer starting at the given destination offset.

Destination and source IP indexes are fetched as two 8-bit immediate operands `ip_dst_index` and `ip_src_index` respectively. Number of bytes to be copied is then popped off of the stack, followed by source data buffer starting offset and then destination data buffer start offset, in that order.

VM then proceeds to copy data from source to destination, taking into account not to overflow buffers.

