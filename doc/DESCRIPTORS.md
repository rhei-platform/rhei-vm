This is **optional** VM feature.

Static fixed-sized record descriptors are always available and free since they translate directly into constant offsets and lengths. Other types of IP descriptors **will** incur performance penalty, for which price they bring built-in support for byte buffer parsing and developer-friendly data access.


# Thoughts

compiler/assembler when it parses record descriptor will determine if it is fixed-sized or variable-sized kind of descriptor.

Elements are **always** arrays: single element is array of 1.

Combinations of element size / array lengths:


- fixed / fixed
offset, count, element_size


- fixed / variable
same as above, element count is decided at runtime by parser


- variable / fixed
offset, count, index_of_table_of_all_entries

@ index_of_table_of_all_entries
... next `count` entries
offset, count, element_size
OR
offset, count, index_of_table_of_all_entries for this element

if an element is variable length array, entries for this array are apended to the end of resolved offset table at runtime. This way indexes of all element names remain fixed.

Example:

    u8 names.length
    cs[] names
    cs address
    u32 zip_code

translates to (if data contains value 0x02 at 'names.length' position):

    0:  0,                      1,  1   ; names.length == 2
    1:  1,                      2,  4   ; cs[2], element offsets start at idx = 4
    2:  37 = 13 + 24,           1,  47  ; address, length 47
    3:  85 = 47 + 37 + 1,       1,  4   ; zip_code
    4:  1,                      1,  12  ; names[ 0 ], element_size (12) includes '\0'
    5:  13,                     1,  24  ; names[ 1 ], element_size (24) includes '\0'



- variable / variable
same as above, element count is decided at runtime by parser


Defining variable length array's length at runtime:

- by providing the length in an element declared before the array start, i.e.

    u16 names.length
    cs[] names

This element's name has to have a form of

    array_name '.length'

and *has* to be declared somewhere *before* the array itself.

- by using dynamic descriptors with properly crafted grammar



Compiler maps symbolic element names to proper descriptor table entry indexes at compile time.
A symbolic name always resovles to index of an entry

    offset, count, element size

or

    offset, count, MAX_UINT

Descriptor resolver will then resolve offset and length at runtime based on field number/index and element index.


Descriptor definition is parsed from `record` into table containing description of fields:

field_type, array_length, field_name

- field_type: BASE_TYPE, RECORD_TYPE
- array_length: 1 for non-arrays, > 1 for arrays
- field_name: null-terminated string with parsed field name



# IP descriptors

These work on IP buffer contents. There are two main types of descriptors:

- raw
- structured
- and custom ?? (as in custom parsing functions that produce offset/lengt tables???)

Descriptors are applied on an IP data buffer and as a result, descriptor function produces a table with offset/length pairs, i.e.:

field# | offset | length
-------------------------
0       |   0   |  4
1       |   4   | 12
2       |  16   |  4
....

This table is used by VM ops:

- `field_offset #N, #F`
- `field_length #N, #F`

where `#N` is descriptor number and `#F` is field number. These ops push a field offset and length on stack , respectively. VM op:

- `field_count #N`

pushes to stack a field count for the given descriptor.

All this happens transparently "behind the scenes" and is taken care of by compiler, i.e. in source code you reference fields by name and compiler takes care of translating that to field number and emitting appropriate `field_offset` and `field_legth` ops.

Descriptors are applied on IP data buffers by executing VM op:

- `descriptor #N, #IP`

where `#N` is descriptor number and `#IP` is IP id. Upon completion, this operation generates the descriptor table for this specific IP. This is usually done once an IP is received on a port, i.e code:

    read 0, 1
    descriptor 4, 1

reads an IP from port #0 into IP slot #1, and then applies descriptor #4 to data buffer of that same IP #1.


## Raw descriptors

This is just a fancy way of saying that there is no descriptor. If you designate an IP with `raw` descriptor, this just means you will be:

- access underlying IP data buffer as `byte buffer`
- providing `offset` value yourself for starting point where you want to access (load/store) the buffer content (bytes)

## Structured descriptors

There are two main categories of `structured` descriptors:

- static
- dynamic

**Static** descriptors have one sub-type called `fixed` descriptors. **Fixed** descriptors are such that all their field's offsets and sizes are known at compile time. In this case, code will be generated to contain literal values of field offsets and sizes and will have no overhead of calling descriptor parsers at run time.

### Static descriptors

Static descriptors declare types and positions of all of the IP fileds upfront. Descriptor which contains no declaration of fileds of `string` type is known as `fixed` static descriptor type.

Example of a static descriptor:

    record: SampleRecord
        cs  name
        u8  age
        cs  address
        u32 zip_code

The same descriptor changed to be recognized as a `fixed` sub-type:

    record: SampleRecord
        u8[60]  name
        u8      age
        u8[120] address
        u32     zip_code

What makes a `static` descriptor non-`fixed` are additions of string type fields:

- `cs`: C-style string, NULL-terminated char array
- `ps`: Pascal-style string, 16-bit string length followed by char array of that size
- `utf8s`: UTF-8 string, 16-bit byte count followed by that many bytes of the string content. Basically the same as the `ps` type

Fields can be declared as fixed-length arrays, like in the examples above, or they can be declared as one of two types of variable-length arrays:

- array of strings
- array of types

**Array of strings** is declared like so:

    record: SampleRecord
    cs[]  name

This will instruct descriptor parser to 

## Dynamic descriptors

While static descriptore are created from record-structure-like declarations, dynamic descriptors are created from parsing grammars and can be compared somewhat to regular expressions. The parsing grammar used to declare dynamic descriptors is almost identical to the **parsing expression grammar**, or **PEG**, with few semantic differences which are related to the context (Rhei VM) in which the grammar syntax is used.

Static descriptors have fairly simple syntax and produce straight-forward and predictable results. Dynamic descriptors, on the other hand, have a potential to produce unwanted results. This is due to the flexibility they offer in defining an IP data buffer structure. This flexibility is at the same time a strength and a weakness of dynamic descriptors.

For example, this simple declaration is meant to parse a content of a data buffer as an array of lines each ending in new-line (LF) ASCII character:

    r <- ((![\n] .)* [\n])* !.

The problem with this declaration is that, while it will successfully match syntactically correct data buffer (i.e. a text file containing a bunch of lines), it will produce *an empty* offset/length table!

In order to get at least one entry in the generated table, descriptor should be declared, for example, like this:

    r <- line* !.
    line <- (![\n] .)* [\n]

