/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "contrib/ipbufferaccess.h"
#include "vm/lib/memory.h"


void fbIPBufferAccess_init(FBIPBufferAcces *buff, byte_t *data, size_t data_size) {

    buff->buffer = data;
    buff->position = data;
    buff->end = data + data_size;
    buff->status = BUFFER_ACCESS_OK;
}

void fbIPBufferAccess_position(FBIPBufferAcces *buff, size_t new_position) {

    if( buff->buffer + new_position > buff->end ) {
        buff->status = BUFFER_ACCESS_ERROR;
        return;
    }

    buff->position = buff->buffer + new_position;
}

void fbIPBufferAccess_advance(FBIPBufferAcces *buff, size_t count) {

    if( buff->position + count > buff->end ) {
        buff->status = BUFFER_ACCESS_ERROR;
        return;
    }

    buff->position += count;
}

void fbIPBufferAccess_retreat(FBIPBufferAcces *buff, size_t count) {

    if( buff->position - count < buff->buffer ) {
        buff->status = BUFFER_ACCESS_ERROR;
        return;
    }

    buff->position -= count;
}


///////////////////////////////////////////////////////////////////////////////
//
// Range functions
//
///////////////////////////////////////////////////////////////////////////////


void fbIPBufferAccess_fillRange(FBIPBufferAcces *buff, size_t start_index, byte_t *value, size_t value_size, size_t count) {

    if( buff->buffer + start_index > buff->end ) {
        buff->status = BUFFER_ACCESS_ERROR;
        return;
    }

    if( buff->buffer + start_index + (count * value_size) > buff->end ) {

        size_t max_available_space = fbIPBufferAccess_length( buff ) - start_index;
        count = max_available_space / value_size;

        if( count == 0 ) {
            return;
        }
    }

    if( value_size == 1 ) {
        FB_SET( buff->position + start_index, *value, count );
        return;
    }

    byte_t *dst = buff->buffer + start_index;

    while( count > 0 ) {

        FB_COPY_ARRAY( dst, value, byte_t, value_size );
        dst += value_size;
        count--;
    }

}

void fbIPBufferAccess_copyRange(FBIPBufferAcces *dst_buff, FBIPBufferAcces *src_buff, size_t start_index, size_t count) {

}

///////////////////////////////////////////////////////////////////////////////
//
// Readers
//
///////////////////////////////////////////////////////////////////////////////

char fbIPBufferAccess_read_char(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    char value = fbData_get_char( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(char) );
    return value;
}

int8_t fbIPBufferAccess_read_int8(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    int8_t value = fbData_get_int8( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(int8_t) );
    return value;
}
int16_t fbIPBufferAccess_read_int16(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    int16_t value = fbData_get_int16( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(int16_t) );
    return value;
}
int32_t fbIPBufferAccess_read_int32(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    int32_t value = fbData_get_int32( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(int32_t) );
    return value;
}
int64_t fbIPBufferAccess_read_int64(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    int64_t value = fbData_get_int64( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(int64_t) );
    return value;
}

uint8_t fbIPBufferAccess_read_uint8(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    uint8_t value = fbData_get_uint8( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(uint8_t) );
    return value;
}
uint16_t fbIPBufferAccess_read_uint16(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    uint16_t value = fbData_get_uint16( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(uint16_t) );
    return value;
}
uint32_t fbIPBufferAccess_read_uint32(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    uint32_t value = fbData_get_uint32( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(uint32_t) );
    return value;
}
uint64_t fbIPBufferAccess_read_uint64(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    uint64_t value = fbData_get_uint64( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(uint64_t) );
    return value;
}

float fbIPBufferAccess_read_float(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    float value = fbData_get_float( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(float) );
    return value;
}
double fbIPBufferAccess_read_double(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    double value = fbData_get_double( fbIPBufferAccess_dataAtPosition(buff) );
    fbIPBufferAccess_advance( buff, sizeof(double) );
    return value;
}



///////////////////////////////////////////////////////////////////////////////
//
// Writers
//
///////////////////////////////////////////////////////////////////////////////

void fbIPBufferAccess_write_char(FBIPBufferAcces *buff, char value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_char( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(char) );
}

void fbIPBufferAccess_write_int8(FBIPBufferAcces *buff, int8_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int8( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(int8_t) );
}
void fbIPBufferAccess_write_int16(FBIPBufferAcces *buff, int16_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int16( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(int16_t) );
}
void fbIPBufferAccess_write_int32(FBIPBufferAcces *buff, int32_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int32( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(int32_t) );
}
void fbIPBufferAccess_write_int64(FBIPBufferAcces *buff, int64_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int64( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(int64_t) );
}

void fbIPBufferAccess_write_uint8(FBIPBufferAcces *buff, uint8_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint8( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(uint8_t) );
}
void fbIPBufferAccess_write_uint16(FBIPBufferAcces *buff, uint16_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint16( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(uint16_t) );
}
void fbIPBufferAccess_write_uint32(FBIPBufferAcces *buff, uint32_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint32( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(uint32_t) );
}
void fbIPBufferAccess_write_uint64(FBIPBufferAcces *buff, uint64_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint64( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(uint64_t) );
}

void fbIPBufferAccess_write_float(FBIPBufferAcces *buff, float value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_float( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(float) );
}
void fbIPBufferAccess_write_double(FBIPBufferAcces *buff, double value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_double( fbIPBufferAccess_dataAtPosition(buff), value );
    fbIPBufferAccess_advance( buff, sizeof(double) );
}

void fbIPBufferAccess_write_array(FBIPBufferAcces *buff, byte_t *data, size_t length) {
    size_t space = buff->end - buff->position;
    size_t to_write = space < length ? space : length;
    FB_COPY_ARRAY_E( buff->position, data, 1, to_write );
    fbIPBufferAccess_advance( buff, to_write );
}
