/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_IP_BUFFER_ACCESS_H
#define VM_IP_BUFFER_ACCESS_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"
#include "vm/interpreter/data_access.h"

#include "vm/interpreter/checks.h"


typedef enum _FBBufferAccesStatus {
    BUFFER_ACCESS_OK,
    BUFFER_ACCESS_ERROR,

} FBBufferAccesStatus;

typedef struct _FBIPBufferAcces {
    FBBufferAccesStatus status;
    byte_t *end;
    byte_t *position;
    byte_t *buffer;
} FBIPBufferAcces;

void fbIPBufferAccess_init(FBIPBufferAcces *buff, byte_t *data, size_t data_size);
void fbIPBufferAccess_position(FBIPBufferAcces *buff, size_t new_position);
void fbIPBufferAccess_advance(FBIPBufferAcces *buff, size_t count);
void fbIPBufferAccess_retreat(FBIPBufferAcces *buff, size_t count);

void fbIPBufferAccess_fillRange(FBIPBufferAcces *buff, size_t start_index, byte_t *value, size_t value_size, size_t count);
void fbIPBufferAccess_copyRange(FBIPBufferAcces *dst_buff, FBIPBufferAcces *src_buff, size_t start_index, size_t count);


static inline byte_t* fbIPBufferAccess_dataAtPosition(FBIPBufferAcces *buff) {
    return buff->position;
}


///////////////////////////////////////////////////////////////////////////////
//
// Utils
//
///////////////////////////////////////////////////////////////////////////////

static inline bool fbIPBufferAccess_atEndOfBuffer(FBIPBufferAcces *buff) {
    return buff->position >= buff->end;
}

static inline size_t fbIPBufferAccess_length(FBIPBufferAcces *buff) {
    return buff->end - buff->buffer;
}

static inline bool fbIPBufferAccess_satusOK(FBIPBufferAcces *buff) {
    return buff->status == BUFFER_ACCESS_OK;
}

static inline void fbIPBufferAccess_resetSatus(FBIPBufferAcces *buff) {
    buff->status = BUFFER_ACCESS_OK;
}


///////////////////////////////////////////////////////////////////////////////
//
// Getters
//
///////////////////////////////////////////////////////////////////////////////

static inline char fbIPBufferAccess_get_char(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_char( fbIPBufferAccess_dataAtPosition(buff) );
}

static inline int8_t fbIPBufferAccess_get_int8(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_int8( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline int16_t fbIPBufferAccess_get_int16(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_int16( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline int32_t fbIPBufferAccess_get_int32(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_int32( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline int64_t fbIPBufferAccess_get_int64(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_int64( fbIPBufferAccess_dataAtPosition(buff) );
}

static inline uint8_t fbIPBufferAccess_get_uint8(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_uint8( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline uint16_t fbIPBufferAccess_get_uint16(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_uint16( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline uint32_t fbIPBufferAccess_get_uint32(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_uint32( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline uint64_t fbIPBufferAccess_get_uint64(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_uint64( fbIPBufferAccess_dataAtPosition(buff) );
}

static inline float fbIPBufferAccess_get_float(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_float( fbIPBufferAccess_dataAtPosition(buff) );
}
static inline double fbIPBufferAccess_get_double(FBIPBufferAcces *buff) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    return fbData_get_double( fbIPBufferAccess_dataAtPosition(buff) );
}


///////////////////////////////////////////////////////////////////////////////
//
// Setters
//
///////////////////////////////////////////////////////////////////////////////

static inline void fbIPBufferAccess_set_char(FBIPBufferAcces *buff, char value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_char( fbIPBufferAccess_dataAtPosition(buff), value );
}

static inline void fbIPBufferAccess_set_int8(FBIPBufferAcces *buff, int8_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int8( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_int16(FBIPBufferAcces *buff, int16_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int16( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_int32(FBIPBufferAcces *buff, int32_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int32( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_int64(FBIPBufferAcces *buff, int64_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_int64( fbIPBufferAccess_dataAtPosition(buff), value );
}

static inline void fbIPBufferAccess_set_uint8(FBIPBufferAcces *buff, uint8_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint8( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_uint16(FBIPBufferAcces *buff, uint16_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint16( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_uint32(FBIPBufferAcces *buff, uint32_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint32( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_uint64(FBIPBufferAcces *buff, uint64_t value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_uint64( fbIPBufferAccess_dataAtPosition(buff), value );
}

static inline void fbIPBufferAccess_set_float(FBIPBufferAcces *buff, float value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_float( fbIPBufferAccess_dataAtPosition(buff), value );
}
static inline void fbIPBufferAccess_set_double(FBIPBufferAcces *buff, double value) {
    CHECK_TRUE( ! fbIPBufferAccess_atEndOfBuffer( buff ) );
    fbData_set_double( fbIPBufferAccess_dataAtPosition(buff), value );
}


///////////////////////////////////////////////////////////////////////////////
//
// Readers
//
///////////////////////////////////////////////////////////////////////////////

char fbIPBufferAccess_read_char(FBIPBufferAcces *buff);

int8_t fbIPBufferAccess_read_int8(FBIPBufferAcces *buff);
int16_t fbIPBufferAccess_read_int16(FBIPBufferAcces *buff);
int32_t fbIPBufferAccess_read_int32(FBIPBufferAcces *buff);
int64_t fbIPBufferAccess_read_int64(FBIPBufferAcces *buff);

uint8_t fbIPBufferAccess_read_uint8(FBIPBufferAcces *buff);
uint16_t fbIPBufferAccess_read_uint16(FBIPBufferAcces *buff);
uint32_t fbIPBufferAccess_read_uint32(FBIPBufferAcces *buff);
uint64_t fbIPBufferAccess_read_uint64(FBIPBufferAcces *buff);

float fbIPBufferAccess_read_float(FBIPBufferAcces *buff);
double fbIPBufferAccess_read_double(FBIPBufferAcces *buff);



///////////////////////////////////////////////////////////////////////////////
//
// Writers
//
///////////////////////////////////////////////////////////////////////////////

void fbIPBufferAccess_write_char(FBIPBufferAcces *buff, char value);

void fbIPBufferAccess_write_int8(FBIPBufferAcces *buff, int8_t value);
void fbIPBufferAccess_write_int16(FBIPBufferAcces *buff, int16_t value);
void fbIPBufferAccess_write_int32(FBIPBufferAcces *buff, int32_t value);
void fbIPBufferAccess_write_int64(FBIPBufferAcces *buff, int64_t value);

void fbIPBufferAccess_write_uint8(FBIPBufferAcces *buff, uint8_t value);
void fbIPBufferAccess_write_uint16(FBIPBufferAcces *buff, uint16_t value);
void fbIPBufferAccess_write_uint32(FBIPBufferAcces *buff, uint32_t value);
void fbIPBufferAccess_write_uint64(FBIPBufferAcces *buff, uint64_t value);

void fbIPBufferAccess_write_float(FBIPBufferAcces *buff, float value);
void fbIPBufferAccess_write_double(FBIPBufferAcces *buff, double value);

void fbIPBufferAccess_write_array(FBIPBufferAcces *buff, byte_t *data, size_t length);

///////////////////////////////////////////////////////////////////////////////
//
// Helpers
//
///////////////////////////////////////////////////////////////////////////////

//
// These two are just helper functions so we don't have to subtract pointers.
// Depending how you read/write data (i.e. mixing get/set & read/write),
// return values of these may or may not correspond to how much data is
// written into the buffer (headCount), or how much space is left in there (tailCount).
//
static inline size_t fbIPBufferAccess_headCount(FBIPBufferAcces *buff) {
    return buff->position - buff->buffer;
}

static inline size_t fbIPBufferAccess_tailCount(FBIPBufferAcces *buff) {
    return buff->end - buff->position;
}


#ifdef __cplusplus
}
#endif

#endif // VM_IP_BUFFER_ACCESS_H
