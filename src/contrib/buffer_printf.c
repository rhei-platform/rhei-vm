/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include <limits.h>
#include <math.h>

#include "vm/interpreter/data_access.h"
#include "vm/lib/memory.h"
#include "contrib/ipbufferaccess.h"

#include "contrib/buffer_printf.h"

/*
    https://en.wikipedia.org/wiki/Printf_format_string#Syntax

    %[parameter][flags][width][.precision][length]type

    - parameter
        NOT IMPLEMENTED

    - flags
        NOT IMPLEMENTED

    - width
        minimum number of characters to output

    - precision
        specifies a maximum limit on the output
        - floating point: number of digits to the right of the decimal point, rounded
        - string: number of characters to output, truncated

    - length
        B - byte
        S - short, 2 bytes
        I - int, 32-bit IEEE float, 4 bytes
        L - long, 64-bit IEEE float, 8 bytes

    - type
        % - the percent char
        d, i - signed integral number
        u - unsigned integral number
        f - fixed point real number
        e - floating point real number
        x, X - unsigned integral number as hexadecimal
        s - null-terminated string
        c - char
*/

typedef enum _CtbPrintfFormatType {
    FORMAT_TYPE_NONE,

    // FORMAT_TYPE_WIDTH,
    // FORMAT_TYPE_PRECISION,

    FORMAT_TYPE_PERCENT_CHAR,

    FORMAT_TYPE_SIGNED_INTEGRAL,
    FORMAT_TYPE_UNSIGNED_INTEGRAL,

    FORMAT_TYPE_FIXED_REAL,
    FORMAT_TYPE_FLOATING_REAL,

    FORMAT_TYPE_HEXADECIMAL,
    FORMAT_TYPE_HEXADECIMAL_CAPS,

    FORMAT_TYPE_CHAR,
    FORMAT_TYPE_STRING,

    FORMAT_TYPE_INVALID,
} CtbPrintfFormatType;

typedef struct _CtbPrintfFormatSpec {
    CtbPrintfFormatType  type;
    uint32_t            width;
    uint32_t            precision;
    uint32_t            length; // == number of bytes to read for numeric values
    // uint32_t            base;
} CtbPrintfFormatSpec;

static inline void _ctb_clear_spec( CtbPrintfFormatSpec *spec ) {
    FB_CLEAR( spec, CtbPrintfFormatSpec );
}

typedef struct _CtbPrintfOpResult {
    CtbPrintfStatus status;
    union {
        char c;
        int64_t i64;
        uint64_t ui64;
        // float f;
        double d;
        unsigned int n;
    } value;
} CtbPrintfOpResult;

static inline void _ctb_clear_opres( CtbPrintfOpResult *res ) {
    FB_CLEAR( res, CtbPrintfOpResult );
}

//////////////////////////////////////////////////////////////////////////////////////
// https://stackoverflow.com/a/7097567
//////////////////////////////////////////////////////////////////////////////////////

/* 
   Double to ASCII Conversion without sprintf.
   Roughly equivalent to: sprintf(s, "%.14g", n);
*/

#include <math.h>
#include <string.h>

static double PRECISION = 0.00000000000001; // 1e-14
// static int MAX_NUMBER_STRING_SIZE = 32;

/**
 * Double to ASCII
 */
unsigned int _ctb_dtoa( FBByteBuffer *s, double value ) {
    unsigned int start_idx = s->count;
    // handle special cases
    if (isnan(value)) {
        fbByteBuffer_addArray( s, (byte_t *)"nan", 3 );
    } else if (isinf(value)) {
        fbByteBuffer_addArray( s, (byte_t *)"inf", 3 );
    } else if (value == 0.0) {
        fbByteBuffer_addArray( s, (byte_t *)"0", 1 );
    } else {
        int digit, m, m1;
        int neg = (value < 0);
        if (neg)
            value = -value;
        // calculate magnitude
        m = log10(value);
        int useExp = (m >= 14 || (neg && m >= 9) || m <= -9);
        if (neg) {
            fbByteBuffer_addValue( s, '-' );
        }
        // set up for scientific notation
        if (useExp) {
            if (m < 0)
               m -= 1;
            value = value / pow(10.0, m);
            if( value >= 10.0 ) {
                // a special case on one-ness
                // without this
                // 1.0e-9 would be printed as ":e-10" (see below)
                value /= 10.0;
                m += 1;
            }
            m1 = m;
            m = 0;
        }
        // if (m < 1.0) { // ???
        if (m < 0) {
            m = 0;
        }
        // convert the number
        while (value > PRECISION || m >= 0) {
            double weight = pow(10.0, m);
            if (weight > 0 && !isinf(weight)) {
                digit = floor(value / weight);
                value -= (digit * weight);
                // this is where 1.0e-9 would be printed as ":e-10"
                fbByteBuffer_addValue( s, '0' + digit );
            }
            if (m == 0 && value > 0) {
                fbByteBuffer_addValue( s, '.' );
            }
            m--;
        }
        if (useExp) {
            // convert the exponent
            int i, j;
            fbByteBuffer_addValue( s, 'e' );
            if (m1 > 0) {
                fbByteBuffer_addValue( s, '+' );
            } else {
                fbByteBuffer_addValue( s, '-' );
                m1 = -m1;
            }
            // use 'm' as a char counter
            m = 0;
            // this prints out the exponent, but with digits in reverse
            while (m1 > 0) {
                fbByteBuffer_addValue( s, '0' + m1 % 10 );
                m1 /= 10;
                m++;
            }
            // get pointer to where magnitude string starts
            byte_t *c = s->buffer + s->count - m;
            // flip the exponent digits around
            for (i = 0, j = m-1; i<j; i++, j--) {
                // swap without temporary
                c[i] ^= c[j];
                c[j] ^= c[i];
                c[i] ^= c[j];
            }
        }
    }
    return s->count - start_idx;
}
//////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////////





/*
    Returns parsed number or UINT_MAX if nothing is parsed.

    Passed char pointer will be left as is if nothing is parsed
    or it will be updated to point to the first next char after
    parsed number.
*/
CtbPrintfOpResult _ctb_parse_number( FBIPBufferAcces *fmt ) {

    CtbPrintfOpResult opres;
    _ctb_clear_opres( &opres );
    opres.status = CTB_PRINTF_STATUS_ERROR;

    unsigned int result = UINT_MAX;
    unsigned int multiplier = 1;

    byte_t *start = fbIPBufferAccess_dataAtPosition(fmt);

    while( ! fbIPBufferAccess_atEndOfBuffer(fmt) ) {
        // stop on non-number or EOS
        char chr = fbIPBufferAccess_get_char(fmt);
        if( chr < '0' || chr > '9' ) {
            break;
        }
        fbIPBufferAccess_read_char(fmt);
    }
    // fmt is either unchanged
    // or it points one char beyond the last digit
    // of the number to convert

    if( fbIPBufferAccess_dataAtPosition(fmt) > start ) {
        // got some digits to convert
        // fmt is one char past the last digit, thus -1
        byte_t *current = fbIPBufferAccess_dataAtPosition(fmt) - 1;
        result = 0;
        while( current >= start ) {
            result += (*current - '0') * multiplier;
            multiplier *= 10;
            current--;
        }
        opres.status = CTB_PRINTF_STATUS_OK;
    }

    opres.value.n = result;
    return opres;
}

/*
    Returns 'length' format value or UINT_MAX if nothing is parsed.

    Passed char pointer will be left as is if nothing is parsed
    or it will be updated to point to the first next char after
    parsed number.
*/
CtbPrintfOpResult _ctb_parse_length( FBIPBufferAcces *fmt ) {

    CtbPrintfOpResult opres;
    _ctb_clear_opres( &opres );
    opres.status = CTB_PRINTF_STATUS_ERROR;

    unsigned int result = UINT_MAX;

    if( ! fbIPBufferAccess_atEndOfBuffer(fmt) ) {

        // peek at the char
        switch( fbIPBufferAccess_get_char(fmt) ) {

            case 'B':
                result = 1;
                opres.status = CTB_PRINTF_STATUS_OK;
            break;

            case 'S':
                result = 2;
                opres.status = CTB_PRINTF_STATUS_OK;
            break;

            case 'I':
                result = 4;
                opres.status = CTB_PRINTF_STATUS_OK;
            break;

            case 'L':
                result = 8;
                opres.status = CTB_PRINTF_STATUS_OK;
            break;
        }

        if( opres.status == CTB_PRINTF_STATUS_OK ) {
            // consume the char
            fbIPBufferAccess_read_char(fmt);
        }
    }

    opres.value.n = result;
    return opres;
}

/*
    Returns 'type' format value or UINT_MAX if nothing is parsed.

    Passed char pointer will be left as is if nothing is parsed
    or it will be updated to point to the first next char after
    parsed number.
*/
CtbPrintfFormatType _ctb_parse_type( FBIPBufferAcces *fmt ) {

    CtbPrintfFormatType result = FORMAT_TYPE_NONE;

    if( ! fbIPBufferAccess_atEndOfBuffer(fmt) ) {

        // peek at the char
        switch( fbIPBufferAccess_get_char(fmt) ) {

            case '%':
                result = FORMAT_TYPE_PERCENT_CHAR;
            break;

            case 'd':
            case 'i':
                result = FORMAT_TYPE_SIGNED_INTEGRAL;
            break;

            case 'u':
                result = FORMAT_TYPE_UNSIGNED_INTEGRAL;
            break;

            case 'f':
                result = FORMAT_TYPE_FIXED_REAL;
            break;

            case 'e':
                result = FORMAT_TYPE_FLOATING_REAL;
            break;

            case 'x':
                result = FORMAT_TYPE_HEXADECIMAL_CAPS;
            break;

            case 'X':
                result = FORMAT_TYPE_HEXADECIMAL;
            break;

            case 's':
                result = FORMAT_TYPE_STRING;
            break;

            case 'c':
                result = FORMAT_TYPE_CHAR;
            break;
        }

        if( result != FORMAT_TYPE_NONE ) {
            // consume the character
            fbIPBufferAccess_read_char(fmt);
        }
    }

    return result;
}

/*
    If passed-in string format parameter starts with '%', parses
    the format specificiers, fills in passed-in 'spec' parameter
    and returns number of consumed chars.

    If passed-in string format parameter doesn't start with '%',
    returns 0 and marks 'spec' as invalid.
*/
void _ctb_parse_format( FBIPBufferAcces *fmt, CtbPrintfFormatSpec *spec ) {

    spec->type = FORMAT_TYPE_NONE;

    if( fbIPBufferAccess_atEndOfBuffer(fmt) ) return;

    if( fbIPBufferAccess_get_char(fmt) != '%' ) {
        spec->type = FORMAT_TYPE_INVALID;
        return;
    }

    // skip '%'
    fbIPBufferAccess_read_char(fmt);

    if( fbIPBufferAccess_atEndOfBuffer(fmt) ) {
        spec->type = FORMAT_TYPE_INVALID;
        return;
    }

    CtbPrintfOpResult opres;

    //
    // 'parameter' - NOT IMPLEMENTED
    //

    //
    // 'flags' - NOT IMPLEMENTED
    //

    //
    // 'width' - optional
    //
    unsigned int width = UINT_MAX;

    opres = _ctb_parse_number( fmt );
    if( opres.status == CTB_PRINTF_STATUS_OK ) {
        width = opres.value.n;
    }


    //
    // 'precision' - optional
    //
    unsigned int precision = UINT_MAX;
    // are we looking at '.'?
    if( fbIPBufferAccess_get_char(fmt) == '.' ) {
        // yes we are, skip it
        fbIPBufferAccess_read_char(fmt);

        opres = _ctb_parse_number( fmt );

        if( opres.status != CTB_PRINTF_STATUS_OK ) {

            spec->type = FORMAT_TYPE_INVALID;
            return;
        }

        precision = opres.value.n;
    }

    //
    // 'length' - optional
    //
    unsigned int length = UINT_MAX;

    opres = _ctb_parse_length( fmt );
    if( opres.status == CTB_PRINTF_STATUS_OK ) {
        length = opres.value.n;
    }

    //
    // type
    //
    CtbPrintfFormatType type = _ctb_parse_type( fmt );

    //
    // Set default values for omitted parts
    //
    if( length == UINT_MAX ) {
        switch( type ) {
            case FORMAT_TYPE_SIGNED_INTEGRAL:
            case FORMAT_TYPE_UNSIGNED_INTEGRAL:
                length = 4;
            break;
            case FORMAT_TYPE_FIXED_REAL:
            case FORMAT_TYPE_FLOATING_REAL:
                length = 4;
            break;
            case FORMAT_TYPE_HEXADECIMAL:
            case FORMAT_TYPE_HEXADECIMAL_CAPS:
                length = 4;
            break;
            default:
                // length = UINT_MAX;
            break;
        }
    }

    //
    // Set format spec based on parsed values
    //
    spec->type = type;
    spec->length = length;
    spec->precision = precision;
    spec->width = width;
}

CtbPrintfOpResult _ctb_get_signed_integral( uint32_t length, FBIPBufferAcces *data_buf ) {

    CtbPrintfOpResult opres;
    _ctb_clear_opres( &opres );
    opres.status = CTB_PRINTF_STATUS_ERROR;

    int64_t value = INT64_MAX;
    byte_t *buf = fbIPBufferAccess_dataAtPosition(data_buf);

    switch( length ) {

        case 1:
            if( fbIPBufferAccess_tailCount( data_buf ) < 1 ) {
                length = 0;
                break;
            }
            value = fbData_get_int8( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 2:
            if( fbIPBufferAccess_tailCount( data_buf ) < 2 ) {
                length = 0;
                break;
            }
            value = fbData_get_int16( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 4:
            if( fbIPBufferAccess_tailCount( data_buf ) < 4 ) {
                length = 0;
                break;
            }
            value = fbData_get_int32( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 8:
            if( fbIPBufferAccess_tailCount( data_buf ) < 8 ) {
                length = 0;
                break;
            }
            value = fbData_get_int64( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        default:
            length = 0;
        break;
    }

    fbIPBufferAccess_advance( data_buf, length );

    opres.value.i64 = value;
    return opres;
}

CtbPrintfOpResult _ctb_get_unsigned_integral( uint32_t length, FBIPBufferAcces *data_buf ) {

    CtbPrintfOpResult opres;
    _ctb_clear_opres( &opres );
    opres.status = CTB_PRINTF_STATUS_ERROR;

    uint64_t value = UINT64_MAX;
    byte_t *buf = fbIPBufferAccess_dataAtPosition(data_buf);

    switch( length ) {

        case 1:
            if( fbIPBufferAccess_tailCount( data_buf ) < 1 ) {
                length = 0;
                break;
            }
            value = fbData_get_uint8( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 2:
            if( fbIPBufferAccess_tailCount( data_buf ) < 2 ) {
                length = 0;
                break;
            }
            value = fbData_get_uint16( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 4:
            if( fbIPBufferAccess_tailCount( data_buf ) < 4 ) {
                length = 0;
                break;
            }
            value = fbData_get_uint32( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 8:
            if( fbIPBufferAccess_tailCount( data_buf ) < 8 ) {
                length = 0;
                break;
            }
            value = fbData_get_uint64( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        default:
            length = 0;
        break;
    }

    fbIPBufferAccess_advance( data_buf, length );

    opres.value.ui64 = value;
    return opres;
}

CtbPrintfOpResult _ctb_get_real( uint32_t length, FBIPBufferAcces *data_buf ) {

    CtbPrintfOpResult opres;
    _ctb_clear_opres( &opres );
    opres.status = CTB_PRINTF_STATUS_ERROR;

    double value = NAN;
    byte_t *buf = fbIPBufferAccess_dataAtPosition(data_buf);

    switch( length ) {

        case 4:
            if( fbIPBufferAccess_tailCount( data_buf ) < 4 ) {
                length = 0;
                break;
            }
            value = fbData_get_float( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        case 8:
            if( fbIPBufferAccess_tailCount( data_buf ) < 8 ) {
                length = 0;
                break;
            }
            value = fbData_get_double( buf );
            opres.status = CTB_PRINTF_STATUS_OK;
        break;

        default:
            length = 0;
        break;
    }

    fbIPBufferAccess_advance( data_buf, length );

    opres.value.d = value;
    return opres;
}

CtbPrintfOpResult _ctb_get_char( FBIPBufferAcces *data_buf ) {

    CtbPrintfOpResult opres;
    _ctb_clear_opres( &opres );
    opres.status = CTB_PRINTF_STATUS_ERROR;

    if( ! fbIPBufferAccess_atEndOfBuffer( data_buf ) ) {

        opres.value.c = fbIPBufferAccess_read_char(data_buf);
        opres.status = CTB_PRINTF_STATUS_OK;
    }

    return opres;
}

// strnlen() is non-standard function
// here's a simple implementation
static inline size_t _ctb_strnlen(const char *str, size_t maxlen) {
    size_t len;
    for( len = 0; len < maxlen && str[ len ]; len++ );
    return len;
}
char* _ctb_get_string( FBIPBufferAcces *data_buf ) {

    char *value = NULL;
    byte_t *buf = fbIPBufferAccess_dataAtPosition(data_buf);

    size_t length = _ctb_strnlen( (const char *)buf, fbIPBufferAccess_tailCount(data_buf) );
    value = FB_GROW_ARRAY_E( value, 1, length + 1 );
    FB_COPY_ARRAY_E( value, buf, 1, length );
    value[length] = 0;

    fbIPBufferAccess_advance( data_buf, length + 1 );

    return value;
}

unsigned int _ctb_ltoa( FBByteBuffer *output, int64_t value ) {

    bool negative = value < 0;
    if( negative ) value = - value;

    // UINT64_MAX = 18446744073709551615 -> 20 characters + one for '-'
    char result[21];
    int num_digits = 0;
    int idx = 0;

    do {
        char digit = value % 10;
        value /= 10;

        result[ idx ] = '0' + digit;
        num_digits++;
        idx++;

    } while( value != 0 );

    if( negative ) {
        result[ idx ] = '-';
        num_digits++;
    }

    // we've got all digits, but in reverse order
    idx = num_digits;
    while( idx ) {
        idx--;
        fbByteBuffer_addValue( output, result[ idx ] );
    }

    return num_digits;
}

unsigned int _ctb_ultoa( FBByteBuffer *output, uint64_t value ) {

    // UINT64_MAX = 18446744073709551615 -> 20 characters + one for '-'
    char result[21];
    int num_digits = 0;
    int idx = 0;

    do {
        char digit = value % 10;
        value /= 10;

        result[ idx ] = '0' + digit;
        num_digits++;
        idx++;

    } while( value != 0 );

    // we've got all digits, but in reverse order
    idx = num_digits;
    while( idx ) {
        idx--;
        fbByteBuffer_addValue( output, result[ idx ] );
    }

    return num_digits;
}

unsigned int _ctb_ultoh( FBByteBuffer *output, uint64_t value, bool use_caps ) {

    // int64 -> 64 / 4 = 16 characters
    char result[16];
    int num_digits = 0;
    int idx = 0;
    char a_offset = use_caps ? 'A' - 10 : 'a' - 10;

    do {
        char digit = value % 16;
        value /= 16;

        result[ idx ] = digit < 10 ? '0' + digit : a_offset + digit;
        num_digits++;
        idx++;

    } while( value != 0 );

    // we've got all digits, but in reverse order
    idx = num_digits;
    while( idx ) {
        idx--;
        fbByteBuffer_addValue( output, result[ idx ] );
    }

    return num_digits;
}

CtbPrintfResult fbContrib_printf( FBByteBuffer *output, const char *fmt, byte_t *data_buf, size_t data_size ) {

    CtbPrintfResult result;
    result.status = CTB_PRINTF_STATUS_OK;

    CtbPrintfFormatSpec spec;

    FBIPBufferAcces data_b;
    FBIPBufferAcces *data = &data_b;
    fbIPBufferAccess_init( data, data_buf, data_size );

    FBIPBufferAcces format_b;
    FBIPBufferAcces *format = &format_b;
    fbIPBufferAccess_init( format, (byte_t *)fmt, strlen(fmt) );


    while( ! fbIPBufferAccess_atEndOfBuffer( format ) ) {

        if( fbIPBufferAccess_get_char( format ) != '%' ) {
            // just transfer anything that does not look like format specifyer
            fbByteBuffer_addValue( output, fbIPBufferAccess_read_char( format ) );

        } else {

            // we are positioned at the format specifier start
            // let's parse it and see if we get anything
            _ctb_clear_spec( &spec );
            _ctb_parse_format( format, &spec );

            if( spec.type == FORMAT_TYPE_INVALID ) {

                result.status = CTB_PRINTF_STATUS_ERROR;
                break;
            }

            bool gotError = false;

            switch( spec.type ) {

                case FORMAT_TYPE_PERCENT_CHAR: {
                    fbByteBuffer_addValue( output, '%' );
                }
                break;

                case FORMAT_TYPE_SIGNED_INTEGRAL: {
                    CtbPrintfOpResult opres = _ctb_get_signed_integral( spec.length, data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    _ctb_ltoa( output, opres.value.i64 );
                }
                break;

                case FORMAT_TYPE_UNSIGNED_INTEGRAL: {
                    CtbPrintfOpResult opres = _ctb_get_unsigned_integral( spec.length, data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    _ctb_ultoa( output, opres.value.ui64 );
                }
                break;

                case FORMAT_TYPE_FIXED_REAL: {
                    CtbPrintfOpResult opres = _ctb_get_real( spec.length, data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    // TODO: our _ctb_dtoa() automatically switches fixed/floating notation
                    _ctb_dtoa( output, opres.value.d );
                }
                break;

                case FORMAT_TYPE_FLOATING_REAL: {
                    CtbPrintfOpResult opres = _ctb_get_real( spec.length, data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    // TODO: our _ctb_dtoa() automatically switches fixed/floating notation
                    _ctb_dtoa( output, opres.value.d );
                }
                break;

                case FORMAT_TYPE_HEXADECIMAL_CAPS: {
                    CtbPrintfOpResult opres = _ctb_get_unsigned_integral( spec.length, data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    _ctb_ultoh( output, opres.value.ui64, true );
                }
                break;

                case FORMAT_TYPE_HEXADECIMAL: {
                    CtbPrintfOpResult opres = _ctb_get_unsigned_integral( spec.length, data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    _ctb_ultoh( output, opres.value.ui64, false );
                }
                break;

                case FORMAT_TYPE_STRING: {
                    char *value = _ctb_get_string( data );
                    fbByteBuffer_addArray( output, (byte_t *)value, strlen(value) );
                    FB_FREE_ARRAY( value );
                }
                break;

                case FORMAT_TYPE_CHAR: {
                    CtbPrintfOpResult opres = _ctb_get_char( data );
                    if( opres.status != CTB_PRINTF_STATUS_OK ) {
                        // stop processing and return error
                        gotError = true;
                        break;
                    }
                    fbByteBuffer_addValue( output, opres.value.c );

                }
                break;

                default:
                break;
            }

            if( gotError ) {
                result.status = CTB_PRINTF_STATUS_ERROR;
                break;
            }
        }
    }

    result.chars_printed = output->count;
    return result;
}
