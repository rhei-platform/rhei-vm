/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "contrib/descriptor.h"
#include "vm/lib/memory.h"

#include <stdio.h> // printf
#include <stdlib.h> // exit

#define SET_TABLE_INDEX_FLAG( x ) ( ( x ) | INDEX_FLAG )

typedef struct __update_data_t {
    index_t index_of_element_to_update;
    FBElementBuffer *referenced_elements; // FBDescriptorTableElement
} _update_data_t;

index_t _collect_array_of_strings( byte_t *el_data, index_t start_offset, index_t el_count, byte_t *dataend, FBElementBuffer *referenced_elements ) {

    if( sizeof(FBDescriptorTableElement) != referenced_elements->elementSize ) {
        printf("wrong buffer element type for referenced_elements parameter\n");
        exit(2);
    }

    FBDescriptorTableElement te;
    index_t element_size = 0;
    index_t offset = 0;

    while( el_count > 0 ) {

        while( *el_data && el_data < dataend ) {
            el_data++;
            element_size++;
        }
        // add terminating '\0' to element_size
        if( el_data < dataend ) {
            el_data++;
            element_size++;
        }
        // element_size = size of string;

        te.offset = start_offset + offset;
        te.count = 1;
        te.element_size = element_size;

        fbElementBuffer_addValue( referenced_elements, &te );

        if( el_data >= dataend ) {
            // we've got to the data buffer end, need to finish up here
            break;
        }

        offset += element_size;
        element_size = 0;
        el_count--;
    }

    return offset;
}


index_t _collect_record( FBDescriptorElement *dsc, index_t start_offset, byte_t *data, index_t data_size, FBElementBuffer *table ) {

    if( T_RECORD_HDR != dsc->type ) {
        printf("Expected first descriptor element to be record header, got %i instead\n", dsc->type);
        exit(1);
    }

    index_t offset = start_offset;
    index_t length = 0;
    index_t element_size = 0;
    byte_t *eldata;
    byte_t *dataend = data + data_size;

    FBDescriptorTableElement table_element;

    // resulting table we will return to caller
    // FBElementBuffer *table = fbElementBuffer_create( sizeof(FBDescriptorTableElement) );

    // additional parsed elements for array and record types
    // we will apend these to 'table' once we parse all elements
    FBElementBuffer *addl_element_arrays = fbElementBuffer_create( sizeof(_update_data_t) );
    FBElementBuffer *referenced_elements;


    // add 1 to account for descriptor record header
    // in the for-loop condition
    index_t el_count = dsc->length + 1;
    for( index_t idx = 1; idx < el_count; idx++ ) {

        FBDescriptorElement el = dsc[ idx ];

        length = el.length;

        switch( el.type ) {

            case T_INT8:
                element_size = SIZE_OF_INT8;
            break;
            case T_UINT8:
                element_size = SIZE_OF_UINT8;
            break;
            case T_INT16:
                element_size = SIZE_OF_INT16;
            break;
            case T_UINT16:
                element_size = SIZE_OF_UINT16;
            break;
            case T_INT32:
                element_size = SIZE_OF_INT32;
            break;
            case T_UINT32:
                element_size = SIZE_OF_UINT32;
            break;
            case T_INT64:
                element_size = SIZE_OF_INT64;
            break;
            case T_UINT64:
                element_size = SIZE_OF_UINT64;
            break;

            case T_FLOAT:
                element_size = SIZE_OF_FLOAT;
            break;
            case T_DOUBLE:
                element_size = SIZE_OF_DOUBLE;
            break;

            case T_CSTRING:
                length = el.length;
                eldata = data + offset;
                if( el.length == 1 ) {

                    // this element is a single string
                    element_size = 0;
                    while( *eldata && eldata < dataend ) {
                        eldata++;
                        element_size++;
                    }
                    // add '\0' to element_size
                    if( eldata < dataend ) {
                        element_size++;
                    }
                    // element_size = size of string;

                } else {

                    // this element is a string array
                    // we will have to parse each string in the array
                    // as an element of its own in the resulting 'table'
                    // each string from the array will have its own entry
                    // with 'offset' and 'length' values

                    // dynamic buffer to keep all parsed offset/value pairs
                    // for each string from array
                    referenced_elements = fbElementBuffer_create( sizeof(FBDescriptorTableElement) );
                    // collect all the strings
                    // 'element_size' is size of the entire string array
                    element_size = _collect_array_of_strings( eldata, offset, el.length, dataend, referenced_elements );
                    // set INDEX_FLAG bit since we are storing actual
                    // offset/value positions later in the 'table'
                    element_size = SET_TABLE_INDEX_FLAG( element_size );
                    // element_size = size of ALL strings in array;
                }
            break;
            case T_PSTRING:
                // element_size = size of string;
            // break;
            case T_UTF8STRING:
                // element_size = size of string;
            // break;

            case T_RECORD_REF:
                // type of this element is a record
                // we need to get to where that record descriptor starts
                // and give *that* descriptor to _collect_record
                referenced_elements = fbElementBuffer_create( sizeof(FBDescriptorTableElement) );
                FBDescriptorElement *rec_desc = dsc + el.record_ref_index;
                element_size = _collect_record( rec_desc, offset, data, data_size, referenced_elements );
                element_size = SET_TABLE_INDEX_FLAG( element_size );
                // element_size = size of record(s);
            break;

            default:
                printf("unsupported type %d\n", el.type);
                fbElementBuffer_destroy( table );
                fbElementBuffer_destroy( addl_element_arrays );
                return INDEX_T_MAX;
        }

        // save stuff
        table_element.offset = offset;
        table_element.count = length;
        table_element.element_size = element_size;
        fbElementBuffer_addValue( table, &table_element );

        if( IS_TABLE_INDEX( element_size ) ) {
            // since 'element_size' for 'table' entries marked with INDEX_FLAG
            // represents the entire array/record size, we need to set 'length'
            // here to 1 to correctly calculate next offset value
            length = 1;
            // get the actual size of the entire array/record
            element_size = GET_TABLE_INDEX( element_size );

            // save 'referenced_elements' list for later
            // when we will add it to the 'table'
            _update_data_t udt;
            // the last element added to 'table'
            // we'll need to update its 'element_size' member
            // with the index of where we inserted offset/value
            // pairs from 'referenced_elements'
            udt.index_of_element_to_update = table->count - 1;
            // list of elements to add to 'table' at the end
            udt.referenced_elements = referenced_elements;
            fbElementBuffer_addValue( addl_element_arrays, &udt );
        }

        // printf("---> ofs %zu\tlen %zu\n", offset, length * element_size);

        // set up offset for the next element
        offset += length * element_size;
    }

    // finish up
    // go through all additional element tables we parsed, if any,
    // and add each one to the end of the final 'table'
    for( index_t i = 0; i<addl_element_arrays->count; i++ ) {
        _update_data_t *d = (_update_data_t*) fbElementBuffer_getElementPointer( addl_element_arrays, i );

        FBElementBuffer *referenced_elements = d->referenced_elements;

        index_t start_index = table->count;
        // add this 'referenced_elements' to 'table'
        for( index_t j = 0; j < referenced_elements->count; j++ ) {
            FBDescriptorTableElement *te = (FBDescriptorTableElement*) fbElementBuffer_getElementPointer( referenced_elements, j );
            // update INDEX_FLAG value
            if( IS_TABLE_INDEX( te->element_size ) ) {
                te->element_size = SET_TABLE_INDEX_FLAG( start_index + GET_TABLE_INDEX( te->element_size ) );
            }
            fbElementBuffer_addValue( table, te );
        }
        // update 'element_size' of the parent element to be the index
        // to the first entry of this 'referenced_elements' table we added
        FBDescriptorTableElement* element_to_update = (FBDescriptorTableElement*) fbElementBuffer_getElementPointer( table, d->index_of_element_to_update );
        element_to_update->element_size = SET_TABLE_INDEX_FLAG( start_index );

        // don't need this any more
        fbElementBuffer_destroy( referenced_elements );
    }
    // don't need this any more
    fbElementBuffer_destroy( addl_element_arrays );

    return offset - start_offset;
}

index_t fbContrib_descriptor( FBDescriptorElement *dsc, byte_t *data, index_t data_size, FBElementBuffer* table ) {


    if( sizeof(FBDescriptorTableElement) != table->elementSize ) {
        printf("wrong buffer element type for table parameter\n");
        exit(2);
    }

    return _collect_record( dsc, 0, data, data_size, table );
}
