/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_BUFFER_PRINTF_H
#define VM_BUFFER_PRINTF_H

#include "vm/interpreter/types.h"
#include "vm/lib/databuffer.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef enum _CtbPrintfStatus {
    CTB_PRINTF_STATUS_OK,
    CTB_PRINTF_STATUS_ERROR,
} CtbPrintfStatus;

typedef struct _CtbPrintfResult {
    CtbPrintfStatus status;
    size_t chars_printed;
} CtbPrintfResult;

CtbPrintfResult fbContrib_printf( FBByteBuffer *output, const char *fmt, byte_t *data_buf, size_t data_size );


#ifdef __cplusplus
}
#endif

#endif // VM_BUFFER_PRINTF_H
