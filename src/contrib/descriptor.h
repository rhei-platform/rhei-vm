/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_DESCRIPTOR_H
#define VM_DESCRIPTOR_H

#include <limits.h>

#include "vm/interpreter/types.h"
#include "vm/lib/databuffer.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef enum _FBElementDataType
{
    T_NIL = 0x00,

    BASE_TYPE = 0x40,

    T_INT8,
    T_UINT8,
    T_INT16,
    T_UINT16,
    T_INT32,
    T_UINT32,
    T_INT64,
    T_UINT64,

    T_FLOAT,
    T_DOUBLE,

    T_CSTRING,
    T_PSTRING,
    T_UTF8STRING,

    COMPLEX_TYPE = 0x80,

    T_RECORD_REF,
    T_RECORD_HDR,
    T_MULTIARRAY,

} FBElementDataType;

typedef enum _FBElementDataTypeSize {

    SIZE_OF_NIL = (size_t)0,

    SIZE_OF_INT8 = sizeof(int8_t),
    SIZE_OF_UINT8 = sizeof(uint8_t),
    SIZE_OF_INT16 = sizeof(int16_t),
    SIZE_OF_UINT16 = sizeof(uint16_t),
    SIZE_OF_INT32 = sizeof(int32_t),
    SIZE_OF_UINT32 = sizeof(uint32_t),
    SIZE_OF_INT64 = sizeof(int64_t),
    SIZE_OF_UINT64 = sizeof(uint64_t),

    SIZE_OF_FLOAT = sizeof(float),
    SIZE_OF_DOUBLE = sizeof(double),

} FBElementDataTypeSize;

typedef struct _FBDescriptorElement {

    // element type
    FBElementDataType type;
    // if type != T_RECORD_HDR, this is number of elements in array
    // if type == T_RECORD_HDR, this is number of elements following this element
    //      that comprise this record descriptor
    index_t length;
    // if type == T_RECORD_REF, this is index where start of the record definition is
    //      entry at the index MUST be of T_RECORD_HDR type
    // if type != T_RECORD_REF, value of this field is undefined
    index_t record_ref_index;

} FBDescriptorElement;

// use MSB as a flag to indicate 'element_size' is an index and not the element size
const index_t INDEX_FLAG = ~(INDEX_T_MAX >> 1);

#define IS_TABLE_INDEX( x ) (x & INDEX_FLAG)
#define GET_TABLE_INDEX( x ) (x & ~INDEX_FLAG)


/*
descriptor offsets table MUST start with entry
{ 0, count, INDEX_FLAG + 1 }
where 'count' is number of elements in top-level record

i.e. descriptor:

    cs   name
    u8   age
    cs   address
    u32  zip_code
    f[2]  coords

might parse a data buffer like so:

 0: ofs 0   el 5    len 1 > 
 1: ofs 0   el 1    len 5    ; name
 2: ofs 5   el 1    len 1    ; age
 3: ofs 6   el 2    len 6 >  ; address[]
 4: ofs 29  el 1    len 4    ; zip_code
 5: ofs 33  el 2    len 8    ; coords[0], coords[1]
 6: ofs 0   el 1    len 12   ; address[0]
 7: ofs 12  el 1    len 11   ; address[1]
*/
typedef struct _FBDescriptorTableElement {

    index_t offset;
    index_t count;
    index_t element_size; // or table_index if MSB bit is set

} FBDescriptorTableElement;


index_t fbContrib_descriptor( FBDescriptorElement *dsc, byte_t *data, index_t data_size, FBElementBuffer* table );

#ifdef __cplusplus
}
#endif

#endif // VM_DESCRIPTOR_H
