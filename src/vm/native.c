/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */


#include "vm/native.h"
#include "vm/core/connection.h"
#include "vm/core/input_output.h"

FBInformationPacketHandle fbNativeConnection_read( FBNativeContextHandle handle ) {

    FBConnection *connection = FB_GET_CONNECTION_FROM_HANDLE( handle );

    if( ! fbConnection_canRead( connection ) ) {
        return FB_INVALID_PACKET_HANDLE;
    }

    FBInformationPacketHandle packet = fbConnection_read( connection );

    return packet;
}

bool fbNativeConnection_write( FBNativeContextHandle handle, FBInformationPacketHandle packet ) {

    FBConnection *connection = FB_GET_CONNECTION_FROM_HANDLE( handle );

    if( ! fbConnection_canWrite( connection ) ) {
        return false;
    }

    bool writeSuccess = fbConnection_write( connection, packet );

    return writeSuccess;
}

void fbNativeConnection_close( FBNativeContextHandle handle ) {

    FBInputOutput *port = FB_GET_PORT_FROM_HANDLE( handle );

    fbInputOutput_close( port );
}

bool fbNativeConnection_isTerminated( FBNativeContextHandle handle )
{
    FBConnection *connection = FB_GET_CONNECTION_FROM_HANDLE( handle );
    return fbConnection_isTerminated( connection );
}

bool fbNativeConnection_isEmpty( FBNativeContextHandle handle )
{
    FBConnection *connection = FB_GET_CONNECTION_FROM_HANDLE( handle );
    return fbConnection_isEmpty( connection );
}

bool fbNativeConnection_isFull( FBNativeContextHandle handle )
{
    FBConnection *connection = FB_GET_CONNECTION_FROM_HANDLE( handle );
    return fbConnection_isFull( connection );
}
