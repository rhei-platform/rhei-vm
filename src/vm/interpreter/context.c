/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/interpreter/context.h"
#include "vm/lib/memory.h"

#include <string.h>


void fbContext_init_allocate( FBContext *context ) {
    fbContext_init_allocate_iii( context, 0, 0, 0 );
}

void fbContext_init_allocate_i( FBContext *context, index_t numberOfPackets ) {
    fbContext_init_allocate_iii( context, numberOfPackets, 0, 0 );
}

void fbContext_init_allocate_iii( FBContext *context, index_t numberOfPackets, index_t numberOfInputs, index_t numberOfOutputs ) {
    
    context->code = NULL;
    context->codeEnd = NULL;    

    context->componentData = NULL;
    context->constantPool = NULL;
    context->packets = NULL;
    context->packetHandles = NULL;
    if( numberOfPackets ) {
        context->packets = FB_GROW_ARRAY( context->packets, byte_t*, numberOfPackets );
        context->packetHandles = FB_GROW_ARRAY( context->packetHandles, FBInformationPacketHandle, numberOfPackets );

        for(index_t idx = 0; idx < numberOfPackets; idx++) {
            context->packets[ idx ] = NULL;
            context->packetHandles[ idx ] = FB_INVALID_PACKET_HANDLE;
        }
    }

    FB_CONTEXT_RESET_IP( context );
    context->state = CONTEXT_INITIALIZED;

    context->stackTop = &context->stack[0];

    context->inputs = NULL;
    context->inputsCount = 0;
    if( numberOfInputs ) {
        context->inputs = FB_GROW_ARRAY( context->inputs, FBInputOutput, numberOfInputs );
        context->inputsCount = numberOfInputs;

        for(index_t idx = 0; idx < numberOfInputs; idx++) {
            fbInputOutput_init( &context->inputs[ idx ], context );
        }
    }

    context->outputs = NULL;
    context->outputsCount = 0;
    if( numberOfOutputs ) {
        context->outputs = FB_GROW_ARRAY( context->outputs, FBInputOutput, numberOfOutputs );
        context->outputsCount = numberOfOutputs;

        for(index_t idx = 0; idx < numberOfOutputs; idx++) {
            fbInputOutput_init( &context->outputs[ idx ], context );
        }
    }

    context->component = NULL;

    fbError_setStatus( context, STATUS_OK );
}

void fbContext_init_allocate_fromComponent( FBContext *context, FBComponent *component ) {

    fbContext_init_allocate_iii( context, component->informationPacketCount, component->inputCount, component->outputCount );

    if( component->dataSize > 0 ) {
        fbContext_createData( context, component->dataSize );
    }
    if( component->constantPoolSize > 0 ) {
        fbContext_setConstantPool( context, component->constantPool );
    }

    // TODO raise error if there is no setup or loop code ???
    // Initially start with setup() code
    fbContext_setCode( context, component->setupCode, component->setupCodeSize );

    context->component = component;
}

void fbContext_free( FBContext *context ) {

    context->code = NULL;
    context->codeEnd = NULL;

    FB_FREE_ARRAY( context->componentData );
    context->componentData = NULL;
    context->constantPool = NULL;
    FB_FREE_ARRAY( context->packets );
    context->packets = NULL;
    // TODO drop all unreleased packets?
    FB_FREE_ARRAY( context->packetHandles );
    context->packetHandles = NULL;

    FB_CONTEXT_CLEAR_IP( context );
    context->state = CONTEXT_TERMINATED;

    FB_FREE_ARRAY( context->inputs );
    context->inputsCount = 0;
    FB_FREE_ARRAY( context->outputs );
    context->outputsCount = 0;

    context->stackTop = NULL;

    context->component = NULL;

    fbError_setStatus( context, STATUS_NONE );
}

void fbContext_setCode( FBContext *context, byte_t *codeBuffer, index_t codeSize ) {

    context->code = codeBuffer;
    context->codeEnd = context->code + codeSize - 1;

    FB_CONTEXT_RESET_IP( context );
}

void fbContext_createData( FBContext *context, index_t dataSize ) {

    context->componentData = FB_GROW_ARRAY( context->componentData, byte_t, dataSize );
}

void fbContext_setConstantPool( FBContext *context, byte_t *constantsBuffer ) {

    context->constantPool = constantsBuffer;
}


void fbContext_reset( FBContext *context ) {

    FB_CONTEXT_RESET_IP( context );
    context->state = CONTEXT_RUNNING;

    context->stackTop = &context->stack[0];

    fbError_setStatus( context, STATUS_OK );
}

FBInputOutput* fbContext_getInputPort( FBContext *context, index_t portIndex ) {

    if( context->inputs == NULL )
        return NULL;

    return &context->inputs[ portIndex ];
}

FBInputOutput* fbContext_getOutputPort( FBContext *context, index_t portIndex ) {

    if( context->outputs == NULL )
        return NULL;

    return &context->outputs[ portIndex ];

}

bool fbContext_hasOpenPorts( FBContext *context ) {

    bool hasOpenPorts = false;

    for( index_t idx = 0; idx < context->inputsCount; idx++ ) {

        if( fbInputOutput_isOpen( &context->inputs[ idx ] ) ) {
            hasOpenPorts = true;
            break;
        }
    }

    if( ! hasOpenPorts ) {

        for( index_t idx = 0; idx < context->outputsCount; idx++ ) {

            if( fbInputOutput_isOpen( &context->outputs[ idx ] ) ) {
                hasOpenPorts = true;
                break;
            }
        }
    }

    return hasOpenPorts;
}


void fbContext_setState( FBContext *context, FBContextState state ) {

    context->state = state;
}

FBContextState fbContext_getState( FBContext *context ) {

    return context->state;
}

