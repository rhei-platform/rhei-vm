/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_DATA_ACCESS_H
#define VM_DATA_ACCESS_H

#include <stdint.h>

#include "vm/interpreter/context.h"

#ifdef __cplusplus
extern "C" {
#endif


void fbContext_push( FBContext *context, FBStackValue value );
void fbContext_push64( FBContext *context, int64_t value );
FBStackValue fbContext_pop( FBContext *context );
int64_t fbContext_pop64( FBContext *context );


// This will be compiled to a constant value by both gcc and clang
// when optimizations are turned on. if-else dead-code branches will
// be removed we'll get clean assembly output.
static inline bool is_host_big_endian( void )
{
    union {
        uint16_t i16;
        uint8_t i8[2];
    } test_value = { .i16 = 1 };

    return test_value.i8[0] == 0; 
}


/*
    Following functions will neatly compile into one-instruction code on
    64-bit little-endian CPUs and into a bit more code for 64-bit get/set
    variants on 32-bit little-endian CPUs by both gcc and clang when
    optimizations are turned on.

    To test and play with the code, visit
    https://godbolt.org/g/exdI4f
    and add/remove bits and pieces to see results for different targets.

*/


///////////////////////////////////////////////////////////////////////////////
//
// INT getters
//
///////////////////////////////////////////////////////////////////////////////

// As of v8.2, GCC can't optimize out OR-ing and shift-ing
// versions of fbData_get_int16, fbData_get_uint16,
// fbData_get_int64 and fbData_get_uint64 by using
// MOVZWL and MOVQ on x86_64 and LDRSH/LDRH and LDRD on ARM.
// To get around that we use is_host_big_endian() to help
// GCC on little-endian targets.
// clang seems to not have this issue.

static inline int8_t fbData_get_int8( byte_t *data )
    { return (int8_t) ( data[0] ); }

static inline uint8_t fbData_get_uint8( byte_t *data )
    { return (uint8_t) ( data[0] ); }

static inline int16_t fbData_get_int16( byte_t *data )
    {
        if( is_host_big_endian() )
        return (int16_t) ( data[1] << 8 | data[0] );
        else
        return *( (int16_t*) data );
    }

static inline uint16_t fbData_get_uint16( byte_t *data )
    {
        if( is_host_big_endian() )
        return (uint16_t) ( data[1] << 8 | data[0] );
        else
        return *( (uint16_t*) data );
    }

static inline int32_t fbData_get_int32( byte_t *data )
    { return (int32_t) ( data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0] ); }

static inline uint32_t fbData_get_uint32( byte_t *data )
    { return (uint32_t) ( data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0] ); }

static inline int64_t fbData_get_int64( byte_t *data )
    {
        if( is_host_big_endian() )
        return (int64_t) ( (int64_t)data[7] << 56 | (int64_t)data[6] << 48 | (int64_t)data[5] << 40 | (int64_t)data[4] << 32 | data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0] );
        else
        return *( (int64_t*) data );
    }

static inline uint64_t fbData_get_uint64( byte_t *data )
    {
        if( is_host_big_endian() )
        return (uint64_t) ( (uint64_t)data[7] << 56 | (uint64_t)data[6] << 48 | (uint64_t)data[5] << 40 | (uint64_t)data[4] << 32 | data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0] );
        else
        return *( (uint64_t*) data );
    }


///////////////////////////////////////////////////////////////////////////////
//
// common type getters
//
///////////////////////////////////////////////////////////////////////////////

static inline bool fbData_get_bool( byte_t *data )
    { return (bool) fbData_get_int8( data ); }

static inline byte_t fbData_get_byte( byte_t *data )
    { return (byte_t) fbData_get_uint8( data ); }

static inline int fbData_get_int( byte_t *data )
    { return (int) fbData_get_int32( data ); }

static inline float fbData_get_float( byte_t *data )
    {
        FBFloatValue value;
        value.asInt32 = fbData_get_int32( data );
        return value.asFloat;
    }

static inline double fbData_get_double( byte_t *data )
    {
        FBDoubleValue value;
        value.asInt64 = fbData_get_int64( data );
        return value.asDouble;
    }

static inline char fbData_get_char( byte_t *data )
    { return (char) fbData_get_int8( data ); }





///////////////////////////////////////////////////////////////////////////////
//
// INT setters
//
///////////////////////////////////////////////////////////////////////////////

// Optimizing setters for buffered data access is not implemented in either
// gcc or clang. We explicitely use is_host_big_endian() here.


static inline void fbData_set_int8( uint8_t *data, int8_t value )
    { *( (int8_t*) data ) = value; }

static inline void fbData_set_uint8( uint8_t *data, uint8_t value )
    { *( (uint8_t*) data ) = value; }


static inline void fbData_set_int16( uint8_t *data, int16_t value )
    {
        if( is_host_big_endian() )
        { data[0] = value; data[1] = value >> 8; }
        else
        *( (int16_t*) data ) = value;
    }

static inline void fbData_set_uint16( uint8_t *data, uint16_t value )
    {
        if( is_host_big_endian() )
        { data[0] = value; data[1] = value >> 8; }
        else
        *( (uint16_t*) data ) = value;
    }


static inline void fbData_set_int32( uint8_t *data, int32_t value )
    {
        if( is_host_big_endian() )
        { data[0] = value; data[1] = value >> 8; data[2] = value >> 16; data[3] = value >> 24; }
        else
        *( (int32_t*) data ) = value;
    }

static inline void fbData_set_uint32( uint8_t *data, uint32_t value )
    {
        if( is_host_big_endian() )
        { data[0] = value; data[1] = value >> 8; data[2] = value >> 16; data[3] = value >> 24; }
        else
        *( (uint32_t*) data ) = value;
    }


static inline void fbData_set_int64( uint8_t *data, int64_t value )
    {
        if( is_host_big_endian() )
        { data[0] = value; data[1] = value >> 8; data[2] = value >> 16; data[3] = value >> 24; data[4] = value >> 32; data[5] = value >> 40; data[6] = value >> 48; data[7] = value >> 56; }
        else
        *( (int64_t*) data ) = value;
    }

static inline void fbData_set_uint64( uint8_t *data, uint64_t value )
    {
        if( is_host_big_endian() )
        { data[0] = value; data[1] = value >> 8; data[2] = value >> 16; data[3] = value >> 24; data[4] = value >> 32; data[5] = value >> 40; data[6] = value >> 48; data[7] = value >> 56; }
        else
        *( (uint64_t*) data ) = value;
    }


///////////////////////////////////////////////////////////////////////////////
//
// Common type setters
//
///////////////////////////////////////////////////////////////////////////////

static inline void fbData_set_bool( uint8_t *data, bool value )
    { fbData_set_int8( data, value ); }

static inline void fbData_set_byte( uint8_t *data, byte_t value )
    { fbData_set_uint8( data, value ); }

static inline void fbData_set_int( uint8_t *data, int value )
    { fbData_set_int32( data, value ); }

static inline void fbData_set_float( uint8_t *data, float value )
    {
        FBFloatValue tvalue;
        tvalue.asFloat = value;
        fbData_set_int32( data, tvalue.asInt32 );
    }

static inline void fbData_set_double( uint8_t *data, double value )
    {
        FBDoubleValue tvalue;
        tvalue.asDouble = value;
        fbData_set_int64( data, tvalue.asInt64 );
    }

static inline void fbData_set_char( uint8_t *data, char value )
    { fbData_set_int8( data, value ); }




#ifdef __cplusplus
}
#endif

#endif // VM_DATA_ACCESS_H
