/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_TYPES_H
#define VM_TYPES_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

// byte_t has to be unsigned
// if it was signed, when MSB is 1
// it would fill in higher bytes when
// casting to wider words
typedef uint8_t byte_t;

typedef size_t index_t;
#define INDEX_T_MAX SIZE_MAX

typedef int32_t FBStackValue;

typedef union {
    float asFloat;
    int32_t asInt32;
} FBFloatValue;

typedef union {
    double asDouble;
    int64_t asInt64;
} FBDoubleValue;



typedef struct _FBInputOutput FBInputOutput;
typedef struct _FBConnection FBConnection;
typedef struct _FBContext FBContext;
typedef struct _FBComponent FBComponent;
typedef struct _FBNetworkNativeIO FBNetworkNativeIO;


typedef int32_t FBInformationPacketHandle;

// static const FBInformationPacketHandle FB_INVALID_PACKET_HANDLE = INT32_MAX;
// static const FBInformationPacketHandle FB_INVALID_PACKET_HANDLE = 0;
#define FB_INVALID_PACKET_HANDLE (0)



#ifdef __cplusplus
}
#endif

#endif // VM_TYPES_H
