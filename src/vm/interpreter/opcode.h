/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_OPCODE_H
#define VM_OPCODE_H


typedef enum _FBVMInstruction {

    OP_NOP = 0,
    OP_INVALID,

    // flow-control:
    OP_END,
    OP_TERMINATE,

    OP_SWITCH,
    OP_JUMP,

    OP_JUMP_IF_FALSE, // boolean
    OP_JUMP_IF_LESS_EQUAL, // <= 0
    OP_JUMP_IF_GREATER_EQUAL, // >= 0
    OP_JUMP_IF_LESS, // < 0
    OP_JUMP_IF_GREATER, // > 0
    OP_JUMP_IF_EQUAL, // == 0
    OP_JUMP_IF_NOT_EQUAL, // != 0

    // version of OP_JUMP_IF_EQUAL for numbers
    // int, long, float & double all have 0 represented by 0x0
    // OP_JUMP_IF_ZERO is same as OP_JUMP_IF_EQUAL for 32-bit values
    // OP_JUMP_IF_ZERO, // int, float
    // 64-bit values
    OP_JUMP_IF_ZERO_2, // long, double


    // constant values:
    OP_BCONST_I8, // load const using imm byte offset
    OP_BCONST_I16,
    OP_BCONST_I32,
    OP_BCONST_I64,

    OP_SCONST_I8, // load const using imm short offset
    OP_SCONST_I16,
    OP_SCONST_I32,
    OP_SCONST_I64,

    OP_PCONST_I8, // load const using offset from stack
    OP_PCONST_I16,
    OP_PCONST_I32,
    OP_PCONST_I64,

    OP_CONST_0,
    OP_CONST_1,
    OP_CONST_M1,

    OP_BPUSH, // push imm 8-bit signed value
    OP_SPUSH, // push imm 16-bit signed value


    // stack manipulation:
    OP_DUP,
    OP_DUP_2,


    // information packet load/store:
    OP_CLOAD_I8, // load value from component data buffer location using 32-bit offset on the stack
    OP_CLOAD_I16,
    OP_CLOAD_I32,
    OP_CLOAD_I64,

    OP_LOAD_I8, // load value from 8-bit data buffer location using 32-bit IP index and 32-bit offset on the stack
    OP_LOAD_I16,
    OP_LOAD_I32,
    OP_LOAD_I64,

    OP_BLOAD_I8, // load value from 8-bit data buffer location using 8-bit imm IP index and 32-bit offset on the stack
    OP_BLOAD_I16,
    OP_BLOAD_I32,
    OP_BLOAD_I64,

    OP_CSTORE_I8, // store value on the stack into component data buffer location using 32-bit offset on the stack
    OP_CSTORE_I16,
    OP_CSTORE_I32,
    OP_CSTORE_I64,

    OP_STORE_I8, // store value on the stack into 8-bit data buffer location using 32-bit IP index and 32-bit offset on the stack
    OP_STORE_I16,
    OP_STORE_I32,
    OP_STORE_I64,

    OP_BSTORE_I8, // store value on the stack into 8-bit data buffer location using 8-bit imm IP index and 32-bit offset on the stack
    OP_BSTORE_I16,
    OP_BSTORE_I32,
    OP_BSTORE_I64,


    // arithmetic:
    // unary:
    OP_NEG_I,
    OP_NEG_L,
    OP_NEG_F,
    OP_NEG_D,

    OP_NOT, // boolean

    OP_BIT_INVERT_I,
    OP_BIT_INVERT_L,

    // binary:
    OP_MUL_I,
    OP_MUL_L,
    OP_MUL_F,
    OP_MUL_D,

    OP_DIV_I,
    OP_DIV_L,
    OP_DIV_F,
    OP_DIV_D,

    OP_MOD_I,
    OP_MOD_L,

    OP_ADD_I,
    OP_ADD_L,
    OP_ADD_F,
    OP_ADD_D,

    OP_SUB_I,
    OP_SUB_L,
    OP_SUB_F,
    OP_SUB_D,

    // bitwise:
    OP_SHIFT_LEFT_I,
    OP_SHIFT_LEFT_L,
    OP_SHIFT_RIGHT_I,
    OP_SHIFT_RIGHT_L,
    OP_BIT_AND_I,
    OP_BIT_AND_L,
    OP_BIT_XOR_I,
    OP_BIT_XOR_L,
    OP_BIT_OR_I,
    OP_BIT_OR_L,

    // logic:
    // push 1 if A > B, 0 if A == B, -1 if A < B
    OP_CMP_I,
    OP_CMP_UI,
    OP_CMP_L,
    OP_CMP_UL,
    OP_CMP_F,
    OP_CMP_D,

    OP_AND, // boolean
    OP_OR, // boolean


    // system:
    OP_BREQUEST_READ, // local IP index and INPUT port index
    OP_REQUEST_READ, // local IP index and INPUT port index
    OP_BREQUEST_WRITE, // local IP index and OUTPUT port index
    OP_REQUEST_WRITE, // local IP index and OUTPUT port index
    OP_CREATE_IP, // local IP index and packet size
    OP_SCREATE_IP, // local IP index and packet size
    OP_DROP_IP, // local IP index

    OP_YIELD,

    OP_IP_LENGTH, // local IP index
    OP_IP_COPY, // local dst IP index, local src IP index

} FBVMInstruction;


#endif // VM_OPCODE_H
