/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_CONTEXT_H
#define VM_CONTEXT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"
#include "vm/lib/databuffer.h"
#include "vm/core/input_output.h"
#include "vm/error.h"
#include "vm/core/component.h"




typedef struct _FBContextRequestData
{
    index_t informationPacketIndex;
    union {
    index_t portIndex;
    index_t packetSize;
    };
} FBContextRequestData;



#define STACK_DEPTH 256

typedef enum _FBContextState
{
    CONTEXT_INITIALIZED, // this status should never appear in run queue, the context is terminated if it has this status
    CONTEXT_RUNNING, // these are in run queue and fbContext_run() will execute the code
    CONTEXT_SUSPENDED, // these are in run queue but fbContext_run() will skip code execution
    CONTEXT_TERMINATED, // these should be removed from run queue, fbContext_run() will skip code execution

} FBContextState;


struct _FBContext
{
    // FBVMInstruction Pointer
    byte_t *ip;
    FBContextState state;
    // TODO: this is used by fbError, maybe move this out to error registry or something?
    FBContextStatus status;

    byte_t *code;
    byte_t *codeEnd;

    // TODO add element count for data and pool
    // and do CHECK_* calls in vm for offsets
    byte_t *componentData;
    byte_t *constantPool;
    byte_t* *packets;
    FBInformationPacketHandle *packetHandles;

    // TODO dynamically grow stack
    // i.e. use FBByteBuffer or add dedicated functions
    FBStackValue stack[ STACK_DEPTH ];
    FBStackValue *stackTop;

    FBContextRequestData contextRequestData;

    FBInputOutput *inputs;
    FBInputOutput *outputs;

    // TODO
    // remove these two and get their values from *component
    index_t inputsCount;
    index_t outputsCount;

    FBComponent *component;
};

#define FB_CONTEXT_RESET_REQUEST_DATA( context ) \
    { (context)->contextRequestData.informationPacketIndex = 0; (context)->contextRequestData.portIndex = 0; }

#define FB_CONTEXT_RESET_IP( context ) \
    { (context)->ip = (context)->code; }
#define FB_CONTEXT_CLEAR_IP( context ) \
    { (context)->ip = NULL; }

// Storage for component contexts is allocated and managed by fbNetwork_()
// so we don't have fbContext_create() function here

void fbContext_init_allocate( FBContext *context );
void fbContext_init_allocate_i( FBContext *context, index_t numberOfPackets );
void fbContext_init_allocate_iii( FBContext *context, index_t numberOfPackets, index_t numberOfInputs, index_t numberOfOutputs );
void fbContext_init_allocate_fromComponent( FBContext *context, FBComponent *component );

void fbContext_free( FBContext *context );

void fbContext_createData( FBContext *context, index_t dataSize );
void fbContext_setCode( FBContext *context, byte_t *codeBuffer, index_t codeSize );
void fbContext_setConstantPool( FBContext *context, byte_t *constantsBuffer );

void fbContext_reset( FBContext *context );

FBInputOutput* fbContext_getInputPort( FBContext *context, index_t portIndex );
FBInputOutput* fbContext_getOutputPort( FBContext *context, index_t portIndex );
bool fbContext_hasOpenPorts( FBContext *context );

void fbContext_setState( FBContext *context, FBContextState state );
FBContextState fbContext_getState( FBContext *context );

FBContextStatus fbContext_run( FBContext *context );


#ifdef __cplusplus
}
#endif

#endif // VM_CONTEXT_H
