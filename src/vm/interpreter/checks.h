/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_CHECKS_H
#define VM_CHECKS_H

#include <assert.h>
#include <stdio.h>

#define CHECK_ARRAY_BOUNDS( array, index ) \
    { assert( index < (array).count ); }
    // { assert( index < (array).count && index >= 0 ); }

#define CHECK_CODE_BOUNDS( code_start, code_end, ip ) \
    { assert( ip >= code_start && ip <= code_end ); }

#define CHECK_DESCRIPTOR_TYPE( descriptor, ttype ) \
    { assert( (descriptor).type == ttype ); }

#define CHECK_DESCRIPTOR_IS_BASE_TYPE( descriptor ) \
    { assert( IS_BASE_TYPE( (descriptor).type ) ); }

#define CHECK_STACK_NOT_FULL( context ) \
    { assert( (context)->stackTop < ( (context)->stack + STACK_DEPTH ) ); }

#define CHECK_STACK_NOT_EMPTY( context ) \
    { assert( (context)->stackTop > &(context)->stack[0] ); }

#define CHECK_STACK_EMPTY( context ) \
    ( assert( (context)->stackTop == &((context)->stack[0]) ) )

#define CHECK_CONTEXT_STATE( context, state_value ) \
    { assert( (context)->state == state_value ); }

#define CHECK_NOT_NULL( value ) \
    { assert( (value) != NULL ); }

#define CHECK_SAME( left, right ) \
    { assert( (left) == (right) ); }

#define CHECK_TRUE( cond ) \
    { assert( ! ! (cond) ); }

#endif // VM_CHECKS_H
