/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include <stdio.h>
#include <string.h>

#include "vm/interpreter/context.h"
#include "vm/interpreter/opcode.h"
#include "vm/interpreter/data_access.h"

#include "vm/lib/memory.h"
#include "vm/error.h"

#include "vm/interpreter/checks.h"
#include "vm/debug/vm_debug.h"


void fbContext_push64( FBContext *context, int64_t value ) {

    fbContext_push( context, value & 0x00000000FFFFFFFF );
    fbContext_push( context, value >> 32 );
}

void fbContext_push( FBContext *context, FBStackValue value ) {

    CHECK_STACK_NOT_FULL( context );

    *context->stackTop = value;
    context->stackTop++;
}

int64_t fbContext_pop64( FBContext *context ) {

    int64_t value = (int64_t)fbContext_pop( context ) << 32;
    value |= (uint32_t)fbContext_pop( context );

    return value;
}

FBStackValue fbContext_pop( FBContext *context ) {

    CHECK_STACK_NOT_EMPTY( context );

    context->stackTop--;
    return *context->stackTop;
}





FBContextStatus fbContext_run( FBContext *context ) {
#define READ_BYTE() (*context->ip++)
#define POP() fbContext_pop( context )
#define POP64() fbContext_pop64( context )
#define PUSH( value ) fbContext_push( context, value )
#define PUSH64( value ) fbContext_push64( context, value )
#define TO_IMMEDIATE_SIGNED( value ) (int16_t)value

    uint16_t immediate_unsigned_value;
    uint16_t immediate_offset;
    FBStackValue value;

    FBStackValue offset;
    FBStackValue index;
    FBStackValue lValue;
    FBStackValue rValue;

    FBFloatValue floatValue;
    FBFloatValue lFloatValue;
    FBFloatValue rFloatValue;

    FBDoubleValue doubleValue;
    FBDoubleValue lDoubleValue;
    FBDoubleValue rDoubleValue;

    int64_t long_value;
    int64_t l_long_value;
    int64_t r_long_value;

    for( ;; ) {

        immediate_unsigned_value = 0;
        immediate_offset = 0;

        // fbDebug_traceInstruction( context->ip, 0 );

        FBVMInstruction instruction = READ_BYTE();

        switch( instruction )
        {
            case OP_NOP:
                TRACE("OP_NOP");
                break;

            case OP_INVALID:
                TRACE("OP_INVALID");
                return STATUS_ERROR_INVALID_OP;
                break;

            case OP_END:
                TRACE("OP_END");
                return STATUS_CODE_END;
                break;
            case OP_TERMINATE:
                TRACE("OP_TERMINATE");
                return STATUS_REQUEST_TERMINATE;
                break;

            case OP_SWITCH:
                TRACE("OP_SWITCH");
                // key value to match
                value = POP();
                // lookup table offset in constant pool
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();

                byte_t *lookup_table = context->constantPool + immediate_offset;

                // default case jump offset
                int32_t jump_offset = fbData_get_int32( lookup_table );
                lookup_table += 4;
                int32_t pair_count = fbData_get_int32( lookup_table );
                lookup_table += 4;

                // TODO replace linear search with faster implementation
                for( int32_t idx = 0; idx < pair_count; idx++ ) {
                    int32_t key = fbData_get_int32( lookup_table );
                    lookup_table += 4;
                    int32_t offset_value = fbData_get_int32( lookup_table );
                    lookup_table += 4;

                    if( key == value ) {
                        jump_offset = offset_value;
                        break;
                    }
                }

                context->ip += jump_offset;
                break;


            case OP_JUMP:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                TRACE("OP_JUMP @%i", immediate_offset);
                context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                break;


            case OP_JUMP_IF_LESS_EQUAL:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                value = POP();
                TRACE("OP_JUMP_IF_LESS_EQUAL( %i ) @%i", value, immediate_offset);
                if( value <= 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;
            case OP_JUMP_IF_GREATER_EQUAL:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                value = POP();
                TRACE("OP_JUMP_IF_GREATER_EQUAL( %i ) @%i", value, immediate_offset);
                if( value >= 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;
            case OP_JUMP_IF_LESS:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                value = POP();
                TRACE("OP_JUMP_IF_LESS( %i ) @%i", value, immediate_offset);
                if( value < 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;
            case OP_JUMP_IF_GREATER:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                value = POP();
                TRACE("OP_JUMP_IF_GREATER( %i ) @%i", value, immediate_offset);
                if( value > 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;
            case OP_JUMP_IF_FALSE:
                // Fall through
            case OP_JUMP_IF_EQUAL:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                value = POP();
                TRACE("OP_JUMP_IF_EQUAL( %i ) @%i", value, immediate_offset);
                if( value == 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;
            case OP_JUMP_IF_NOT_EQUAL:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                value = POP();
                TRACE("OP_JUMP_IF_NOT_EQUAL( %i ) @%i", value, immediate_offset);
                if( value != 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;


            case OP_JUMP_IF_ZERO_2:
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                long_value = POP64();
                TRACE("OP_JUMP_IF_ZERO_2( %lli ) @%i", long_value, immediate_offset);
                if( long_value == 0 ) {
                    context->ip += TO_IMMEDIATE_SIGNED( immediate_offset );
                }
                break;


            case OP_SCONST_I8:
                immediate_offset = READ_BYTE() << 8;
                // Fall through
            case OP_BCONST_I8:
                immediate_offset |= READ_BYTE();
                value = fbData_get_int8( context->constantPool + immediate_offset );
                TRACE("OP_CONST_I8( %i ) @%i", value, immediate_offset);
                PUSH( value );
                break;

            case OP_SCONST_I16:
                immediate_offset = READ_BYTE() << 8;
                // Fall through
            case OP_BCONST_I16:
                immediate_offset |= READ_BYTE();
                value = fbData_get_int16( context->constantPool + immediate_offset );
                TRACE("OP_CONST_I16( %i ) @%i", value, immediate_offset);
                PUSH( value );
                break;

            case OP_SCONST_I32:
                immediate_offset = READ_BYTE() << 8;
                // Fall through
            case OP_BCONST_I32:
                immediate_offset |= READ_BYTE();
                value = fbData_get_int32( context->constantPool + immediate_offset );
                TRACE("OP_CONST_I32( %i ) @%i", value, immediate_offset);
                PUSH( value );
                break;

            case OP_SCONST_I64:
                immediate_offset = READ_BYTE() << 8;
                // Fall through
            case OP_BCONST_I64:
                immediate_offset |= READ_BYTE();
                long_value = fbData_get_int64( context->constantPool + immediate_offset );
                TRACE("OP_CONST_I64( %i ) @%i", value, immediate_offset);
                PUSH64( long_value );
                break;


            case OP_PCONST_I8:
                offset = POP();
                value = fbData_get_int8( context->constantPool + (uint16_t) offset );
                TRACE("OP_PCONST_I8( %i ) @%i", value, offset);
                PUSH( value );
                break;

            case OP_PCONST_I16:
                offset = POP();
                value = fbData_get_int16( context->constantPool + (uint16_t) offset );
                TRACE("OP_PCONST_I16( %i ) @%i", value, offset);
                PUSH( value );
                break;

            case OP_PCONST_I32:
                offset = POP();
                value = fbData_get_int32( context->constantPool + (uint16_t) offset );
                TRACE("OP_PCONST_I32( %i ) @%i", value, offset);
                PUSH( value );
                break;

            case OP_PCONST_I64:
                offset = POP();
                long_value = fbData_get_int64( context->constantPool + (uint16_t) offset );
                TRACE("OP_PCONST_I64( %lli ) @%i", long_value, offset);
                PUSH64( long_value );
                break;

            case OP_CONST_0:
                TRACE("OP_CONST_0");
                PUSH( 0 );
                break;
            case OP_CONST_1:
                TRACE("OP_CONST_1");
                PUSH( 1 );
                break;
            case OP_CONST_M1:
                TRACE("OP_CONST_M1");
                PUSH( -1 );
                break;

            case OP_SPUSH:
                immediate_unsigned_value = READ_BYTE() << 8;
                immediate_unsigned_value |= READ_BYTE();
                TRACE("OP_SPUSH %i", immediate_unsigned_value);
                PUSH( TO_IMMEDIATE_SIGNED( immediate_unsigned_value ) );
                break;
            case OP_BPUSH:
                TRACE("OP_BPUSH ...");
                PUSH( TO_IMMEDIATE_SIGNED( (int8_t) READ_BYTE() ) );
                break;


            case OP_DUP:
                value = POP();
                TRACE("OP_DUP %i", value);
                PUSH( value );
                PUSH( value );
                break;
            case OP_DUP_2: {
                FBStackValue value1 = POP();
                FBStackValue value2 = POP();
                TRACE("OP_DUP_2 %i, %i", value1, value2);
                PUSH( value2 );
                PUSH( value1 );
                PUSH( value2 );
                PUSH( value1 );
            }
                break;


            case OP_CLOAD_I8:
                offset = POP();
                value = fbData_get_int8( context->componentData + offset );
                TRACE("OP_CLOAD_I8 @%i -> %i ", offset, value);
                PUSH( value );
                break;
            case OP_CLOAD_I16:
                offset = POP();
                value = fbData_get_int16( context->componentData + offset );
                TRACE("OP_CLOAD_I16 @%i -> %i ", offset, value);
                PUSH( value );
                break;
            case OP_CLOAD_I32:
                offset = POP();
                value = fbData_get_int32( context->componentData + offset );
                TRACE("OP_CLOAD_I32 @%i -> %i ", offset, value);
                PUSH( value );
                break;
            case OP_CLOAD_I64:
                offset = POP();
                long_value = fbData_get_int64( context->componentData + offset );
                TRACE("OP_CLOAD_I64 @%i -> %lli ", offset, long_value);
                PUSH64( long_value );
                break;

            case OP_LOAD_I8:
                // index
                value = POP();
                offset = POP();
                lValue = fbData_get_int8( context->packets[ value ] + offset );
                TRACE("OP_LOAD_I8 [%i] @%i -> %i ", value, offset, lValue);
                PUSH( lValue );
                break;
            case OP_LOAD_I16:
                // index
                value = POP();
                offset = POP();
                lValue = fbData_get_int16( context->packets[ value ] + offset );
                TRACE("OP_LOAD_I16 [%i] @%i -> %i ", value, offset, lValue);
                PUSH( lValue );
                break;
            case OP_LOAD_I32:
                // index
                value = POP();
                offset = POP();
                lValue = fbData_get_int32( context->packets[ value ] + offset );
                TRACE("OP_LOAD_I32 [%i] @%i -> %i ", value, offset, lValue);
                PUSH( lValue );
                break;
            case OP_LOAD_I64:
                // index
                value = POP();
                offset = POP();
                long_value = fbData_get_int64( context->packets[ value ] + offset );
                TRACE("OP_LOAD_I64 [%i] @%i -> %lli ", value, offset, long_value);
                PUSH64( long_value );
                break;

            case OP_BLOAD_I8:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                value = fbData_get_int8( context->packets[ immediate_unsigned_value ] + offset );
                TRACE("OP_BLOAD_I8 [%i] @%i -> %i ", immediate_unsigned_value, offset, value);
                PUSH( value );
                break;
            case OP_BLOAD_I16:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                value = fbData_get_int16( context->packets[ immediate_unsigned_value ] + offset );
                TRACE("OP_BLOAD_I16 [%i] @%i -> %i ", immediate_unsigned_value, offset, value);
                PUSH( value );
                break;
            case OP_BLOAD_I32:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                value = fbData_get_int32( context->packets[ immediate_unsigned_value ] + offset );
                TRACE("OP_BLOAD_I32 [%i] @%i -> %i ", immediate_unsigned_value, offset, value);
                PUSH( value );
                break;
            case OP_BLOAD_I64:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                long_value = fbData_get_int64( context->packets[ immediate_unsigned_value ] + offset );
                TRACE("OP_BLOAD_I64 [%i] @%i -> %lli ", immediate_unsigned_value, offset, long_value);
                PUSH64( long_value );
                break;

            case OP_CSTORE_I8:
                offset = POP();
                value = POP();
                TRACE("OP_CSTORE_I8 @%i -> %i ", offset, value);
                fbData_set_int8( context->componentData + offset, value );
                break;
            case OP_CSTORE_I16:
                offset = POP();
                value = POP();
                TRACE("OP_CSTORE_I16 @%i -> %i ", offset, value);
                fbData_set_int16( context->componentData + offset, value );
                break;
            case OP_CSTORE_I32:
                offset = POP();
                value = POP();
                TRACE("OP_CSTORE_I32 @%i -> %i ", offset, value);
                fbData_set_int32( context->componentData + offset, value );
                break;
            case OP_CSTORE_I64:
                offset = POP();
                long_value = POP64();
                TRACE("OP_CSTORE_I64 @%i -> %lli ", offset, long_value);
                fbData_set_int64( context->componentData + offset, long_value );
                break;

            case OP_STORE_I8:
                index = POP();
                offset = POP();
                value = POP();
                TRACE("OP_STORE_I8 [%i] @%i -> %i ", index, offset, value);
                fbData_set_int8( context->packets[ index ] + offset, value );
                break;
            case OP_STORE_I16:
                index = POP();
                offset = POP();
                value = POP();
                TRACE("OP_STORE_I16 [%i] @%i -> %i ", index, offset, value);
                fbData_set_int16( context->packets[ index ] + offset, value );
                break;
            case OP_STORE_I32:
                index = POP();
                offset = POP();
                value = POP();
                TRACE("OP_STORE_I32 [%i] @%i -> %i ", index, offset, value);
                fbData_set_int32( context->packets[ index ] + offset, value );
                break;
            case OP_STORE_I64:
                index = POP();
                offset = POP();
                long_value = POP64();
                TRACE("OP_STORE_I64 [%i] @%i -> %lli ", index, offset, long_value);
                fbData_set_int64( context->packets[ index ] + offset, long_value );
                break;

            case OP_BSTORE_I8:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                value = POP();
                TRACE("OP_BSTORE_I8 [%i] @%i -> %i ", immediate_unsigned_value, offset, value);
                fbData_set_int8( context->packets[ immediate_unsigned_value ] + offset, value );
                break;
            case OP_BSTORE_I16:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                value = POP();
                TRACE("OP_BSTORE_I16 [%i] @%i -> %i ", immediate_unsigned_value, offset, value);
                fbData_set_int16( context->packets[ immediate_unsigned_value ] + offset, value );
                break;
            case OP_BSTORE_I32:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                value = POP();
                TRACE("OP_BSTORE_I32 [%i] @%i -> %i ", immediate_unsigned_value, offset, value);
                fbData_set_int32( context->packets[ immediate_unsigned_value ] + offset, value );
                break;
            case OP_BSTORE_I64:
                immediate_unsigned_value = READ_BYTE();
                offset = POP();
                long_value = POP64();
                TRACE("OP_BSTORE_I64 [%i] @%i -> %lli ", immediate_unsigned_value, offset, long_value);
                fbData_set_int64( context->packets[ immediate_unsigned_value ] + offset, long_value );
                break;


            case OP_NEG_I:
                value = POP();
                TRACE("OP_NEG_I %i ", value);
                PUSH( - value );
                break;
            case OP_NEG_L:
                long_value = POP64();
                TRACE("OP_NEG_L %lli ", long_value);
                PUSH64( - long_value );
                break;
            case OP_NEG_F:
                floatValue.asInt32 = POP();
                TRACE("OP_NEG_F %f ", floatValue.asFloat);
                floatValue.asFloat = - floatValue.asFloat;
                PUSH( floatValue.asInt32 );
                break;
            case OP_NEG_D:
                doubleValue.asInt64 = POP64();
                TRACE("OP_NEG_D %f ", doubleValue.asDouble);
                doubleValue.asDouble = - doubleValue.asDouble;
                PUSH64( doubleValue.asInt64 );
                break;

            case OP_NOT:
                value = POP();
                TRACE("OP_NOT %i ", value);
                PUSH( ! value );
                break;

            case OP_BIT_INVERT_I:
                value = POP();
                TRACE("OP_BIT_INVERT_I %i ", value);
                PUSH( ~ value );
                break;
            case OP_BIT_INVERT_L:
                long_value = POP64();
                TRACE("OP_BIT_INVERT_L %lli ", long_value);
                PUSH64( ~ long_value );
                break;


            case OP_MUL_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_MUL_I %i, %i ", lValue, rValue);
                value = lValue * rValue;
                PUSH( value );
                break;
            case OP_MUL_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_MUL_L %lli, %lli ", l_long_value, r_long_value);
                long_value = l_long_value * r_long_value;
                PUSH64( long_value );
                break;
            case OP_MUL_F:
                rFloatValue.asInt32 = POP();
                lFloatValue.asInt32 = POP();
                TRACE("OP_MUL_F %f, %f ", lFloatValue.asFloat, rFloatValue.asFloat);
                floatValue.asFloat = lFloatValue.asFloat * rFloatValue.asFloat;
                PUSH( floatValue.asInt32 );
                break;
            case OP_MUL_D:
                rDoubleValue.asInt64 = POP64();
                lDoubleValue.asInt64 = POP64();
                TRACE("OP_MUL_D %f, %f ", lDoubleValue.asDouble, rDoubleValue.asDouble);
                doubleValue.asDouble = lDoubleValue.asDouble * rDoubleValue.asDouble;
                PUSH64( doubleValue.asInt64 );
                break;

            case OP_DIV_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_DIV_I %i, %i ", lValue, rValue);
                value = lValue / rValue;
                PUSH( value );
                break;
            case OP_DIV_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_DIV_L %lli, %lli ", l_long_value, r_long_value);
                long_value = l_long_value / r_long_value;
                PUSH64( long_value );
                break;
            case OP_DIV_F:
                rFloatValue.asInt32 = POP();
                lFloatValue.asInt32 = POP();
                TRACE("OP_DIV_F %f, %f ", lFloatValue.asFloat, rFloatValue.asFloat);
                floatValue.asFloat = lFloatValue.asFloat / rFloatValue.asFloat;
                PUSH( floatValue.asInt32 );
                break;
            case OP_DIV_D:
                rDoubleValue.asInt64 = POP64();
                lDoubleValue.asInt64 = POP64();
                TRACE("OP_DIV_D %f, %f ", lDoubleValue.asDouble, rDoubleValue.asDouble);
                doubleValue.asDouble = lDoubleValue.asDouble / rDoubleValue.asDouble;
                PUSH64( doubleValue.asInt64 );
                break;

            case OP_MOD_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_MOD_I %i, %i ", lValue, rValue);
                value = lValue % rValue;
                PUSH( value );
                break;
            case OP_MOD_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_MOD_L %lli, %lli ", l_long_value, r_long_value);
                long_value = l_long_value % r_long_value;
                PUSH64( long_value );
                break;

            case OP_ADD_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_ADD_I %i, %i ", lValue, rValue);
                value = lValue + rValue;
                PUSH( value );
                break;
            case OP_ADD_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_ADD_L %lli, %lli ", l_long_value, r_long_value);
                long_value = l_long_value + r_long_value;
                PUSH64( long_value );
                break;
            case OP_ADD_F:
                rFloatValue.asInt32 = POP();
                lFloatValue.asInt32 = POP();
                TRACE("OP_ADD_F %f, %f ", lFloatValue.asFloat, rFloatValue.asFloat);
                floatValue.asFloat = lFloatValue.asFloat + rFloatValue.asFloat;
                PUSH( floatValue.asInt32 );
                break;
            case OP_ADD_D:
                rDoubleValue.asInt64 = POP64();
                lDoubleValue.asInt64 = POP64();
                TRACE("OP_ADD_D %f, %f ", lDoubleValue.asDouble, rDoubleValue.asDouble);
                doubleValue.asDouble = lDoubleValue.asDouble + rDoubleValue.asDouble;
                PUSH64( doubleValue.asInt64 );
                break;

            case OP_SUB_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_SUB_I %i, %i ", lValue, rValue);
                value = lValue - rValue;
                PUSH( value );
                break;
            case OP_SUB_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_SUB_L %lli, %lli ", l_long_value, r_long_value);
                long_value = l_long_value - r_long_value;
                PUSH64( long_value );
                break;
            case OP_SUB_F:
                rFloatValue.asInt32 = POP();
                lFloatValue.asInt32 = POP();
                TRACE("OP_SUB_F %f, %f ", lFloatValue.asFloat, rFloatValue.asFloat);
                floatValue.asFloat = lFloatValue.asFloat - rFloatValue.asFloat;
                PUSH( floatValue.asInt32 );
                break;
            case OP_SUB_D:
                rDoubleValue.asInt64 = POP64();
                lDoubleValue.asInt64 = POP64();
                TRACE("OP_SUB_D %f, %f ", lDoubleValue.asDouble, rDoubleValue.asDouble);
                doubleValue.asDouble = lDoubleValue.asDouble - rDoubleValue.asDouble;
                PUSH64( doubleValue.asInt64 );
                break;


            case OP_SHIFT_LEFT_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_SHIFT_LEFT_I %i, %i ", lValue, rValue);
                value = lValue << rValue;
                PUSH( value );
                break;
            case OP_SHIFT_LEFT_L:
                rValue = POP();
                l_long_value = POP64();
                TRACE("OP_SHIFT_LEFT_L %lli, %i ", l_long_value, rValue);
                long_value = l_long_value << rValue;
                PUSH64( long_value );
                break;
            case OP_SHIFT_RIGHT_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_SHIFT_RIGHT_I %i, %i ", lValue, rValue);
                value = lValue >> rValue;
                PUSH( value );
                break;
            case OP_SHIFT_RIGHT_L:
                rValue = POP();
                l_long_value = POP64();
                TRACE("OP_SHIFT_RIGHT_L %lli, %i ", l_long_value, rValue);
                long_value = l_long_value >> rValue;
                PUSH64( long_value );
                break;

            case OP_BIT_AND_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_BIT_AND_I %i, %i ", lValue, rValue);
                value = lValue & rValue;
                PUSH( value );
                break;
            case OP_BIT_AND_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_BIT_AND_L %lli, %lli ", l_long_value, l_long_value);
                long_value = l_long_value & r_long_value;
                PUSH64( long_value );
                break;
            case OP_BIT_XOR_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_BIT_XOR_I %i, %i ", lValue, rValue);
                value = lValue ^ rValue;
                PUSH( value );
                break;
            case OP_BIT_XOR_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_BIT_XOR_L %lli, %lli ", l_long_value, l_long_value);
                long_value = l_long_value ^ r_long_value;
                PUSH64( long_value );
                break;
            case OP_BIT_OR_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_BIT_OR_I %i, %i ", lValue, rValue);
                value = lValue | rValue;
                PUSH( value );
                break;
            case OP_BIT_OR_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_BIT_OR_L %lli, %lli ", l_long_value, l_long_value);
                long_value = l_long_value | r_long_value;
                PUSH64( long_value );
                break;


            case OP_CMP_I:
                rValue = POP();
                lValue = POP();
                TRACE("OP_CMP_I %i, %i ", lValue, rValue);
                if( lValue > rValue )
                    PUSH( 1 );
                else if( lValue == rValue )
                    PUSH( 0 );
                else if( lValue < rValue )
                    PUSH( -1 );
                break;
            case OP_CMP_UI:
                rValue = POP();
                lValue = POP();
                TRACE("OP_CMP_UI %i, %i ", lValue, rValue);
                if( (uint32_t)lValue > (uint32_t)rValue )
                    PUSH( 1 );
                else if( lValue == rValue )
                    PUSH( 0 );
                else if( (uint32_t)lValue < (uint32_t)rValue )
                    PUSH( -1 );
                break;
            case OP_CMP_L:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_CMP_L %lli, %lli ", l_long_value, l_long_value);
                if( l_long_value > r_long_value )
                    PUSH( 1 );
                else if( l_long_value == r_long_value )
                    PUSH( 0 );
                else if( l_long_value < r_long_value )
                    PUSH( -1 );
                break;
            case OP_CMP_UL:
                r_long_value = POP64();
                l_long_value = POP64();
                TRACE("OP_CMP_UL %lli, %lli ", l_long_value, l_long_value);
                if( (uint64_t)l_long_value > (uint64_t)r_long_value )
                    PUSH( 1 );
                else if( l_long_value == r_long_value )
                    PUSH( 0 );
                else if( (uint64_t)l_long_value < (uint64_t)r_long_value )
                    PUSH( -1 );
                break;
            case OP_CMP_F:
                rFloatValue.asInt32 = POP();
                lFloatValue.asInt32 = POP();
                TRACE("OP_CMP_F %f, %f ", lFloatValue.asFloat, rFloatValue.asFloat);
                if( lFloatValue.asFloat > rFloatValue.asFloat )
                    PUSH( 1 );
                else if( lFloatValue.asFloat == rFloatValue.asFloat )
                    PUSH( 0 );
                else if( lFloatValue.asFloat < rFloatValue.asFloat )
                    PUSH( -1 );
                break;
            case OP_CMP_D:
                rDoubleValue.asInt64 = POP64();
                lDoubleValue.asInt64 = POP64();
                TRACE("OP_CMP_D %f, %f ", lDoubleValue.asDouble, rDoubleValue.asDouble);
                if( lDoubleValue.asDouble > rDoubleValue.asDouble )
                    PUSH( 1 );
                else if( lDoubleValue.asDouble == rDoubleValue.asDouble )
                    PUSH( 0 );
                else if( lDoubleValue.asDouble < rDoubleValue.asDouble )
                    PUSH( -1 );
                break;

            case OP_AND:
                rValue = POP();
                lValue = POP();
                TRACE("OP_AND %i, %i ", lValue, rValue);
                value = lValue && rValue;
                PUSH( value );
                break;
            case OP_OR:
                rValue = POP();
                lValue = POP();
                TRACE("OP_OR %i, %i ", lValue, rValue);
                value = lValue || rValue;
                PUSH( value );
                break;


            case OP_BREQUEST_READ:
                context->contextRequestData.informationPacketIndex = READ_BYTE();
                context->contextRequestData.portIndex = READ_BYTE();
                TRACE("OP_BREQUEST_READ IP %zu, P %zu ", context->contextRequestData.informationPacketIndex, context->contextRequestData.portIndex);
                return STATUS_REQUEST_READ;
                break;
            case OP_REQUEST_READ:
                context->contextRequestData.informationPacketIndex = POP();
                context->contextRequestData.portIndex = POP();
                TRACE("OP_REQUEST_READ IP %zu, P %zu ", context->contextRequestData.informationPacketIndex, context->contextRequestData.portIndex);
                return STATUS_REQUEST_READ;
                break;
            case OP_BREQUEST_WRITE:
                context->contextRequestData.informationPacketIndex = READ_BYTE();
                context->contextRequestData.portIndex = READ_BYTE();
                TRACE("OP_BREQUEST_WRITE IP %zu, P %zu ", context->contextRequestData.informationPacketIndex, context->contextRequestData.portIndex);
                return STATUS_REQUEST_WRITE;
                break;
            case OP_REQUEST_WRITE:
                context->contextRequestData.informationPacketIndex = POP();
                context->contextRequestData.portIndex = POP();
                TRACE("OP_REQUEST_WRITE IP %zu, P %zu ", context->contextRequestData.informationPacketIndex, context->contextRequestData.portIndex);
                return STATUS_REQUEST_WRITE;
                break;

            case OP_SCREATE_IP:
                // IP index
                immediate_unsigned_value = READ_BYTE();
                // IP size
                value = POP();
                TRACE("OP_SCREATE_IP %i, [%i] ", immediate_unsigned_value, value);
                context->contextRequestData.informationPacketIndex = immediate_unsigned_value;
                context->contextRequestData.packetSize = value;
                return STATUS_REQUEST_CREATE;
            case OP_CREATE_IP:
                // IP index
                immediate_unsigned_value = READ_BYTE();
                // IP size
                immediate_offset = READ_BYTE() << 8;
                immediate_offset |= READ_BYTE();
                TRACE("OP_CREATE_IP %i, [%i] ", immediate_unsigned_value, immediate_offset);
                context->contextRequestData.informationPacketIndex = immediate_unsigned_value;
                context->contextRequestData.packetSize = immediate_offset;
                return STATUS_REQUEST_CREATE;
            case OP_DROP_IP:
                // IP index
                immediate_unsigned_value = READ_BYTE();
                TRACE("OP_DROP_IP %i", immediate_unsigned_value);
                context->contextRequestData.informationPacketIndex = immediate_unsigned_value;
                context->contextRequestData.packetSize = 0;
                return STATUS_REQUEST_DROP;


            case OP_YIELD:
                TRACE("OP_YIELD");
                return STATUS_YIELD;
                break;


            case OP_IP_LENGTH:
                // IP index
                immediate_unsigned_value = READ_BYTE();
                value = fbInformationPacket_getDataBufferSize( context->packetHandles[ immediate_unsigned_value ] );
                TRACE("OP_IP_LENGTH( %i ) = %i", immediate_unsigned_value, value);
                PUSH( value );
                break;
                break;


            case OP_IP_COPY:
                // IP dst index
                immediate_unsigned_value = READ_BYTE();
                // IP src index
                immediate_offset = READ_BYTE();
                // data length
                value = POP();
                // src offset
                rValue = POP();
                // dst offset
                lValue = POP();
                // do copy
                value = fbInformationPacket_copyRange(
                    context->packetHandles[ immediate_unsigned_value ] , lValue,
                    context->packetHandles[ immediate_offset ], rValue,
                    value );
                TRACE("OP_IP_COPY #%i, %i <- #%i, %i [%i]", immediate_unsigned_value, lValue, immediate_offset, rValue, value);
                PUSH( value );
                break;


            default:
                TRACE("STATUS_ERROR_UNKNOWN_OP -> terminate");
                return STATUS_ERROR_UNKNOWN_OP;
                break;
        }


        if( context->ip < context->code || context->ip > context->codeEnd ) {
            TRACE("STATUS_ERROR_OVERRUN -> terminate");
            return STATUS_ERROR_OVERRUN;
        }
    }

    TRACE("STATUS_REQUEST_TERMINATE -> terminate");
    return STATUS_REQUEST_TERMINATE;
}
