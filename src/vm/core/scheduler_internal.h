/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_SCHEDULER_INTERNAL_H
#define VM_SCHEDULER_INTERNAL_H


#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"
#include "vm/interpreter/context.h"
#include "vm/error.h"

void _internal_fbScheduler_serviceRequests( FBContext *context, FBContextStatus runStatus );
void _internal_fbScheduler_serviceCodeSwitch( FBContext *context );
FBContextStatus _internal_fbScheduler_runSlice( FBContext *context );

#ifdef __cplusplus
}
#endif

#endif // VM_SCHEDULER_INTERNAL_H
