/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_INFORMATION_PACKET_H
#define VM_INFORMATION_PACKET_H

#include "vm/interpreter/types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _FBInformationPacketTuple
{
    FBInformationPacketHandle handle;
    byte_t *buffer;
} FBInformationPacketTuple;


FBInformationPacketHandle fbInformationPacket_create( index_t capacity );
void fbInformationPacket_drop( FBInformationPacketHandle ip );
byte_t* fbInformationPacket_getDataBuffer( FBInformationPacketHandle ip );
index_t fbInformationPacket_getDataBufferSize( FBInformationPacketHandle ip );

FBInformationPacketTuple fbInformationPacket_create_tuple( index_t capacity );

index_t fbInformationPacket_copyRange( FBInformationPacketHandle dst_ip, index_t dst_offset, FBInformationPacketHandle src_ip, index_t src_offset, index_t length );
index_t fbInformationPacket_fillRange( FBInformationPacketHandle ip, index_t offset, index_t length, byte_t value );

#ifdef __cplusplus
}
#endif

#endif // VM_INFORMATION_PACKET_H
