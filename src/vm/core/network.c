/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/network.h"

#include "vm/interpreter/context.h"
#include "vm/lib/memory.h"

void _fbNetwork_initNativeIO( FBNetworkNativeIO *nativeio ) {

    nativeio->port = FB_GROW_ARRAY( NULL, FBInputOutput, 1 );
    fbInputOutput_init( nativeio->port, NULL );
}
void _fbNetwork_freeNativeIO( FBNetworkNativeIO *nativeio ) {

    FB_FREE_ARRAY( nativeio->port );
}

FBNetworkContext *fbNetwork_create(
    size_t contextCount, size_t connectionCount, 
    size_t inputCount, size_t outputCount, 
    size_t nativeInputCount, size_t nativeOutputCount ) {

    FBNetworkContext *networkContext = FB_GROW_ARRAY( NULL, FBNetworkContext, 1 );
    fbNetwork_clear( networkContext );

    fbNetwork_init( networkContext, 
        contextCount, connectionCount, 
        inputCount, outputCount, 
        nativeInputCount, nativeOutputCount );

    return networkContext;
}

void fbNetwork_init( FBNetworkContext *networkContext,
    size_t contextCount, size_t connectionCount, 
    size_t inputCount, size_t outputCount, 
    size_t nativeInputCount, size_t nativeOutputCount ) {

    if( contextCount ) {
        networkContext->allContextsCount = contextCount;
        networkContext->allContexts = FB_GROW_ARRAY( NULL, FBContext, contextCount );
    }

    if( connectionCount ) {
        networkContext->allConnectionsCount = connectionCount;
        networkContext->allConnections = FB_GROW_ARRAY( NULL, FBConnection, connectionCount );
    }

    if( inputCount ) {
        networkContext->networkInputsCount = inputCount;
        networkContext->networkInputs = FB_GROW_ARRAY( NULL, FBInputOutput *, inputCount );
        FB_CLEAR_ARRAY( networkContext->networkInputs, FBInputOutput *, inputCount );
    }

    if( outputCount ) {
        networkContext->networkOutputsCount = outputCount;
        networkContext->networkOutputs = FB_GROW_ARRAY( NULL, FBInputOutput *, outputCount );
        FB_CLEAR_ARRAY( networkContext->networkOutputs, FBInputOutput *, outputCount );
    }

    if( nativeInputCount ) {
        networkContext->nativeInputsCount = nativeInputCount;
        networkContext->nativeInputs = FB_GROW_ARRAY( NULL, FBNetworkNativeIO, nativeInputCount );
        for( size_t idx = 0; idx < nativeInputCount; idx++ ) {
            _fbNetwork_initNativeIO( &networkContext->nativeInputs[ idx ] );
        }
    }

    if( nativeOutputCount ) {
        networkContext->nativeOutputsCount = nativeOutputCount;
        networkContext->nativeOutputs = FB_GROW_ARRAY( NULL, FBNetworkNativeIO, nativeOutputCount );
        for( size_t idx = 0; idx < nativeOutputCount; idx++ ) {
            _fbNetwork_initNativeIO( &networkContext->nativeOutputs[ idx ] );
        }
    }
}

void fbNetwork_destroy( FBNetworkContext *context ) {

    fbNetwork_free( context );

    FB_FREE_ARRAY( context );
}

void fbNetwork_free( FBNetworkContext *context ) {

    FB_FREE_ARRAY( context->allConnections );
    FB_FREE_ARRAY( context->allContexts );
    FB_FREE_ARRAY( context->networkInputs );
    FB_FREE_ARRAY( context->networkOutputs );
    for( size_t idx = 0; idx < context->nativeInputsCount; idx++ ) {
        _fbNetwork_freeNativeIO( &context->nativeInputs[ idx ] );
    }
    FB_FREE_ARRAY( context->nativeInputs );
    for( size_t idx = 0; idx < context->nativeOutputsCount; idx++ ) {
        _fbNetwork_freeNativeIO( &context->nativeOutputs[ idx ] );
    }
    FB_FREE_ARRAY( context->nativeOutputs );

    fbNetwork_clear( context );
}

void fbNetwork_clear( FBNetworkContext *context ) {

    FB_CLEAR( context, FBNetworkContext );
}

FBContext* fbNetwork_getContext( FBNetworkContext *context, index_t contextIndex ) {

    if( context->allContextsCount <= contextIndex ) {
        return NULL;
    }

    return &context->allContexts[ contextIndex ];
}

FBConnection* fbNetwork_getConnection( FBNetworkContext *context, index_t connectionIndex ) {

    if( context->allConnectionsCount <= connectionIndex ) {
        return NULL;
    }

    return &context->allConnections[ connectionIndex ];
}

FBInputOutput** fbNetwork_getInputPtr( FBNetworkContext *context, index_t inputIndex ) {

    if( context->networkInputsCount <= inputIndex ) {
        return NULL;
    }

    return &context->networkInputs[ inputIndex ];
}

FBInputOutput** fbNetwork_getOutputPtr( FBNetworkContext *context, index_t outputIndex ) {

    if( context->networkOutputsCount <= outputIndex ) {
        return NULL;
    }

    return &context->networkOutputs[ outputIndex ];
}

FBInputOutput* fbNetwork_getInput( FBNetworkContext *context, index_t inputIndex ) {

    FBInputOutput* *inputPtr = fbNetwork_getInputPtr( context, inputIndex );

    if( inputPtr == NULL ) {
        return NULL;
    }

    return *inputPtr;
}
FBInputOutput* fbNetwork_getOutput( FBNetworkContext *context, index_t outputIndex ) {

    FBInputOutput* *outputPtr = fbNetwork_getOutputPtr( context, outputIndex );

    if( outputPtr == NULL ) {
        return NULL;
    }

    return *outputPtr;
}

FBNetworkNativeIO* fbNetwork_getNativeInput( FBNetworkContext *context, index_t inputIndex ) {

    if( context->nativeInputsCount <= inputIndex ) {
        return NULL;
    }

    return &context->nativeInputs[ inputIndex ];
}

FBNetworkNativeIO* fbNetwork_getNativeOutput( FBNetworkContext *context, index_t outputIndex ) {

    if( context->nativeOutputsCount <= outputIndex ) {
        return NULL;
    }

    return &context->nativeOutputs[ outputIndex ];
}

bool fbNetwork_setInput( FBNetworkContext *context, index_t inputIndex, FBInputOutput* input ) {

    if( context->networkInputsCount <= inputIndex ) {
        return false;
    }

    context->networkInputs[ inputIndex ] = input;

    return true;
}
bool fbNetwork_setOutput( FBNetworkContext *context, index_t outputIndex, FBInputOutput* output ) {

    if( context->networkOutputsCount <= outputIndex ) {
        return false;
    }

    context->networkOutputs[ outputIndex ] = output;

    return true;
}

bool fbNetwork_hasRunningContexts( FBNetworkContext *networkContext ) {

    for( index_t idx = 0; idx < networkContext->allContextsCount; idx++ ) {

        FBContext *context = &networkContext->allContexts[ idx ];

        if( fbContext_getState( context ) == CONTEXT_RUNNING ) {
            return true;
        }
    }

    return false;
}

bool fbNetwork_hasSuspendedContexts( FBNetworkContext *networkContext ) {

    for( index_t idx = 0; idx < networkContext->allContextsCount; idx++ ) {

        FBContext *context = &networkContext->allContexts[ idx ];

        if( fbContext_getState( context ) == CONTEXT_SUSPENDED ) {
            return true;
        }
    }

    return false;
}
