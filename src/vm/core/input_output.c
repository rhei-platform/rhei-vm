/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/input_output.h"
#include "vm/core/information_packet.h"
#include "vm/interpreter/context.h"
#include "vm/error.h"


void fbInputOutput_init( FBInputOutput *port, FBContext *owner ) {

    port->closed = true;
    port->connection = NULL;
    port->owner = owner;
}

void fbInputOutput_close( FBInputOutput *port ) {

    port->closed = true;
}

void fbInputOutput_closeConnection( FBInputOutput *port ) {

    fbInputOutput_close( port );

    if( port->connection != NULL && (! fbConnection_isTerminated( port->connection ) ) ) {
        fbConnection_terminate( port->connection );
    }
}

bool fbInputOutput_isClosed( FBInputOutput *port ) {

    return port->closed;
}

bool fbInputOutput_isOpen( FBInputOutput *port ) {

    return ! port->closed;
}



// INPUT port
FBInformationPacketHandle fbInput_read( FBInputOutput *port ) {

    if( fbInputOutput_isClosed( port ) ) {
        // throw new IllegalStateException("Can't read from closed connection.");
        fbError_setStatus( port->owner, STATUS_ERROR_PORT_CLOSED );
        return FB_INVALID_PACKET_HANDLE;
    }

    return fbConnection_read( port->connection );
}

bool fbInput_cannotRead( FBInputOutput *port ) {

    return fbInputOutput_isClosed( port ) || fbConnection_isTerminated( port->connection ) || fbConnection_isEmpty( port->connection );
}

bool fbInput_canRead( FBInputOutput *port ) {

    return fbInputOutput_isOpen( port ) && ! fbConnection_isTerminated( port->connection ) && ! fbConnection_isEmpty( port->connection );
}

// OUTPUT port
bool fbOutput_write( FBInputOutput *port, FBInformationPacketHandle ip ) {

    if( fbInputOutput_isClosed( port ) ) {
        // throw new IllegalStateException("Can't write to closed connection.");
        fbError_setStatus( port->owner, STATUS_ERROR_PORT_CLOSED );
        return false;
    }

    return fbConnection_write( port->connection, ip );
}

bool fbOutput_cannotWrite( FBInputOutput *port ) {

    return fbInputOutput_isClosed( port ) || fbConnection_isTerminated( port->connection ) || fbConnection_isFull( port->connection );
}

bool fbOutput_canWrite( FBInputOutput *port ) {

    return fbInputOutput_isOpen( port ) && ! fbConnection_isTerminated( port->connection ) && ! fbConnection_isFull( port->connection );
}