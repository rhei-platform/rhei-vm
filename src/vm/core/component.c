/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/component.h"
#include "vm/lib/memory.h"

FBComponent* fbComponent_create( index_t setupCodeSize, index_t loopCodeSize, index_t constantPoolSize ) {

    FBComponent *component = FB_GROW_ARRAY( NULL, FBComponent, 1 );
    FB_CLEAR_ARRAY( component, FBComponent, 1 );

    component->setupCode = FB_GROW_ARRAY( NULL, byte_t, setupCodeSize );
    component->setupCodeSize = setupCodeSize;
    component->loopCode = FB_GROW_ARRAY( NULL, byte_t, loopCodeSize );
    component->loopCodeSize = loopCodeSize;
    component->constantPool = FB_GROW_ARRAY( NULL, byte_t, constantPoolSize );
    component->constantPoolSize = constantPoolSize;

    return component;
}

void fbComponent_destroy( FBComponent *component ) {

    if( component == NULL )
        return;

    FB_FREE_ARRAY( component->setupCode );
    FB_FREE_ARRAY( component->loopCode );
    FB_FREE_ARRAY( component->constantPool );

    FB_FREE_ARRAY( component );
}
