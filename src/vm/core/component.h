/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_COMPONENT_H
#define VM_COMPONENT_H

#include "vm/interpreter/types.h"

struct _FBComponent
{
    byte_t *setupCode;
    index_t setupCodeSize;
    byte_t *loopCode;
    index_t loopCodeSize;

    byte_t *constantPool;
    index_t constantPoolSize;

    index_t dataSize;
    index_t stackSize;
    index_t informationPacketCount;
    index_t inputCount;
    index_t outputCount;
};

FBComponent* fbComponent_create( index_t setupCodeSize, index_t loopCodeSize, index_t constantPoolSize );
void fbComponent_destroy( FBComponent *component );

#endif // VM_COMPONENT_H
