/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/scheduler.h"

#include "vm/core/scheduler_internal.h"

#include "vm/core/information_packet.h"

#include "vm/interpreter/context.h"


void _internal_fbScheduler_serviceCodeSwitch( FBContext *context ) {

    if( context->code == context->component->loopCode ) {
        FB_CONTEXT_RESET_IP( context );
        return;
    }

    fbContext_setCode( context, context->component->loopCode, context->component->loopCodeSize );
    fbContext_reset( context );
}


void _internal_fbScheduler_serviceRequests( FBContext *context, FBContextStatus runStatus ) {

    switch( runStatus )
    {
        case STATUS_REQUEST_READ: {
            index_t ipIndex = context->contextRequestData.informationPacketIndex;
            index_t portIndex = context->contextRequestData.portIndex;

            FBInformationPacketHandle ip = fbInput_read( &context->inputs[ portIndex ] );

            if( ip == FB_INVALID_PACKET_HANDLE ) {

                // if connection is empty...
                if( fbError_getStatus( context ) == STATUS_ERROR_CONNECTION_EMPTY ) {
                    // ...schedule the context to be serviced for read request again
                    fbError_setStatus( context, STATUS_REQUEST_READ );
                    fbContext_setState( context, CONTEXT_SUSPENDED );
                } else {
                    // either port is closed or connection has been terminated,
                    // this context can't continue execution.
                    // error status is in the context status field
                    fbScheduler_terminateContext( context );
                }
                break;
            }

            context->packetHandles[ ipIndex ] = ip;
            context->packets[ ipIndex ] = fbInformationPacket_getDataBuffer( ip );

            FB_CONTEXT_RESET_REQUEST_DATA( context );

            fbError_setStatus( context, STATUS_OK );
            fbContext_setState( context, CONTEXT_RUNNING );
        }
            break;

        case STATUS_REQUEST_WRITE: {
            index_t ipIndex = context->contextRequestData.informationPacketIndex;
            index_t portIndex = context->contextRequestData.portIndex;

            bool writeSuccessful = fbOutput_write( &context->outputs[ portIndex ], context->packetHandles[ ipIndex ] );

            if( ! writeSuccessful ) {
                
                // if connection is full...
                if( fbError_getStatus( context ) == STATUS_ERROR_CONNECTION_FULL ) {
                    // ...schedule the context to be serviced for write request again
                    fbError_setStatus( context, STATUS_REQUEST_WRITE );
                    fbContext_setState( context, CONTEXT_SUSPENDED );
                } else {
                    // either port is closed or connection has been terminated,
                    // this context can't continue execution.
                    // error status is in the context status field
                    fbScheduler_terminateContext( context );
                }
                break;
            }

            context->packetHandles[ ipIndex ] = FB_INVALID_PACKET_HANDLE;
            context->packets[ ipIndex ] = NULL;

            FB_CONTEXT_RESET_REQUEST_DATA( context );

            fbError_setStatus( context, STATUS_OK );
            fbContext_setState( context, CONTEXT_RUNNING );
        }
            break;

        case STATUS_REQUEST_CREATE: {
            index_t ipIndex = context->contextRequestData.informationPacketIndex;

            FBInformationPacketHandle ip = fbInformationPacket_create( context->contextRequestData.packetSize );

            if( ip == FB_INVALID_PACKET_HANDLE ) {

                // packet create request was not successful,
                // schedule us for the next round
                fbError_setStatus( context, STATUS_REQUEST_CREATE );
                fbContext_setState( context, CONTEXT_SUSPENDED );
                break;
            }

            context->packetHandles[ ipIndex ] = ip;
            context->packets[ ipIndex ] = fbInformationPacket_getDataBuffer( ip );

            FB_CONTEXT_RESET_REQUEST_DATA( context );

            fbError_setStatus( context, STATUS_OK );
            fbContext_setState( context, CONTEXT_RUNNING );
        }
            break;

        case STATUS_REQUEST_DROP: {
            index_t ipIndex = context->contextRequestData.informationPacketIndex;

            fbInformationPacket_drop( context->packetHandles[ ipIndex ] );
            context->packetHandles[ ipIndex ] = FB_INVALID_PACKET_HANDLE;
            context->packets[ ipIndex ] = NULL;

            FB_CONTEXT_RESET_REQUEST_DATA( context );

            fbError_setStatus( context, STATUS_OK );
            fbContext_setState( context, CONTEXT_RUNNING );
        }
            break;

        case STATUS_YIELD:
            // bytecode yielded its slice for others
            // clear its status and schedule it for run again in next slice
            fbError_setStatus( context, STATUS_OK );
            fbContext_setState( context, CONTEXT_RUNNING );
            break;

        case STATUS_CODE_END:
            // reaching end of code resets the context
            // preparing it for another run.
            // it is up to calling functions to decide if this context is
            // to be ran again (i.e. if it is running loop() code) or something
            // else should be done (i.e. replace setup() code with loop() code and
            // continue running this context)
            fbContext_reset( context );
            fbError_setStatus( context, STATUS_CODE_END );
            break;

        case STATUS_REQUEST_TERMINATE:
            fbScheduler_terminateContext( context );
            break;

        case STATUS_ERROR_UNKNOWN_OP:
            fbScheduler_terminateContext( context );
            break;

        case STATUS_ERROR_OVERRUN:
            fbScheduler_terminateContext( context );
            break;

        // case STATUS_OK:
        //     break;

        default:
            // unhandled statuses shuold terminate context
            fbScheduler_terminateContext( context );
            break;
    }
}
