/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_INPUT_OUTPUT_H
#define VM_INPUT_OUTPUT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"

#include "vm/core/connection.h"


struct _FBInputOutput
{
    FBConnection *connection;
    bool closed;
    FBContext *owner;
};

// Storage for ports is allocated and managed by fbContext_() and fbNetwork_()
// so we don't have fbInputOutput_create() function here

void fbInputOutput_init( FBInputOutput *port, FBContext *owner );
void fbInputOutput_close( FBInputOutput *port );
void fbInputOutput_closeConnection( FBInputOutput *port );
bool fbInputOutput_isClosed( FBInputOutput *port );
bool fbInputOutput_isOpen( FBInputOutput *port );

// INPUT port
FBInformationPacketHandle fbInput_read( FBInputOutput *port );
bool fbInput_cannotRead( FBInputOutput *port );
bool fbInput_canRead( FBInputOutput *port );

// OUTPUT port
bool fbOutput_write( FBInputOutput *port, FBInformationPacketHandle ip );
bool fbOutput_cannotWrite( FBInputOutput *port );
bool fbOutput_canWrite( FBInputOutput *port );


#ifdef __cplusplus
}
#endif

#endif // VM_INPUT_OUTPUT_H
