/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/connection.h"
#include "vm/core/input_output.h"
#include "vm/core/information_packet.h"
#include "vm/interpreter/context.h"
#include "vm/error.h"

void fbConnection_init( FBConnection *connection, FBInputOutput *source, FBInputOutput *sink ) {
    fbConnection_init_i( connection, source, sink, 1 );
}

void fbConnection_init_i( FBConnection *connection, FBInputOutput *source, FBInputOutput *sink, uint32_t capacity )
{
    // CHECK_NOT_NULL( connection )
    // CHECK_NOT_NULL( source )
    // CHECK_NOT_NULL( sink )

    connection->source = source;
    if( source ) {
        source->connection = connection;
        source->closed = false;
    }

    connection->sink = sink;
    if( sink ) {
        sink->connection = connection;
        sink->closed = false;
    }

    connection->terminated = false;
    connection->isTerminating = false;

    fbIPRingBuffer_init( &connection->fifoBuffer, capacity );
}

bool fbConnection_canRead( FBConnection *connection ) {

    return ! fbConnection_isTerminated( connection ) && fbInputOutput_isOpen( connection->sink ) && ! fbConnection_isEmpty( connection );
}

FBInformationPacketHandle fbConnection_read( FBConnection *connection )
{
    if( fbConnection_isTerminated( connection ) ) {
        // throw new IllegalStateException( "Trying to read from terminated connection." );
        if( connection->sink )
            fbError_setStatus( connection->sink->owner, STATUS_ERROR_CONNECTION_TERMINATED );
        return FB_INVALID_PACKET_HANDLE;
    }
    if( ! connection->sink || fbInputOutput_isClosed( connection->sink ) ) {
        // throw new IllegalStateException( "Trying to read from connection with closed sink port." );
        if( connection->sink )
            fbError_setStatus( connection->sink->owner, STATUS_ERROR_PORT_CLOSED );
        return FB_INVALID_PACKET_HANDLE;
    }
    if( fbConnection_isEmpty( connection ) ) {
        // throw new IllegalStateException( "Trying to read from empty connection." );
        if( connection->sink )
            fbError_setStatus( connection->sink->owner, STATUS_ERROR_CONNECTION_EMPTY );
        return FB_INVALID_PACKET_HANDLE;
    }

    FBInformationPacketHandle ip;

    fbIPRingBuffer_pop( &connection->fifoBuffer, &ip );

    return ip;
}

bool fbConnection_canWrite( FBConnection *connection ) {

    return ! fbConnection_isTerminated( connection ) && fbInputOutput_isOpen( connection->source ) && ! fbConnection_isFull( connection );
}

bool fbConnection_write( FBConnection *connection, FBInformationPacketHandle ip )
{
    if( fbConnection_isTerminated( connection ) ) {

        // throw new IllegalStateException( "Trying to write to terminated connection." );
        if( connection->source )
            fbError_setStatus( connection->source->owner, STATUS_ERROR_CONNECTION_TERMINATED );
        return false;
    }
    if( ! connection->source || fbInputOutput_isClosed( connection->source ) ) {
        // throw new IllegalStateException( "Trying to read from connection with closed source port." );
        if( connection->source )
            fbError_setStatus( connection->source->owner, STATUS_ERROR_PORT_CLOSED );
        return false;
    }
    if( fbConnection_isFull( connection ) ) {

        // throw new IllegalStateException( "Trying to write to full connection." );
        if( connection->source )
            fbError_setStatus( connection->source->owner, STATUS_ERROR_CONNECTION_FULL );
        return false;
    }

    return fbIPRingBuffer_push( &connection->fifoBuffer, ip );
}

void fbConnection_terminate( FBConnection *connection )
{
    // ports also call connection.terminate() on close
    // and we don't want to cause stack overflow when we call
    // port.close() on the other side of the connection.
    if( connection->isTerminating ) {
        return;
    }
    connection->isTerminating = true;

    if( connection->source && ! fbInputOutput_isClosed( connection->source ) ) {
        fbInputOutput_close( connection->source );
    }
    if( connection->sink && ! fbInputOutput_isClosed( connection->sink ) ) {
        fbInputOutput_close( connection->sink );
    }

    // empty and dispose of queue and IPs
    FBInformationPacketHandle ip;
    while( fbIPRingBuffer_pop( &connection->fifoBuffer, &ip ) ) {
        fbInformationPacket_drop( ip );
    }
    fbIPRingBuffer_destroy( &connection->fifoBuffer );

    connection->terminated = true;
    connection->isTerminating = false;
}

bool fbConnection_isTerminated( FBConnection *connection )
{
    return connection->terminated;
}

bool fbConnection_isEmpty( FBConnection *connection )
{
    return connection->terminated || fbIPRingBuffer_isEmpty( &connection->fifoBuffer );
}

bool fbConnection_isFull( FBConnection *connection )
{
    return ( ! connection->terminated ) && fbIPRingBuffer_isFull( &connection->fifoBuffer );
}

