/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/scheduler.h"
#include "vm/core/scheduler_internal.h"

#include "vm/core/information_packet.h"
#include "vm/core/input_output.h"

#include "vm/interpreter/context.h"
#include "vm/core/connection.h"

#include "vm/core/network.h"

#include <string.h>
#include <stdlib.h>



// uint16_t _get_connection_id( FBConnection *connection ) {
//     uint64_t wide_id = (uintptr_t)connection;
//     uint16_t id =
//         (uint16_t)(wide_id >> 48) ^
//         (uint16_t)(wide_id >> 32) ^
//         (uint16_t)(wide_id >> 16) ^
//         (uint16_t)(wide_id >> 0);
//     return id;
// }
FBNativeContextHandle _get_native_context_id( FBNetworkNativeIO *nativeio ) {

    FBNativeContextHandle handle = FB_NATIVE_IO_TO_HANDLE( nativeio );

    return handle;
}

void fbScheduler_serviceNativeInputHandlers( FBNetworkContext *networkContext ) {

    //---------------------------------------------------------------
    // Call native handlers for network input ports
    //---------------------------------------------------------------
    for( index_t idx = 0; idx < networkContext->nativeInputsCount; idx++ ) {

        FBNetworkNativeIO *nativeInput = fbNetwork_getNativeInput( networkContext, idx );

        if( STATUS_NATIVE_FN_TERMINATED == nativeInput->state ) continue;

        //
        // native functions should provide data to write to network inputs
        //
        if( nativeInput->fn != NULL ) {

            FBNativeContextHandle handle = _get_native_context_id( nativeInput );

            if( fbConnection_canWrite( nativeInput->port->connection ) ) {
                // standard call to native function to service the connection
                (*nativeInput->fn)( handle );
            }

            if( fbConnection_isTerminated( nativeInput->port->connection ) ) {
                // last call to native function to allow for cleanup
                nativeInput->state = STATUS_NATIVE_FN_TERMINATED;

                (*nativeInput->fn)( handle );
            }
        }
    }
}

void fbScheduler_serviceNativeOutputHandlers( FBNetworkContext *networkContext ) {

    //---------------------------------------------------------------
    // Call native handlers for network output port
    //---------------------------------------------------------------
    for( index_t idx = 0; idx < networkContext->nativeOutputsCount; idx++ ) {

        FBNetworkNativeIO *nativeOutput = fbNetwork_getNativeOutput( networkContext, idx );

        if( STATUS_NATIVE_FN_TERMINATED == nativeOutput->state ) continue;

        //
        // native functions should read data from network outputs
        //
        if( nativeOutput->fn != NULL ) {

            FBNativeContextHandle handle = _get_native_context_id( nativeOutput );

            if( fbConnection_canRead( nativeOutput->port->connection ) ) {
                // standard call to native function to service the connection
                (*nativeOutput->fn)( handle );
            }

            if( fbConnection_isTerminated( nativeOutput->port->connection ) ) {
                // last call to native function to allow for cleanup
                nativeOutput->state = STATUS_NATIVE_FN_TERMINATED;

                (*nativeOutput->fn)( handle );
            }
        }
    }
}

void fbScheduler_terminateConnectionIfNeeded( FBConnection *connection ) {

    if( ! fbConnection_isTerminated( connection ) ) {

        // if sink port is closed we can terminate connection
        if( ! connection->sink || fbInputOutput_isClosed( connection->sink ) ) {
            fbConnection_terminate( connection );
            return;
        }

        // if source port is closed and connection is empty we can terminate it
        if( ( ! connection->source || fbInputOutput_isClosed( connection->source ) ) && fbConnection_isEmpty( connection ) ) {
            fbConnection_terminate( connection );
        }
    }
}

bool fbScheduler_mainLoop( FBNetworkContext *networkContext ) {

    // TODO: remove this or have another variant of fbScheduler_mainLoop?
    bool hasServicedAtLeastOneContext = false;
    // bool hasRunningContexts = false;

    //---------------------------------------------------------------
    // Terminate connections before servicing native functions
    //---------------------------------------------------------------
    // Terminating eligible connections now will
    // correctly trigger terminating calls for native handlers
    for( index_t idx = 0; idx < networkContext->allConnectionsCount; idx++ ) {

        FBConnection *connection = &networkContext->allConnections[ idx ];

        fbScheduler_terminateConnectionIfNeeded( connection );
    }

    //---------------------------------------------------------------
    // Service native input handlers
    //---------------------------------------------------------------
    fbScheduler_serviceNativeInputHandlers( networkContext );

    //---------------------------------------------------------------
    // Terminate connections before servicing contexts
    //---------------------------------------------------------------
    for( index_t idx = 0; idx < networkContext->allConnectionsCount; idx++ ) {

        FBConnection *connection = &networkContext->allConnections[ idx ];

        fbScheduler_terminateConnectionIfNeeded( connection );
    }

    //---------------------------------------------------------------
    // Service contexts
    //---------------------------------------------------------------
    for( index_t idx = 0; idx < networkContext->allContextsCount; idx++ ) {

        FBContext *context = &networkContext->allContexts[ idx ];

        //---------------------------------------------------------------
        // Only work with RUNNING and SUSPENDED contexts
        //---------------------------------------------------------------
        if( fbContext_getState( context ) != CONTEXT_RUNNING && fbContext_getState( context ) != CONTEXT_SUSPENDED ) {
            continue;
        }

        //---------------------------------------------------------------
        // Terminate context if it has no open ports
        //---------------------------------------------------------------
        if( ! fbContext_hasOpenPorts( context ) ) {
            fbScheduler_terminateContext( context );
            // this one's dead, skip to the next
            continue;
        }

        //---------------------------------------------------------------
        // Run context
        //---------------------------------------------------------------
        // at this point context is either RUNNING or SUSPENDED
        // run_slice() runs bytecode for RUNNING context
        // or handles statuses and requests for SUSPENDED context
        FBContextStatus runStatus = _internal_fbScheduler_runSlice( context );

        //---------------------------------------------------------------
        // Service requests and status changes
        //---------------------------------------------------------------
        _internal_fbScheduler_serviceRequests( context, runStatus );

        hasServicedAtLeastOneContext = true;

        //---------------------------------------------------------------
        // Switch from running setup() to running loop() if needed
        //---------------------------------------------------------------
        runStatus = fbError_getStatus( context );
        if( runStatus == STATUS_CODE_END ) {
            _internal_fbScheduler_serviceCodeSwitch( context );
        }

        // if( fbContext_getState( context ) == CONTEXT_RUNNING ) {
        //     hasRunningContexts = true;
        // }

        // context is now either RUNNING, SUSPENDED or TERMINATED
    }

    //---------------------------------------------------------------
    // Terminate connections before servicing native functions
    //---------------------------------------------------------------
    // Terminating eligible connections now will
    // correctly trigger terminating calls for native handlers
    for( index_t idx = 0; idx < networkContext->allConnectionsCount; idx++ ) {

        FBConnection *connection = &networkContext->allConnections[ idx ];

        fbScheduler_terminateConnectionIfNeeded( connection );
    }

    //---------------------------------------------------------------
    // Service native output handlers
    //---------------------------------------------------------------
    fbScheduler_serviceNativeOutputHandlers( networkContext );

    // return hasRunningContexts;
    return hasServicedAtLeastOneContext;
}

FBContextStatus _internal_fbScheduler_runSlice( FBContext *context ) {

    FBContextStatus runStatus;

    // if we are not scheduled to run, just return current status
    if( fbContext_getState( context ) == CONTEXT_RUNNING ) {

        runStatus = fbContext_run( context );
        // save the run status in the context
        fbError_setStatus( context, runStatus );

    } else {

        runStatus = fbError_getStatus( context );
    }

    return runStatus;
}


void fbScheduler_terminateContext( FBContext *context ) {

    fbContext_setState( context, CONTEXT_TERMINATED );

    for( index_t idx = 0; idx < context->inputsCount; idx++ ) {

        // terminate connection and close ports on both ends for input ports
        fbInputOutput_closeConnection( &context->inputs[ idx ] );
    }

    for( index_t idx = 0; idx < context->outputsCount; idx++ ) {

        // just close port on this side for output ports
        fbInputOutput_close( &context->outputs[ idx ] );
    }
}
