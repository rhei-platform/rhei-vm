/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_CONNECTION_H
#define VM_CONNECTION_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"

#include "vm/lib/ipringbuffer.h"


struct _FBConnection
{
    bool terminated;
    bool isTerminating;
    FBInputOutput *source;
    FBInputOutput *sink;

    FBIPRingBuffer fifoBuffer;
};

// Storage for connections is allocated and managed by fbNetwork_()
// so we don't have fbConnection_create() function here

void fbConnection_init( FBConnection *connection, FBInputOutput *source, FBInputOutput *sink );
void fbConnection_init_i( FBConnection *connection, FBInputOutput *source, FBInputOutput *sink, uint32_t capacity );

bool fbConnection_canRead( FBConnection *connection );
FBInformationPacketHandle fbConnection_read( FBConnection *connection );
bool fbConnection_canWrite( FBConnection *connection );
bool fbConnection_write( FBConnection *connection, FBInformationPacketHandle ip );

bool fbConnection_isEmpty( FBConnection *connection );
bool fbConnection_isFull( FBConnection *connection );

void fbConnection_terminate( FBConnection *connection );
bool fbConnection_isTerminated( FBConnection *connection );


#ifdef __cplusplus
}
#endif

#endif // VM_CONNECTION_H
