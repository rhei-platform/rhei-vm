/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_INFORMATION_PACKET_INTERNAL_H
#define VM_INFORMATION_PACKET_INTERNAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"

#define MAX_HANDLES 512
#define MIN_HANDLE_VALUE 128
// it MUST be at least MAX_HANDLE_VALUE = MIN_HANDLE_VALUE + MAX_HANDLES
#define MAX_HANDLE_VALUE MIN_HANDLE_VALUE + MAX_HANDLES + 5000

typedef struct _registry_entry_t
{
    FBInformationPacketHandle handle;
    byte_t *buffer_pointer;
    index_t buffer_size;
} registry_entry_t;

void _internal_fbInformationPacket_resetRegistry();
registry_entry_t* _internal_fbInformationPacket_getEntry( FBInformationPacketHandle handle );bool _internal_fbInformationPacket_isValidHandle( FBInformationPacketHandle handle );
FBInformationPacketHandle _internal_fbInformationPacket_getNextHandle();
registry_entry_t* _internal_fbInformationPacket_getNextEntryLocation();
FBInformationPacketHandle _internal_fbInformationPacket_createEntry( size_t capacity );
bool _internal_fbInformationPacket_dropEntry( FBInformationPacketHandle handle );


#ifdef __cplusplus
}
#endif

#endif // VM_INFORMATION_PACKET_INTERNAL_H
