/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/information_packet.h"
#include "vm/lib/memory.h"

#include "vm/core/information_packet_internal.h"


FBInformationPacketHandle fbInformationPacket_create( index_t capacity ) {

    return _internal_fbInformationPacket_createEntry( capacity );
}

void fbInformationPacket_drop( FBInformationPacketHandle ip ) {

    _internal_fbInformationPacket_dropEntry( ip );
}

inline byte_t* fbInformationPacket_getDataBuffer( FBInformationPacketHandle ip ) {

    registry_entry_t *entry = _internal_fbInformationPacket_getEntry( ip );

    if( NULL == entry )
        return NULL;

    return entry->buffer_pointer;
}

inline index_t fbInformationPacket_getDataBufferSize( FBInformationPacketHandle ip ) {

    registry_entry_t *entry = _internal_fbInformationPacket_getEntry( ip );

    if( NULL == entry )
        return 0;

    return entry->buffer_size;
}

FBInformationPacketTuple fbInformationPacket_create_tuple( index_t capacity ) {

    FBInformationPacketTuple tuple;

    tuple.handle = fbInformationPacket_create( capacity );
    tuple.buffer = fbInformationPacket_getDataBuffer( tuple.handle );

    return tuple;
}

index_t fbInformationPacket_copyRange( FBInformationPacketHandle dst_ip, index_t dst_offset, FBInformationPacketHandle src_ip, index_t src_offset, index_t length ) {

    byte_t *dst = fbInformationPacket_getDataBuffer( dst_ip );
    if( NULL == dst )
        return 0;
    byte_t *src = fbInformationPacket_getDataBuffer( src_ip );
    if( NULL == src )
        return 0;
    index_t dst_size = fbInformationPacket_getDataBufferSize( dst_ip );
    index_t src_size = fbInformationPacket_getDataBufferSize( src_ip );

    if( dst_offset >= dst_size ) {
        return 0;
    }
    if( src_offset >= src_size ) {
        return 0;
    }

    index_t to_copy = length;
    if( dst_offset + to_copy > dst_size ) {
        to_copy = dst_size - dst_offset;
    }
    if( src_offset + to_copy > src_size ) {
        to_copy = src_size - src_offset;
    }

    FB_COPY_ARRAY( dst + dst_offset, src + src_offset, byte_t, to_copy );

    return to_copy;
}

index_t fbInformationPacket_fillRange( FBInformationPacketHandle ip, index_t offset, index_t length, byte_t value ) {

    byte_t *data = fbInformationPacket_getDataBuffer( ip );
    if( NULL == data )
        return 0;
    index_t size = fbInformationPacket_getDataBufferSize( ip );

    if( offset > size ) {
        return 0;
    }

    index_t to_fill = length;
    if( offset + to_fill > size ) {
        to_fill = size - offset;
    }

    FB_SET( data + offset, value, to_fill );

    return to_fill;
}
