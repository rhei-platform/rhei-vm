/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_NETWORK_H
#define VM_NETWORK_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"
#include "vm/core/input_output.h"
#include "vm/lib/databuffer.h"
#include "vm/native.h"

typedef struct _FBNetworkContext
{
    FBContext *allContexts;
    index_t allContextsCount;

    FBConnection *allConnections;
    index_t allConnectionsCount;

    FBInputOutput* *networkInputs;
    index_t networkInputsCount;

    FBInputOutput* *networkOutputs;
    index_t networkOutputsCount;


    FBNetworkNativeIO *nativeInputs;
    index_t nativeInputsCount;

    FBNetworkNativeIO *nativeOutputs;
    index_t nativeOutputsCount;

} FBNetworkContext;


FBNetworkContext *fbNetwork_create( 
    size_t contextCount, size_t connectionCount, 
    size_t inputCount, size_t outputCount, 
    size_t nativeInputCount, size_t nativeOutputCount );
void fbNetwork_destroy( FBNetworkContext *networkContext );

void fbNetwork_init( FBNetworkContext *networkContext,
    size_t contextCount, size_t connectionCount, 
    size_t inputCount, size_t outputCount, 
    size_t nativeInputCount, size_t nativeOutputCount );
void fbNetwork_clear( FBNetworkContext *networkContext );
void fbNetwork_free( FBNetworkContext *networkContext );

FBContext* fbNetwork_getContext( FBNetworkContext *networkContext, index_t contextIndex );
FBConnection* fbNetwork_getConnection( FBNetworkContext *networkContext, index_t connectionIndex );
FBInputOutput** fbNetwork_getInputPtr( FBNetworkContext *networkContext, index_t inputIndex );
FBInputOutput* fbNetwork_getInput( FBNetworkContext *networkContext, index_t inputIndex );
FBInputOutput** fbNetwork_getOutputPtr( FBNetworkContext *networkContext, index_t outputIndex );
FBInputOutput* fbNetwork_getOutput( FBNetworkContext *networkContext, index_t outputIndex );
FBNetworkNativeIO* fbNetwork_getNativeInput( FBNetworkContext *networkContext, index_t inputIndex );
FBNetworkNativeIO* fbNetwork_getNativeOutput( FBNetworkContext *networkContext, index_t outputIndex );

bool fbNetwork_setInput( FBNetworkContext *networkContext, index_t inputIndex, FBInputOutput* input );
bool fbNetwork_setOutput( FBNetworkContext *networkContext, index_t outputIndex, FBInputOutput* output );

bool fbNetwork_hasRunningContexts( FBNetworkContext *networkContext );
bool fbNetwork_hasSuspendedContexts( FBNetworkContext *networkContext );

#ifdef __cplusplus
}
#endif

#endif // VM_NETWORK_H
