/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_SCHEDULER_H
#define VM_SCHEDULER_H


#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"
#include "vm/core/network.h"

bool fbScheduler_mainLoop( FBNetworkContext *networkContext );

void fbScheduler_terminateContext( FBContext *context );

#ifdef __cplusplus
}
#endif

#endif // VM_SCHEDULER_H
