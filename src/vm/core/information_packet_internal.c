/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/core/information_packet.h"

#include "vm/core/information_packet_internal.h"

#include <stdlib.h>
#include <string.h>


// TODO make the array dynamic
index_t _internal_IPRegistry_entryCount = 0;
registry_entry_t _internal_IPRegistry_entryArray[ MAX_HANDLES ] = {{0,NULL,0}};

FBInformationPacketHandle _internal_IPRegistry_nextHandle = MIN_HANDLE_VALUE;

void _internal_fbInformationPacket_resetRegistry() {

    for( index_t i = 0; i < _internal_IPRegistry_entryCount; i++ ) {
        registry_entry_t entry = _internal_IPRegistry_entryArray[ i ];
        free( (void*) entry.buffer_pointer );
    }

    _internal_IPRegistry_nextHandle = MIN_HANDLE_VALUE;
    _internal_IPRegistry_entryCount = 0;

    // memset( _internal_IPRegistry_entryArray, 0, MAX_HANDLES * sizeof(registry_entry_t) );
    for( index_t i = 0; i < MAX_HANDLES; i++ ) {
        registry_entry_t *entry = _internal_IPRegistry_entryArray + i;
        entry->handle = FB_INVALID_PACKET_HANDLE;
        entry->buffer_pointer = NULL;
        entry->buffer_size = 0;
        // TODO: warning: variable 'entry' set but not used [-Wunused-but-set-variable]
        // (void)entry;
    }
}


int _internal_fbInformationPacket_compareSearchFn(const FBInformationPacketHandle *key, const registry_entry_t *element) {

   return ( (*key) - (*element).handle );
}

int _internal_fbInformationPacket_compareSortFn(const registry_entry_t *left, const registry_entry_t *right) {

    if( (*left).handle == FB_INVALID_PACKET_HANDLE ) return 1;
    if( (*right).handle == FB_INVALID_PACKET_HANDLE ) return -1;

   return ( (*left).handle - (*right).handle );
}

registry_entry_t* _internal_fbInformationPacket_getEntry( FBInformationPacketHandle handle ) {

    void *result = bsearch( &handle, _internal_IPRegistry_entryArray, _internal_IPRegistry_entryCount, sizeof(registry_entry_t), (int (*)(const void *, const void *))_internal_fbInformationPacket_compareSearchFn );

    if( ! result ) return NULL;

    return (registry_entry_t*)result;
}

bool _internal_fbInformationPacket_isValidHandle( FBInformationPacketHandle handle ) {

    return !! _internal_fbInformationPacket_getEntry( handle );
}

FBInformationPacketHandle _internal_fbInformationPacket_getNextHandle() {

    while( _internal_fbInformationPacket_isValidHandle( _internal_IPRegistry_nextHandle ) ) {
        _internal_IPRegistry_nextHandle++;
        if( _internal_IPRegistry_nextHandle > MAX_HANDLE_VALUE )
            _internal_IPRegistry_nextHandle = MIN_HANDLE_VALUE;
    }

    return _internal_IPRegistry_nextHandle;
}

registry_entry_t* _internal_fbInformationPacket_getNextEntryLocation() {

    if( _internal_IPRegistry_entryCount>= MAX_HANDLES )
        return NULL;

    registry_entry_t *entry = &_internal_IPRegistry_entryArray[ _internal_IPRegistry_entryCount ];
    _internal_IPRegistry_entryCount++;

    return entry;
}

FBInformationPacketHandle _internal_fbInformationPacket_createEntry( size_t capacity ) {

    if( _internal_IPRegistry_entryCount >= MAX_HANDLES ) {
        return FB_INVALID_PACKET_HANDLE;
    }

    FBInformationPacketHandle handle = _internal_fbInformationPacket_getNextHandle();

    registry_entry_t *entry = _internal_fbInformationPacket_getNextEntryLocation();

    if( entry == NULL )
        return FB_INVALID_PACKET_HANDLE;

    entry->handle = handle;
    entry->buffer_pointer = realloc( NULL, capacity );
    entry->buffer_size = capacity;

    qsort( _internal_IPRegistry_entryArray, _internal_IPRegistry_entryCount, sizeof(registry_entry_t), (int (*)(const void *, const void *))_internal_fbInformationPacket_compareSortFn );

    return entry->handle;
}

bool _internal_fbInformationPacket_dropEntry( FBInformationPacketHandle handle ) {

    registry_entry_t *entry = _internal_fbInformationPacket_getEntry( handle );

    if( NULL == entry ) return false;

    entry->handle = FB_INVALID_PACKET_HANDLE;
    free( (void*) entry->buffer_pointer );
    entry->buffer_pointer = NULL;
    entry->buffer_size = 0;

    qsort( _internal_IPRegistry_entryArray, _internal_IPRegistry_entryCount, sizeof(registry_entry_t), (int (*)(const void *, const void *))_internal_fbInformationPacket_compareSortFn );

    _internal_IPRegistry_entryCount--;

    return true;
}

