/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/error.h"
#include "vm/interpreter/context.h"

void fbError_setStatus( FBContext *context, FBContextStatus status ) {

    if( context == NULL )
        // silently ignore this case
        // native connection port on native function side has owner == NULL
        return;

    context->status = status;
}

FBContextStatus fbError_getStatus( FBContext *context ) {

    if( context == NULL )
        // raise hell??
        return STATUS_NONE;

    return context->status;
}

