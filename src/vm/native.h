/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef _VM_NATIVE_H
#define _VM_NATIVE_H


#include "vm/interpreter/types.h"
#include "vm/core/information_packet.h"
#include "vm/core/connection.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void* FBNativeContextHandle;

typedef void ( *FBPacketHandlerFn )( FBNativeContextHandle handle );


typedef enum _FBNativeFnState
{
    // STATUS_NATIVE_FN_INITIAL, // first time it's called
    STATUS_NATIVE_FN_LIVE, // all seems to be well, you can do it's main work
    STATUS_NATIVE_FN_TERMINATED, // last time it's called, the port is closed and connection terminated

} FBNativeFnState;


struct _FBNetworkNativeIO
{
    FBNativeFnState state;
    FBInputOutput *port;
    FBPacketHandlerFn fn;
    byte_t *configBuffer;
    size_t bufferSize;
    void *tag;
};

#define FB_NATIVE_IO_TO_HANDLE( nativeio ) \
    ( (FBNativeContextHandle) (nativeio) )
#define FB_NATIVE_IO_FROM_HANDLE( handle ) \
    ( (FBNetworkNativeIO *) (handle) )

#define FB_GET_IS_TERMINATED_FROM_HANDLE( handle ) \
    ( STATUS_NATIVE_FN_TERMINATED == ( (FBNetworkNativeIO *) (handle) ) -> state )
#define FB_GET_STATE_FROM_HANDLE( handle ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> state )
#define FB_GET_CONNECTION_FROM_HANDLE( handle ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> port -> connection )
#define FB_GET_PORT_FROM_HANDLE( handle ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> port )
#define FB_GET_BUFFER_FROM_HANDLE( handle ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> configBuffer )
#define FB_GET_BUFFER_SIZE_FROM_HANDLE( handle ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> bufferSize )
#define FB_GET_TAG_FROM_HANDLE( handle ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> tag )
#define FB_SET_TAG_FROM_HANDLE( handle, value ) \
    ( ( (FBNetworkNativeIO *) (handle) ) -> tag = value )


FBInformationPacketHandle fbNativeConnection_read( FBNativeContextHandle handle );
bool fbNativeConnection_write( FBNativeContextHandle handle, FBInformationPacketHandle packet );

void fbNativeConnection_close( FBNativeContextHandle handle );

bool fbNativeConnection_isTerminated( FBNativeContextHandle handle );
bool fbNativeConnection_isEmpty( FBNativeContextHandle handle );
bool fbNativeConnection_isFull( FBNativeContextHandle handle );

#ifdef __cplusplus
}
#endif

#endif // _VM_NATIVE_H
