/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include <stdlib.h>

#include "vm/lib/ipringbuffer.h"

//=============================================================================
static uint32_t fbIPRingBuffer_capacity(FBIPRingBuffer *rbuff)
{
	return rbuff->capacity;
}

// We need this one since we don't keep track of tail
static uint32_t fbIPRingBuffer_tail(FBIPRingBuffer *rbuff)
{
	uint32_t tail;

	tail = rbuff->head + rbuff->count;
	// wrap around
	if(tail >= rbuff->capacity)
		tail = tail - rbuff->capacity;
	return tail;
}

static FBInformationPacketHandle fbIPRingBuffer_loadFromHead(FBIPRingBuffer *rbuff)
{
	return rbuff->buffer[ rbuff->head ];
}

static void fbIPRingBuffer_storeToTail(FBIPRingBuffer *rbuff, FBInformationPacketHandle handle)
{
	uint32_t tail = fbIPRingBuffer_tail(rbuff);

	rbuff->buffer[ tail ] = handle;
}

static void fbIPRingBuffer_headAdvanceOne(FBIPRingBuffer *rbuff)
{
	rbuff->head++;
	// wrap around
	if(rbuff->head == rbuff->capacity)
		rbuff->head = 0;

	rbuff->count--;
}

static void fbIPRingBuffer_tailAdvanceOne(FBIPRingBuffer *rbuff)
{
	// we calculate tail position.
	// to do that we need 'head' and 'count'.
	// tail is advanced only when we insert data
	// AND there's space in the buffer.
	// in that case all we need to do to advance tail
	// is to increment count by one.
	rbuff->count++;
}

//=============================================================================
void fbIPRingBuffer_init(FBIPRingBuffer *rbuff, uint32_t capacity)
{
	rbuff->buffer = realloc( NULL, sizeof( FBInformationPacketHandle ) * capacity );
	rbuff->capacity = capacity;
	rbuff->count = 0;
	rbuff->head = 0;
}

void fbIPRingBuffer_destroy(FBIPRingBuffer *rbuff)
{
	free( rbuff->buffer );
	rbuff->buffer = NULL;
	rbuff->capacity = 0;
	rbuff->count = 0;
	rbuff->head = 0;
}


bool fbIPRingBuffer_isEmpty(FBIPRingBuffer *rbuff)
{
	return fbIPRingBuffer_availableData(rbuff) == 0;
}

bool fbIPRingBuffer_isFull(FBIPRingBuffer *rbuff)
{
	return fbIPRingBuffer_availableData(rbuff) == fbIPRingBuffer_capacity(rbuff);
}

uint32_t fbIPRingBuffer_availableData(FBIPRingBuffer *rbuff)
{
	return rbuff->count;
}

uint32_t fbIPRingBuffer_availableSpace(FBIPRingBuffer *rbuff)
{
	return fbIPRingBuffer_capacity(rbuff) - fbIPRingBuffer_availableData(rbuff);
}

bool fbIPRingBuffer_push(FBIPRingBuffer *rbuff, FBInformationPacketHandle data)
{
	if(fbIPRingBuffer_isFull(rbuff))
		return false;

	// save data where tail is
	fbIPRingBuffer_storeToTail(rbuff, data);

	// move tail forward
	fbIPRingBuffer_tailAdvanceOne(rbuff);

	return true;
}

bool fbIPRingBuffer_pop(FBIPRingBuffer *rbuff, FBInformationPacketHandle *destination)
{
	if(fbIPRingBuffer_isEmpty(rbuff))
		return false;

	// get data from where head is
	*destination = fbIPRingBuffer_loadFromHead(rbuff);

	// move head forward
	fbIPRingBuffer_headAdvanceOne(rbuff);

	return true;
}

bool fbIPRingBuffer_peek(FBIPRingBuffer *rbuff, FBInformationPacketHandle *destination)
{
    if(fbIPRingBuffer_isEmpty(rbuff))
        return false;

    // get data from where head is
	*destination = fbIPRingBuffer_loadFromHead(rbuff);

    return true;

}

void fbIPRingBuffer_flush(FBIPRingBuffer *rbuff)
{
	rbuff->count = 0;
	rbuff->head = 0;
}
