/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include <stdlib.h>

#include "vm/lib/memory.h"

void* fbMemory_reallocate( void* previous, size_t newSize ) {

    if( newSize == 0 ) {

        free(previous);
        return NULL;
    }

    return realloc( previous, newSize );                              
}                                                                 
