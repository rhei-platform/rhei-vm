/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_MEMORY_H
#define VM_MEMORY_H

#include <stddef.h>
#include <string.h>

#define FB_GROW_CAPACITY( capacity ) \
    ( (capacity) < 8 ? 8 : (capacity) * 2 )

#define FB_GROW_ARRAY( previous, type, count ) \
    (type*) fbMemory_reallocate( previous, sizeof(type) * (count) )

#define FB_GROW_ARRAY_E( previous, element_size, count ) \
    fbMemory_reallocate( previous, (element_size) * (count) )

#define FB_FREE_ARRAY( pointer ) \
    fbMemory_reallocate( pointer, 0 )

#define FB_CLEAR( pointer, type ) \
    memset( pointer, 0, sizeof( type ) )

#define FB_CLEAR_ARRAY( pointer, type, count ) \
    memset( pointer, 0, sizeof( type ) * (count) )

#define FB_SET( pointer, value, count ) \
    memset( pointer, value, count )

#define FB_COPY_ARRAY( dst, src, type, count ) \
    memcpy( dst, src, sizeof( type ) * (count) )

#define FB_COPY_ARRAY_E( dst, src, element_size, count ) \
    memcpy( dst, src, (element_size) * (count) )

#ifdef __cplusplus
extern "C" {
#endif

void* fbMemory_reallocate( void* previous, size_t newSize );

#ifdef __cplusplus
}
#endif



#endif // VM_MEMORY_H
