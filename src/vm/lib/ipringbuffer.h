/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_IP_RING_BUFFER_H
#define VM_IP_RING_BUFFER_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"

typedef struct _FBIPRingBuffer {
    uint32_t capacity;
    uint32_t count;
    uint32_t head;
    //uint32_t tail; // we use fill count to track position
    FBInformationPacketHandle *buffer;
} FBIPRingBuffer;

void fbIPRingBuffer_init(FBIPRingBuffer *rbuff, uint32_t capacity);
bool fbIPRingBuffer_isEmpty(FBIPRingBuffer *rbuff);
bool fbIPRingBuffer_isFull(FBIPRingBuffer *rbuff);
uint32_t fbIPRingBuffer_availableData(FBIPRingBuffer *rbuff);
uint32_t fbIPRingBuffer_availableSpace(FBIPRingBuffer *rbuff);
bool fbIPRingBuffer_push(FBIPRingBuffer *rbuff, FBInformationPacketHandle value);
bool fbIPRingBuffer_pop(FBIPRingBuffer *rbuff, FBInformationPacketHandle *destination);
bool fbIPRingBuffer_peek(FBIPRingBuffer *rbuff, FBInformationPacketHandle *destination);
void fbIPRingBuffer_flush(FBIPRingBuffer *rbuff);
void fbIPRingBuffer_destroy(FBIPRingBuffer *rbuff);

#ifdef __cplusplus
}
#endif

#endif // VM_IP_RING_BUFFER_H
