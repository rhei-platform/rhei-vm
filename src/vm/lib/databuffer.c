/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include <string.h>


#include "vm/lib/databuffer.h"

#include "vm/lib/memory.h"
#include "vm/error.h"
#include "vm/interpreter/checks.h"



static void growByteBuffer( FBByteBuffer* dataBuffer, index_t count ) {

    if( dataBuffer->capacity < dataBuffer->count + count ) {

        index_t newCount = dataBuffer->count + count;

        do {
            index_t newCapacity = dataBuffer->capacity;
            dataBuffer->capacity = FB_GROW_CAPACITY( newCapacity );
        } while( dataBuffer->capacity < newCount );

        dataBuffer->buffer = FB_GROW_ARRAY( dataBuffer->buffer, byte_t, dataBuffer->capacity );
    }
}

void fbByteBuffer_init( FBByteBuffer* dataBuffer ) {

    dataBuffer->count = 0;
    dataBuffer->capacity = 0;
    dataBuffer->buffer = NULL;
}

void fbByteBuffer_init_i( FBByteBuffer* dataBuffer, index_t capacity ) {

    fbByteBuffer_init( dataBuffer );

    growByteBuffer( dataBuffer, capacity );
}

void fbByteBuffer_addValue( FBByteBuffer* dataBuffer, byte_t value ) {

    growByteBuffer( dataBuffer, 1 );

    dataBuffer->buffer[ dataBuffer->count ] = value;
    dataBuffer->count++;
}

void fbByteBuffer_addArray( FBByteBuffer* dataBuffer, byte_t *data, index_t count ) {

    growByteBuffer( dataBuffer, count );

    memcpy( dataBuffer->buffer + dataBuffer->count, data, count );
    dataBuffer->count += count;
}

void fbByteBuffer_free( FBByteBuffer* dataBuffer ) {

    FB_FREE_ARRAY( dataBuffer->buffer );
    fbByteBuffer_init( dataBuffer );
}

void fbByteBuffer_reset( FBByteBuffer *dataBuffer ) {

    dataBuffer->count = 0;
}






static void growElementBuffer( FBElementBuffer* dataBuffer, index_t count ) {

    if( dataBuffer->capacity < dataBuffer->count + count ) {

        index_t newCount = dataBuffer->count + count;

        do {
            index_t newCapacity = dataBuffer->capacity;
            dataBuffer->capacity = FB_GROW_CAPACITY( newCapacity );
        } while( dataBuffer->capacity < newCount );

        dataBuffer->buffer = FB_GROW_ARRAY_E( dataBuffer->buffer, dataBuffer->elementSize, dataBuffer->capacity );
    }
}

static void* getElementPointerUnchecked( FBElementBuffer* dataBuffer, index_t index ) {

    return ( void* )( ( byte_t* ) dataBuffer->buffer + index * dataBuffer->elementSize );
}

FBElementBuffer* fbElementBuffer_create( index_t elementSize ) {

    FBElementBuffer *dataBuffer = FB_GROW_ARRAY( NULL, FBElementBuffer, 1 );

    fbElementBuffer_init( dataBuffer, elementSize );

    return dataBuffer;
}

void fbElementBuffer_destroy( FBElementBuffer* dataBuffer ) {

    fbElementBuffer_free( dataBuffer );

    FB_FREE_ARRAY( dataBuffer );
}


void fbElementBuffer_init( FBElementBuffer* dataBuffer, index_t elementSize ) {

    dataBuffer->count = 0;
    dataBuffer->capacity = 0;
    dataBuffer->elementSize = elementSize;
    dataBuffer->buffer = NULL;
}

void fbElementBuffer_init_i( FBElementBuffer* dataBuffer, index_t elementSize, index_t capacity ) {

    fbElementBuffer_init( dataBuffer, elementSize );

    growElementBuffer( dataBuffer, capacity );
}

void fbElementBuffer_addValue( FBElementBuffer* dataBuffer, void *value ) {

    growElementBuffer( dataBuffer, 1 );

    // dataBuffer->buffer[ dataBuffer->count ] = value;
    void *dst = getElementPointerUnchecked( dataBuffer, dataBuffer->count );
    memcpy( dst, value, dataBuffer->elementSize );
    dataBuffer->count++;
}

void fbElementBuffer_addArray( FBElementBuffer* dataBuffer, void *data, index_t count ) {

    growElementBuffer( dataBuffer, count );

    void *dst = getElementPointerUnchecked( dataBuffer, dataBuffer->count );
    memcpy( dst, data, count * dataBuffer->elementSize );
    dataBuffer->count += count;
}

void fbElementBuffer_free( FBElementBuffer* dataBuffer ) {

    FB_FREE_ARRAY( dataBuffer->buffer );
    fbElementBuffer_init( dataBuffer, dataBuffer->elementSize );
}

void* fbElementBuffer_getElementPointer( FBElementBuffer* dataBuffer, index_t index ) {

    if( dataBuffer->count <= index )
        return NULL;
    
    return getElementPointerUnchecked( dataBuffer, index );
}

void fbElementBuffer_removeElement( FBElementBuffer* dataBuffer, index_t index ) {

    if( dataBuffer->count <= index )
        return;

    if( dataBuffer->count - 1 != index ) {
        // not the last element, we need to move the ones after it

        size_t tailCount = dataBuffer->count - (index + 1);

        void *dst = getElementPointerUnchecked( dataBuffer, index );
        void *src = getElementPointerUnchecked( dataBuffer, index + 1 );

        memmove( dst, src, dataBuffer->elementSize * tailCount );
    }

    dataBuffer->count --;
}
