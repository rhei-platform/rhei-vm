/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_DATABUFFER_H
#define VM_DATABUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"

typedef struct _FBByteBuffer
{
    index_t count;
    index_t capacity;
    byte_t *buffer;
} FBByteBuffer;

void fbByteBuffer_init( FBByteBuffer* dataBuffer );
void fbByteBuffer_init_i( FBByteBuffer* dataBuffer, index_t capacity );
void fbByteBuffer_addValue( FBByteBuffer* dataBuffer, byte_t value );
void fbByteBuffer_addArray( FBByteBuffer* dataBuffer, byte_t *data, index_t count );
void fbByteBuffer_free( FBByteBuffer* dataBuffer );
void fbByteBuffer_reset( FBByteBuffer *dataBuffer );

static inline bool fbByteBuffer_atEndOfBuffer( FBByteBuffer *dataBuffer ) {
    return dataBuffer->count == dataBuffer->capacity;
}


typedef struct _FBElementBuffer
{
    index_t count;
    index_t capacity;
    index_t elementSize;
    void *buffer;
} FBElementBuffer;

FBElementBuffer* fbElementBuffer_create( index_t elementSize );
void fbElementBuffer_destroy( FBElementBuffer* dataBuffer );

void fbElementBuffer_init( FBElementBuffer* dataBuffer, index_t elementSize );
void fbElementBuffer_init_i( FBElementBuffer* dataBuffer, index_t elementSize, index_t capacity );
void fbElementBuffer_addValue( FBElementBuffer* dataBuffer, void *value );
void fbElementBuffer_addArray( FBElementBuffer* dataBuffer, void *data, index_t count );
// !!! NOTE !!!
// DO NOT keep around return value of this function, keep the index to the value instead (the 'index' parameter).
// Any call to addValue() or addArray() could reallocate underlying byte buffer and possibly make the pointer
// point to invalid memory location.
void* fbElementBuffer_getElementPointer( FBElementBuffer* dataBuffer, index_t index );
void fbElementBuffer_free( FBElementBuffer* dataBuffer );

void fbElementBuffer_removeElement( FBElementBuffer* dataBuffer, index_t index );


#define fbElementBuffer_get( eb, idx, type ) \
    *((type*) fbElementBuffer_getElementPointer( eb, idx ))

#ifdef __cplusplus
}
#endif

#endif // VM_DATABUFFER_H
