/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_DEBUG_H
#define VM_DEBUG_H

// #include "metadata.h"
#include "vm/lib/databuffer.h"
#include "vm/core/network.h"

#include "vm/error.h" // FBContextStatus

#ifdef __cplusplus
extern "C" {
#endif

// void fbDebug_decodeElementDescriptor( FBElementDescriptor descriptor );
// void fbDebug_decodeElementDescriptorArray( FBElementDescriptorArray* elementDescriptorArray );

//====================================================================================
// Instructions
//

void fbDebug_decodeCode( FBByteBuffer *code );
void fbDebug_decodeCode_bv( byte_t *code, index_t count );

void fbDebug_traceInstruction( byte_t *code, index_t offset );

#define IS_STACK_EMPTY( context ) \
    ( (context)->stackTop == &((context)->stack[0]) )

//
//====================================================================================


//====================================================================================
// Printer
//

void fbDebug_printNetworkContext( FBNetworkContext *networkContext );
void fbDebug_printNetworkContextState( FBNetworkContext *networkContext );

//
//====================================================================================


//====================================================================================
// Dumper
//

typedef enum _DumpState {

    DUMP_STATE_NONE,

    DUMP_STATE_CONNECTION_LIVE,
    DUMP_STATE_CONNECTION_TERMINATED,

    DUMP_STATE_PORT_NULL,
    DUMP_STATE_PORT_OPEN,
    DUMP_STATE_PORT_CLOSED,

    DUMP_STATE_CONTEXT_INITIALIZED,
    DUMP_STATE_CONTEXT_RUNNING,
    DUMP_STATE_CONTEXT_SUSPENDED,
    DUMP_STATE_CONTEXT_TERMINATED,

} DumpState;

typedef struct _DumpContextState {

    index_t contextIndex;
    DumpState state;
    FBContextStatus contextStatus;

} DumpContextState;

typedef struct _DumpConnectionState {

    index_t connectionIndex;
    DumpState state;
    DumpState sourceState;
    DumpState sinkState;
    size_t fifoCapacity;
    size_t fifoCount;

} DumpConnectionState;

typedef struct _DumpStateData {

    size_t contextCount;
    size_t connectionCount;
    size_t nativeInputsCount;
    size_t nativeOutputsCount;

    DumpContextState *contextStates;
    DumpConnectionState *connectionStates;
    DumpConnectionState *nativeInputStates;
    DumpConnectionState *nativeOutputStates;

} DumpStateData;


DumpStateData* fbDebug_dumpNetworkContextStateData( FBNetworkContext *networkContext );
void fbDebug_freeDumpData( DumpStateData *data );

//
//====================================================================================


#ifdef VM_TRACE

#define TRACE(...) {printf("[trace] "__VA_ARGS__);printf("\n");}
#else

#define TRACE(...)

#endif

#ifdef __cplusplus
}
#endif

#endif // VM_DEBUG_H
