/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/debug/vm_debug.h"


#include "vm/interpreter/context.h"
#include "vm/lib/memory.h"


//====================================================================================
//
// Ptr helper
//
//====================================================================================


// PRIxPTR
#include <inttypes.h>
// printf
#include <stdio.h>



static uintptr_t ptrVal( void *ptr ) {

    return ((uintptr_t)ptr) & 0x00000000000fffff;
}

#define PTRFMT "0x%05" PRIxPTR


//====================================================================================
//
// Dumper
//
//====================================================================================


static DumpState _getPortState( FBInputOutput *port ) {

    if( port )
        if( fbInputOutput_isOpen( port ) )
            return DUMP_STATE_PORT_OPEN;
        else
            return DUMP_STATE_PORT_CLOSED;
    else
        return DUMP_STATE_PORT_NULL;
}

static void _getConnectionStateData( FBConnection *conn, DumpConnectionState *connState ) {

    if( fbConnection_isTerminated( conn ) )
        connState->state = DUMP_STATE_CONNECTION_TERMINATED;
    else
        connState->state = DUMP_STATE_CONNECTION_LIVE;

    connState->sourceState = _getPortState( conn->source );
    connState->sinkState = _getPortState( conn->sink );
    connState->fifoCapacity = conn->fifoBuffer.capacity;
    connState->fifoCount = conn->fifoBuffer.count;
}


void fbDebug_freeDumpData( DumpStateData *data ) {
    FB_FREE_ARRAY( data );
}

DumpStateData* fbDebug_dumpNetworkContextStateData( FBNetworkContext *networkContext ) {

    DumpStateData *dumpData = FB_GROW_ARRAY( NULL, DumpStateData, 1 );

    // printf("\n");
    // printf("context  R S T - STATUS");

    dumpData->contextCount = networkContext->allContextsCount;
    dumpData->contextStates = FB_GROW_ARRAY( NULL, DumpContextState, dumpData->contextCount );

    FBContext *ctx;
    for( index_t idx = 0; idx < networkContext->allContextsCount; idx++ ) {

        ctx = fbNetwork_getContext( networkContext, idx );
        // printf("CTX-%zu", idx);

        DumpContextState *ctxState = &dumpData->contextStates[ idx ];
        ctxState->contextIndex = idx;
        switch( fbContext_getState( ctx ) ) {

            case CONTEXT_RUNNING:
                ctxState->state = DUMP_STATE_CONTEXT_RUNNING;
                break;

            case CONTEXT_SUSPENDED:
                ctxState->state = DUMP_STATE_CONTEXT_SUSPENDED;
                break;

            case CONTEXT_TERMINATED:
                ctxState->state = DUMP_STATE_CONTEXT_TERMINATED;
                break;

            default:
                ctxState->state = DUMP_STATE_CONTEXT_INITIALIZED;
                break;
        }

        FBContextStatus status = fbError_getStatus( ctx );
        ctxState->contextStatus = status;
    }

    // printf("connection T SR SN FIFO");

    dumpData->connectionCount = networkContext->allConnectionsCount;
    dumpData->connectionStates = FB_GROW_ARRAY( NULL, DumpConnectionState, dumpData->connectionCount );

    FBConnection *conn;
    for( index_t idx = 0; idx < networkContext->allConnectionsCount; idx++ ) {

        conn = fbNetwork_getConnection( networkContext, idx );

        // printf("CON-%zu", idx);
        DumpConnectionState *connState = &dumpData->connectionStates[ idx ];
        connState->connectionIndex = idx;
        _getConnectionStateData( conn, connState );
    }

    // printf("native-io  T SR SN FIFO");

    dumpData->nativeInputsCount = networkContext->nativeInputsCount;
    dumpData->nativeInputStates = FB_GROW_ARRAY( NULL, DumpConnectionState, dumpData->nativeInputsCount );

    FBNetworkNativeIO *nio;
    for( index_t idx = 0; idx < networkContext->nativeInputsCount; idx++ ) {

        nio = fbNetwork_getNativeInput( networkContext, idx );

        // printf("NTV-in[%zu]", idx);
        DumpConnectionState *connState = &dumpData->nativeInputStates[ idx ];
        connState->connectionIndex = idx;
        _getConnectionStateData( nio->port->connection, connState );
    }

    dumpData->nativeOutputsCount = networkContext->nativeOutputsCount;
    dumpData->nativeOutputStates = FB_GROW_ARRAY( NULL, DumpConnectionState, dumpData->nativeOutputsCount );

    // FBNetworkNativeIO *nio;
    for( index_t idx = 0; idx < networkContext->nativeOutputsCount; idx++ ) {

        nio = fbNetwork_getNativeOutput( networkContext, idx );

        // printf("NTV-out[%zu]", idx);
        DumpConnectionState *connState = &dumpData->nativeOutputStates[ idx ];
        connState->connectionIndex = idx;
        _getConnectionStateData( nio->port->connection, connState );
    }

    return dumpData;
}



//====================================================================================
//
// Printer
//
//====================================================================================




/*
Sample output:

CTX-0: 0x01600
    inp[ 0 ]: 0x068b0
    inp[ 1 ]: 0x068c8
    out[ 0 ]: 0x068e0

CTX-1: 0x01a80
    inp[ 0 ]: 0x043c0
    inp[ 1 ]: 0x043d8
    out[ 0 ]: 0x06900

CTX-2: 0x01f00
    inp[ 0 ]: 0x04410
    out[ 0 ]: 0x04430

CON-0:
    source: [ 0x068e0 ] --[ 0x04660 ]--> sink: [ 0x043c0 ]

CON-1:
    source: [ 0x06900 ] --[ 0x04690 ]--> sink: [ 0x04410 ]

NET-in[ 0 ]: 0x068b0
NET-in[ 1 ]: 0x068c8
NET-in[ 2 ]: 0x043d8

NET-out[ 0 ]: 0x04430

NTV-in[ 0 ]:  source: [ none ] --[ 0x04700 ]--> sink: [ 0x068b0 ]
NTV-in[ 1 ]:  source: [ none ] --[ 0x04738 ]--> sink: [ 0x068c8 ]
NTV-in[ 2 ]:  source: [ none ] --[ 0x04770 ]--> sink: [ 0x043d8 ]

NTV-out[ 0 ]:  source: [ 0x04430 ] --[ 0x047e0 ]--> sink: [ none ]

*/
void fbDebug_printNetworkContext( FBNetworkContext *networkContext ) {

    FBContext *ctx;
    FBInputOutput *port;
    FBConnection *conn;
    FBNetworkNativeIO *nio;

    for( index_t idx = 0; idx < networkContext->allContextsCount; idx++ ) {

        printf("\n");

        ctx = fbNetwork_getContext( networkContext, idx );
        printf("CTX-%zu: " PTRFMT "\n", idx, ptrVal(ctx));

        for( index_t inidx = 0; inidx < ctx->component->inputCount; inidx++ ) {

            port = fbContext_getInputPort( ctx, inidx );
            printf("    inp[ %zu ]: " PTRFMT "\n", inidx, ptrVal(port));
        }

        for( index_t outidx = 0; outidx < ctx->component->outputCount; outidx++ ) {

            port = fbContext_getOutputPort( ctx, outidx );
            printf("    out[ %zu ]: " PTRFMT "\n", outidx, ptrVal(port));
        }
    }

    if( networkContext->allContextsCount ) printf("\n");

    for( index_t idx = 0; idx < networkContext->allConnectionsCount; idx++ ) {

        printf("\n");

        conn = fbNetwork_getConnection( networkContext, idx );
        printf("CON-%zu:\n", idx);
        printf("    ");
        printf("source: [ " PTRFMT " ] ", ptrVal(conn->source));
        printf("--[ " PTRFMT " ]--> ", ptrVal(conn));
        printf("sink: [ " PTRFMT " ]\n", ptrVal(conn->sink));
    }

    if( networkContext->allConnectionsCount ) printf("\n");

    for( index_t idx = 0; idx < networkContext->networkInputsCount; idx++ ) {

        port = fbNetwork_getInput( networkContext, idx );
        printf("NET-in[ %zu ]: " PTRFMT "\n", idx, ptrVal(port));
    }

    if( networkContext->networkInputsCount ) printf("\n");

    for( index_t idx = 0; idx < networkContext->networkOutputsCount; idx++ ) {

        port = fbNetwork_getOutput( networkContext, idx );
        printf("NET-out[ %zu ]: " PTRFMT "\n", idx, ptrVal(port));
    }

    if( networkContext->networkOutputsCount ) printf("\n");

    for( index_t idx = 0; idx < networkContext->nativeInputsCount; idx++ ) {

        nio = fbNetwork_getNativeInput( networkContext, idx );

        printf("NTV-in[ %zu ]:  ", idx);
        printf("source: [ " PTRFMT " ] ", ptrVal(nio->port->connection->source));
        printf("--[ " PTRFMT " ]--> ", ptrVal(nio->port->connection));
        printf("sink: [ " PTRFMT " ]\n", ptrVal(nio->port->connection->sink));
    }

    if( networkContext->nativeInputsCount ) printf("\n");

    for( index_t idx = 0; idx < networkContext->nativeOutputsCount; idx++ ) {

        nio = fbNetwork_getNativeOutput( networkContext, idx );

        printf("NTV-out[ %zu ]:  ", idx);
        printf("source: [ " PTRFMT " ] ", ptrVal(nio->port->connection->source));
        printf("--[ " PTRFMT " ]--> ", ptrVal(nio->port->connection));
        printf("sink: [ " PTRFMT " ]\n", ptrVal(nio->port->connection->sink));
    }

    if( networkContext->nativeOutputsCount ) printf("\n");
}

/*
Sample output:


context  R S T - STATUS
CTX-0    x       REQUEST_READ
CTX-1    x       OK
CTX-2    x       YIELD

connection T SR SN FIFO
CON-0        x  o  x..
CON-1        o  o  ...
CON-3      x -  -  -

native-io  T SR SN FIFO
NTV-in[0]    -  o  .
NTV-in[1]    -  o  x
NTV-in[2]    -  o  x
NTV-ou[0]    o  .  .

*/
static void _printPortState( DumpState portState ) {

    switch( portState ) {
        case DUMP_STATE_PORT_OPEN:
            printf("o  ");
        break;
        case DUMP_STATE_PORT_CLOSED:
            printf("c  ");
        break;
        // case DUMP_STATE_PORT_NULL:
        default:
            printf("-  ");
        break;
    }
}
static void _printConnectionState( DumpConnectionState *connState ) {

    if( connState->state == DUMP_STATE_CONNECTION_TERMINATED )
        printf("x ");
    else
    // if( connState->state == DUMP_STATE_CONNECTION_LIVE )
        printf("  ");

    _printPortState( connState->sourceState );
    _printPortState( connState->sinkState );
    
    index_t fifoCount = connState->fifoCount;
    index_t fifoCapacity = connState->fifoCapacity;
    if( fifoCapacity == 0 )
        printf("-");
    else
    {
        for( index_t i = 0; i < fifoCount; i++ )
            printf("x");
        for( index_t i = 0; i < fifoCapacity - fifoCount; i++ )
            printf(".");
    }
}
void _printContextState( DumpState state ) {

    switch( state ) {

        case DUMP_STATE_CONTEXT_RUNNING:
            printf("x       ");
            break;

        case DUMP_STATE_CONTEXT_SUSPENDED:
            printf("  x     ");
            break;

        case DUMP_STATE_CONTEXT_TERMINATED:
            printf("    x   ");
            break;

        // case DUMP_STATE_CONTEXT_INITIALIZED:
        default:
            printf("      x ");
            break;
    }
}
void _printContextStatus( FBContextStatus contextStatus ) {

    switch( contextStatus ) {

        case STATUS_OK:
            printf("OK");
            break;
        case STATUS_NONE:
            printf("NONE");
            break;

        case STATUS_REQUEST_CREATE:
            printf("REQUEST CREATE");
            break;
        case STATUS_REQUEST_DROP:
            printf("REQUEST DROP");
            break;
        case STATUS_REQUEST_READ:
            printf("REQUEST READ");
            break;
        case STATUS_REQUEST_TERMINATE:
            printf("REQUEST TERMINATE");
            break;
        case STATUS_REQUEST_WRITE:
            printf("REQUEST WRITE");
            break;

        case STATUS_INVALID_STATE:
            printf("INVALID STATE");
            break;
        case STATUS_YIELD:
            printf("YIELD");
            break;
        case STATUS_CODE_END:
            printf("CODE END");
            break;

        case STATUS_ERROR_CONNECTION_EMPTY:
            printf("ERROR: CONNECTION EMPTY");
            break;
        case STATUS_ERROR_CONNECTION_FULL:
            printf("ERROR: CONNECTION FULL");
            break;
        case STATUS_ERROR_CONNECTION_TERMINATED:
            printf("ERROR: CONNECTION TERMINATED");
            break;

        case STATUS_ERROR_PORT_CLOSED:
            printf("ERROR: PORT CLOSED");
            break;

        default:
            printf("%d", contextStatus);
            break;
    }
}
void _printContextId( DumpContextState *ctxState, FBNetworkContext *networkContext ) {

    FBContext *ctx = fbNetwork_getContext( networkContext, ctxState->contextIndex );
    printf("    [ " PTRFMT " ]", ptrVal(ctx));
}
void _printConnectionId( DumpConnectionState *connState, FBNetworkContext *networkContext ) {

    FBConnection *conn = fbNetwork_getConnection( networkContext, connState->connectionIndex );
    printf("    [ " PTRFMT " ]", ptrVal(conn));
}
void fbDebug_printNetworkContextState( FBNetworkContext *networkContext ) {

    DumpStateData *data = fbDebug_dumpNetworkContextStateData( networkContext );


    printf("\n");
    printf("context  R S T - STATUS");

    // FBContext *ctx;
    DumpContextState *ctxState;
    for( index_t idx = 0; idx < data->contextCount; idx++ ) {

        printf("\n");

        ctxState = &data->contextStates[ idx ];
        printf("CTX-%zu", idx);
        printf("    ");

        _printContextState( ctxState->state );
        _printContextStatus( ctxState->contextStatus );
        _printContextId( ctxState, networkContext );
    }

    printf("\n");
    printf("connection T SR SN FIFO");

    // FBConnection *conn;
    DumpConnectionState *connState;
    for( index_t idx = 0; idx < data->connectionCount; idx++ ) {

        printf("\n");

        connState = &data->connectionStates[ idx ];
        printf("CON-%zu", idx);
        printf("      ");
        _printConnectionState( connState );
        _printConnectionId( connState, networkContext );
    }

    printf("\n");
    printf("native-io  T SR SN FIFO");

    // FBNetworkNativeIO *nio;
    DumpConnectionState *nioState;
    for( index_t idx = 0; idx < data->nativeInputsCount; idx++ ) {

        nioState = &data->nativeInputStates[ idx ];

        printf("\n");
        printf("NTV-in[%zu]", idx);
        printf("  ");
        _printConnectionState( nioState );
    }

    // FBNetworkNativeIO *nio;
    for( index_t idx = 0; idx < data->nativeOutputsCount; idx++ ) {

        nioState = &data->nativeOutputStates[ idx ];

        printf("\n");
        printf("NTV-out[%zu]", idx);
        printf(" ");
        _printConnectionState( nioState );
    }

    printf("\n");

    fbDebug_freeDumpData( data );
}
