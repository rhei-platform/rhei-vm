/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include <stdio.h>

#include "vm/debug/vm_debug.h"
#include "vm/interpreter/opcode.h"

/*
void fbDebug_decodeElementDescriptor( FBElementDescriptor descriptor ) {

    switch (descriptor.type)
    {
        case T_RECORD_HDR:
            printf("T_RECORD_HDR  size( %zu ), elements( %zu )", descriptor.offset, descriptor.count); 
            break;
    
        case T_RECORD_REF:
            printf("T_RECORD_REF  @( %zu ), off( %zu ), [ %zu ]", descriptor.table_index, descriptor.offset, descriptor.count); 
            break;
    
        case T_MULTIARRAY:
            printf("T_MULTIARRAY  @( %zu ), [ %zu ]", descriptor.table_index, descriptor.count); 
            break;
    
        case T_BOOL:
            printf("T_BOOL  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_BYTE:
            printf("T_BYTE  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_INT:
            printf("T_INT  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_FLOAT:
            printf("T_FLOAT  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_DOUBLE:
            printf("T_DOUBLE  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_CHAR:
            printf("T_CHAR  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_INT8:
            printf("T_INT8  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_UINT8:
            printf("T_UINT8  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_INT16:
            printf("T_INT16  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_UINT16:
            printf("T_UINT16  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_INT32:
            printf("T_INT32  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_UINT32:
            printf("T_UINT32  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_INT64:
            printf("T_INT64  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        case T_UINT64:
            printf("T_UINT64  off( %zu ), [ %zu ]", descriptor.offset, descriptor.count); 
            break;
    
        default:
            printf("Unknown data type %d", descriptor.type); 
            break;
    }
}

void fbDebug_decodeElementDescriptorArray( FBElementDescriptorArray* elementDescriptorArray ) {

    for (index_t index = 0; index < elementDescriptorArray->count; index++) {
        
        FBElementDescriptor descriptor = elementDescriptorArray->array[index];

        printf("%04d ", (int)index);

        if( descriptor.type != T_RECORD_HDR )
            printf("  "); 

        fbDebug_decodeElementDescriptor( descriptor );

        printf("\n");
    }
}
*/

static int decodeInstruction( byte_t *code, index_t offset ) {

    FBVMInstruction instruction = code[ offset ];

    switch( instruction )
    {
        case OP_INVALID:
            printf("OP_INVALID");
            break;

        case OP_END:
            printf("OP_END");
            break;
        case OP_TERMINATE:
            printf("OP_TERMINATE");
            break;

        case OP_SWITCH:
            printf("OP_SWITCH");
            break;
        case OP_JUMP:
            printf("OP_JUMP");
            break;


        case OP_JUMP_IF_FALSE:
            printf("OP_JUMP_IF_FALSE");
            break;
        case OP_JUMP_IF_LESS_EQUAL:
            printf("OP_JUMP_IF_LESS_EQUAL");
            break;
        case OP_JUMP_IF_GREATER_EQUAL:
            printf("OP_JUMP_IF_GREATER_EQUAL");
            break;
        case OP_JUMP_IF_LESS:
            printf("OP_JUMP_IF_LESS");
            break;
        case OP_JUMP_IF_GREATER:
            printf("OP_JUMP_IF_GREATER");
            break;
        case OP_JUMP_IF_EQUAL:
            printf("OP_JUMP_IF_EQUAL");
            break;
        case OP_JUMP_IF_NOT_EQUAL:
            printf("OP_JUMP_IF_NOT_EQUAL");
            break;






        case OP_JUMP_IF_ZERO_2:
            printf("OP_JUMP_IF_ZERO_2");
            break;



        case OP_BCONST_I8:
            printf("OP_BCONST_I8");
            break;
        case OP_BCONST_I16:
            printf("OP_BCONST_I16");
            break;
        case OP_BCONST_I32:
            printf("OP_BCONST_I32");
            break;
        case OP_BCONST_I64:
            printf("OP_BCONST_I64");
            break;
        case OP_SCONST_I8:
            printf("OP_SCONST_I8");
            break;
        case OP_SCONST_I16:
            printf("OP_SCONST_I16");
            break;
        case OP_SCONST_I32:
            printf("OP_SCONST_I32");
            break;
        case OP_SCONST_I64:
            printf("OP_SCONST_I64");
            break;

        case OP_CONST_0:
            printf("OP_CONST_0");
            break;
        case OP_CONST_1:
            printf("OP_CONST_1");
            break;
        case OP_CONST_M1:
            printf("OP_CONST_M1");
            break;

        case OP_BPUSH:
            printf("OP_BPUSH");
            break;
        case OP_SPUSH:
            printf("OP_SPUSH");
            break;



        case OP_DUP:
            printf("OP_DUP");
            break;
        case OP_DUP_2:
            printf("OP_DUP_2");
            break;



        case OP_CLOAD_I8:
            printf("OP_CLOAD_I8");
            break;
        case OP_CLOAD_I16:
            printf("OP_CLOAD_I16");
            break;
        case OP_CLOAD_I32:
            printf("OP_CLOAD_I32");
            break;
        case OP_CLOAD_I64:
            printf("OP_CLOAD_I64");
            break;

        case OP_LOAD_I8:
            printf("OP_LOAD_I8");
            break;
        case OP_LOAD_I16:
            printf("OP_LOAD_I16");
            break;
        case OP_LOAD_I32:
            printf("OP_LOAD_I32");
            break;
        case OP_LOAD_I64:
            printf("OP_LOAD_I64");
            break;

        case OP_BLOAD_I8:
            printf("OP_BLOAD_I8");
            break;
        case OP_BLOAD_I16:
            printf("OP_BLOAD_I16");
            break;
        case OP_BLOAD_I32:
            printf("OP_BLOAD_I32");
            break;
        case OP_BLOAD_I64:
            printf("OP_BLOAD_I64");
            break;

        case OP_CSTORE_I8:
            printf("OP_CSTORE_I8");
            break;
        case OP_CSTORE_I16:
            printf("OP_CSTORE_I16");
            break;
        case OP_CSTORE_I32:
            printf("OP_CSTORE_I32");
            break;
        case OP_CSTORE_I64:
            printf("OP_CSTORE_I64");
            break;

        case OP_STORE_I8:
            printf("OP_STORE_I8");
            break;
        case OP_STORE_I16:
            printf("OP_STORE_I16");
            break;
        case OP_STORE_I32:
            printf("OP_STORE_I32");
            break;
        case OP_STORE_I64:
            printf("OP_STORE_I64");
            break;

        case OP_BSTORE_I8:
            printf("OP_BSTORE_I8");
            break;
        case OP_BSTORE_I16:
            printf("OP_BSTORE_I16");
            break;
        case OP_BSTORE_I32:
            printf("OP_BSTORE_I32");
            break;
        case OP_BSTORE_I64:
            printf("OP_BSTORE_I64");
            break;




        case OP_NEG_I:
            printf("OP_NEG_I");
            break;
        case OP_NEG_L:
            printf("OP_NEG_L");
            break;
        case OP_NEG_F:
            printf("OP_NEG_F");
            break;
        case OP_NEG_D:
            printf("OP_NEG_D");
            break;

        case OP_NOT:
            printf("OP_NOT");
            break;

        case OP_BIT_INVERT_I:
            printf("OP_BIT_INVERT_I");
            break;
        case OP_BIT_INVERT_L:
            printf("OP_BIT_INVERT_L");
            break;


        case OP_MUL_I:
            printf("OP_MUL_I");
            break;
        case OP_MUL_L:
            printf("OP_MUL_L");
            break;
        case OP_MUL_F:
            printf("OP_MUL_F");
            break;
        case OP_MUL_D:
            printf("OP_MUL_D");
            break;

        case OP_DIV_I:
            printf("OP_DIV_I");
            break;
        case OP_DIV_L:
            printf("OP_DIV_L");
            break;
        case OP_DIV_F:
            printf("OP_DIV_F");
            break;
        case OP_DIV_D:
            printf("OP_DIV_D");
            break;

        case OP_MOD_I:
            printf("OP_MOD_I");
            break;
        case OP_MOD_L:
            printf("OP_MOD_L");
            break;

        case OP_ADD_I:
            printf("OP_ADD_I");
            break;
        case OP_ADD_L:
            printf("OP_ADD_L");
            break;
        case OP_ADD_F:
            printf("OP_ADD_F");
            break;
        case OP_ADD_D:
            printf("OP_ADD_D");
            break;

        case OP_SUB_I:
            printf("OP_SUB_I");
            break;
        case OP_SUB_L:
            printf("OP_SUB_L");
            break;
        case OP_SUB_F:
            printf("OP_SUB_F");
            break;
        case OP_SUB_D:
            printf("OP_SUB_D");
            break;


        case OP_SHIFT_LEFT_I:
            printf("OP_SHIFT_LEFT_I");
            break;
        case OP_SHIFT_LEFT_L:
            printf("OP_SHIFT_LEFT_L");
            break;
        case OP_SHIFT_RIGHT_I:
            printf("OP_SHIFT_RIGHT_I");
            break;
        case OP_SHIFT_RIGHT_L:
            printf("OP_SHIFT_RIGHT_L");
            break;
        case OP_BIT_AND_I:
            printf("OP_BIT_AND_I");
            break;
        case OP_BIT_AND_L:
            printf("OP_BIT_AND_L");
            break;
        case OP_BIT_XOR_I:
            printf("OP_BIT_XOR_I");
            break;
        case OP_BIT_XOR_L:
            printf("OP_BIT_XOR_L");
            break;
        case OP_BIT_OR_I:
            printf("OP_BIT_OR_I");
            break;
        case OP_BIT_OR_L:
            printf("OP_BIT_OR_L");
            break;



        case OP_CMP_I:
            printf("OP_CMP_I");
            break;
        case OP_CMP_UI:
            printf("OP_CMP_UI");
            break;
        case OP_CMP_L:
            printf("OP_CMP_L");
            break;
        case OP_CMP_UL:
            printf("OP_CMP_UL");
            break;
        case OP_CMP_F:
            printf("OP_CMP_F");
            break;
        case OP_CMP_D:
            printf("OP_CMP_D");
            break;

        case OP_AND:
            printf("OP_AND");
            break;
        case OP_OR:
            printf("OP_OR");
            break;



        case OP_REQUEST_READ:
            printf("OP_REQUEST_READ");
            break;
        case OP_REQUEST_WRITE:
            printf("OP_REQUEST_WRITE");
            break;
        case OP_CREATE_IP:
            printf("OP_CREATE_IP");
            break;
        case OP_SCREATE_IP:
            printf("OP_SCREATE_IP");
            break;
        case OP_DROP_IP:
            printf("OP_DROP_IP");
            break;
   
        default:
            printf("Unknown instruction: %d", instruction);
            break;
    }

    return ++offset;
}

void fbDebug_decodeCode( FBByteBuffer *code ) {

    fbDebug_decodeCode_bv( code->buffer, code->count );

}

void fbDebug_decodeCode_bv( byte_t *code, index_t count ) {

    for( index_t offset = 0; offset < count; ) {
        
        printf("%04d ", (int)offset);

        offset = decodeInstruction( code, offset );

        printf("\n");
    }
}


void fbDebug_traceInstruction( byte_t *code, index_t offset ) {
    #ifdef VM_TRACE
    decodeInstruction( code, offset );
    TRACE("\n");
    #else
    (void)code;
    (void)offset;
    #endif
}
