/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_BINARY_H
#define VM_BINARY_H

#include "vm/core/component.h"

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


#define BINARY_MAGIC_NUMBER ((uint32_t)(*"Rhei"))
#define BINARY_MAJOR_VERSION 1
#define BINARY_MINOR_VERSION 0


/*
Binary {
    u4      magic;
    u2      major_version;
    u2      minor_version;

    u4      constant_pool_size;
    u4      constant_pool_offset;

    u2      setup_code_size;
    u2      setup_code_offset;
    u2      loop_code_size;
    u2      loop_code_offset;

    u4      component_data_size;
    u4      component_stack_size;
    u2      information_packet_count;
    u2      input_count;
    u2      output_count;

    u2      component_name_size;

    // payload[] - right after component_name_size
    // @0
    u1      component_name[ component_name_size ];

    // @constant_pool_offset
    u1      constant_pool[ constant_pool_size ];

    // @setup_code_offset
    u1      setup_code[ setup_code_size ];

    // @loop_code_offset
    u1      loop_code[ loop_code_size ];

    // @(loop_code_offset + loop_code_size)
    debug_info  debug_data;
}
*/

#pragma pack(push,1)


typedef struct _FBComponentHeader
{
    uint32_t constant_pool_size;
    uint32_t constant_pool_offset;

    uint16_t setup_code_size;
    uint16_t setup_code_offset;
    uint16_t loop_code_size;
    uint16_t loop_code_offset;

    uint32_t component_data_size;
    uint32_t component_stack_size;
    uint16_t information_packet_count;
    uint16_t input_count;
    uint16_t output_count;

    uint16_t component_name_size;
    // char component_name[];
    // the first thing in payload[] is this component's name
    uint8_t payload[0];
} FBComponentHeader;

typedef struct _FBBinary
{
    uint32_t magic;
    uint16_t major_version;
    uint16_t minor_version;

    FBComponentHeader component_header;
} FBBinary;


#pragma pack(pop)

FBComponent *fbBinary_getComponent( byte_t *binaryData, size_t dataSize );

#ifdef __cplusplus
}
#endif

#endif // VM_BINARY_H
