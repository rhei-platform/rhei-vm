/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/util/network_builder.h"
#include "vm/core/network.h"
#include "vm/interpreter/context.h"

#include "vm/lib/memory.h"

#include "vm/interpreter/checks.h"


#define ADD_EXTRA_ELEMENT( type, buffer ) \
{ \
    type dummy; \
    fbElementBuffer_addValue( &(buffer), &dummy ); \
}


FBComponent* fbNetworkBuilder_getComponent( FBNetworkBuilderData *data, index_t componentIndex ) {

    CHECK_ARRAY_BOUNDS( data->allComponentInstances, componentIndex );
    return *((FBComponent**) fbElementBuffer_getElementPointer( &data->allComponentInstances, componentIndex ));
}

FBNetworkBuilderDataConnection* fbNetworkBuilder_getConnectionData( FBNetworkBuilderData *data, index_t connectionIndex ) {

    CHECK_ARRAY_BOUNDS( data->allConnections, connectionIndex );
    return (FBNetworkBuilderDataConnection*) fbElementBuffer_getElementPointer( &data->allConnections, connectionIndex );
}

FBNetworkBuilderDataIO* fbNetworkBuilder_getNetworkInputData( FBNetworkBuilderData *data, index_t inputIndex ) {

    CHECK_ARRAY_BOUNDS( data->networkInputs, inputIndex );
    return (FBNetworkBuilderDataIO*) fbElementBuffer_getElementPointer( &data->networkInputs, inputIndex );
}

FBNetworkBuilderDataIO* fbNetworkBuilder_getNetworkOutputData( FBNetworkBuilderData *data, index_t outputIndex ) {

    CHECK_ARRAY_BOUNDS( data->networkOutputs, outputIndex );
    return (FBNetworkBuilderDataIO*) fbElementBuffer_getElementPointer( &data->networkOutputs, outputIndex );
}

FBNetworkBuilderDataNativeIO* fbNetworkBuilder_getNativeInputData( FBNetworkBuilderData *data, index_t inputIndex ) {

    CHECK_ARRAY_BOUNDS( data->nativeInputs, inputIndex );
    return (FBNetworkBuilderDataNativeIO*) fbElementBuffer_getElementPointer( &data->nativeInputs, inputIndex );
}

FBNetworkBuilderDataNativeIO* fbNetworkBuilder_getNativeOutputData( FBNetworkBuilderData *data, index_t outputIndex ) {

    CHECK_ARRAY_BOUNDS( data->nativeOutputs, outputIndex );
    return (FBNetworkBuilderDataNativeIO*) fbElementBuffer_getElementPointer( &data->nativeOutputs, outputIndex );
}

static bool _component_has_input_port( FBComponent *component, index_t portIndex ) {

    return component->inputCount > portIndex;
}

static bool _component_has_output_port( FBComponent *component, index_t portIndex ) {

    return component->outputCount > portIndex;
}


index_t fbNetworkBuilder_addComponentInstance( FBNetworkBuilderData *buildData, FBComponent *component ) {

    fbElementBuffer_addValue( &buildData->allComponentInstances, &component );

    return buildData->allComponentInstances.count - 1;
}

index_t fbNetworkBuilder_addConnection_Ctx2Ctx( FBNetworkBuilderData *buildData, index_t sourceContextIndex, index_t sourceOutputPortIndex, index_t sinkContextIndex, index_t sinkInputPortIndex, index_t capacity ) {

    FBComponent *context;
    context = fbNetworkBuilder_getComponent( buildData, sourceContextIndex );
    CHECK_NOT_NULL( context );
    CHECK_TRUE( _component_has_output_port( context, sourceOutputPortIndex ) );
    context = fbNetworkBuilder_getComponent( buildData, sinkContextIndex );
    CHECK_NOT_NULL( context );
    CHECK_TRUE( _component_has_input_port( context, sinkInputPortIndex ) );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataConnection, buildData->allConnections )
    index_t connectionIndex = buildData->allConnections.count - 1;

    FBNetworkBuilderDataConnection *nbConnection = fbNetworkBuilder_getConnectionData( buildData, connectionIndex );
    nbConnection->type = CONNECTION_TYPE_DEFAULT;
    nbConnection->nativeInputIndex = SIZE_MAX;
    nbConnection->nativeOutputIndex = SIZE_MAX;
    nbConnection->sourceContextIndex = sourceContextIndex;
    nbConnection->sourceOutputPortIndex = sourceOutputPortIndex;
    nbConnection->sinkContextIndex = sinkContextIndex;
    nbConnection->sinkInputPortIndex = sinkInputPortIndex;
    nbConnection->capacity = capacity;

    return connectionIndex;
}
index_t fbNetworkBuilder_addConnection_Ntv2Ctx( FBNetworkBuilderData *buildData,
    index_t nativeInputIndex,
    index_t sinkContextIndex, index_t sinkInputPortIndex,
    index_t capacity ) {

    CHECK_ARRAY_BOUNDS( buildData->nativeInputs, nativeInputIndex );
    FBComponent *context = fbNetworkBuilder_getComponent( buildData, sinkContextIndex );
    CHECK_NOT_NULL( context );
    CHECK_TRUE( _component_has_input_port( context, sinkInputPortIndex ) );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataConnection, buildData->allConnections )
    index_t connectionIndex = buildData->allConnections.count - 1;

    FBNetworkBuilderDataConnection *nbConnection = fbNetworkBuilder_getConnectionData( buildData, connectionIndex );
    nbConnection->type = CONNECTION_TYPE_NATIVE_SOURCE;
    nbConnection->nativeInputIndex = nativeInputIndex;
    nbConnection->nativeOutputIndex = SIZE_MAX;
    nbConnection->sourceContextIndex = SIZE_MAX;
    nbConnection->sourceOutputPortIndex = SIZE_MAX;
    nbConnection->sinkContextIndex = sinkContextIndex;
    nbConnection->sinkInputPortIndex = sinkInputPortIndex;
    nbConnection->capacity = capacity;

    return connectionIndex;
}

index_t fbNetworkBuilder_addConnection_Ctx2Ntv( FBNetworkBuilderData *buildData,
    index_t sourceContextIndex, index_t sourceOutputPortIndex,
    index_t nativeOutputIndex,
    index_t capacity ) {

    FBComponent *context = fbNetworkBuilder_getComponent( buildData, sourceContextIndex );
    CHECK_NOT_NULL( context );
    CHECK_TRUE( _component_has_output_port( context, sourceOutputPortIndex ) );
    CHECK_ARRAY_BOUNDS( buildData->nativeOutputs, nativeOutputIndex );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataConnection, buildData->allConnections )
    index_t connectionIndex = buildData->allConnections.count - 1;

    FBNetworkBuilderDataConnection *nbConnection = fbNetworkBuilder_getConnectionData( buildData, connectionIndex );
    nbConnection->type = CONNECTION_TYPE_NATIVE_SINK;
    nbConnection->nativeOutputIndex = nativeOutputIndex;
    nbConnection->nativeInputIndex = SIZE_MAX;
    nbConnection->sourceContextIndex = sourceContextIndex;
    nbConnection->sourceOutputPortIndex = sourceOutputPortIndex;
    nbConnection->sinkContextIndex = SIZE_MAX;
    nbConnection->sinkInputPortIndex = SIZE_MAX;
    nbConnection->capacity = capacity;

    return connectionIndex;
}

index_t fbNetworkBuilder_addConnection_Ntv2Ntv( FBNetworkBuilderData *buildData,
    index_t nativeInputIndex,
    index_t nativeOutputIndex,
    index_t capacity ) {

    CHECK_ARRAY_BOUNDS( buildData->nativeInputs, nativeInputIndex );
    CHECK_ARRAY_BOUNDS( buildData->nativeOutputs, nativeOutputIndex );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataConnection, buildData->allConnections )
    index_t connectionIndex = buildData->allConnections.count - 1;

    FBNetworkBuilderDataConnection *nbConnection = fbNetworkBuilder_getConnectionData( buildData, connectionIndex );
    nbConnection->type = CONNECTION_TYPE_NATIVE_SOURCE_SINK;
    nbConnection->nativeOutputIndex = nativeOutputIndex;
    nbConnection->nativeInputIndex = nativeInputIndex;
    nbConnection->sourceContextIndex = SIZE_MAX;
    nbConnection->sourceOutputPortIndex = SIZE_MAX;
    nbConnection->sinkContextIndex = SIZE_MAX;
    nbConnection->sinkInputPortIndex = SIZE_MAX;
    nbConnection->capacity = capacity;

    return connectionIndex;
}

index_t fbNetworkBuilder_setNetworkInput( FBNetworkBuilderData *buildData, index_t contextIndex, index_t portIndex ) {

    FBComponent *context = fbNetworkBuilder_getComponent( buildData, contextIndex );
    CHECK_NOT_NULL( context );
    CHECK_TRUE( _component_has_input_port( context, portIndex ) );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataIO, buildData->networkInputs )
    index_t networkInputIndex = buildData->networkInputs.count - 1;

    FBNetworkBuilderDataIO *nbNetworkInput = fbNetworkBuilder_getNetworkInputData( buildData, networkInputIndex );
    nbNetworkInput->contextIndex = contextIndex;
    nbNetworkInput->portIndex = portIndex;

    return networkInputIndex;
}

index_t fbNetworkBuilder_setNetworkOutput( FBNetworkBuilderData *buildData, index_t contextIndex, index_t portIndex ) {

    FBComponent *context = fbNetworkBuilder_getComponent( buildData, contextIndex );
    CHECK_NOT_NULL( context );
    CHECK_TRUE( _component_has_output_port( context, portIndex ) );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataIO, buildData->networkOutputs )
    index_t networkOutputIndex = buildData->networkOutputs.count - 1;

    FBNetworkBuilderDataIO *nbNetworkOutput = fbNetworkBuilder_getNetworkOutputData( buildData, networkOutputIndex );
    nbNetworkOutput->contextIndex = contextIndex;
    nbNetworkOutput->portIndex = portIndex;

    return networkOutputIndex;
}

index_t fbNetworkBuilder_setNativeInput( FBNetworkBuilderData *buildData, FBPacketHandlerFn fn, byte_t *configBuffer, size_t bufferSize ) {

    CHECK_NOT_NULL( fn );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataNativeIO, buildData->nativeInputs )
    index_t nioIndex = buildData->nativeInputs.count - 1;

    FBNetworkBuilderDataNativeIO *nbNIO = fbNetworkBuilder_getNativeInputData( buildData, nioIndex );
    nbNIO->configBuffer = configBuffer;
    nbNIO->bufferSize = bufferSize;
    nbNIO->fn = fn;

    return nioIndex;
}

index_t fbNetworkBuilder_setNativeOutput( FBNetworkBuilderData *buildData, FBPacketHandlerFn fn, byte_t *configBuffer, size_t bufferSize ) {

    CHECK_NOT_NULL( fn );

    ADD_EXTRA_ELEMENT( FBNetworkBuilderDataNativeIO, buildData->nativeOutputs )
    index_t nioIndex = buildData->nativeOutputs.count - 1;

    FBNetworkBuilderDataNativeIO *nbNIO = fbNetworkBuilder_getNativeOutputData( buildData, nioIndex );
    nbNIO->configBuffer = configBuffer;
    nbNIO->bufferSize = bufferSize;
    nbNIO->fn = fn;

    return nioIndex;
}



void fbNetworkBuilder_init( FBNetworkBuilderData *buildData ) {

    fbElementBuffer_init( &buildData->allComponentInstances, sizeof( FBComponent * ) );
    fbElementBuffer_init( &buildData->allConnections, sizeof( FBNetworkBuilderDataConnection ) );
    fbElementBuffer_init( &buildData->networkInputs, sizeof( FBNetworkBuilderDataIO ) );
    fbElementBuffer_init( &buildData->networkOutputs, sizeof( FBNetworkBuilderDataIO ) );
    fbElementBuffer_init( &buildData->nativeInputs, sizeof( FBNetworkBuilderDataNativeIO ) );
    fbElementBuffer_init( &buildData->nativeOutputs, sizeof( FBNetworkBuilderDataNativeIO ) );
}

void fbNetworkBuilder_free( FBNetworkBuilderData *buildData ) {

    fbElementBuffer_free( &buildData->allComponentInstances );
    fbElementBuffer_free( &buildData->allConnections );
    fbElementBuffer_free( &buildData->networkInputs );
    fbElementBuffer_free( &buildData->networkOutputs );
    fbElementBuffer_free( &buildData->nativeInputs );
    fbElementBuffer_free( &buildData->nativeOutputs );
}





void _internal_fbNetworkBuilder_buildContexts( FBNetworkBuilderData *buildData, FBNetworkContext *networkContext ) {

    FBElementBuffer *allComponentInstances = &buildData->allComponentInstances;

    for( index_t idx = 0; idx < allComponentInstances->count; idx++ ) {

        // uninitialized struct, we will initialize it now
        FBContext *context = fbNetwork_getContext( networkContext, idx );

        FBComponent *component = fbNetworkBuilder_getComponent( buildData, idx );

        fbContext_init_allocate_fromComponent( context, component );
        fbContext_reset( context );
    }
}

void _internal_fbNetworkBuilder_buildConnections( FBNetworkBuilderData *buildData, FBNetworkContext *networkContext ) {

    FBElementBuffer *allConnections = &buildData->allConnections;

    for( index_t idx = 0; idx < allConnections->count; idx++ ) {

        // uninitialized struct, we will initialize it now
        FBConnection *connection = fbNetwork_getConnection( networkContext, idx );

        FBNetworkBuilderDataConnection *connectionData = fbNetworkBuilder_getConnectionData( buildData, idx );

        switch( connectionData->type ) {

            case CONNECTION_TYPE_DEFAULT: {
                FBContext *sourceCtx = fbNetwork_getContext( networkContext, connectionData->sourceContextIndex );
                FBInputOutput *source = fbContext_getOutputPort( sourceCtx, connectionData->sourceOutputPortIndex );
                FBContext *sinkCtx = fbNetwork_getContext( networkContext, connectionData->sinkContextIndex );
                FBInputOutput *sink = fbContext_getInputPort( sinkCtx, connectionData->sinkInputPortIndex );

                fbConnection_init_i( connection, source, sink, connectionData->capacity );
            }
            break;

            case CONNECTION_TYPE_NATIVE_SINK: {
                FBNetworkNativeIO *nativeOutput = fbNetwork_getNativeOutput( networkContext, connectionData->nativeOutputIndex );

                // FBNetworkBuilderDataIO *outputData = fbNetworkBuilder_getNetworkOutputData( buildData, connectionData->nativeIOIndex );
                // CHECK connectionData->sourceContextIndex == outputData->contextIndex
                // CHECK connectionData->sourceOutputPortIndex == outputData->portIndex
                FBContext *ctx = fbNetwork_getContext( networkContext, connectionData->sourceContextIndex );
                FBInputOutput *outputPort = fbContext_getOutputPort( ctx, connectionData->sourceOutputPortIndex );

                fbConnection_init( connection, outputPort, nativeOutput->port );
            }
            break;

            case CONNECTION_TYPE_NATIVE_SOURCE: {
                FBNetworkNativeIO *nativeInput = fbNetwork_getNativeInput( networkContext, connectionData->nativeInputIndex );

                // FBNetworkBuilderDataNativeIO *inputData = fbNetworkBuilder_getNativeInputData( buildData, connectionData->nativeIOIndex );
                // CHECK connectionData->sinkContextIndex == inputData->contextIndex
                // CHECK connectionData->sinkInputPortIndex == inputData->portIndex
                FBContext *ctx = fbNetwork_getContext( networkContext, connectionData->sinkContextIndex );
                FBInputOutput *inputPort = fbContext_getInputPort( ctx, connectionData->sinkInputPortIndex );

                fbConnection_init( connection, nativeInput->port, inputPort );
            }
            break;

            case CONNECTION_TYPE_NATIVE_SOURCE_SINK: {
                FBNetworkNativeIO *nativeInput = fbNetwork_getNativeInput( networkContext, connectionData->nativeInputIndex );
                FBNetworkNativeIO *nativeOutput = fbNetwork_getNativeOutput( networkContext, connectionData->nativeOutputIndex );

                // FBNetworkBuilderDataNativeIO *inputData = fbNetworkBuilder_getNativeInputData( buildData, connectionData->nativeIOIndex );
                // CHECK connectionData->sinkContextIndex == inputData->contextIndex
                // CHECK connectionData->sinkInputPortIndex == inputData->portIndex
                // FBNetworkBuilderDataIO *outputData = fbNetworkBuilder_getNetworkOutputData( buildData, connectionData->nativeIOIndex );
                // CHECK connectionData->sourceContextIndex == outputData->contextIndex
                // CHECK connectionData->sourceOutputPortIndex == outputData->portIndex

                fbConnection_init( connection, nativeInput->port, nativeOutput->port );
            }
            break;

            default:
            break;
        }
    }
}

void _internal_fbNetworkBuilder_buildInputs( FBNetworkBuilderData *buildData, FBNetworkContext *networkContext ) {

    FBElementBuffer *networkInputs = &buildData->networkInputs;

    for( index_t idx = 0; idx < networkInputs->count; idx++ ) {

        // uninitialized struct, we will initialize it now
        FBInputOutput* *portPtr = fbNetwork_getInputPtr( networkContext, idx );

        FBNetworkBuilderDataIO *inputData = fbNetworkBuilder_getNetworkInputData( buildData, idx );

        FBContext *ctx = fbNetwork_getContext( networkContext, inputData->contextIndex );
        FBInputOutput *inputPort = fbContext_getInputPort( ctx, inputData->portIndex );

        *portPtr = inputPort;
    }
}

void _internal_fbNetworkBuilder_buildOutputs( FBNetworkBuilderData *buildData, FBNetworkContext *networkContext ) {

    FBElementBuffer *networkOutputs = &buildData->networkOutputs;

    for( index_t idx = 0; idx < networkOutputs->count; idx++ ) {

        // uninitialized struct, we will initialize it now
        FBInputOutput* *portPtr = fbNetwork_getOutputPtr( networkContext, idx );

        FBNetworkBuilderDataIO *outputData = fbNetworkBuilder_getNetworkOutputData( buildData, idx );

        FBContext *ctx = fbNetwork_getContext( networkContext, outputData->contextIndex );
        FBInputOutput *outputPort = fbContext_getOutputPort( ctx, outputData->portIndex );

        *portPtr = outputPort;
    }
}

void _internal_fbNetworkBuilder_buildNativeInputs( FBNetworkBuilderData *buildData, FBNetworkContext *networkContext ) {

    FBElementBuffer *nativeInputs = &buildData->nativeInputs;

    for( index_t idx = 0; idx < nativeInputs->count; idx++ ) {

        // uninitialized struct, we will initialize it now
        FBNetworkNativeIO *nativeInput = fbNetwork_getNativeInput( networkContext, idx );

        FBNetworkBuilderDataNativeIO *inputData = fbNetworkBuilder_getNativeInputData( buildData, idx );

        nativeInput->state = STATUS_NATIVE_FN_LIVE;
        nativeInput->fn = inputData->fn;
        nativeInput->configBuffer = inputData->configBuffer;
        nativeInput->bufferSize = inputData->bufferSize;
        nativeInput->tag = NULL;
        // nativeInput->port: initialized by call to fbNetwork_init()
    }
}

void _internal_fbNetworkBuilder_buildNativeOutputs( FBNetworkBuilderData *buildData, FBNetworkContext *networkContext ) {

    FBElementBuffer *nativeOutputs = &buildData->nativeOutputs;

    for( index_t idx = 0; idx < nativeOutputs->count; idx++ ) {

        // uninitialized struct, we will initialize it now
        FBNetworkNativeIO *nativeOutput = fbNetwork_getNativeOutput( networkContext, idx );

        FBNetworkBuilderDataNativeIO *outputData = fbNetworkBuilder_getNativeOutputData( buildData, idx );

        nativeOutput->state = STATUS_NATIVE_FN_LIVE;
        nativeOutput->fn = outputData->fn;
        nativeOutput->configBuffer = outputData->configBuffer;
        nativeOutput->bufferSize = outputData->bufferSize;
        nativeOutput->tag = NULL;
        // nativeOutput->port: initialized by call to fbNetwork_init()
    }
}




FBNetworkContext *fbNetworkBuilder_build( FBNetworkBuilderData *buildData ) {

    FBNetworkContext *networkContext = fbNetwork_create(
        buildData->allComponentInstances.count, buildData->allConnections.count, 
        buildData->networkInputs.count, buildData->networkOutputs.count, 
        buildData->nativeInputs.count, buildData->nativeOutputs.count );

    // !!! order of operations is important !!!
    _internal_fbNetworkBuilder_buildContexts( buildData, networkContext );
    _internal_fbNetworkBuilder_buildInputs( buildData, networkContext );
    _internal_fbNetworkBuilder_buildOutputs( buildData, networkContext );
    _internal_fbNetworkBuilder_buildNativeInputs( buildData, networkContext );
    _internal_fbNetworkBuilder_buildNativeOutputs( buildData, networkContext );
    _internal_fbNetworkBuilder_buildConnections( buildData, networkContext );

    return networkContext;
}
