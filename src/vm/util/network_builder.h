/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_NETWORK_BUILDER_H
#define VM_NETWORK_BUILDER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"
#include "vm/core/network.h"
#include "vm/lib/databuffer.h"
#include "vm/core/component.h"

typedef enum _FBNetworkBuilderConnectionType {
    CONNECTION_TYPE_DEFAULT,
    CONNECTION_TYPE_NATIVE_SOURCE,
    CONNECTION_TYPE_NATIVE_SINK,
    CONNECTION_TYPE_NATIVE_SOURCE_SINK,
} FBNetworkBuilderConnectionType;

typedef struct _FBNetworkBuilderDataConnection
{
    FBNetworkBuilderConnectionType type;
    index_t nativeInputIndex;
    index_t nativeOutputIndex;
    index_t sourceContextIndex;
    index_t sourceOutputPortIndex;
    index_t sinkContextIndex;
    index_t sinkInputPortIndex;
    index_t capacity;

} FBNetworkBuilderDataConnection;

typedef struct _FBNetworkBuilderDataIO
{
    index_t contextIndex;
    index_t portIndex;

} FBNetworkBuilderDataIO;

typedef struct _FBNetworkBuilderDataNativeIO
{
    FBPacketHandlerFn fn;
    byte_t *configBuffer;
    size_t bufferSize;

} FBNetworkBuilderDataNativeIO;

typedef struct _FBNetworkBuilderData
{
    // FBComponent *
    FBElementBuffer allComponentInstances;
    // FBNetworkBuilderDataConnection
    FBElementBuffer allConnections;
    // FBNetworkBuilderDataIO
    FBElementBuffer networkInputs;
    // FBNetworkBuilderDataIO*
    FBElementBuffer networkOutputs;
    // FBNetworkBuilderDataNativeIO
    FBElementBuffer nativeInputs;
    // FBNetworkBuilderDataNativeIO
    FBElementBuffer nativeOutputs;

} FBNetworkBuilderData;


void fbNetworkBuilder_init( FBNetworkBuilderData *buildData );
void fbNetworkBuilder_free( FBNetworkBuilderData *buildData );
FBNetworkContext *fbNetworkBuilder_build( FBNetworkBuilderData *buildData );

index_t fbNetworkBuilder_addComponentInstance( FBNetworkBuilderData *buildData, FBComponent *component );
index_t fbNetworkBuilder_addConnection_Ctx2Ctx( FBNetworkBuilderData *buildData,
    index_t sourceContextIndex, index_t sourceOutputPortIndex,
    index_t sinkContextIndex, index_t sinkInputPortIndex,
    index_t capacity );
index_t fbNetworkBuilder_addConnection_Ntv2Ctx( FBNetworkBuilderData *buildData,
    index_t nativeInputIndex,
    index_t sinkContextIndex, index_t sinkInputPortIndex,
    index_t capacity );
index_t fbNetworkBuilder_addConnection_Ctx2Ntv( FBNetworkBuilderData *buildData,
    index_t sourceContextIndex, index_t sourceOutputPortIndex,
    index_t nativeOutputIndex,
    index_t capacity );
index_t fbNetworkBuilder_addConnection_Ntv2Ntv( FBNetworkBuilderData *buildData,
    index_t nativeInputIndex,
    index_t nativeOutputIndex,
    index_t capacity );
index_t fbNetworkBuilder_setNetworkInput( FBNetworkBuilderData *buildData, index_t contextIndex, index_t portIndex );
index_t fbNetworkBuilder_setNetworkOutput( FBNetworkBuilderData *buildData, index_t contextIndex, index_t portIndex );
index_t fbNetworkBuilder_setNativeInput( FBNetworkBuilderData *buildData, FBPacketHandlerFn fn, byte_t *configBuffer, size_t bufferSize );
index_t fbNetworkBuilder_setNativeOutput( FBNetworkBuilderData *buildData, FBPacketHandlerFn fn, byte_t *configBuffer, size_t bufferSize );

FBComponent* fbNetworkBuilder_getComponent( FBNetworkBuilderData *data, index_t componentIndex );
FBNetworkBuilderDataConnection* fbNetworkBuilder_getConnectionData( FBNetworkBuilderData *data, index_t connectionIndex );
FBNetworkBuilderDataIO* fbNetworkBuilder_getNetworkInputData( FBNetworkBuilderData *data, index_t inputIndex );
FBNetworkBuilderDataIO* fbNetworkBuilder_getNetworkOutputData( FBNetworkBuilderData *data, index_t outputIndex );
FBNetworkBuilderDataNativeIO* fbNetworkBuilder_getNativeInputData( FBNetworkBuilderData *data, index_t inputIndex );
FBNetworkBuilderDataNativeIO* fbNetworkBuilder_getNativeOutputData( FBNetworkBuilderData *data, index_t outputIndex );

#ifdef __cplusplus
}
#endif

#endif // VM_NETWORK_BUILDER_H
