/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/binary.h"
#include "vm/lib/memory.h"

#include "vm/interpreter/data_access.h"

FBComponent *fbBinary_getComponent( byte_t *binaryData, size_t dataSize ) {

    if( dataSize < sizeof( FBBinary ) ) {
        // TODO return error number or enum
        return NULL;
    }

    // FBBinary *binary = (FBBinary *) binaryData;

    uint32_t magic = fbData_get_uint32( binaryData );
    binaryData += 4;

    if( magic != BINARY_MAGIC_NUMBER ) {
        // TODO return error number or enum
        return NULL;
    }

    uint16_t major_version = fbData_get_uint16( binaryData );
    binaryData += 2;
    uint16_t minor_version = fbData_get_uint16( binaryData );
    binaryData += 2;

    if( major_version != BINARY_MAJOR_VERSION && minor_version != BINARY_MINOR_VERSION ) {
        // TODO return error number or enum
        return NULL;
    }


    // FBComponentHeader *header = &binary->component_header;

    uint32_t constant_pool_size = fbData_get_uint32( binaryData );
    binaryData += 4;
    uint32_t constant_pool_offset = fbData_get_uint32( binaryData );
    binaryData += 4;

    uint16_t setup_code_size = fbData_get_uint16( binaryData );
    binaryData += 2;
    uint16_t setup_code_offset = fbData_get_uint16( binaryData );
    binaryData += 2;
    uint16_t loop_code_size = fbData_get_uint16( binaryData );
    binaryData += 2;
    uint16_t loop_code_offset = fbData_get_uint16( binaryData );
    binaryData += 2;

    uint32_t component_data_size = fbData_get_uint32( binaryData );
    binaryData += 4;
    uint32_t component_stack_size = fbData_get_uint32( binaryData );
    binaryData += 4;
    uint16_t information_packet_count = fbData_get_uint16( binaryData );
    binaryData += 2;
    uint16_t input_count = fbData_get_uint16( binaryData );
    binaryData += 2;
    uint16_t output_count = fbData_get_uint16( binaryData );
    binaryData += 2;

    // uint16_t component_name_size = fbData_get_uint16( binaryData );
    binaryData += 2;

    byte_t *payload = binaryData;



    FBComponent *component = fbComponent_create( setup_code_size, loop_code_size, constant_pool_size );

    component->dataSize = component_data_size;
    component->stackSize = component_stack_size;
    component->informationPacketCount = information_packet_count;
    component->inputCount = input_count;
    component->outputCount = output_count;


    FB_COPY_ARRAY_E( component->setupCode, payload + setup_code_offset, 1, setup_code_size );
    FB_COPY_ARRAY_E( component->loopCode, payload + loop_code_offset, 1, loop_code_size );
    FB_COPY_ARRAY_E( component->constantPool, payload + constant_pool_offset, 1, constant_pool_size );

    return component;
}
