/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_ERROR_H
#define VM_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vm/interpreter/types.h"

typedef enum _FBContextStatus
{
    STATUS_OK = 0,

    STATUS_NONE,

    // System statuses
    STATUS_REQUEST_WRITE,
    STATUS_REQUEST_READ,
    STATUS_REQUEST_CREATE,
    STATUS_REQUEST_DROP,
    STATUS_REQUEST_TERMINATE,

    STATUS_INVALID_STATE,
    STATUS_CODE_END,
    STATUS_YIELD,

    // Error statuses
    STATUS_ERROR_CONNECTION_TERMINATED,
    STATUS_ERROR_CONNECTION_EMPTY,
    STATUS_ERROR_CONNECTION_FULL,

    STATUS_ERROR_PORT_CLOSED,

    STATUS_ERROR_INVALID_OP,
    STATUS_ERROR_UNKNOWN_OP,
    STATUS_ERROR_OVERRUN,

} FBContextStatus;


void fbError_setStatus( FBContext *context, FBContextStatus status );
FBContextStatus fbError_getStatus( FBContext *context );

#ifdef __cplusplus
}
#endif

#endif // VM_ERROR_H
