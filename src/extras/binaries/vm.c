/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "vm/native.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    (void)argc;
    (void)argv;

    printf("VM runner v0.1\n");

    return 0;
}