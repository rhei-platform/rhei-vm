/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#include "metadata.h"
#include "vm/lib/memory.h"
#include "vm/interpreter/checks.h"

const FBElementDescriptor FB_BaseComponentDataDescriptor = { T_RECORD_REF, 0, 1, 0 };

static FBElementDescriptor singleElement_bool = { T_BOOL, 0, 1, 0 };
static FBElementDescriptor singleElement_byte = { T_BYTE, 0, 1, 0 };
static FBElementDescriptor singleElement_int = { T_INT, 0, 1, 0 };
static FBElementDescriptor singleElement_float = { T_FLOAT, 0, 1, 0 };
static FBElementDescriptor singleElement_double = { T_DOUBLE, 0, 1, 0 };

static FBElementDescriptor singleElement_char = { T_CHAR, 0, 1, 0 };

static FBElementDescriptor singleElement_int8 = { T_INT8, 0, 1, 0 };
static FBElementDescriptor singleElement_uint8 = { T_UINT8, 0, 1, 0 };
static FBElementDescriptor singleElement_int16 = { T_INT16, 0, 1, 0 };
static FBElementDescriptor singleElement_uint16 = { T_UINT16, 0, 1, 0 };
static FBElementDescriptor singleElement_int32 = { T_INT32, 0, 1, 0 };
static FBElementDescriptor singleElement_uint32 = { T_UINT32, 0, 1, 0 };
static FBElementDescriptor singleElement_int64 = { T_INT64, 0, 1, 0 };
static FBElementDescriptor singleElement_uint64 = { T_UINT64, 0, 1, 0 };

static FBElementDescriptor singleElement_reference = { T_OBJECT_REFERENCE, 0, 1, 0 };

const FBElementDescriptor *fbSingleElementDescriptor( FBDataType type ) {

    switch (type) {

        case T_BOOL: return &singleElement_bool;
        case T_BYTE: return &singleElement_byte;
        case T_INT: return &singleElement_int;
        case T_FLOAT: return &singleElement_float;
        case T_DOUBLE: return &singleElement_double;

        case T_CHAR: return &singleElement_char;
        // case T_SHORT: return SIZE_OF_SHORT;

        case T_INT8: return &singleElement_int8;
        case T_UINT8: return &singleElement_uint8;
        case T_INT16: return &singleElement_int16;
        case T_UINT16: return &singleElement_uint16;
        case T_INT32: return &singleElement_int32;
        case T_UINT32: return &singleElement_uint32;
        case T_INT64: return &singleElement_int64;
        case T_UINT64: return &singleElement_uint64;

        case T_OBJECT_REFERENCE: return &singleElement_reference;

        // case T_STRING: return SIZE_OF_OBJECT_REFERENCE;

        // case T_NIL:
        // case T_RECORD_REF:
        // case T_MULTI_ARRAY:
        default: {}
    }

    return NULL;
}

void fbElementDescriptorArrayInit(FBElementDescriptorArray* elementDescriptorArray) {

    elementDescriptorArray->count = 0;
    elementDescriptorArray->capacity = 0;
    elementDescriptorArray->array = NULL;
}

void fbElementDescriptorArrayFree(FBElementDescriptorArray* elementDescriptorArray) {

    FB_FREE_ARRAY( elementDescriptorArray->array );
    fbElementDescriptorArrayInit(elementDescriptorArray);
}

void fbElementDescriptorArrayAdd(FBElementDescriptorArray* elementDescriptorArray, FBElementDescriptor elementDescriptor) {

    if( elementDescriptorArray->capacity < elementDescriptorArray->count + 1 ) {

        int oldCapacity = elementDescriptorArray->capacity;
        elementDescriptorArray->capacity = FB_GROW_CAPACITY( oldCapacity );
        elementDescriptorArray->array = FB_GROW_ARRAY( elementDescriptorArray->array, FBElementDescriptor, elementDescriptorArray->capacity );
    }

    elementDescriptorArray->array[elementDescriptorArray->count] = elementDescriptor;
    elementDescriptorArray->count++;
}



static size_t elementSize( FBDataType type ) {

    switch (type) {

        case T_NIL: return SIZE_OF_NIL;

        case T_BOOL: return SIZE_OF_BOOL;
        case T_BYTE: return SIZE_OF_BYTE;
        case T_INT: return SIZE_OF_INT;
        case T_FLOAT: return SIZE_OF_FLOAT;
        case T_DOUBLE: return SIZE_OF_DOUBLE;

        case T_CHAR: return SIZE_OF_CHAR;
        // case T_SHORT: return SIZE_OF_SHORT;

        case T_INT8: return SIZE_OF_INT8;
        case T_UINT8: return SIZE_OF_UINT8;
        case T_INT16: return SIZE_OF_INT16;
        case T_UINT16: return SIZE_OF_UINT16;
        case T_INT32: return SIZE_OF_INT32;
        case T_UINT32: return SIZE_OF_UINT32;
        case T_INT64: return SIZE_OF_INT64;
        case T_UINT64: return SIZE_OF_UINT64;

        case T_OBJECT_REFERENCE: return SIZE_OF_OBJECT_REFERENCE;

        // case T_STRING: return SIZE_OF_OBJECT_REFERENCE;

        // case T_RECORD_REF:
        // case T_MULTI_ARRAY:
        default: {}
    }

    return SIZE_OF_NIL;
}

/*
    *data - points to the start of the array of records
    we find the record size from descriptor table
    and update data position using record size and index information

    - uses descriptor->table_index

    return
        - *data that points to start of record
        - record descriptor (T_RECORD_REF)
        - offset and count are invalid in result descriptor
*/
static FBBoundedDataElement element_by_index_for_record( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t index ) {

    const FBElementDescriptor *descriptor = bde.descriptor;
    CHECK_DESCRIPTOR_TYPE( *descriptor, T_RECORD_REF );
    CHECK_ARRAY_BOUNDS( *descriptor, index );

    uint8_t *data = bde.data;

    // get record size from descriptor array
    CHECK_ARRAY_BOUNDS( *elementDescriptorArray, descriptor->table_index );
    FBElementDescriptor *record_size_descriptor = elementDescriptorArray->array + descriptor->table_index;
    CHECK_DESCRIPTOR_TYPE( *record_size_descriptor, T_RECORD_HDR );
    size_t element_size = record_size_descriptor->offset;

    data += element_size * index;

    // TODO descriptor should be { T_RECORD_REF, 0, 1, table_index }
    FBBoundedDataElement result = { data, descriptor };
    // result.data = data;
    // result.descriptor = descriptor ;

    return result;
}

/*
    *data - points to the start of the array of elements
    we update data position using element type size and index information

    - uses descriptor->type

    return
        - *data that points to start of the element
        - element descriptor
        - offset and count are invalid in result descriptor
*/
static FBBoundedDataElement element_by_index_for_element( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t index ) {

    const FBElementDescriptor *descriptor = bde.descriptor;
    CHECK_DESCRIPTOR_IS_BASE_TYPE( *descriptor );
    CHECK_ARRAY_BOUNDS( *descriptor, index );

    uint8_t *data = bde.data;
    (void)elementDescriptorArray;
    // FBElementDescriptorArray descriptorArray = context->descriptorArray;

    // get element size based on its type
    size_t element_size = elementSize( descriptor->type );

    data += element_size * index;

    FBBoundedDataElement result = { data, descriptor };
    // FBBoundedDataElement result = { data, fbSingleElementDescriptor( descriptor->type ) };
    // result.data = data;
    // result.descriptor = descriptor ;

    return result;
}

/*
    *data - points to the start of this record
    we find element descriptor from descriptor table based off of the index
    and update data position using element offset

    - uses descriptor->table_index

    return
        - *data that points to start of the element
        - the element descriptor
        - offset is invalid in result descriptor
*/
static FBBoundedDataElement element_by_index_for_record_element( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t index ) {

    const FBElementDescriptor *descriptor = bde.descriptor;
    CHECK_DESCRIPTOR_TYPE( *descriptor, T_RECORD_REF );

    uint8_t *data = bde.data;

    // get record size element from descriptor array
    CHECK_ARRAY_BOUNDS( *elementDescriptorArray, descriptor->table_index );
    FBElementDescriptor *record_size_descriptor = elementDescriptorArray->array + descriptor->table_index;
    CHECK_DESCRIPTOR_TYPE( *record_size_descriptor, T_RECORD_HDR );
    // make sure index parameter points to an element in this record
    CHECK_ARRAY_BOUNDS( *record_size_descriptor, index );
    // get the actual element descriptor from descriptor array
    FBElementDescriptor *element_descriptor = record_size_descriptor + (index + 1);

    data += element_descriptor->offset;

    FBBoundedDataElement result = { data, element_descriptor };
    // result.data = data;
    // result.descriptor = descriptor ;

    return result;
}

FBBoundedDataElement fb_element_by_index( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t index ) {

    const FBElementDescriptor *descriptor = bde.descriptor;

    if ( descriptor->type == T_RECORD_REF ) {
        return element_by_index_for_record( elementDescriptorArray, bde, index );
    }

    if( IS_BASE_TYPE( descriptor->type ) ) {
        return element_by_index_for_element( elementDescriptorArray, bde, index );
    }

    return bde;
}

FBBoundedDataElement fb_element_by_name( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t tag ) {
    return element_by_index_for_record_element( elementDescriptorArray, bde, tag );
}
