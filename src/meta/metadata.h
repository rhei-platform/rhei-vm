/* Copyright 2019 Ravendyne Inc. */
/* SPDX-License-Identifier: Apache-2.0 */

#ifndef VM_METADATA_H
#define VM_METADATA_H

#include "vm/interpreter/types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _FBDataType
{
    T_NIL = 0x0000,

    BASE_TYPE = 0x1000,
    T_BOOL,
    T_BYTE,
    T_INT,
    T_FLOAT,
    T_DOUBLE,

    T_CHAR,
    // T_SHORT,

    T_INT8,
    T_UINT8,
    T_INT16,
    T_UINT16,
    T_INT32,
    T_UINT32,
    T_INT64,
    T_UINT64,

    REFERENCE_TYPE = 0x2000,
    // pointers to other buffers
    T_OBJECT_REFERENCE, // void*

    COMPLEX_TYPE = 0x3000,
    T_RECORD_REF,
    T_RECORD_HDR,
    T_MULTIARRAY,

} FBDataType;

typedef enum _FBDataTypeSize {

    SIZE_OF_NIL = (size_t)0,

    SIZE_OF_BOOL = sizeof(bool),
    SIZE_OF_BYTE = sizeof(byte_t),
    SIZE_OF_INT = sizeof(int),
    SIZE_OF_FLOAT = sizeof(float),
    SIZE_OF_DOUBLE = sizeof(double),

    SIZE_OF_CHAR = sizeof(char),
    // SIZE_OF_SHORT = sizeof(short),

    SIZE_OF_INT8 = sizeof(int8_t),
    SIZE_OF_UINT8 = sizeof(uint8_t),
    SIZE_OF_INT16 = sizeof(int16_t),
    SIZE_OF_UINT16 = sizeof(uint16_t),
    SIZE_OF_INT32 = sizeof(int32_t),
    SIZE_OF_UINT32 = sizeof(uint32_t),
    SIZE_OF_INT64 = sizeof(int64_t),
    SIZE_OF_UINT64 = sizeof(uint64_t),

    SIZE_OF_OBJECT_REFERENCE = sizeof(void*),
} FBDataTypeSize;

#define IS_BASE_TYPE( type ) \
    ( type > BASE_TYPE && type < REFERENCE_TYPE )

#define IS_NIL( type ) \
    ( type == T_NIL )


typedef struct _FBElementDescriptor
{
    FBDataType type;
    index_t offset;
    // for arrays: number of elements
    // for non-arrays: size of field, a.k.a. length
    index_t count;
    // if this is a T_RECORD or T_MULTIARRAY,
    // where in the descriptor table can we find element
    // definition?
    index_t table_index;
} FBElementDescriptor;

extern const FBElementDescriptor FB_BaseComponentDataDescriptor;
const FBElementDescriptor *fbSingleElementDescriptor( FBDataType type );

// TODO: use FBElementBuffer
typedef struct _FBElementDescriptorArray
{
    index_t count;
    index_t capacity;
    FBElementDescriptor *array;
} FBElementDescriptorArray;


void fbElementDescriptorArrayInit(FBElementDescriptorArray* elementDescriptorArray);
void fbElementDescriptorArrayAdd(FBElementDescriptorArray* elementDescriptorArray, FBElementDescriptor elementDescriptor);
void fbElementDescriptorArrayFree(FBElementDescriptorArray* elementDescriptorArray);



typedef struct _FBBoundedDataElement
{
    uint8_t *data;
    const FBElementDescriptor *descriptor;
} FBBoundedDataElement;

FBBoundedDataElement fb_element_by_index( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t index );
FBBoundedDataElement fb_element_by_name( FBElementDescriptorArray *elementDescriptorArray, FBBoundedDataElement bde, size_t tag );


#ifdef __cplusplus
}
#endif

#endif // VM_METADATA_H
