## HEAD

## current

- reorganize source layout to simplify include paths in build files
- implement `copy` and `length` instructions in VM
- divide servicing native inputs and outputs into separate steps in scheduler
- add copy range and fill range functions for information packet

## v0.0

- initial
